package com.netcorpgps.v3.dispatch.activity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.adapter.ExpenseItemAdapter;
import com.netcorpgps.v3.dispatch.adapter.WaypointItemAdapter;
import com.netcorpgps.v3.dispatch.api.ErrorHandler.VolleyErrorHandler;
import com.netcorpgps.v3.dispatch.api.bean.JobBean;
import com.netcorpgps.v3.dispatch.api.volley.GsonRequest;
import com.netcorpgps.v3.dispatch.application.NetcorpGPSApplication;
import com.netcorpgps.v3.dispatch.db.bean.Expense;
import com.netcorpgps.v3.dispatch.db.bean.Jobs;
import com.netcorpgps.v3.dispatch.db.bean.Waypoint;
import com.netcorpgps.v3.dispatch.db.service.JobsService;
import com.netcorpgps.v3.dispatch.fragment.AvailableJobListFragment;
import com.netcorpgps.v3.dispatch.utils.CommonUtils;
import com.netcorpgps.v3.dispatch.utils.LogUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class JobDetailActivity extends AppCompatActivity {
    private final String LOG_TAG = JobDetailActivity.class.getSimpleName();

    private SwipeRefreshLayout swipeRefreshLayout = null;
    private Toolbar toolbar = null;
    private int jobID;
//    private DbHelper helper = null;
//    private SQLiteDatabase db = null;
    private TextView txtJobCode;
    private TextView txtJobStatus;
    private TextView txtJobStatusLabel;
    private TextView txtDescription;
    private TextView txtRequirement;
    private TextView txtCustomer;
    private TextView txtContact;
    private TextView txtContactNumber;
    private TextView txtPassenger;
    private TextView txtVehicle;
    private TextView txtDriver;
    private TextView txtScheduled;
    private TextView txtStarted;
    private TextView txtJobType;
    private RecyclerView rvExpenses;
    private RecyclerView rvWaypoints;

    private Jobs job = null;
    List<Waypoint> waypointList = null;
    List<Expense> expenseList = null;
    private JobsService jobsService = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
        setContentView(R.layout.activity_job_detail);
        setActionBar();

        jobID = getIntent().getIntExtra("jobID", 0);
//        helper = new DbHelper(this);
//        db = helper.getWritableDatabase();
        jobsService = new JobsService(this);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshListener());
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        txtJobCode = (TextView) findViewById(R.id.txtJobCode);
        txtJobStatus = (TextView) findViewById(R.id.txtJobStatus);
        txtJobStatusLabel = (TextView) findViewById(R.id.txtJobStatusLabel);
        txtDescription = (TextView) findViewById(R.id.txtDescription);
        txtRequirement = (TextView) findViewById(R.id.txtRequirement);
        txtCustomer = (TextView) findViewById(R.id.txtCustomer);
        txtContact = (TextView) findViewById(R.id.txtContact);
        txtContactNumber = (TextView) findViewById(R.id.txtContactNumber);
        txtPassenger = (TextView) findViewById(R.id.txtPassenger);
        txtVehicle = (TextView) findViewById(R.id.txtVehicle);
        txtDriver = (TextView) findViewById(R.id.txtDriver);
        txtScheduled = (TextView) findViewById(R.id.txtScheduled);
        txtStarted = (TextView) findViewById(R.id.txtStarted);
        txtJobType = (TextView) findViewById(R.id.txtJobType);

        rvExpenses = (RecyclerView) findViewById(R.id.rvExpenses);
        rvExpenses.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        rvWaypoints = (RecyclerView) findViewById(R.id.rvWaypoints);
        rvWaypoints.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        boolean success = bindingItems();
        if (success)
            refreshItems();
    }

    public boolean bindingItems() {
        queryJobInfo();
        queryJobExpenses();
        queryJobWaypoints();
        return onItemsLoadComplete();
    }

    public void queryJobInfo() {
//        String selection = JobsContract.JobsEntry.jobID + " = ? ";
//        String[] selectionArgs = { String.valueOf(jobID) };
//        Cursor cursor = db.query(JobsContract.JobsEntry.TABLE_NAME, JobsContract.COLUMNS, selection, selectionArgs, null, null, null);
//        job = null;
//        while(cursor.moveToNext()) {
//            job = JobsContract.parseCursorToJobs(cursor);
//            break;
//        }
//        cursor.close();
        job = jobsService.getJobByID(jobID);
    }

    public void queryJobWaypoints() {
//        String selection = WaypointContract.WaypointEntry.jobID + " = ? ";
//        String[] selectionArgs = { String.valueOf(jobID) };
//        Cursor cursor = db.query(WaypointContract.WaypointEntry.TABLE_NAME, WaypointContract.COLUMNS, selection, selectionArgs, null, null, null);
//        waypointList = new ArrayList<>();
//        while(cursor.moveToNext()) {
//            Waypoint waypoint = WaypointContract.parseCursorToWaypoint(cursor);
//            waypointList.add(waypoint);
//        }
//        cursor.close();
        waypointList = jobsService.getWaypointsByJobID(jobID);
    }

    public void queryJobExpenses() {
//        String selection = ExpenseContract.ExpenseEntry.jobID + " = ? ";
//        String[] selectionArgs = { String.valueOf(jobID) };
//        Cursor cursor = db.query(ExpenseContract.ExpenseEntry.TABLE_NAME, ExpenseContract.COLUMNS, selection, selectionArgs, null, null, null);
//        expenseList = new ArrayList<>();
//        while(cursor.moveToNext()) {
//            Expense expense = ExpenseContract.parseCursorToWaypoint(cursor);
//            expenseList.add(expense);
//        }
//        cursor.close();
        expenseList = jobsService.getExpensesByJobID(jobID);
    }

    public boolean onItemsLoadComplete() {
        if (job == null) {
            LogUtils.errorLog(LOG_TAG, getString(R.string.msg_error_job_status_changed));
            CommonUtils.toast(JobDetailActivity.this, getString(R.string.msg_error_job_status_changed));
            finish();
            return false;
        } else {
            txtJobCode.setText(job.getJobCode());
            txtJobStatus.setText(job.getJobStatus());
            txtJobStatus.setTextColor(CommonUtils.getJobStatusColor(this, job.getJobStatus()));
            txtJobStatusLabel.setTextColor(CommonUtils.getJobStatusColor(this, job.getJobStatus()));
            txtDescription.setText(job.getDescription());
            txtRequirement.setText(job.getRequirement());
            txtCustomer.setText(job.getCustomer());
            txtContact.setText(job.getContactName());
            txtContactNumber.setText(job.getContactNumber());
            txtPassenger.setText(String.valueOf(job.getPassengers()));
            txtVehicle.setText(job.getVehicleName());
            txtDriver.setText(job.getDriverName());
            txtScheduled.setText(job.getScheduledTime());
            txtScheduled.setTextColor(CommonUtils.getScheduledColor(this, job.getScheduledTime()));
            txtStarted.setText(job.getStartTime());
            txtJobType.setText(job.getJobType());
            rvExpenses.setAdapter(new ExpenseItemAdapter(expenseList, null, this));
            rvWaypoints.setAdapter(new WaypointItemAdapter(waypointList, null, this));
            swipeRefreshLayout.setRefreshing(false);
            return true;
        }
    }

    public Toolbar setActionBar() {
        // ActionBar
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        // Disable title
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText(getString(R.string.title_job_detail));
        toolbar.setNavigationIcon(R.drawable.ic_btn_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        return toolbar;
    }

    @Override
    public void onBackPressed() {
        super.finish();
        overridePendingTransition(R.anim.animation_enter,R.anim.animation_leave);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.animation_enter,R.anim.animation_leave);
    }

    public void refreshItems() {
        String ip = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_ip);
        String vehicleID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id);
        String companyDB = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_db);
        String accessToken = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_driver_access_token);
        String driverRecordID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_driver_record_id);
        String driverID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_user_id);
        String deviceID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_device_id);
        String apiGetJobByID = getString(R.string.api_get_job_by_id);
        String url = ip + apiGetJobByID;
        Map map = new HashMap();
        map.put("company", companyDB);
        map.put("vehicleID", vehicleID);
        map.put("driverRecordID", driverRecordID);
        map.put("accessToken", accessToken);
        map.put("driverID", driverID);
        map.put("deviceID", deviceID);
        map.put("jobID", String.valueOf(jobID));
        LogUtils.infoLog(LOG_TAG, url + ": " + map.toString());
        GsonRequest<JobBean> jsObjRequest = new GsonRequest<>(Request.Method.POST, url, JobBean.class, map, null, new GetJobListListener(), new GetErrorListener());
        jsObjRequest.setShouldCache(false);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(Integer.parseInt(getString(R.string.sys_timeout)), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsObjRequest.setTag(getString(R.string.request_tag_get_job_waypoints));
        NetcorpGPSApplication.getInstance().getRequestQueue().add(jsObjRequest);
    }

    class GetJobListListener implements Response.Listener<JobBean> {

        @Override
        public void onResponse(JobBean response) {
            try {
                if (response != null) {

                    jobsService.deleteExpenseByJobID(jobID);

                    jobsService.deleteWaypointByJobID(jobID);

                    if (response.getStatus() == 1) {
                        Jobs job = response.getJob();
                        if (job == null) {
                            jobsService.deleteJobByID(jobID);
                            if (!CommonUtils.isEmpty(response.getMsg())) {
                                LogUtils.debugLog(LOG_TAG, response.getMsg());
                                CommonUtils.toast(JobDetailActivity.this, response.getMsg());
                            }
                            return;
                        }

                        String sysDatetime = jobsService.querySysDatetime(job.getJobID());

                        if (!CommonUtils.isEmpty(sysDatetime) && !CommonUtils.isEmpty(job.getLastStatusTime())) {
                            if (CommonUtils.compareDatetime(sysDatetime, job.getLastStatusTime())) {
                                job = jobsService.queryJobStatusBySysDatetime(String.valueOf(job.getJobID()), sysDatetime, job);
                            }
                        }

                        jobsService.insertJob(job);

                        for (Waypoint waypoint : response.getWaypoints()) {

                            jobsService.insertWaypoint(waypoint);
                        }

                        double total = 0;
                        for (Expense expense : response.getExpenses()) {
                            String money = expense.getExpense();
                            if (!CommonUtils.isEmpty(money)) {
                                String pure = money.replace("$", "");
                                if (!CommonUtils.isEmpty(money)) {
                                    total +=Double.valueOf(pure);
                                }
                            }

                            jobsService.insertExpense(expense);
                        }
                        if (response.getExpenses() != null && response.getExpenses().size() > 0) {
                            Expense expense = new Expense();
                            expense.setJobID(jobID);
                            expense.setExpenseName("Total");
                            expense.setExpense("$" + total);

                            jobsService.insertExpense(expense);
                        }
                    } else {
                        jobsService.deleteJobByID(jobID);
                        if (!CommonUtils.isEmpty(response.getMsg())) {
                            LogUtils.errorLog(LOG_TAG, response.getMsg());
                            CommonUtils.toast(JobDetailActivity.this, response.getMsg());
                        }
                    }

                }
            } catch (Exception e) {
                CommonUtils.toast(JobDetailActivity.this, e.getMessage());
                LogUtils.errorLog(LOG_TAG, e.getMessage());
            } finally {
                bindingItems();
            }
        }
    }

    class GetErrorListener implements Response.ErrorListener {
        @Override
        public void onErrorResponse(VolleyError error) {
            String message = VolleyErrorHandler.handleError(JobDetailActivity.this, error);

            CommonUtils.toast(JobDetailActivity.this, message);
            LogUtils.errorLog(LOG_TAG, error.getMessage());
            bindingItems();
        }
    }

    public class SwipeRefreshListener implements SwipeRefreshLayout.OnRefreshListener {

        @Override
        public void onRefresh() {
            refreshItems();
        }
    }

}
