package com.netcorpgps.v3.dispatch.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.api.ErrorHandler.VolleyErrorHandler;
import com.netcorpgps.v3.dispatch.api.bean.AcknowBean;
import com.netcorpgps.v3.dispatch.api.bean.AdminBean;
import com.netcorpgps.v3.dispatch.api.bean.DriverBean;
import com.netcorpgps.v3.dispatch.api.volley.GsonRequest;
import com.netcorpgps.v3.dispatch.application.NetcorpGPSApplication;
import com.netcorpgps.v3.dispatch.utils.CommonUtils;
import com.netcorpgps.v3.dispatch.utils.LocationUtils;
import com.netcorpgps.v3.dispatch.utils.LogFileUtils;
import com.netcorpgps.v3.dispatch.utils.LogUtils;
import com.netcorpgps.v3.dispatch.utils.PermissionUtils;
import com.netcorpgps.v3.dispatch.utils.dialog.BasicDialogResponse;
import com.netcorpgps.v3.dispatch.utils.dialog.DialogUtils;
import com.netcorpgps.v3.dispatch.utils.dialog.MultiChoiceResponse;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Login Activity
 */
public class LoginActivity extends AppCompatActivity implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private final String LOG_TAG = LoginActivity.class.getSimpleName();
    private ImageView imgLogo;
    private LinearLayout linearLayoutURL;
    private Button btnSubmit;
    private EditText etURL, etUsername, etPassword;
    private TextView txtImei;

    private String loginMode = "";
    private String uniqueID = "";

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private GeocoderHandler geocoderHandler;
    private ProgressDialog progressDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        LogUtils.debugLog(LOG_TAG, "onCreate");
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);
        // Create the location client to start receiving updates
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        initWidget();
    }


    @Override
    protected void onStart() {
        super.onStart();
//        LogUtils.debugLog(LOG_TAG, "onStart");
        // Connect the client.
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        LogUtils.debugLog(LOG_TAG, "onResume");
    }

    protected void onPause() {
        super.onPause();
//        LogUtils.debugLog(LOG_TAG, "onPause");
    }

    protected void onStop() {
        // only stop if it's connected, otherwise we crash
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            // Disconnecting the client invalidates it.
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
        super.onStop();
//        LogUtils.debugLog(LOG_TAG, "onStop");
    }

    private void getUniqueID() {
        uniqueID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_imei, null);
        if (CommonUtils.isEmpty(uniqueID)) {
            uniqueID = CommonUtils.getUniqueID(this);
            if (!CommonUtils.isEmpty(uniqueID)) {
                NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_imei, uniqueID);
            }
        }
        setTxtImei(uniqueID);
    }

    /**
     * Initialize all widgets
     */
    private void initWidget() {
        linearLayoutURL = (LinearLayout) findViewById(R.id.linearLayoutURL);
        imgLogo = (ImageView) findViewById(R.id.imgLogo);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        etUsername = (EditText) findViewById(R.id.etUsername);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etURL = (EditText) findViewById(R.id.etURL);
        txtImei = (TextView) findViewById(R.id.txtImei);

        setModeAsDriver();
        imgLogo.setOnLongClickListener(new LogoLongClickListener());
        btnSubmit.setOnClickListener(new SubmitClickListener());
        getUniqueID();
    }

    private void loginAsAdmin() {
        progressDialog = ProgressDialog.show(this, "", getString(R.string.msg_loading), true);
        String apiLoginAsAdmin = getString(R.string.api_login_as_admin);
        // Todo
        // remove needed
        if (etURL.getText().toString() == null || "".equals(etURL.getText().toString().trim())){
            etURL.setText(getString(R.string.sys_ip));
            if (etUsername.getText().toString() == null || "".equals(etUsername.getText().toString().trim()))
                etUsername.setText("kevin@netcorp.com.au");
            if (etPassword.getText().toString() == null || "".equals(etPassword.getText().toString().trim()))
                etPassword.setText("D3VNETCOR");
        } else if("n3jd".equals(etURL.getText().toString().trim())) {
            etURL.setText("https://v3.netcorpgps.com.au");
            if (etUsername.getText().toString() == null || "".equals(etUsername.getText().toString().trim()))
                etUsername.setText("company@netcorpgps.com.au");
//                etUsername.setText("ccharlwood@ssc.nsw.gov.au");
            if (etPassword.getText().toString() == null || "".equals(etPassword.getText().toString().trim()))
                etPassword.setText("NetcorpGPS@123");
//                etPassword.setText("5754");
        } else if("astra".equals(etURL.getText().toString().trim())) {
            etURL.setText("http://gps.132121.com.au");
            if (etUsername.getText().toString() == null || "".equals(etUsername.getText().toString().trim()))
//                etUsername.setText("company@netcorpgps.com.au");
                etUsername.setText("company@netcorpgps.com.au");
            if (etPassword.getText().toString() == null || "".equals(etPassword.getText().toString().trim()))
//                etPassword.setText("NetcorpGPS@123");
                etPassword.setText("1234!");
        }

        Map<String, String> map = new HashMap<>();
        String username = etUsername.getText().toString();
        String password = etPassword.getText().toString();
        String ip = etURL.getText().toString();
        String url = ip + apiLoginAsAdmin;
        map.put("username", username);
        map.put("password", password);
        map.put("imei", uniqueID);
        LogUtils.infoLog(LOG_TAG, url + ": " + map.toString());
        GsonRequest<AdminBean> jsObjRequest = new GsonRequest<>(Request.Method.POST, url, AdminBean.class, map, null, new AdminLoginListener(), new LoginErrorListener());
        jsObjRequest.setShouldCache(false);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(Integer.parseInt(getString(R.string.sys_timeout)), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsObjRequest.setTag(getString(R.string.request_tag_login_admin));
        NetcorpGPSApplication.getInstance().getRequestQueue().add(jsObjRequest);
    }

    private void loginAsDriver() {
        if (!LocationUtils.isGPSEnabled(this)) {

            String title = getString(R.string.dialog_title_google_location);
            String message = getString(R.string.dialog_msg_google_location);

            String posButton = getString(R.string.dialog_btn_agree);
            String nagButton = getString(R.string.dialog_btn_disagree);
            DialogUtils.showConfirmationDialog(this, title, message, null, null, posButton, nagButton, new LocationDialogResponse());
        } else {

            // Todo
            // remove needed
            if (etUsername.getText().toString() == null || "".equals(etUsername.getText().toString().trim()))
                etUsername.setText("david");
            if (etPassword.getText().toString() == null || "".equals(etPassword.getText().toString().trim()))
                etPassword.setText("1234");
            String apiLoginCheck = getString(R.string.api_login_check);
            String companyDB = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_db);
            String vehicleID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id);
            String deviceID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_device_id);
            String ip = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_ip);
            if (!CommonUtils.isConfigured()) {
                CommonUtils.toast(this, getString(R.string.msg_error_device_configuration));
            } else {
                progressDialog = ProgressDialog.show(this, "", getString(R.string.msg_loading), true);
                Map<String, String> map = new HashMap<>();
                String username = etUsername.getText().toString();
                String password = etPassword.getText().toString();
                String url = ip + apiLoginCheck;
                map.put("username", username);
                map.put("password", password);
                map.put("imei", uniqueID);
                map.put("vehicleID", vehicleID);
                map.put("company", companyDB);
                map.put("deviceID", deviceID);
//                map.put("type", getString(R.string.record_type_login));
//                map.put("device_datetime", CommonUtils.getCurrentTime());
                LogUtils.infoLog(LOG_TAG, url + ": " + map.toString());
                GsonRequest<DriverBean> jsObjRequest = new GsonRequest<>(Request.Method.POST, url, DriverBean.class, map, null, new DriverLoginListener(getString(R.string.request_tag_login_check)), new LoginErrorListener());
                jsObjRequest.setShouldCache(false);
                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(Integer.parseInt(getString(R.string.sys_timeout)), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                jsObjRequest.setTag(getString(R.string.request_tag_login_check));
                NetcorpGPSApplication.getInstance().getRequestQueue().add(jsObjRequest);
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        // New location has now been determined
        NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_gms_latitude, Double.toString(location.getLatitude()));
        NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_gms_longitude, Double.toString(location.getLongitude()));
        Message msg = geocoderHandler.obtainMessage();
        msg.arg1 = 1;
        geocoderHandler.sendMessage(msg);
        NetcorpGPSApplication.sharedPrefManager.setLong(R.string.shared_pref_gms_location_timestamp, System.currentTimeMillis());
    }

    // Handler that receives messages from the thread
    private final class GeocoderHandler extends Handler {
        public GeocoderHandler(Looper looper) {
            super(looper);
        }
        @Override
        public void handleMessage(Message msg) {
            NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_gms_location_address, LocationUtils.getFullAddress(LoginActivity.this));
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
        HandlerThread thread = new HandlerThread("ServiceStartArguments");
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler
        Looper mServiceLooper = thread.getLooper();
        geocoderHandler = new GeocoderHandler(mServiceLooper);
    }

    @Override
    public void onConnectionSuspended(int i) {
        if (i == CAUSE_SERVICE_DISCONNECTED) {
            CommonUtils.toast(this, getString(R.string.msg_error_conn_disconn));
        } else if (i == CAUSE_NETWORK_LOST) {
            CommonUtils.toast(this, getString(R.string.msg_error_conn_network_lost));
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        CommonUtils.toast(this, connectionResult.getErrorMessage());
    }

    // Trigger new location updates at interval
    protected void startLocationUpdates() {
        // Get last known recent location.
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, LocationUtils.REQUEST_ACCESS_LOCATION);
        } else {
            // Create the location request
            mLocationRequest = LocationRequest.create()
                    .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                    .setInterval(LocationUtils.UPDATE_INTERVAL)
                    .setFastestInterval(LocationUtils.FASTEST_INTERVAL);
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }

    }

    /**
     * Implement OnClickListener for login
     */
    private class SubmitClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            if (checkAllPermissions()) {
                if (!CommonUtils.isEmpty(uniqueID)) {
//                Toast.makeText(LoginActivity.this, uniqueID, Toast.LENGTH_SHORT).show();
                    if (loginMode.equals(CommonUtils.LOGIN_MODE_ADMIN)) {
                        loginAsAdmin();
                    } else {
                        loginAsDriver();
                    }
                } else {
                    getUniqueID();
                }
            }
        }
    }

    // Check screen orientation or screen rotate event here
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Checks the orientation of the screen for landscape and portrait and set portrait mode always
//        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
//            Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
//        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
//            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
//        }
        if (loginMode.equals(CommonUtils.LOGIN_MODE_DRIVER)) {
            linearLayoutURL.setVisibility(View.GONE);
        } else {
            linearLayoutURL.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Implement LongClickListener for switching login mode
     */
    private class LogoLongClickListener implements View.OnLongClickListener {

        @Override
        public boolean onLongClick(View v) {
            if (linearLayoutURL.isShown()) {
                setModeAsDriver();
                clearEditFeilds();
            } else {
                // To ask whether switch on Admin mode
                String title = getString(R.string.dialog_title_login_as_admin);
                String msg = String.format(getString(R.string.dialog_msg_login_as_admin), getString(R.string.dialog_btn_no));
                String posButton = getString(R.string.dialog_btn_yes);
                String nagButton = getString(R.string.dialog_btn_no);
                DialogUtils.showConfirmationDialog(LoginActivity.this, title, msg, null, null, posButton, nagButton, new LogAsAdminDialogResponse());
            }
            return true;
        }
    }

    /**
     * Implement DialogResponseInterface
     */
    private class LocationDialogResponse implements BasicDialogResponse {

        @Override
        public MaterialDialog.SingleButtonCallback onPositive() {
            MaterialDialog.SingleButtonCallback callback = new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    String action = Settings.ACTION_LOCATION_SOURCE_SETTINGS;
                    LoginActivity.this.startActivity(new Intent(action));
                }
            };
            return callback;
        }

        @Override
        public MaterialDialog.SingleButtonCallback onNegative() {
            return null;
        }

        @Override
        public MaterialDialog.SingleButtonCallback onNeutral() {
            return null;
        }
    }

    private class LogAsAdminDialogResponse implements BasicDialogResponse {

        @Override
        public MaterialDialog.SingleButtonCallback onPositive() {
            MaterialDialog.SingleButtonCallback callback = new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    setModeAsAdmin();
                    clearEditFeilds();
                }
            };
            return callback;
        }

        @Override
        public MaterialDialog.SingleButtonCallback onNegative() {
            return null;
        }

        @Override
        public MaterialDialog.SingleButtonCallback onNeutral() {
            return null;
        }
    }

    private class AcknowDialogResponse implements MultiChoiceResponse {
        private DriverBean mDriverBean;
        AcknowDialogResponse(DriverBean driverBean) {
            this.mDriverBean = driverBean;
        }

        @Override
        public MaterialDialog.ListCallbackMultiChoice itemsCallbackMultiChoice() {
            MaterialDialog.ListCallbackMultiChoice callback = new MaterialDialog.ListCallbackMultiChoice() {

                @Override
                public boolean onSelection(MaterialDialog dialog, Integer[] which, CharSequence[] text) {
                    if (which != null && mDriverBean.getAcknowList() != null && which.length == mDriverBean.getAcknowList().size()) {
                        dialog.getActionButton(DialogAction.POSITIVE).setEnabled(true);
                    } else {
                        dialog.getActionButton(DialogAction.POSITIVE).setEnabled(false);
                    }
                    return true;
                }
            };
            return callback;
        }


        @Override
        public MaterialDialog.SingleButtonCallback onPositive() {
            MaterialDialog.SingleButtonCallback callback = new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    progressDialog = ProgressDialog.show(LoginActivity.this, "", getString(R.string.msg_loading), true);
                    String companyDB = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_db);
                    String vehicleID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id);
                    String deviceID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_device_id);
                    String ip = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_ip);
                    String apiLoginAsDriver = getString(R.string.api_login_as_driver);
                    Map<String, String> map = new HashMap<>();
                    String username = etUsername.getText().toString();
                    String password = etPassword.getText().toString();
                    String url = ip + apiLoginAsDriver;
                    map.put("username", username);
                    map.put("password", password);
                    map.put("imei", uniqueID);
                    map.put("vehicleID", vehicleID);
                    map.put("company", companyDB);
                    map.put("deviceID", deviceID);
                    map.put("type", getString(R.string.record_type_login));
                    map.put("device_datetime", CommonUtils.getCurrentTime());
                    map.put("device_timezone_name", CommonUtils.getTimeZoneName());
                    map.put("device_timezone_id", CommonUtils.getTimeZoneID());
                    String coordinate = "";
                    String address = "";
                    // check whether latest location is available
                    if (LocationUtils.isNewLocationAvailable()) {
                        coordinate = LocationUtils.getCoordinate();
                        address = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_gms_location_address);
                    }
                    map.put("device_coordinate", coordinate);
                    map.put("device_address", address);
                    map.put("sys_datetime", CommonUtils.getSysTime());

                    List<AcknowBean> acknowList = mDriverBean.getAcknowList();
                    StringBuffer acknowledge = new StringBuffer("");
                    for (AcknowBean acknowBean : acknowList) {
                        acknowledge.append(acknowBean.getItme() + "; ");
                    }
                    map.put("acknowledge", acknowledge.toString());
                    LogUtils.infoLog(LOG_TAG, url + ": " + map.toString());
                    GsonRequest<DriverBean> jsObjRequest = new GsonRequest<>(Request.Method.POST, url, DriverBean.class, map, null, new DriverLoginListener(getString(R.string.request_tag_login_driver)), new LoginErrorListener());
                    jsObjRequest.setShouldCache(false);
                    jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(Integer.parseInt(getString(R.string.sys_timeout)), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    jsObjRequest.setTag(getString(R.string.request_tag_login_driver));
                    NetcorpGPSApplication.getInstance().getRequestQueue().add(jsObjRequest);
                }
            };
            return callback;
        }

        @Override
        public MaterialDialog.SingleButtonCallback onNegative() {
            return null;
        }

        @Override
        public MaterialDialog.SingleButtonCallback onNeutral() {
            MaterialDialog.SingleButtonCallback callback = new MaterialDialog.SingleButtonCallback(){

                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    dialog.setSelectedIndices(new Integer[]{0, 1, 2});
                }
            };
            return callback;
        }
    }

    private boolean checkAllPermissions() {
        String[] permissions = PermissionUtils.checkAllPermissions(this);
        if (permissions != null && permissions.length > 0) {
            ActivityCompat.requestPermissions(this, permissions, PermissionUtils.PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PermissionUtils.PERMISSIONS:
                if (grantResults.length > 0) {
                    boolean isAllGranted = true;
                    for (int result : grantResults) {
                        if (result != PackageManager.PERMISSION_GRANTED) {
                            isAllGranted = false;
                        }
                    }
                    if (isAllGranted) {
                        LogFileUtils.processLogcat();
                        // obtain IMEI number
                        uniqueID = CommonUtils.getUniqueID(this);
                        if (!CommonUtils.isEmpty(uniqueID)) {
                            NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_imei, uniqueID);
                        }
                        setTxtImei(uniqueID);
                        // Begin polling for new location updates.
                        startLocationUpdates();
                    }
                }
                break;

            default:
                break;
        }
    }

    private void setTxtImei(String imei) {
        if (txtImei != null)
            txtImei.setText(String.format(getString(R.string.txt_imei), imei));
    }

    private void setModeAsDriver() {
        linearLayoutURL.setVisibility(View.GONE);
        loginMode = CommonUtils.LOGIN_MODE_DRIVER;
    }

    private void setModeAsAdmin() {
        linearLayoutURL.setVisibility(View.VISIBLE);
        loginMode = CommonUtils.LOGIN_MODE_ADMIN;
    }

    private void clearEditFeilds() {
        etURL.setText("");
        etUsername.setText("");
        etPassword.setText("");
    }

     class AdminLoginListener implements Response.Listener<AdminBean> {
        @Override
        public void onResponse(AdminBean response) {
            try {
                if (response != null) {

                    if (response.getStatus() == 1) {
                        LogUtils.debugLog(LOG_TAG, response.getName());
                        NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_mode, loginMode);
                        NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_ip, etURL.getText().toString());
                        NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_company_db, response.getCompanyDB());
                        NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_company_name, response.getCompanyName());
                        NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_vehicle_id, response.getVehicleID());
                        NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_vehicle_name, response.getVehicleName());
                        NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_device_id, response.getDeviceID());
                        LogUtils.debugLog(LOG_TAG, "Admin user login, userID:" + response.getId() + ", username:" + response.getName());
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.putExtra("admin", response);
                        startActivity(intent);
                        finish();
                    } else {
                        CommonUtils.toast(LoginActivity.this, response.getMsg());
                        LogUtils.errorLog(LOG_TAG, response.getMsg());
                    }
                }
            } catch (Exception e) {
                CommonUtils.toast(LoginActivity.this, e.getMessage());
                LogUtils.errorLog(LOG_TAG, e.getMessage());
            } finally {
                progressDialog.dismiss();
            }
        }
    }

    class DriverLoginListener implements Response.Listener<DriverBean> {
        private String mTag;

        DriverLoginListener(String tag) {
            mTag = tag;
        }

        @Override
        public void onResponse(DriverBean response) {
            try {

                if (response != null) {

                    if (response.getStatus() == 1) {
                        if (mTag.equals(getString(R.string.request_tag_login_check))) {
                            String items[] = new String[response.getAcknowList().size()];
                            int itemsIds[] = new int[response.getAcknowList().size()];
                            for (int i = 0; i < response.getAcknowList().size(); i++) {
                                items[i] = response.getAcknowList().get(i).getItme();
                                itemsIds[i] = response.getAcknowList().get(i).getId();
                            }
                            AcknowDialogResponse acknowDialogResponse = new AcknowDialogResponse(response);
                            String title = getString(R.string.dialog_title_acknowledge);
                            String posButton = getString(R.string.dialog_btn_agree);
                            String nagButton = getString(R.string.dialog_btn_disagree);
                            String neuButton = getString(R.string.dialog_btn_disagree);
                            DialogUtils.showMultipleDialog(LoginActivity.this, title, null, null, null, items, itemsIds, posButton, nagButton, neuButton, acknowDialogResponse);
                        } else if (mTag.equals(getString(R.string.request_tag_login_driver))) {
                            String username = etUsername.getText().toString();
                            String password = etPassword.getText().toString();
                            NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_user_id, response.getId());
                            NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_username, username);
                            NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_drivername, response.getFname() + " " + response.getLname());
                            NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_password, password);
                            NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_mode, loginMode);
                            NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_driver_record_id, response.getDriverRecordID());
                            NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_driver_access_token, response.getToken());
                            NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_driver_status, response.getDriverStatus());
                            NetcorpGPSApplication.sharedPrefManager.setBoolean(R.string.shared_pref_login_status, true);
                            NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_chat_msg_retention, response.getChatMsgRetention());
                            Set<String> set = new HashSet<String>(response.getChatResponseSet());
                            NetcorpGPSApplication.sharedPrefManager.setStringSet(R.string.shared_pref_quick_msg_response, set);
//                            CommonUtils.toast(LoginActivity.this, "Welcome back " + username + " \n Welcome back " + username);
                            LogUtils.debugLog(LOG_TAG, "Driver login, userID:" + response.getId() + ", username:" + response.getFname() + " " + response.getLname());
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
//                            intent.putExtra("driver", response);
                            startActivity(intent);
                            finish();
                        }

                    } else {
                        CommonUtils.toast(LoginActivity.this, response.getMsg());
                        LogUtils.errorLog(LOG_TAG, response.getMsg());
                    }
                }
            } catch (Exception e) {
                CommonUtils.toast(LoginActivity.this, e.getMessage());
                LogUtils.errorLog(LOG_TAG, e.getMessage(), e);
            } finally {
                progressDialog.dismiss();
            }
        }
    }

    class LoginErrorListener implements Response.ErrorListener {
        @Override
        public void onErrorResponse(VolleyError error) {
            String message = VolleyErrorHandler.handleError(LoginActivity.this, error);
            progressDialog.dismiss();
            CommonUtils.toast(LoginActivity.this, message);
            LogUtils.errorLog(LOG_TAG, error.getMessage(), error);
        }
    }

}
