package com.netcorpgps.v3.dispatch.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.messaging.FirebaseMessaging;
import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.api.ErrorHandler.VolleyErrorHandler;
import com.netcorpgps.v3.dispatch.api.bean.AdminBean;
import com.netcorpgps.v3.dispatch.api.bean.JobListBean;
import com.netcorpgps.v3.dispatch.api.bean.ResponseBean;
import com.netcorpgps.v3.dispatch.api.volley.GsonRequest;
import com.netcorpgps.v3.dispatch.application.NetcorpGPSApplication;
import com.netcorpgps.v3.dispatch.db.bean.Jobs;
import com.netcorpgps.v3.dispatch.db.service.DriverRecordService;
import com.netcorpgps.v3.dispatch.db.service.DriverStatusRecordService;
import com.netcorpgps.v3.dispatch.db.service.JobsService;
import com.netcorpgps.v3.dispatch.fragment.AboutFragment;
import com.netcorpgps.v3.dispatch.fragment.AcceptedJobListFragment;
import com.netcorpgps.v3.dispatch.fragment.AdminSettingsFragment;
import com.netcorpgps.v3.dispatch.fragment.AvailableJobListFragment;
import com.netcorpgps.v3.dispatch.fragment.CompletedJobListFragment;
import com.netcorpgps.v3.dispatch.fragment.HomeFragment;
import com.netcorpgps.v3.dispatch.fragment.MessageFragment;
import com.netcorpgps.v3.dispatch.fragment.SettingsFragment;
import com.netcorpgps.v3.dispatch.gcm.RegistrationGCMTokenReceiver;
import com.netcorpgps.v3.dispatch.receiver.NetworkStateReceiver;
import com.netcorpgps.v3.dispatch.receiver.SystemControllerReceiver;
import com.netcorpgps.v3.dispatch.service.ChatService;
import com.netcorpgps.v3.dispatch.service.ForegroundService;
import com.netcorpgps.v3.dispatch.service.SyncService;
import com.netcorpgps.v3.dispatch.service.SystemService;
import com.netcorpgps.v3.dispatch.utils.CommonUtils;
import com.netcorpgps.v3.dispatch.utils.LocationUtils;
import com.netcorpgps.v3.dispatch.utils.LogUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Main Panel Activity
 */
public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private final String LOG_TAG = MainActivity.class.getSimpleName();
    private Toolbar toolbar = null;
    private String loginMode = "";
    private boolean loginStatus = false;
    private NavigationView navigationView;
    private DrawerLayout drawer;
    private CollapsingToolbarLayout collapsingToolbar;
    private LinearLayout sysMsgLayout;
    private TextView sysMsgTxt;
    private NetworkStateReceiver networkStateReceiver;
    private SystemControllerReceiver systemControllerReceiver;
    public FloatingActionButton fab;
    public TextView fabTxt;
    private boolean isNetworkReceiverRegistered;
    private boolean isSystemReceiverRegistered;
    private boolean isConnected = false;
    private RegistrationGCMTokenReceiver mGCMTokenReceiver;
    private boolean isGCMTokenReceiverRegistered;
    //    private DbHelper helper = null;
//    private SQLiteDatabase db = null;
    private GeocoderHandler geocoderHandler;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Intent foregroundServiceIntent;
    private Intent syncServiceIntent;
    private Intent systemServiceIntent;
    private Intent chatServiceIntent;
    private TextView txtUsername;
    private ImageView ivMystatus;
    private ImageView ivArrow;
    private TextView txtStatus;
    private RelativeLayout layoutArrow;
    private JobsService jobsService = null;
    private ProgressDialog progressDialog = null;
    private RelativeLayout countdownLayout;
    private ImageView ivCDT;
    private boolean isCDT = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtils.debugLog(LOG_TAG, "onCreate");
        init();
    }

    @Override
    protected void onStart() {
        super.onStart();
//        LogUtils.debugLog(LOG_TAG, "onStart");
        if (jobsService == null)
            jobsService = new JobsService(this);
        registerNetworkReceiver();
//        registerSystemReceiver();
        // Connect the client.
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
        navigationViewItemSelected();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        registerGCMTokenReceiver();
//        LogUtils.debugLog(LOG_TAG, "onResume");
    }

    protected void onPause() {
        super.onPause();
//        unregisterGCMTokenReceiver();
//        LogUtils.debugLog(LOG_TAG, "onPause");
    }

    protected void onStop() {
//        LogUtils.debugLog(LOG_TAG, "onStop");

        // only stop if it's connected, otherwise we crash
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            // Disconnecting the client invalidates it.
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
        unregisterNetworkReceiver();
//        unregisterSystemReceiver();
        super.onStop();
    }

    protected void onDestroy() {
        super.onDestroy();
        LogUtils.debugLog(LOG_TAG, "onDestroy");
        if (foregroundServiceIntent != null)
            stopService(foregroundServiceIntent);
        if (syncServiceIntent != null)
            stopService(syncServiceIntent);
        if (systemServiceIntent != null)
            stopService(systemServiceIntent);
        if (chatServiceIntent != null)
            stopService(chatServiceIntent);
        dismissPDialog();
    }

    private void init() {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        helper = new DbHelper(this);
//        db = helper.getWritableDatabase();
        checkPlayServices();
//        mGCMTokenReceiver = new RegistrationGCMTokenReceiver();
        // Registering BroadcastReceiver
//        registerGCMTokenReceiver();

//        if (checkPlayServices()) {
//            // Start IntentService to register this application with GCM.
//            Intent intent = new Intent(this, RegistrationIntentService.class);
//            startService(intent);
//        }
        loginMode = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_mode);
        LogUtils.debugLog(LOG_TAG, "Login mode: " + loginMode);
        if (loginMode.equals(CommonUtils.LOGIN_MODE_ADMIN)) {
            setContentView(R.layout.activity_main_admin);
        } else if (loginMode.equals(CommonUtils.LOGIN_MODE_DRIVER)) {
            loginStatus = NetcorpGPSApplication.sharedPrefManager.getBoolean(R.string.shared_pref_login_status);
            if (!loginStatus) {
                finishActivity();
                return;
            }
            setContentView(R.layout.activity_main_driver);
            // Create the location client to start receiving updates
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();

            // Foreground Service
            if (foregroundServiceIntent == null) {
                foregroundServiceIntent = new Intent(this, ForegroundService.class);
                startService(foregroundServiceIntent);
            }

            // Sync Service
            if (syncServiceIntent == null) {
                syncServiceIntent = new Intent(this, SyncService.class);
                startService(syncServiceIntent);
            }

            // System Command Service
            if (systemServiceIntent == null) {
                systemServiceIntent = new Intent(this, SystemService.class);
                startService(systemServiceIntent);
            }

            // Chat Command Service
            if (chatServiceIntent == null) {
                chatServiceIntent = new Intent(this, ChatService.class);
                startService(chatServiceIntent);
            }

        } else {
            // ineligible login
            LogUtils.debugLog(LOG_TAG, "This is an ineligible login, back to login screen");
            Intent intent = new Intent(this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        setActionBar();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                syncMenuCounter();
                syncMenu();
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(this);
        View headerLayout = navigationView.getHeaderView(0);
        txtUsername = (TextView) headerLayout.findViewById(R.id.txt_username);

        sysMsgLayout = (LinearLayout) findViewById(R.id.sys_msg);
        sysMsgTxt = (TextView) findViewById(R.id.txt_msg);
        countdownLayout = (RelativeLayout) findViewById(R.id.countdownLayout);
        ivCDT = (ImageView) findViewById(R.id.ivCDT);

        if (loginMode.equals(CommonUtils.LOGIN_MODE_ADMIN)) {
            // Navigation Bar
            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.menu_nav_admin);
            AdminBean admin = (AdminBean) getIntent().getSerializableExtra("admin");
            txtUsername.setText(admin.getName());
            launchFragement(new AdminSettingsFragment());
        } else {
            String driverName = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_drivername);
            txtUsername.setText(driverName);

            FirebaseMessaging.getInstance().subscribeToTopic(getString(R.string.default_notification_channel_name));

//             Floating Action Button
            fab = (FloatingActionButton) findViewById(R.id.btnSOS);

            fab.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch(event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            ivCDT.setImageResource(R.drawable.ic_cdt_three);
                            countdownLayout.setVisibility(View.VISIBLE);
                            cdt.start();
                            isCDT = true;
                            return true;
                        case MotionEvent.ACTION_MOVE:
//                            LogUtils.debugLog(LOG_TAG, "ACTION_MOVE");
                            return true;
                        default:
                            countdownLayout.setVisibility(View.GONE);
                            if (isCDT == true)
                                CommonUtils.toast(MainActivity.this, "Cancel!");
                            cdt.cancel();
                            return false;
                    }
                }
            });
//            fab.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
////                    new CountDownTimer(30000, 1000) {
////
////                        public void onTick(long millisUntilFinished) {
////                            CommonUtils.toast(MainActivity.this, "onTick");
////                        }
////
////                        public void onFinish() {
////                            CommonUtils.toast(MainActivity.this, "onFinish");
////                        }
////
////                        public void cancel() {
////
////                        }
////                    }.start();
//                    // check whether latest location is available
//                }
//            });
            fabTxt = (TextView) findViewById(R.id.fabTxt);
            layoutArrow = (RelativeLayout) headerLayout.findViewById(R.id.layout_arrow);
            layoutArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switchMenu();
                }
            });
            txtUsername.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switchMenu();
                }
            });
            ivArrow = (ImageView) headerLayout.findViewById(R.id.iv_arrow);

            txtStatus = (TextView) headerLayout.findViewById(R.id.txt_status);
            ivMystatus = (ImageView) headerLayout.findViewById(R.id.iv_mystatus);
            ivMystatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switchMenu();
                }
            });
            String driverStatus = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_driver_status);
            changeDriverStatus(driverStatus);
            openFragment(getIntent());
            syncMenu();
            queryDataFromServer();
        }
    }

    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        LogUtils.debugLog(LOG_TAG, "onNewIntent");
        if (!loginStatus) {
            finishActivity();
        } else {
            openFragment(intent);
        }
    }

    public void openFragment(Intent intent) {
        String type = intent.getStringExtra("type");
        int jobID = intent.getIntExtra("jobID", 0);
        if (type != null && type.equalsIgnoreCase(getString(R.string.notification_type_paged))) {
            launchFragement(new AvailableJobListFragment());
            Intent newIntent = new Intent(this, JobDetailActivity.class);
            newIntent.putExtra("jobID", jobID);
            startActivity(newIntent);
        } else if (type != null && type.equalsIgnoreCase(getString(R.string.notification_type_assigned))) {
            launchFragement(new AcceptedJobListFragment());
            Intent newIntent = new Intent(this, JobDetailActivity.class);
            newIntent.putExtra("jobID", jobID);
            startActivity(newIntent);
        } else {
            launchFragement(new HomeFragment());
        }
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getCurrentFragment();
        if (fragment instanceof MessageFragment) {
            ((MessageFragment) fragment).onBackPressed();
        }
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Fragment fragment = getCurrentFragment();
        String fragmentName = null;
        if (fragment != null) {
            fragmentName = fragment.getClass().getSimpleName();
        }

        // Handle navigation view item clicks here.
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_home:
                if (!CommonUtils.isEmpty(fragmentName) && !fragmentName.equalsIgnoreCase(HomeFragment.class.getSimpleName())) {
                    launchFragement(new HomeFragment());
                }
                break;
            case R.id.nav_available_jobs:
                if (!CommonUtils.isEmpty(fragmentName) && !fragmentName.equalsIgnoreCase(AvailableJobListFragment.class.getSimpleName())) {
                    launchFragement(new AvailableJobListFragment());
                }
                break;
            case R.id.nav_accepted_jobs:
                if (!CommonUtils.isEmpty(fragmentName) && !fragmentName.equalsIgnoreCase(AcceptedJobListFragment.class.getSimpleName())) {
                    launchFragement(new AcceptedJobListFragment());
                }
                break;
            case R.id.nav_completed_jobs:
                if (!CommonUtils.isEmpty(fragmentName) && !fragmentName.equalsIgnoreCase(CompletedJobListFragment.class.getSimpleName())) {
                    launchFragement(new CompletedJobListFragment());
                }
                break;
            case R.id.nav_message:
                if (!CommonUtils.isEmpty(fragmentName) && !fragmentName.equalsIgnoreCase(MessageFragment.class.getSimpleName())) {
                    launchFragement(new MessageFragment());
                }
                break;
            case R.id.nav_setting:
                if (!CommonUtils.isEmpty(fragmentName) && !fragmentName.equalsIgnoreCase(SettingsFragment.class.getSimpleName())) {
                    launchFragement(new SettingsFragment());
                }
                break;
            case R.id.nav_exit:
                logout();
                break;
            case R.id.nav_about:
                if (!CommonUtils.isEmpty(fragmentName) && !fragmentName.equalsIgnoreCase(AboutFragment.class.getSimpleName())) {
                    launchFragement(new AboutFragment());
                }
                break;
            case R.id.nav_admin_exit:
                logout();
                break;
            case R.id.nav_admin_setting:
                launchFragement(new AdminSettingsFragment());
                break;
//            case R.id.nav_status_online:
//                changeDriverStatusToServer(getString(R.string.driver_status_online));
//                break;
            case R.id.nav_status_available:
                changeDriverStatusToServer(getString(R.string.driver_status_available));
                break;
            case R.id.nav_status_break:
                changeDriverStatusToServer(getString(R.string.driver_status_break));
                break;
            case R.id.nav_status_waiting:
                changeDriverStatusToServer(getString(R.string.driver_status_waiting));
                break;
            case R.id.nav_status_active:
                changeDriverStatusToServer(getString(R.string.driver_status_active));
                break;
            case R.id.nav_status_return_to_base:
                changeDriverStatusToServer(getString(R.string.driver_status_return_to_base));
                break;
            case R.id.nav_status_not_available:
                changeDriverStatusToServer(getString(R.string.driver_status_not_available));
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public Fragment getCurrentFragment() {
        try {
            FragmentManager fragmentManager = getSupportFragmentManager();
            if (fragmentManager.getBackStackEntryCount() == 0) {
                return null;
            }
            FragmentManager.BackStackEntry entry = fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1);
            if (entry == null) {
                return null;
            }
            String fragmentTag = entry.getName();
            if (CommonUtils.isEmpty(fragmentTag)) {
                return null;
            }
            Fragment currentFragment = fragmentManager.findFragmentByTag(fragmentTag);
            return currentFragment;
        } catch (Exception e) {
            LogUtils.errorLog(LOG_TAG, "getCurrentFragment  : " + e.getMessage());
            return null;
        }
    }

    // select item of menu
    public void navigationViewItemSelected() {
        Fragment fragment = getCurrentFragment();
        String fragmentName = null;
        if (fragment != null) {
            fragmentName = fragment.getClass().getSimpleName();
        }
        if (!CommonUtils.isEmpty(fragmentName) && fragmentName.equalsIgnoreCase(HomeFragment.class.getSimpleName())) {
            navigationView.setCheckedItem(R.id.nav_home);
        }
        if (!CommonUtils.isEmpty(fragmentName) && fragmentName.equalsIgnoreCase(AvailableJobListFragment.class.getSimpleName())) {
            navigationView.setCheckedItem(R.id.nav_available_jobs);
        }
        if (!CommonUtils.isEmpty(fragmentName) && fragmentName.equalsIgnoreCase(AcceptedJobListFragment.class.getSimpleName())) {
            navigationView.setCheckedItem(R.id.nav_accepted_jobs);
        }
        if (!CommonUtils.isEmpty(fragmentName) && fragmentName.equalsIgnoreCase(CompletedJobListFragment.class.getSimpleName())) {
            navigationView.setCheckedItem(R.id.nav_completed_jobs);
        }
        if (!CommonUtils.isEmpty(fragmentName) && fragmentName.equalsIgnoreCase(MessageFragment.class.getSimpleName())) {
            navigationView.setCheckedItem(R.id.nav_message);
        }
        if (!CommonUtils.isEmpty(fragmentName) && fragmentName.equalsIgnoreCase(AboutFragment.class.getSimpleName())) {
            navigationView.setCheckedItem(R.id.nav_about);
        }
    }

    public void launchFragement(Fragment fragment) {
        try {
            if (!isFinishing()) {
                String backStateName = fragment.getClass().getSimpleName();
                boolean fragmentPopped = getSupportFragmentManager().popBackStackImmediate(backStateName, 0);
                if (!fragmentPopped) {
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment_container, fragment, backStateName)
                            .addToBackStack(backStateName)
                            .commitAllowingStateLoss();
                }
            }
        } catch (Exception e) {
            LogUtils.errorLog(LOG_TAG, e.getMessage(), e);
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    public Toolbar setActionBar() {
        // ActionBar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//        if (loginMode.equals(CommonUtils.LOGIN_MODE_ADMIN)) {
        // Disable title
        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        } else {
//            collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
//        }

        return toolbar;
    }

    public void setActionBarTitle(String title) {
//        if (loginMode.equals(CommonUtils.LOGIN_MODE_ADMIN)) {
        TextView toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        toolbar_title.setText(title);
//        } else {
//            collapsingToolbar.setTitle(title);
//        }
    }

    public void logout() {
        if (loginMode.equals(CommonUtils.LOGIN_MODE_ADMIN)) {
            finishActivity();
        } else {
            Map<String, String> map = new HashMap<>();
            String companyDB = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_db);
            String vehicleID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id);
            String deviceID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_device_id);
            String driverID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_user_id);
            String driverRecordID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_driver_record_id);
            String ip = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_ip);
            String apiLoginAsDriver = getString(R.string.api_logout_as_driver);
            String url = ip + apiLoginAsDriver;
            map.put("driverID", driverID);
            map.put("vehicleID", vehicleID);
            map.put("company", companyDB);
            map.put("deviceID", deviceID);
            map.put("driverRecordID", driverRecordID);
            map.put("type", getString(R.string.record_type_logout));
            map.put("device_datetime", CommonUtils.getCurrentTime());
            map.put("device_timezone_name", CommonUtils.getTimeZoneName());
            map.put("device_timezone_id", CommonUtils.getTimeZoneID());
            String coordinate = "";
            String address = "";
            // check whether latest location is available
            if (LocationUtils.isNewLocationAvailable()) {
                coordinate = LocationUtils.getCoordinate();
                address = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_gms_location_address);
            }
            map.put("device_coordinate", coordinate);
            map.put("device_address", address);
            map.put("sys_datetime", CommonUtils.getSysTime());
            map.put("driverStatus", getString(R.string.driver_status_offline));
            // query accpedted jobs
            GsonRequest<ResponseBean> jsObjRequest = new GsonRequest<>(Request.Method.POST, url, ResponseBean.class, map, null, new LogoutListener(map), new ErrorListener(map));
            jsObjRequest.setShouldCache(false);
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(Integer.parseInt(getString(R.string.sys_timeout)), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            jsObjRequest.setTag(getString(R.string.request_tag_logout_driver));
            NetcorpGPSApplication.getInstance().getRequestQueue().add(jsObjRequest);
        }
    }

    private void finishActivity() {
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        if (foregroundServiceIntent != null)
//            stopService(foregroundServiceIntent);
//        if (syncServiceIntent != null)
//            stopService(syncServiceIntent);
//        if (systemServiceIntent != null)
//            stopService(systemServiceIntent);
        LogUtils.debugLog(LOG_TAG, "User logout");
        NetcorpGPSApplication.sharedPrefManager.setBoolean(R.string.shared_pref_login_status, false);
        startActivity(intent);
        finish();
    }

    public void syncMenu() {
        if (loginMode.equals(CommonUtils.LOGIN_MODE_DRIVER)) {
            JobsService jobsService = new JobsService(this);
            NetcorpGPSApplication.sharedPrefManager.setInt(R.string.shared_pref_available_jobs_num, jobsService.getAvaJobsNum());
            NetcorpGPSApplication.sharedPrefManager.setInt(R.string.shared_pref_accepted_jobs_num, jobsService.getAccJobsNum());
            NetcorpGPSApplication.sharedPrefManager.setInt(R.string.shared_pref_completed_jobs_num, jobsService.getComJobsNum());
            syncMenuCounter();
        }
    }

    public void syncMenuCounter() {
        Fragment fragment = getCurrentFragment();
        if (fragment != null && fragment instanceof MessageFragment) {
            NetcorpGPSApplication.sharedPrefManager.setInt(R.string.shared_pref_chat_msg_num, 0);
        }
        int shared_pref_chat_msg_num = NetcorpGPSApplication.sharedPrefManager.getInt(R.string.shared_pref_chat_msg_num);
        setMenuCounter(R.id.nav_message, shared_pref_chat_msg_num);
        int available_jobs_num = NetcorpGPSApplication.sharedPrefManager.getInt(R.string.shared_pref_available_jobs_num);
        setMenuCounter(R.id.nav_available_jobs, available_jobs_num);
        int accepted_jobs_num = NetcorpGPSApplication.sharedPrefManager.getInt(R.string.shared_pref_accepted_jobs_num);
        setMenuCounter(R.id.nav_accepted_jobs, accepted_jobs_num);
        int completed_jobs_num = NetcorpGPSApplication.sharedPrefManager.getInt(R.string.shared_pref_completed_jobs_num);
        setMenuCounter(R.id.nav_completed_jobs, completed_jobs_num);
    }

    private void setMenuCounter(@IdRes int itemId, int count) {
        Menu menuNav = navigationView.getMenu();
        MenuItem element = menuNav.findItem(itemId);
        if (element != null) {
            String title = element.getTitle().toString();
            String[] titles = title.split("    ");
            String before = titles[0];
            element.setTitle(CommonUtils.getMenuTitleWithNotification(this, before, count));
        }

    }

    public void setSystemMessage(String message) {
        sysMsgLayout.setVisibility(View.VISIBLE);
        sysMsgTxt.setText(message);
    }

    public void closeSysMsgLayout() {
        if (!isConnected()) {
            setSystemMessage(getString(R.string.msg_error_no_network_available));
        } else {
            sysMsgLayout.setVisibility(View.GONE);
            sysMsgTxt.setText("");
        }
    }

    public boolean isConnected() {
        return isConnected;
    }

    public void setConnected(boolean connected) {
        isConnected = connected;
    }

//    private void registerSystemReceiver() {
//        if (!isSystemReceiverRegistered) {
//            systemControllerReceiver = new SystemControllerReceiver(this, new SystemControllerReceiver.SystemControllerResponse() {
//
//                @Override
//                public void exitApp() {
//                    logout();
//                }
//            });
//            IntentFilter intentFilter = new IntentFilter();
//            intentFilter.addAction("command");
//            registerReceiver(systemControllerReceiver, intentFilter);
//            isSystemReceiverRegistered = true;
//        }
//    }

//    private void unregisterSystemReceiver() {
//        if (systemControllerReceiver != null) {
//            unregisterReceiver(systemControllerReceiver);
//            isSystemReceiverRegistered = false;
//        }
//    }

    private void registerNetworkReceiver() {
//        registerReceiver(abcd, new IntentFilter("xyz"));
        if (!isNetworkReceiverRegistered) {
            networkStateReceiver = new NetworkStateReceiver(new NetworkStateReceiver.NetworkStateResponse() {
                @Override
                public void responseToConnectionOn() {
                    setConnected(true);
                    closeSysMsgLayout();
                }

                @Override
                public void responseToConnectionOff() {
                    setConnected(false);
                    setSystemMessage(getString(R.string.msg_error_no_network_available));
                }
            });
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
            intentFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
            intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
            registerReceiver(networkStateReceiver, intentFilter);
            isNetworkReceiverRegistered = true;
        }
    }

    private void unregisterNetworkReceiver() {
        if (networkStateReceiver != null) {
            unregisterReceiver(networkStateReceiver);
            isNetworkReceiverRegistered = false;
        }
    }

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    // GCM is deprecated
    private void registerGCMTokenReceiver() {
        if (!isGCMTokenReceiverRegistered) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(getString(R.string.shared_pref_gcm_registration_complete));
            LocalBroadcastManager.getInstance(this).registerReceiver(mGCMTokenReceiver, intentFilter);
            isGCMTokenReceiverRegistered = true;
        }
    }

    private void unregisterGCMTokenReceiver() {
        if (mGCMTokenReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mGCMTokenReceiver);
            isGCMTokenReceiverRegistered = false;
        }
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
        HandlerThread thread = new HandlerThread("ServiceStartArguments");
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler
        Looper mServiceLooper = thread.getLooper();
        geocoderHandler = new GeocoderHandler(mServiceLooper);
    }

    @Override
    public void onConnectionSuspended(int i) {
        if (i == CAUSE_SERVICE_DISCONNECTED) {
            CommonUtils.toast(this, getString(R.string.msg_error_conn_disconn));
        } else if (i == CAUSE_NETWORK_LOST) {
            CommonUtils.toast(this, getString(R.string.msg_error_conn_network_lost));
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        CommonUtils.toast(this, connectionResult.getErrorMessage());
    }

    @Override
    public void onLocationChanged(Location location) {
        // New location has now been determined
        NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_gms_latitude, Double.toString(location.getLatitude()));
        NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_gms_longitude, Double.toString(location.getLongitude()));

        Message msg = geocoderHandler.obtainMessage();
        msg.arg1 = 1;
        geocoderHandler.sendMessage(msg);
        NetcorpGPSApplication.sharedPrefManager.setLong(R.string.shared_pref_gms_location_timestamp, System.currentTimeMillis());
    }

    // Handler that receives messages from the thread
    private final class GeocoderHandler extends Handler {
        public GeocoderHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_gms_location_address, LocationUtils.getFullAddress(MainActivity.this));
        }
    }

    // Trigger new location updates at interval
    protected void startLocationUpdates() {
        // Get last known recent location.
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, LocationUtils.REQUEST_ACCESS_LOCATION);
        } else {
            // Create the location request
            mLocationRequest = LocationRequest.create()
                    .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                    .setInterval(LocationUtils.UPDATE_INTERVAL)
                    .setFastestInterval(LocationUtils.FASTEST_INTERVAL);
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }

    }

    class LogoutListener implements Response.Listener<ResponseBean> {

        private Map<String, String> mMap;

        public LogoutListener(Map<String, String> map) {
            mMap = map;
        }

        @Override
        public void onResponse(ResponseBean response) {
            try {
                if (response != null) {
                    if (response.getStatus() == 0) {
                        saveSyncDriverRecord(mMap);
                    }
                }
            } catch (Exception e) {
                saveSyncDriverRecord(mMap);
                LogUtils.errorLog(LOG_TAG, e.getMessage(), e);
            } finally {
                finishActivity();
            }
        }
    }

    class ErrorListener implements Response.ErrorListener {

        private Map<String, String> mMap;

        public ErrorListener(Map<String, String> map) {
            mMap = map;
        }

        @Override
        public void onErrorResponse(VolleyError error) {
//            String message = VolleyErrorHandler.handleError(MainActivity.this, error);
            saveSyncDriverRecord(mMap);
            finishActivity();
            LogUtils.errorLog(LOG_TAG, error.getMessage(), error);
        }
    }

    private void saveSyncDriverRecord(Map<String, String> record) {
        DriverRecordService service = new DriverRecordService(this);
        service.insert(record);
        DriverStatusRecordService dsrsService = new DriverStatusRecordService(this);
        dsrsService.insert(record);
    }

    class DriverStatusListener implements Response.Listener<ResponseBean> {

        private Map<String, String> mMap;

        public DriverStatusListener(Map<String, String> map) {
            mMap = map;
        }

        @Override
        public void onResponse(ResponseBean response) {
            try {
                if (response != null) {
                    if (response.getStatus() == 0) {
                        saveSyncDriverStatusRecord(mMap);
                        LogUtils.errorLog(LOG_TAG, response.getMsg());
                    }
                }
            } catch (Exception e) {
                saveSyncDriverStatusRecord(mMap);
                LogUtils.errorLog(LOG_TAG, e.getMessage());
            }
        }
    }

    class DriverStatusErrorListener implements Response.ErrorListener {

        private Map<String, String> mMap;

        public DriverStatusErrorListener(Map<String, String> map) {
            mMap = map;
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            String message = VolleyErrorHandler.handleError(MainActivity.this, error);
            saveSyncDriverStatusRecord(mMap);
            LogUtils.errorLog(LOG_TAG, error.getMessage());
        }
    }

    class SOSListener implements Response.Listener<ResponseBean> {

        @Override
        public void onResponse(ResponseBean response) {
            try {
                if (response != null) {
//                    if (response.getStatus() == 0) {
                        LogUtils.debugLog(LOG_TAG, response.getMsg());
                        CommonUtils.toast(MainActivity.this, response.getMsg());
//                    }
                }
            } catch (Exception e) {
                LogUtils.errorLog(LOG_TAG, e.getMessage());
                CommonUtils.toast(MainActivity.this, e.getMessage());
            }
        }
    }

    class SOSErrorListener implements Response.ErrorListener {

        @Override
        public void onErrorResponse(VolleyError error) {
            CommonUtils.toast(MainActivity.this, error.getMessage());
            LogUtils.errorLog(LOG_TAG, error.getMessage());
        }
    }

    private void saveSyncDriverStatusRecord(Map<String, String> record) {
        DriverStatusRecordService service = new DriverStatusRecordService(this);
        service.insert(record);
    }

    private void switchMenu() {
        MenuItem menuItem = navigationView.getMenu().findItem(R.id.nav_status_available);
        if (menuItem == null) {
            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.menu_nav_status);
            ivArrow.setImageResource(R.drawable.ic_arrow_drop_up);
        } else {
            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.menu_nav_driver);
            ivArrow.setImageResource(R.drawable.ic_arrow_drop_down);
            syncMenuCounter();
        }
    }

    private String changeDriverStatus(String status) {
        if (status.equalsIgnoreCase(getString(R.string.driver_status_available))) {
            ivMystatus.setImageResource(R.drawable.ic_status_available);

        } else if (status.equalsIgnoreCase(getString(R.string.driver_status_online))) {
            ivMystatus.setImageResource(R.drawable.ic_status_online);

        } else if (status.equalsIgnoreCase(getString(R.string.driver_status_break))) {
            ivMystatus.setImageResource(R.drawable.ic_status_break);

        } else if (status.equalsIgnoreCase(getString(R.string.driver_status_waiting))) {
            ivMystatus.setImageResource(R.drawable.ic_status_waiting);

        } else if (status.equalsIgnoreCase(getString(R.string.driver_status_active))) {
            ivMystatus.setImageResource(R.drawable.ic_status_active);

        } else if (status.equalsIgnoreCase(getString(R.string.driver_status_return_to_base))) {
            ivMystatus.setImageResource(R.drawable.ic_status_return_to_base);

        } else if (status.equalsIgnoreCase(getString(R.string.driver_status_not_available))) {
            ivMystatus.setImageResource(R.drawable.ic_status_not_available);
        }
        txtStatus.setText(status);
        NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_driver_status, status);
        return status;
    }

    private void changeDriverStatusToServer(String status) {
        if (!status.equalsIgnoreCase(NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_driver_status))) {
            changeDriverStatus(status);
            Map<String, String> map = new HashMap<>();
            String ip = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_ip);
            String vehicleID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id);
            String companyDB = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_db);
            String accessToken = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_driver_access_token);
            String driverRecordID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_driver_record_id);
            String driverID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_user_id);
            String deviceID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_device_id);
            String apiToChangeDriverStatus = getString(R.string.api_to_change_driver_status);
            String url = ip + apiToChangeDriverStatus;
            map.put("company", companyDB);
            map.put("vehicleID", vehicleID);
            map.put("driverRecordID", driverRecordID);
            map.put("accessToken", accessToken);
            map.put("driverID", driverID);
            map.put("deviceID", deviceID);
            map.put("driverRecordID", driverRecordID);
            map.put("driverStatus", status);
            map.put("device_datetime", CommonUtils.getCurrentTime());
            map.put("device_timezone_name", CommonUtils.getTimeZoneName());
            map.put("device_timezone_id", CommonUtils.getTimeZoneID());
            String coordinate = "";
            String address = "";
            // check whether latest location is available
            if (LocationUtils.isNewLocationAvailable()) {
                coordinate = LocationUtils.getCoordinate();
                address = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_gms_location_address);
            }
            map.put("device_coordinate", coordinate);
            map.put("device_address", address);
            map.put("sys_datetime", CommonUtils.getSysTime());
            LogUtils.infoLog(LOG_TAG, url + ": " + map.toString());
            GsonRequest<ResponseBean> jsObjRequest = new GsonRequest<>(Request.Method.POST, url, ResponseBean.class, map, null, new DriverStatusListener(map), new DriverStatusErrorListener(map));
            jsObjRequest.setShouldCache(false);
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(Integer.parseInt(getString(R.string.sys_timeout)), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            jsObjRequest.setTag(getString(R.string.request_tag_to_change_driver_status));
            NetcorpGPSApplication.getInstance().getRequestQueue().add(jsObjRequest);
        }
    }

    public void showFab() {
        if (fab != null) {
            fabTxt.setVisibility(View.VISIBLE);
            fab.show();
        }
    }

    public void hideFab() {
        if (fab != null) {
            fab.hide();
            fabTxt.setVisibility(View.GONE);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LocationUtils.REQUEST_ACCESS_LOCATION:
                // Begin polling for new location updates.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startLocationUpdates();
                }
            default:
                break;
        }
    }

    // sync all data from server
    public void queryDataFromServer() {
//        openPDialog();

        String ip = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_ip);
        String vehicleID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id);
        String companyDB = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_db);
        String accessToken = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_driver_access_token);
        String driverRecordID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_driver_record_id);
        String driverID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_user_id);
        String deviceID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_device_id);
        String apiGetAvailableJobList = getString(R.string.api_get_available_jobList);
        String url0 = ip + apiGetAvailableJobList;
        Map map = new HashMap();
        map.put("company", companyDB);
        map.put("vehicleID", vehicleID);
        map.put("driverRecordID", driverRecordID);
        map.put("accessToken", accessToken);
        map.put("driverID", driverID);
        map.put("deviceID", deviceID);
        String number = NetcorpGPSApplication.sharedPrefManager.getInt(R.string.shared_pref_completed_job_number, 30) + "";
        map.put("number", number);
        LogUtils.infoLog(LOG_TAG, url0 + ": " + map.toString());
        GsonRequest<JobListBean> jsObjRequest0 = new GsonRequest<>(Request.Method.POST, url0, JobListBean.class, map, null, new GetAvailableJobListListener(), new GetJobErrorListener());
        jsObjRequest0.setShouldCache(false);
        jsObjRequest0.setRetryPolicy(new DefaultRetryPolicy(Integer.parseInt(getString(R.string.sys_timeout)), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsObjRequest0.setTag(getString(R.string.request_tag_get_jobs));
        NetcorpGPSApplication.getInstance().getRequestQueue().add(jsObjRequest0);

        String apiGetCompletedJobList = getString(R.string.api_get_completed_jobList);
        String url2 = ip + apiGetCompletedJobList;
        LogUtils.infoLog(LOG_TAG, url2 + ": " + map.toString());
        GsonRequest<JobListBean> jsObjRequest2 = new GsonRequest<>(Request.Method.POST, url2, JobListBean.class, map, null, new GetCompletedJobListListener(), new GetJobErrorListener());
        jsObjRequest2.setShouldCache(false);
        jsObjRequest2.setRetryPolicy(new DefaultRetryPolicy(Integer.parseInt(getString(R.string.sys_timeout)), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsObjRequest2.setTag(getString(R.string.request_tag_get_completed_jobs));
        NetcorpGPSApplication.getInstance().getRequestQueue().add(jsObjRequest2);

        String apiGetAcceptedJobList = getString(R.string.api_get_accepted_jobList);
        String url1 = ip + apiGetAcceptedJobList;
        LogUtils.infoLog(LOG_TAG, url1 + ": " + map.toString());
        GsonRequest<JobListBean> jsObjRequest1 = new GsonRequest<>(Request.Method.POST, url1, JobListBean.class, map, null, new GetAcceptedJobListListener(), new GetAcceptedJobErrorListener());
        jsObjRequest1.setShouldCache(false);
        jsObjRequest1.setRetryPolicy(new DefaultRetryPolicy(Integer.parseInt(getString(R.string.sys_timeout)), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsObjRequest1.setTag(getString(R.string.request_tag_to_accept_job));
        NetcorpGPSApplication.getInstance().getRequestQueue().add(jsObjRequest1);
    }

    class GetAvailableJobListListener implements Response.Listener<JobListBean> {

        @Override
        public void onResponse(JobListBean response) {
            try {
                if (response != null) {

                    jobsService.deleteAvailableJobs();
                    if (response.getStatus() == 1) {

                        for (Jobs job : response.getJobList()) {
                            long newRowId = jobsService.insertJob(job);
                        }
                        jobsService.syncWaypointAndExpenseWithJob();
                    }
                    LogUtils.debugLog(LOG_TAG, response.getMsg());
                }
            } catch (Exception e) {
                LogUtils.errorLog(LOG_TAG, e.getMessage(), e);

            }
        }
    }

    class GetCompletedJobListListener implements Response.Listener<JobListBean> {

        @Override
        public void onResponse(JobListBean response) {
            try {
                if (response != null) {

                    jobsService.deleteCompletedJobs();
                    if (response.getStatus() == 1) {

                        for (Jobs job : response.getJobList()) {

                            long newRowId = jobsService.insertJob(job);
                        }
                        jobsService.syncWaypointAndExpenseWithJob();
                    }
                    LogUtils.debugLog(LOG_TAG, response.getMsg());
                }
            } catch (Exception e) {
                LogUtils.errorLog(LOG_TAG, e.getMessage(), e);
            }
        }
    }

    class GetAcceptedJobListListener implements Response.Listener<JobListBean> {

        @Override
        public void onResponse(JobListBean response) {
            try {
                if (response != null) {

                    jobsService.deleteAcceptedJobs();
                    if (response.getStatus() == 1) {

                        for (Jobs job : response.getJobList()) {

                            String sysDatetime = jobsService.querySysDatetime(job.getJobID());

                            if (!CommonUtils.isEmpty(sysDatetime) && !CommonUtils.isEmpty(job.getLastStatusTime())) {
                                if (CommonUtils.compareDatetime(sysDatetime, job.getLastStatusTime())) {
                                    job = jobsService.queryJobStatusBySysDatetime(String.valueOf(job.getJobID()), sysDatetime, job);
                                }
                            }
                            jobsService.insertJob(job);
                        }
                        jobsService.syncWaypointAndExpenseWithJob();
                    }
                    LogUtils.debugLog(LOG_TAG, response.getMsg());
                }
            } catch (Exception e) {
                LogUtils.errorLog(LOG_TAG, e.getMessage(), e);
            } finally {
//                dismissPDialog();
                openFragment(getIntent());
            }
        }
    }

    class GetJobErrorListener implements Response.ErrorListener {
        @Override
        public void onErrorResponse(VolleyError error) {
            LogUtils.errorLog(LOG_TAG, error.getMessage(), error);
        }
    }

    class GetAcceptedJobErrorListener implements Response.ErrorListener {
        @Override
        public void onErrorResponse(VolleyError error) {
            LogUtils.errorLog(LOG_TAG, error.getMessage(), error);
            dismissPDialog();
            openFragment(getIntent());
        }
    }

    public void openPDialog() {
        progressDialog = ProgressDialog.show(this, "", getString(R.string.msg_loading), true);
    }

    public void dismissPDialog() {
        if (!MainActivity.this.isFinishing() && progressDialog != null) {
            if (progressDialog.isShowing())
                progressDialog.dismiss();
        }
    }

    // count down timer for long press for sos button
    public CountDownTimer cdt = new CountDownTimer(3300, 1000) {

        @Override
        public void onTick(long millisUntilFinished) {
            if (millisUntilFinished <= 3300 && millisUntilFinished > 2300) {
                ivCDT.setImageResource(R.drawable.ic_cdt_three);
            } else if (millisUntilFinished <= 2300 && millisUntilFinished > 1300) {
                ivCDT.setImageResource(R.drawable.ic_cdt_two);
            } else if (millisUntilFinished <= 1300 && millisUntilFinished > 0) {
                ivCDT.setImageResource(R.drawable.ic_cdt_one);
            }
//            LogUtils.debugLog(LOG_TAG, millisUntilFinished+"");
        }

        @Override
        public void onFinish() {
            countdownLayout.setVisibility(View.GONE);
            isCDT = false;
//            if (LocationUtils.isNewLocationAvailable()) {
//                        Address address = LocationUtils.getAddressObject(MainActivity.this);
//                        String geoUri = "http://maps.google.com/maps?q=loc:" + LocationUtils.getCoordinate() + " (fdsfsfsaf)";
//                        String geoUri = "http://maps.google.com/maps?q=44 Baker Street, Carlingford, New South Wales";
//                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
//                        startActivity(intent);
//                        Snackbar.make(view, LocationUtils.getFullAddress(MainActivity.this), Snackbar.LENGTH_LONG).setAction("Action", null).show();
                Map<String, String> map = new HashMap<>();
                String companyID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_id);
                String companyName = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_name);
                String vehicleID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id);
                String vehicleName = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_name);
                String ip = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_ip);
                String api = getString(R.string.api_send_sos);
                String url = ip + api;
                map.put("companyID", companyID);
                map.put("companyName", companyName);
                map.put("vehicleID", vehicleID);
                map.put("vehicleName", vehicleName);
                map.put("dateTime", CommonUtils.getCurrentTime());
                String coordinate = "";
                String lat = "", lng = "";
                // check whether latest location is available
                if (LocationUtils.isNewLocationAvailable()) {
                    coordinate = LocationUtils.getCoordinate();
                }
                if (!CommonUtils.isEmpty(coordinate)) {
                    String[] tem = coordinate.split(",");
                    if (tem.length == 2) {
                        lat = tem[0];
                        lng = tem[1];
                    }
                }
                map.put("lat", lat);
                map.put("lng", lng);
                map.put("localTime", CommonUtils.getSysTime());
                LogUtils.infoLog(LOG_TAG, url + ": " + map.toString());

                // query accpedted jobs
                GsonRequest<ResponseBean> jsObjRequest = new GsonRequest<>(Request.Method.POST, url, ResponseBean.class, map, null, new SOSListener(), new SOSErrorListener());
                jsObjRequest.setShouldCache(false);
                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(Integer.parseInt(getString(R.string.sys_timeout)), 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                jsObjRequest.setTag(getString(R.string.request_tag_send_sos));
                NetcorpGPSApplication.getInstance().getRequestQueue().add(jsObjRequest);
//            }
        }
    };

}
