package com.netcorpgps.v3.dispatch.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ActionMenuView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.api.bean.AdminBean;
import com.netcorpgps.v3.dispatch.api.bean.VehiclesBean;
import com.netcorpgps.v3.dispatch.fragment.AdminSettingsFragment;
import com.netcorpgps.v3.dispatch.fragment.CompanyListFragment;
import com.netcorpgps.v3.dispatch.fragment.SettingMessageListFragment;
import com.netcorpgps.v3.dispatch.fragment.VehicleListFragment;
import com.netcorpgps.v3.dispatch.utils.LogUtils;

public class SlideInActivity extends AppCompatActivity implements CompanyListFragment.OnCompanyListListener, VehicleListFragment.OnVehicleListListener {

    private final String LOG_TAG = SlideInActivity.class.getSimpleName();
    private Toolbar toolbar = null;
    private ActionMenuView amvMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        LogUtils.debugLog(LOG_TAG, "onCreate");
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_right);
        setContentView(R.layout.activity_slide_in);
        setActionBar();
        String fragment_name = getIntent().getStringExtra("fragment");
//        Fragment fragment = null;
        if("company".equals(fragment_name)) {
            CompanyListFragment fragment = new CompanyListFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, fragment, fragment.getClass().getSimpleName())
                    .addToBackStack(fragment.getClass().getSimpleName())
                    .commitAllowingStateLoss();

        } else if ("vehicle".equals(fragment_name)) {
            VehicleListFragment fragment = new VehicleListFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, fragment, fragment.getClass().getSimpleName())
                    .addToBackStack(fragment.getClass().getSimpleName())
                    .commitAllowingStateLoss();
        } else if ("chatMsg".equals(fragment_name)) {
            SettingMessageListFragment fragment = new SettingMessageListFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, fragment, fragment.getClass().getSimpleName())
                    .addToBackStack(fragment.getClass().getSimpleName())
                    .commitAllowingStateLoss();
        }

    }

    @Override
    public void onBackPressed() {
        super.finish();
        overridePendingTransition(R.anim.animation_enter,R.anim.animation_leave);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.animation_enter,R.anim.animation_leave);
    }

    public void onDestroy() {
//        LogUtils.debugLog(LOG_TAG, "onDestroy");
        super.onDestroy();
    }

    @Override
    public void onCompanyListInteraction(AdminBean.Company item) {
        Intent intent = new Intent();
        intent.setAction(AdminSettingsFragment.AdminSettingBroadcastReceiver.ACTION);
        intent.putExtra("type", "company");
        intent.putExtra("company", item);
        sendBroadcast(intent);
        finish();
        overridePendingTransition(R.anim.animation_enter,R.anim.animation_leave);
    }

    @Override
    public void onVehicleListInteraction(VehiclesBean.Vehicle item) {
        Intent intent = new Intent();
        intent.setAction(AdminSettingsFragment.AdminSettingBroadcastReceiver.ACTION);
        intent.putExtra("type", "vehicle");
        intent.putExtra("vehicle", item);
        sendBroadcast(intent);
        finish();
        overridePendingTransition(R.anim.animation_enter,R.anim.animation_leave);
    }

    public Toolbar setActionBar() {
        // ActionBar
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        // Disable title
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.ic_btn_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        return toolbar;
    }

    public void setActionBarTitle(String title) {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText(title);
    }

//    class BtnBackListener implements View.OnClickListener {
//        @Override
//        public void onClick(View v) {
//            finish();
//            overridePendingTransition(R.anim.animation_enter,R.anim.animation_leave);
//        }
//    }
}
