package com.netcorpgps.v3.dispatch.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.api.bean.SystemBean;
import com.netcorpgps.v3.dispatch.api.volley.GsonRequest;
import com.netcorpgps.v3.dispatch.application.NetcorpGPSApplication;
import com.netcorpgps.v3.dispatch.db.bean.Jobs;
import com.netcorpgps.v3.dispatch.db.service.JobsService;
import com.netcorpgps.v3.dispatch.utils.CommonUtils;
import com.netcorpgps.v3.dispatch.utils.LogFileUtils;
import com.netcorpgps.v3.dispatch.utils.LogUtils;
import com.netcorpgps.v3.dispatch.utils.PermissionUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class SplashActivity extends AppCompatActivity {

    private final String LOG_TAG = SplashActivity.class.getSimpleName();
    private int versionCode;
    private String versionName;
    private ProgressDialog progressDialog;
    private final int MESSAGE_INSTALL_APP = 1;
    private String apkName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        overridePendingTransition(R.anim.fadein, R.anim.fadein);
        boolean loginStatus = NetcorpGPSApplication.sharedPrefManager.getBoolean(R.string.shared_pref_login_status);
        Bundle bundle = getIntent().getExtras();

        if (bundle != null && bundle.getString("type") != null && loginStatus) {
            // acquire notification from fcm server
            getExtras(bundle);
        } else {
            LogUtils.debugLog(LOG_TAG, getString(R.string.log_start_splash_activity));
            setContentView(R.layout.activity_splash);
            NetcorpGPSApplication.sharedPrefManager.setBoolean(R.string.shared_pref_login_status, false);
            if (checkAllPermissions()) {
                checkAppVersion();
            }
        }
        super.onCreate(savedInstanceState);
    }

    private void getExtras(Bundle bundle) {
        LogUtils.debugLog(LOG_TAG, "getExtras");
        try {
            String type = bundle.getString("type");
            LogUtils.debugLog(LOG_TAG, bundle.toString());
            if (!CommonUtils.isEmpty(type)) {
                JobsService jobsService = new JobsService(this);
                if (type.equalsIgnoreCase(getString(R.string.notification_type_paged)) || type.equalsIgnoreCase(getString(R.string.notification_type_assigned))) {
                    Jobs job = new Jobs();
                    int jobID = Integer.valueOf(bundle.getString("jobID"));
                    job.setJobID(jobID);
                    job.setJobCode(bundle.getString("jobCode"));
                    job.setCustomer(bundle.getString("customer"));
                    job.setContactName(bundle.getString("contactName"));
                    job.setContactNumber(bundle.getString("contactNumber"));
                    job.setScheduledTime(bundle.getString("scheduledTime"));
                    job.setStartTime(bundle.getString("startTime"));
                    job.setVehicleName(bundle.getString("vehicleName"));
                    job.setVehicleID(bundle.getString("vehicleID"));
                    job.setDriverID(bundle.getString("driverID"));
                    job.setDriverName(bundle.getString("driverName"));
                    job.setJobType(bundle.getString("jobType"));
                    job.setFromBase(Integer.valueOf(bundle.getString("fromBase")));
                    job.setToBase(Integer.valueOf(bundle.getString("toBase")));
                    job.setDescription(bundle.getString("description"));
                    job.setRequirement(bundle.getString("requirement"));
                    job.setPassengers(Integer.valueOf(bundle.getString("passengers")));
                    job.setJobStatus(bundle.getString("jobStatus"));
                    job.setColorCode(bundle.getString("colorCode"));
                    job.setPagedNumber(bundle.getString("pagedNumber"));

                    long rowId = jobsService.updateJob(job, jobID);
                    if (rowId == 0) {
                        rowId = jobsService.insertJob(job);
                    }

                    Intent intent = new Intent(this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("type", type);
                    intent.putExtra("jobID", jobID);
                    startActivity(intent);
                }
            }
        } catch (Exception e) {
            startSplashScreen();
        }
    }

    /**
     * Splash screen time
     */
    private void startSplashScreen() {


        new Handler().postDelayed(new Runnable() {

			/*
             * Showing splash screen with a timer. This will be useful when you
			 * want to show case your app logo / company
			 */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app menu_action_save activity
                Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(i);
                LogUtils.debugLog(LOG_TAG, getString(R.string.log_start_login_activity));
                // close this activity
                finish();
            }
        }, Integer.parseInt(getString(R.string.splash_time)));
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private int getAppVersion() {
        try {
            PackageInfo packageInfo = this.getPackageManager().getPackageInfo(this.getPackageName(), 0);
            versionCode = packageInfo.versionCode;
            versionName = packageInfo.versionName;
            LogUtils.debugLog(LOG_TAG, String.format(getString(R.string.log_app_version_code), versionCode, versionName));
            return versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            LogUtils.errorLog(LOG_TAG, getString(R.string.msg_package_name) + e, e);
            return -1;
        }
    }

    private void checkAppVersion() {
        String ip = getString(R.string.sys_ip);
        String api = getString(R.string.api_check_app_version);
        String url = ip + api;
        Map map = new HashMap();
        map.put("os", NetcorpGPSApplication.getInstance().getDeviceInformation().getOs());
        LogUtils.infoLog(LOG_TAG, url + ": " + map.toString());
        GsonRequest<SystemBean> jsObjRequest = new GsonRequest<>(Request.Method.POST, url, SystemBean.class, map, null, new GsonRequestListener(), new GsonRequestErrorListener());
        jsObjRequest.setShouldCache(false);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(Integer.parseInt(getString(R.string.sys_timeout)), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsObjRequest.setTag(getString(R.string.request_tag_check_app_version));
        NetcorpGPSApplication.getInstance().getRequestQueue().add(jsObjRequest);
    }

    class GsonRequestListener implements Response.Listener<SystemBean> {

        @Override
        public void onResponse(final SystemBean response) {
            try {
                if (response != null) {

                    if (response.getVersionCode() > getAppVersion()) {

                        LogUtils.infoLog(LOG_TAG, response.toString());

                        Dialog dialog = new AlertDialog.Builder(SplashActivity.this)
                                .setTitle(R.string.dialog_title_app_update)
                                .setCancelable(false)
                                .setMessage(R.string.dialog_msg_new_version)
                                .setPositiveButton(R.string.dialog_btn_update,
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                                InstallAPK downloadAndInstall = new InstallAPK();
                                                downloadAndInstall.execute(response.getAppFullAddress(), response.getAppName());
                                            }
                                        })
                                .setNegativeButton(R.string.dialog_btn_no_thanks,
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,
                                                                int whichButton) {
                                                startSplashScreen();
                                            }
                                        }).create();
                        dialog.show();
                    } else {
                        startSplashScreen();
                    }
                }
            } catch (Exception e) {
                LogUtils.errorLog(LOG_TAG, e.getMessage());
                startSplashScreen();
            }
        }
    }

    class GsonRequestErrorListener implements Response.ErrorListener {

        @Override
        public void onErrorResponse(VolleyError error) {
            LogUtils.errorLog(LOG_TAG, error.getMessage(), error);
            startSplashScreen();
        }
    }

    private class InstallAPK extends AsyncTask<String,Void,Void> {

        int status = 0;

        public void onPreExecute() {
            progressDialog = new ProgressDialog(SplashActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setIndeterminate(false);
            progressDialog.setTitle("Loading");
            progressDialog.setMessage("Please wait...");
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String[] params) {
            try {

                URL url = new URL(params[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestProperty("User-Agent", "");
                connection.setRequestMethod("POST");
                connection.setDoInput(true);
                connection.connect();

                long length = connection.getContentLength();
                progressDialog.setMax((int) length);

                InputStream is = connection.getInputStream();
                FileOutputStream fileOutputStream = null;

                if (is != null) {
                    apkName = params[1];
                    if (LogFileUtils.createLogStorageDir()) {
                        File file = new File(LogFileUtils.getLogStorageDir(), apkName);
                        if (file.exists()) {
                            file.delete();
                        }
                        LogUtils.debugLog(LOG_TAG, "Downloading apk: " + file.getAbsolutePath());

                        fileOutputStream = new FileOutputStream(file);
                        byte[] buf = new byte[1024];
                        int ch = -1;
                        int count = 0;
                        while ((ch = is.read(buf)) != -1) {
                            fileOutputStream.write(buf, 0, ch);
                            count += ch;
                            if (length > 0) {
                                progressDialog.setProgress(count);
                            }
                        }
                        fileOutputStream.flush();
                        fileOutputStream.close();
                    }
                    is.close();
                    connection.disconnect();
                }
            } catch (Exception e) {
                status = 1;
                LogUtils.errorLog(LOG_TAG, e.getMessage(), e);
            }
            return null;
        }

        public void onPostExecute(Void unused) {
            progressDialog.dismiss();
            if (status == 1) {
                LogUtils.errorLog(LOG_TAG, getString(R.string.msg_fail_download));
                CommonUtils.toast(SplashActivity.this, getString(R.string.msg_fail_download));
                startSplashScreen();
            } else {
                LogUtils.debugLog(LOG_TAG, getString(R.string.msg_success_download));
                executeHandler(MESSAGE_INSTALL_APP);
            }

        }
    }

    private void executeHandler(int what) {

        Message message = mHandler.obtainMessage();
        message.what = what;
        message.sendToTarget();
    }

    Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message message) {
            if (message.what == MESSAGE_INSTALL_APP) {
                new Thread(new Runnable() {
                    public void run() {
                        Looper.prepare();
                        install();
                        Looper.loop();
                    }
                }).start();
            }
        }
    };

    private void install() {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//        Uri uri = android.net.Uri.fromFile(new File(LogFileUtils.getLogStorageDir(), apkName));
            File apk = new File(LogFileUtils.getLogStorageDir(), apkName);
            LogUtils.debugLog(LOG_TAG, "Installing akp: " + apk.getAbsolutePath());
//        FileProvider.getUriForFile(this, LogFileUtils.getLogStorageDir(), apk);
//        Uri uri = FileProvider.getUriForFile(this, this.getPackageName(), apk);
            Uri uri = Uri.fromFile(apk);
            if (Build.VERSION.SDK_INT >= 24) {
                uri = FileProvider.getUriForFile(this, "com.netcorpgps.v3.dispatch.application.provider", apk);
            }
            intent.setDataAndType(uri, "application/vnd.android.package-archive");
            startActivity(intent);
            finish();
        } catch (Exception e) {
            CommonUtils.toast(SplashActivity.this, getString(R.string.msg_fail_install));
            LogUtils.errorLog(LOG_TAG, getString(R.string.msg_fail_install), e);
            startSplashScreen();
        }
    }

    private boolean checkAllPermissions() {

        String[] permissions = PermissionUtils.checkAllPermissions(this);
        if (permissions != null && permissions.length > 0) {
            ActivityCompat.requestPermissions(this, permissions, PermissionUtils.PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PermissionUtils.PERMISSIONS:
                if (grantResults.length > 0) {
                    boolean isAllGranted = true;
                    for (int result : grantResults) {
                        if (result != PackageManager.PERMISSION_GRANTED) {
                            isAllGranted = false;
                        }
                    }
                    if (isAllGranted) {
                        LogFileUtils.processLogcat();
                        checkAppVersion();
                    } else {
                        finish();
                    }
                } else {
                    finish();
                }
                break;
            default:
                break;
        }
    }
}
