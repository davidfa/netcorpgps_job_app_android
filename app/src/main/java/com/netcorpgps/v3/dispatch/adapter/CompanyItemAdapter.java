package com.netcorpgps.v3.dispatch.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.api.bean.AdminBean;
import com.netcorpgps.v3.dispatch.fragment.CompanyListFragment.OnCompanyListListener;
import com.netcorpgps.v3.dispatch.utils.CommonUtils;

import java.util.List;

public class CompanyItemAdapter extends RecyclerView.Adapter<CompanyItemAdapter.ViewHolder> {

    private final List<AdminBean.Company> mValues;
    private final OnCompanyListListener mListener;
    private final String mCompanyName;

    public CompanyItemAdapter(List<AdminBean.Company> items, OnCompanyListListener listener, String companyName) {
        mValues = items;
        mListener = listener;
        mCompanyName = companyName;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_company_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mContentView.setText(mValues.get(position).getName());

        // change background color if company is assigned by this device
        if (!CommonUtils.isEmpty(mCompanyName) && mCompanyName.equals(mValues.get(position).getName())) {
            holder.mView.setBackgroundResource(R.color.layout_pressed_bg);
        }

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item_swipe has been selected.
                    mListener.onCompanyListInteraction(holder.getmItem());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final View mView;
        private final TextView mContentView;
        private AdminBean.Company mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mContentView = (TextView) view.findViewById(R.id.txt_company_name);
        }

        public AdminBean.Company getmItem() {
            return mItem;
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
