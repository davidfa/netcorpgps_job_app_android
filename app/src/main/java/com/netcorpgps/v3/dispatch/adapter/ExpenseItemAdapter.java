package com.netcorpgps.v3.dispatch.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.db.bean.Expense;
import com.netcorpgps.v3.dispatch.utils.CommonUtils;

import java.util.List;

/**
 * Created by David Fa on 2/06/2017.
 */

public class ExpenseItemAdapter extends RecyclerView.Adapter<ExpenseItemAdapter.ViewHolder> {

    private final List<Expense> mValues;
    private final OnListListener mListener;
    private final Context mContext;

    public ExpenseItemAdapter(List<Expense> items, OnListListener listener, Context context) {
        mValues = items;
        mListener = listener;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.job_detail_expense_item, parent, false);
        return new ExpenseItemAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.txtExpense.setText(mValues.get(position).getExpense());
        String expenseName = mValues.get(position).getExpenseName();
        holder.txtExpenseName.setText(expenseName);
        if (!CommonUtils.isEmpty(expenseName) && expenseName.equals("Total")) {
            holder.txtExpense.setTypeface(null, Typeface.BOLD);
            holder.txtExpenseName.setTypeface(null, Typeface.BOLD);
        } else {
            holder.txtExpense.setTypeface(null, Typeface.NORMAL);
            holder.txtExpenseName.setTypeface(null, Typeface.NORMAL);
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView txtExpense;
        public final TextView txtExpenseName;
        public Expense mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            txtExpense = (TextView) view.findViewById(R.id.txt_expense);
            txtExpenseName = (TextView) view.findViewById(R.id.txt_expense_name);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + txtExpenseName.getText() + "'";
        }
    }

    public interface OnListListener {
        // TODO: Update argument type and name
        void onListInteraction(Expense item);
    }
}
