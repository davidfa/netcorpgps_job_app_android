package com.netcorpgps.v3.dispatch.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.api.bean.JobBean;
import com.netcorpgps.v3.dispatch.db.bean.Jobs;
import com.netcorpgps.v3.dispatch.utils.CommonUtils;


import java.util.List;


public class JobItemAdapter extends RecyclerView.Adapter<JobItemAdapter.ViewHolder> {

    private final List<Jobs> mValues;
    private final OnJobListListener mListener;
    private final Context mContext;

    public JobItemAdapter(List<Jobs> items, OnJobListListener listener, Context context) {
        mValues = items;
        mListener = listener;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.fragment_job_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mCustomer.setText(mValues.get(position).getCustomer());
        holder.mJobCode.setText(mValues.get(position).getJobCode());
        holder.mScheduledTime.setText(CommonUtils.getFormatDatetime(mValues.get(position).getScheduledTime()));
//        holder.mScheduledTime.setTextColor(CommonUtils.getScheduledColor(mContext, mValues.get(position).getScheduledTime()));
        int statusColor = (CommonUtils.getJobStatusColor(mContext, mValues.get(position).getJobStatus()));
        holder.mStatusLabel.setText(mContext.getText(R.string.txt_status_label));
        holder.mStatusLabel.setTextColor(statusColor);
        holder.mStatus.setText(mValues.get(position).getJobStatus());
        holder.mStatus.setTextColor(statusColor);
//        holder.mDescription.setText(mValues.get(position).getDescription());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item_swipe has been selected.
                    mListener.onJobListInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mCustomer;
        public final TextView mJobCode;
        public final TextView mScheduledTime;
        public final TextView mStatusLabel;
        public final TextView mStatus;
//        public final TextView mDescription;
        public Jobs mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mCustomer = (TextView) view.findViewById(R.id.job_list_customer);
            mJobCode = (TextView) view.findViewById(R.id.job_list_code);
            mScheduledTime = (TextView) view.findViewById(R.id.job_list_scheduled);
            mStatusLabel = (TextView) view.findViewById(R.id.job_list_status_label);
            mStatus = (TextView) view.findViewById(R.id.job_list_status);
//            mDescription = (TextView) view.findViewById(R.id.job_list_description);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mJobCode.getText() + "'";
        }
    }

    public interface OnJobListListener {
        // TODO: Update argument type and name
        void onJobListInteraction(Jobs item);
    }
}
