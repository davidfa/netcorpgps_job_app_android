package com.netcorpgps.v3.dispatch.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.component.layout.SwipeLayout;
import com.netcorpgps.v3.dispatch.db.bean.Jobs;
import com.netcorpgps.v3.dispatch.utils.CommonUtils;

import java.util.List;


/**
 * Created by David Fa on 5/06/2017.
 */

public class JobMenuItemAdapter extends RecyclerView.Adapter<JobMenuItemAdapter.ViewHolder> {
    private Context mContext;
    private List<Jobs> mValues;
    // previous swiped layout
    private SwipeLayout preLayout;
    private OnItemClickListener onItemClickListener;

    public SwipeLayout getPreLayout() {
        return preLayout;
    }

    public JobMenuItemAdapter(Context context, List<Jobs> items, OnItemClickListener listener) {
        this.mContext = context;
        this.mValues = items;
        this.onItemClickListener = listener;
    }



    public interface OnItemClickListener {

        void onCompleted(Jobs item, int position);

        void onAccepted(Jobs item, int position);

        void onStarted(Jobs item, int position);

        void onRejected(Jobs item, int position);

        void onItemClick(Jobs item);

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.fragment_job_menu_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);
        holder.mCustomer.setText(mValues.get(position).getCustomer());
        holder.mJobCode.setText(mValues.get(position).getJobCode());
        holder.mScheduledTime.setText(CommonUtils.getFormatDatetime(mValues.get(position).getScheduledTime()));
        holder.mScheduledTime.setTextColor(CommonUtils.getScheduledColor(mContext, mValues.get(position).getScheduledTime()));
        String jobStatus = mValues.get(position).getJobStatus();
        int statusColor = (CommonUtils.getJobStatusColor(mContext, jobStatus));
        holder.mStatusLabel.setText(mContext.getText(R.string.txt_status_label));
        holder.mStatusLabel.setTextColor(statusColor);
        holder.mStatus.setText(jobStatus);
        holder.mStatus.setTextColor(statusColor);
        holder.swipelayout.setOnSwipeChangeListener(new SwipeLayout.OnSwipeChangeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {
                preLayout = layout;
            }

            @Override
            public void onClose(SwipeLayout layout) {
            }

            @Override
            public void onSwiping(SwipeLayout layout) {
            }

            @Override
            public void onStartOpen(SwipeLayout layout) {
                if (preLayout != null) {
                    preLayout.close();
                }
            }

            @Override
            public void onStartClose(SwipeLayout layout) {
            }
        });

        if (jobStatus.equalsIgnoreCase(mContext.getString(R.string.job_status_paged))) {
            holder.mPagedNum.setText(String.format(mContext.getString(R.string.txt_paged_vehicle_num), mValues.get(position).getPagedNumber()));
            holder.completed.setVisibility(View.GONE);
            holder.started.setVisibility(View.GONE);
        } else if (jobStatus.equalsIgnoreCase(mContext.getString(R.string.job_status_assigned))) {
            holder.completed.setVisibility(View.GONE);
            holder.accepted.setVisibility(View.GONE);
            holder.rejected.setVisibility(View.GONE);
            if (position > 0) {
                holder.started.setVisibility(View.GONE);
            }
        } else if (jobStatus.equalsIgnoreCase(mContext.getString(R.string.job_status_in_progress)) || jobStatus.equalsIgnoreCase(mContext.getString(R.string.job_status_waiting)) || jobStatus.equalsIgnoreCase(mContext.getString(R.string.job_status_loading))) {
            holder.accepted.setVisibility(View.GONE);
            holder.started.setVisibility(View.GONE);
            holder.rejected.setVisibility(View.GONE);
            holder.completed.setVisibility(View.GONE);
        }

        holder.completed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onItemClickListener != null) {
                    onItemClickListener.onCompleted(holder.mItem, position);
                }
            }
        });
        holder.accepted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onItemClickListener != null) {
                    onItemClickListener.onAccepted(holder.mItem, position);
                }
            }
        });
        holder.started.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onItemClickListener != null) {
                    onItemClickListener.onStarted(holder.mItem, position);
                }
            }
        });
        holder.rejected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onItemClickListener != null) {
                    onItemClickListener.onRejected(holder.mItem, position);
                }
            }
        });
        holder.layoutFront.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(holder.mItem);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void deleteItem(int position) {
        mValues.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mValues.size());
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView completed;
        TextView accepted;
        TextView started;
        TextView rejected;
        LinearLayout layoutBack;
        TextView mCustomer;
        TextView mJobCode;
        TextView mScheduledTime;
        TextView mStatusLabel;
        TextView mStatus;
        TextView mPagedNum;
        LinearLayout layoutFront;
        SwipeLayout swipelayout;
        Jobs mItem;
        View mView;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            completed = (TextView) view.findViewById(R.id.completed);
            accepted = (TextView) view.findViewById(R.id.accepted);
            started = (TextView) view.findViewById(R.id.started);
            rejected = (TextView) view.findViewById(R.id.rejected);
            layoutBack = (LinearLayout) view.findViewById(R.id.layout_back);
            layoutFront = (LinearLayout) view.findViewById(R.id.layout_front);
            swipelayout = (SwipeLayout) view.findViewById(R.id.swipelayout);
            mCustomer = (TextView) view.findViewById(R.id.job_list_customer);
            mJobCode = (TextView) view.findViewById(R.id.job_list_code);
            mScheduledTime = (TextView) view.findViewById(R.id.job_list_scheduled);
            mStatusLabel = (TextView) view.findViewById(R.id.job_list_status_label);
            mStatus = (TextView) view.findViewById(R.id.job_list_status);
            mPagedNum = (TextView) view.findViewById(R.id.job_list_paged_number);
        }
    }

}
