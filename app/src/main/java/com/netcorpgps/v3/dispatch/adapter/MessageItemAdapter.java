package com.netcorpgps.v3.dispatch.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.db.bean.Message;
import com.netcorpgps.v3.dispatch.utils.CommonUtils;

import java.io.File;
import java.util.List;

/**
 * Created by David Fa on 17/08/2017.
 */

public class MessageItemAdapter extends RecyclerView.Adapter<MessageItemAdapter.ViewHolder> {

    private final List mValues;
    private final OnMessageListener mListener;
    private final Context mContext;

    private final int viewType_receive = 0;
    private final int viewType_text = 1;
    private final int viewType_audio = 2;
    private final int viewType_picture = 3;

    private LruCache<String, Bitmap> mMemoryCache;

    public MessageItemAdapter(List items, OnMessageListener listener, Context context) {
        mValues = items;
        mListener = listener;
        mContext = context;

        // Get max available VM memory, exceeding this amount will throw an
        // OutOfMemory exception. Stored in kilobytes as LruCache takes an
        // int in its constructor.
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

        // Use 1/8th of the available memory for this memory cache.
        final int cacheSize = maxMemory / 8;

        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return bitmap.getByteCount() / 1024;
            }
        };
    }

    @Override
    public int getItemViewType(int position) {
        int viewType = viewType_text;
        Message msg = (Message) mValues.get(position);
        if (mContext.getString(R.string.message_role_mobile).equalsIgnoreCase(msg.getRole())) {

            if (msg.getType().equalsIgnoreCase(mContext.getString(R.string.message_content_type_audio))) {
                viewType = viewType_audio;
            } else if (msg.getType().equalsIgnoreCase(mContext.getString(R.string.message_content_type_picture))) {
                viewType = viewType_picture;
            } else {
                viewType = viewType_text;
            }
        } else if (mContext.getString(R.string.message_role_admin).equalsIgnoreCase(msg.getRole())) {
            viewType = viewType_receive;
        }
        return viewType;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        switch (viewType) {
            case viewType_receive:
                view = LayoutInflater.from(mContext).inflate(R.layout.chatting_item_msg_text_right, parent, false);
                break;
            case viewType_text:
                view = LayoutInflater.from(mContext).inflate(R.layout.chatting_item_msg_text_left, parent, false);
                break;
            case viewType_audio:
                view = LayoutInflater.from(mContext).inflate(R.layout.chatting_item_msg_voice_left, parent, false);
                break;
            case viewType_picture:
                view = LayoutInflater.from(mContext).inflate(R.layout.chatting_item_msg_picture_left, parent, false);
                break;
        }

        return new MessageItemAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final Message msg = (Message) mValues.get(position);

        if (holder.tvSendTime != null)
            holder.tvSendTime.setText(msg.getTime());
        if (holder.tvUserName != null)
            holder.tvUserName.setText(msg.getSender());

        // audio message
        if (msg.getType().equalsIgnoreCase(mContext.getString(R.string.message_content_type_audio))) {
            if (holder.ivVoice != null) {
                holder.ivVoice.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (null != mListener) {
                            mListener.OnMessageListInteraction(msg);
                        }
                    }
                });
            }

            if (holder.tvLength != null) {
                String time = msg.getVoiceTime();
                if (!CommonUtils.isEmpty(time)) {
                    holder.tvLength.setText(time + "\"");

                    int width = Math.min((int) (240 * (((double) Math.max(Integer.valueOf(time), 1) * 0.1) + 1)), 540);
                    holder.ivVoice.getLayoutParams().width = width;
                }
            }
            String isSynced = msg.getSynced();
            if (!CommonUtils.isEmpty(isSynced)) {
                holder.pbSending.setVisibility(View.GONE);
                if (isSynced.equalsIgnoreCase("true")) {
                    holder.ivStatus.setVisibility(View.GONE);
                } else {
                    holder.ivStatus.setVisibility(View.VISIBLE);
                    holder.ivStatus.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mListener.ResendMsgFileInteraction(msg, position);
                        }
                    });
                }
            } else {
                holder.pbSending.setVisibility(View.VISIBLE);
                holder.ivStatus.setVisibility(View.GONE);
            }

            // picture message
        } else if (msg.getType().equalsIgnoreCase(mContext.getString(R.string.message_content_type_picture))) {
            if (holder.ivPicture != null) {
                holder.ivPicture.setImageResource(R.drawable.default_pic);
                final String imageKey = msg.getFileUrl();
                final Bitmap bitmap = getBitmapFromMemCache(imageKey);
                if (bitmap != null) {
                    holder.ivPicture.setImageBitmap(bitmap);
                } else {
                    // using LruCache to archiving bitmap, it will call back when bitmap has been generated
                    BitmapWorkerTask task = new BitmapWorkerTask(holder.ivPicture);
                    task.execute(imageKey);
                }

                holder.ivPicture.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (null != mListener) {
                            mListener.OnClickPictureInteraction(msg);
                        }
                    }
                });
            }

            String isSynced = msg.getSynced();
            if (!CommonUtils.isEmpty(isSynced)) {
                holder.pbSending.setVisibility(View.GONE);
                if (isSynced.equalsIgnoreCase("true")) {
                    holder.ivStatus.setVisibility(View.GONE);
                } else {
                    holder.ivStatus.setVisibility(View.VISIBLE);
                    holder.ivStatus.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mListener.ResendMsgFileInteraction(msg, position);
                        }
                    });
                }
            } else {
                holder.pbSending.setVisibility(View.VISIBLE);
                holder.ivStatus.setVisibility(View.GONE);
            }


        } else {
            if (holder.tvContent != null)
                holder.tvContent.setText(msg.getMsg());
        }

        if (position > 0) {
            Message premsg = (Message) mValues.get(position - 1);
            String preTime = premsg.getTime();
            String thisTime = msg.getTime();
            if (Math.abs(CommonUtils.getDateDiffInMinsReturnLong(preTime, thisTime)) <= 5) {
                holder.tvSendTime.setVisibility(View.GONE);
            }
        } else {
            holder.tvSendTime.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public TextView tvSendTime;
        public TextView tvUserName;
        public TextView tvContent;
        public TextView tvLength;
        public ImageView ivVoice;
        public ImageView ivPicture;
        public ImageView ivStatus;
        public ProgressBar pbSending;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            tvSendTime = (TextView) view.findViewById(R.id.tv_sendtime);
            tvUserName = (TextView) view.findViewById(R.id.tv_username);
            tvContent = (TextView) view.findViewById(R.id.tv_chatcontent);

            // voice layout
            ivVoice = (ImageView) view.findViewById(R.id.iv_voice);
            tvLength = (TextView) view.findViewById(R.id.tv_length);
            pbSending = (ProgressBar) view.findViewById(R.id.pb_sending);
            ivStatus = (ImageView) view.findViewById(R.id.msg_status);

            // picture layout
            ivPicture = (ImageView) view.findViewById(R.id.iv_picture);
        }

    }

    public interface OnMessageListener {
        void OnMessageListInteraction(Message item);

        void OnClickPictureInteraction(Message item);

        void ResendMsgFileInteraction(Message item, int index);
    }

    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            mMemoryCache.put(key, bitmap);
        }
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }

    class BitmapWorkerTask extends AsyncTask<String, Void, Bitmap> {

        private ImageView mImageView;

        public BitmapWorkerTask(ImageView imageView) {
            this.mImageView = imageView;

        }

        // Decode image in background.
        @Override
        protected Bitmap doInBackground(String... params) {
            String path = params[0];
            File imgFile = new File(path);
            Bitmap bitmap;
            if (imgFile.exists()) {
                bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            } else {
                bitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.default_pic);
            }
            addBitmapToMemoryCache(path, bitmap);
            return bitmap;
        }

        protected void onPostExecute(Bitmap bitmap) {
            if (bitmap != null)
                mImageView.setImageBitmap(bitmap);
        }
    }

}
