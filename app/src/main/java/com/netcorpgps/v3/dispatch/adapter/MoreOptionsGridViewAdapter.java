package com.netcorpgps.v3.dispatch.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.netcorpgps.v3.dispatch.R;

import java.util.List;

/**
 * Created by David Fa on 5/09/2017.
 */

public class MoreOptionsGridViewAdapter extends BaseAdapter {
    private final Context mContext;
    private final List<View> mItmes;
    private final OnResponseListener mListener;

    // 1
    public MoreOptionsGridViewAdapter(Context context, List<View> itmes, OnResponseListener listener) {
        this.mContext = context;
        this.mItmes = itmes;
        this.mListener = listener;
    }

    @Override
    public int getCount() {
        return mItmes.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        return mItmes.get(position);
    }

    public interface OnResponseListener {
        void onResponse(String message);
    }
}
