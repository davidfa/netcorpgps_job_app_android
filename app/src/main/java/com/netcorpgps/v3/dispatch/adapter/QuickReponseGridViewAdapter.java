package com.netcorpgps.v3.dispatch.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.netcorpgps.v3.dispatch.R;

/**
 * Created by David Fa on 5/09/2017.
 */

public class QuickReponseGridViewAdapter extends BaseAdapter {
    private final Context mContext;
    private final String[] reponses;
    private final OnResponseListener mListener;

    // 1
    public QuickReponseGridViewAdapter(Context context, String[] reponses, OnResponseListener listener) {
        this.mContext = context;
        this.reponses = reponses;
        this.mListener = listener;
    }

    @Override
    public int getCount() {
        return reponses.length;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final TextView textView = new TextView(mContext);
        textView.setText(reponses[position]);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimension(R.dimen.text_height_sm));
        textView.setTextColor(ContextCompat.getColor(mContext, R.color.black));
        textView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onResponse(textView.getText().toString());
                }
            }
        });
        return textView;
    }

    public interface OnResponseListener {
        void onResponse(String message);
    }
}
