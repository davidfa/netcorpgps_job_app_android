package com.netcorpgps.v3.dispatch.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.component.layout.SwipeLayout;

import java.util.List;


/**
 * Created by David Fa on 5/06/2017.
 */

public class SettingChatMessageItemAdapter extends RecyclerView.Adapter<SettingChatMessageItemAdapter.ViewHolder> {
    private Context mContext;
    private SwipeLayout preLayout;
    private OnItemClickListener onItemClickListener;
    private List mValues;

    public SwipeLayout getPreLayout() {
        return preLayout;
    }

    public SettingChatMessageItemAdapter(Context context, List list, OnItemClickListener listener) {
        this.mContext = context;
        this.mValues = list;
        this.onItemClickListener = listener;
    }



    public interface OnItemClickListener {

        void onEdit(int position);

        void onDelete(int position);

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.setting_msg_menu_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        String item = (String) mValues.get(position);
        holder.txtMsg.setText(item);

        holder.swipelayout.setOnSwipeChangeListener(new SwipeLayout.OnSwipeChangeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {
                preLayout = layout;
            }

            @Override
            public void onClose(SwipeLayout layout) {
            }

            @Override
            public void onSwiping(SwipeLayout layout) {
            }

            @Override
            public void onStartOpen(SwipeLayout layout) {
                if (preLayout != null) {
                    preLayout.close();
                }
            }

            @Override
            public void onStartClose(SwipeLayout layout) {
            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onItemClickListener != null) {
                    onItemClickListener.onDelete(position);
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return mValues.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView edit;
        TextView delete;
        LinearLayout layoutBack;
        TextView txtMsg;
        LinearLayout layoutFront;
        SwipeLayout swipelayout;
        View mView;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            edit = (TextView) view.findViewById(R.id.edit);
            delete = (TextView) view.findViewById(R.id.delete);
            layoutBack = (LinearLayout) view.findViewById(R.id.layout_back);
            layoutFront = (LinearLayout) view.findViewById(R.id.layout_front);
            swipelayout = (SwipeLayout) view.findViewById(R.id.swipelayout);
            txtMsg = (TextView) view.findViewById(R.id.txtMsg);
        }
    }

}
