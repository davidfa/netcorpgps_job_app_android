package com.netcorpgps.v3.dispatch.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.activity.SlideInActivity;
import com.netcorpgps.v3.dispatch.application.NetcorpGPSApplication;
import com.netcorpgps.v3.dispatch.utils.LogUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by David Fa on 12/09/2017.
 */

public class SettingItemAdapter extends RecyclerView.Adapter<SettingItemAdapter.ViewHolder> {

    private final String LOG_TAG = SettingItemAdapter.class.getSimpleName();
    private final Context mContext;
    private final List<Setting> items;
    private final int ITEM_TYPE_Chat = 0;
    private final int ITEM_TYPE_Sync = 1;
    private final int ITEM_TYPE_CompletedJob = 2;

    public SettingItemAdapter(Context context) {
        mContext = context;
        items = new ArrayList();
        try {
//        Setting sMassage = new Setting(ITEM_TYPE_Chat, mContext.getString(R.string.setting_item_title_chat), mContext.getString(R.string.setting_item_content_chat), null);
            Setting sMassage = new Setting();
            sMassage.setType(ITEM_TYPE_Chat);
            sMassage.setTitle(mContext.getString(R.string.setting_item_title_chat));
            sMassage.setContent(mContext.getString(R.string.setting_item_content_chat));
//            items.add(sMassage);

//        Setting sCompletedJobNumber = new Setting(ITEM_TYPE_CompletedJob, context.getString(R.string.setting_item_title_completedJob), context.getString(R.string.setting_item_content_completedJob), mContext.getResources().getIntArray(R.array.setting_completed_job_number)[NetcorpGPSApplication.sharedPrefManager.getInt(R.string.shared_pref_completed_job_arr_index)]);
            Setting sCompletedJobNumber = new Setting();
            sCompletedJobNumber.setType(ITEM_TYPE_CompletedJob);
            sCompletedJobNumber.setTitle(mContext.getString(R.string.setting_item_title_completedJob));
            sCompletedJobNumber.setContent(mContext.getString(R.string.setting_item_content_completedJob));
            sCompletedJobNumber.setValue(String.valueOf(NetcorpGPSApplication.sharedPrefManager.getInt(R.string.shared_pref_completed_job_number, 30)));
            items.add(sCompletedJobNumber);

            //        Setting sSyncIntervalTime = new Setting(ITEM_TYPE_Sync, mContext.getString(R.string.setting_item_title_sync), mContext.getString(R.string.setting_item_content_sync), mContext.getResources().getStringArray(R.array.setting_sync_time)[NetcorpGPSApplication.sharedPrefManager.getInt(R.string.shared_pref_sync_interval_time_index)]);
            Setting sSyncIntervalTime = new Setting();
            sSyncIntervalTime.setType(ITEM_TYPE_Sync);
            sSyncIntervalTime.setTitle(mContext.getString(R.string.setting_item_title_sync));
            sSyncIntervalTime.setContent(mContext.getString(R.string.setting_item_content_sync));
            int size = mContext.getResources().getStringArray(R.array.setting_sync_time).length - 1;
            int index = Math.min(size, NetcorpGPSApplication.sharedPrefManager.getInt(R.string.shared_pref_sync_interval_time_index));
            String value = mContext.getResources().getStringArray(R.array.setting_sync_time)[index];
            sSyncIntervalTime.setValue(value);
            items.add(sSyncIntervalTime);
        } catch (Exception e) {

            LogUtils.errorLog(LOG_TAG, e.getMessage(), e);
        }

    }

    @Override
    public SettingItemAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.setting_item_text, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SettingItemAdapter.ViewHolder holder, int position) {
        Setting setting = items.get(position);
        final int type = setting.getType();
        holder.txtTitle.setText(setting.getTitle());
        holder.txtContent.setText(setting.getContent());
        holder.txtVal.setText(setting.getValue());
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type == ITEM_TYPE_Sync) {
                    String title = mContext.getString(R.string.setting_dialog_title_sync);
                    String posButton = mContext.getString(R.string.dialog_btn_save);
                    String nagButton = mContext.getString(R.string.dialog_btn_cancel);
                    MaterialDialog dialog = new MaterialDialog.Builder(mContext)
                            .title(title)
                            .items(R.array.setting_sync_time)
                            .itemsCallbackSingleChoice(NetcorpGPSApplication.sharedPrefManager.getInt(R.string.shared_pref_sync_interval_time_index), new MaterialDialog.ListCallbackSingleChoice() {
                                @Override
                                public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                    /**
                                     * If you use alwaysCallSingleChoiceCallback(), which is discussed below,
                                     * returning false here won't allow the newly selected radio button to actually be selected.
                                     **/
//                                    LogUtils.debugLog("fd", text.toString());
                                    holder.txtVal.setText(text.toString());
                                    int[] syncIntervalTimeArr = mContext.getResources().getIntArray(R.array.setting_sync_time_int);
                                    int intervalTime = syncIntervalTimeArr[which] * 60 * 1000;
                                    NetcorpGPSApplication.sharedPrefManager.setInt(R.string.shared_pref_sync_interval_time, intervalTime);
                                    NetcorpGPSApplication.sharedPrefManager.setInt(R.string.shared_pref_sync_interval_time_index, which);
                                    return true;
                                }
                            })
                            .positiveText(posButton)
                            .negativeText(nagButton)
                            .build();

                    dialog.getTitleView().setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
                    dialog.show();
                } else if (type == ITEM_TYPE_CompletedJob) {
                    String title = mContext.getString(R.string.setting_dialog_title_completed_job);
                    String posButton = mContext.getString(R.string.dialog_btn_save);
                    String nagButton = mContext.getString(R.string.dialog_btn_cancel);
                    MaterialDialog dialog = new MaterialDialog.Builder(mContext)
                            .title(title)
                            .items(R.array.setting_completed_job_number)
                            .itemsCallbackSingleChoice(NetcorpGPSApplication.sharedPrefManager.getInt(R.string.shared_pref_completed_job_arr_index, 2), new MaterialDialog.ListCallbackSingleChoice() {
                                @Override
                                public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                    /**
                                     * If you use alwaysCallSingleChoiceCallback(), which is discussed below,
                                     * returning false here won't allow the newly selected radio button to actually be selected.
                                     **/
//                                    LogUtils.debugLog("fd", text.toString());
                                    holder.txtVal.setText(text.toString());
                                    int number = mContext.getResources().getIntArray(R.array.setting_completed_job_number_int)[which];
                                    NetcorpGPSApplication.sharedPrefManager.setInt(R.string.shared_pref_completed_job_number, number);
                                    NetcorpGPSApplication.sharedPrefManager.setInt(R.string.shared_pref_completed_job_arr_index, which);
                                    return true;
                                }
                            })
                            .positiveText(posButton)
                            .negativeText(nagButton)
                            .build();

                    dialog.getTitleView().setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
                    dialog.show();
                } else if (type == ITEM_TYPE_Chat) {
                    Intent intent = new Intent(mContext, SlideInActivity.class);
                    intent.putExtra("fragment", "chatMsg");
                    mContext.startActivity(intent);
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final View mView;
        private final TextView txtTitle;
        private final TextView txtContent;
        private final TextView txtVal;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            txtTitle = (TextView) view.findViewById(R.id.txtTitle);
            txtContent = (TextView) view.findViewById(R.id.txtContent);
            txtVal = (TextView) view.findViewById(R.id.txtVal);
        }
    }

    class Setting {
        int type;
        String title;
        String content;
        String value;

        Setting() {

        }

        Setting(int type, String title, String content, String value) {
            setType(type);
            setTitle(title);
            setContent(content);
            setValue(value);
        }

        Setting(int type, String title, String content, int value) {
            setType(type);
            setTitle(title);
            setContent(content);
            setValue(String.valueOf(value));
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
