package com.netcorpgps.v3.dispatch.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.api.bean.VehiclesBean;
import com.netcorpgps.v3.dispatch.fragment.VehicleListFragment;
import com.netcorpgps.v3.dispatch.utils.CommonUtils;
import com.netcorpgps.v3.dispatch.utils.LogUtils;

import java.util.List;

/**
 * Created by David Fa on 15/05/2017.
 */

public class VehicleItemAdapter extends RecyclerView.Adapter<VehicleItemAdapter.ViewHolder> {

    private final static String LOG_TAG = VehicleItemAdapter.class.getSimpleName();
    private final List<VehiclesBean.Vehicle> mValues;
    private final VehicleListFragment.OnVehicleListListener mListener;
    private final String mVehicleName;
//    private final RecyclerView mRecyclerView;

    public VehicleItemAdapter(List<VehiclesBean.Vehicle> items, VehicleListFragment.OnVehicleListListener listener, String vehicleName, RecyclerView recyclerView) {
        mValues = items;
        mListener = listener;
        mVehicleName = vehicleName;
//        mRecyclerView = recyclerView;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_vehicle_item, parent, false);
        return new VehicleItemAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mVehicleName.setText(mValues.get(position).getName());
        holder.mVehicleStatus.setText(mValues.get(position).getStatus());
        try {
            holder.mVehicleIcon.setImageResource(CommonUtils.getResId(mValues.get(position).getIcon()));
            holder.mView.setBackgroundResource(0);
            // change background color if vehicle is assigned by this device
//        String vehicle_id = CommonUtils.isEmpty(mVehicleID) ? NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle) : mVehicleID;
            if (!CommonUtils.isEmpty(mVehicleName) && mVehicleName.equals(mValues.get(position).getName())) {
                holder.mView.setBackgroundResource(R.color.layout_pressed_bg);
            }
        } catch (Exception e) {
            LogUtils.errorLog(LOG_TAG, e.getMessage(), e);
        }


        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onVehicleListInteraction(holder.getmItem());
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final View mView;
        private final TextView mVehicleName;
        private final TextView mVehicleStatus;
        private final ImageView mVehicleIcon;
        private VehiclesBean.Vehicle mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mVehicleName = (TextView) view.findViewById(R.id.txt_vehicle_name);
            mVehicleStatus = (TextView) view.findViewById(R.id.txt_vehicle_status);
            mVehicleIcon = (ImageView) view.findViewById(R.id.iv_vehicle_icon);
        }

        public VehiclesBean.Vehicle getmItem() {
            return mItem;
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mVehicleName.getText() + "'";
        }
    }
}
