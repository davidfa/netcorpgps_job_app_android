package com.netcorpgps.v3.dispatch.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.db.bean.Waypoint;

import java.util.List;

/**
 * Created by David Fa on 1/06/2017.
 */

public class WaypointItemAdapter extends RecyclerView.Adapter<WaypointItemAdapter.ViewHolder> {

    private final List<Waypoint> mValues;
    private final OnListListener mListener;
    private final Context mContext;

    public WaypointItemAdapter(List<Waypoint> items, OnListListener listener, Context context) {
        mValues = items;
        mListener = listener;
        mContext = context;
    }

    @Override
    public WaypointItemAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.job_detail_waypoint_item, parent, false);
        return new WaypointItemAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        int base = mValues.get(position).getBase();
        if (base == 1) {
            holder.imgIcon.setImageResource(R.drawable.waypoint);
        } else {
            holder.imgIcon.setImageResource(R.drawable.home_base);
        }
        holder.txtAddress.setText(mValues.get(position).getAddress());

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView imgIcon;
        public final TextView txtAddress;
        public Waypoint mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            imgIcon = (ImageView) view.findViewById(R.id.img_icon);
            txtAddress = (TextView) view.findViewById(R.id.txt_address);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + txtAddress.getText() + "'";
        }
    }

    public interface OnListListener {
        // TODO: Update argument type and name
        void onListInteraction(Waypoint item);
    }
}
