package com.netcorpgps.v3.dispatch.api.ErrorHandler;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.netcorpgps.v3.dispatch.R;

/**
 * Created by David Fa on 8/06/2017.
 */

public class VolleyErrorHandler {
    public static String handleError(Context context, VolleyError error) {
        String message = "";
        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
            message = context.getString(R.string.msg_error_timeout);
        } else if (error instanceof AuthFailureError) {
            message = context.getString(R.string.msg_error_authfailure);
        } else if (error instanceof ServerError) {
            message = context.getString(R.string.msg_error_server_error);
        } else if (error instanceof NetworkError) {
            message = context.getString(R.string.msg_error_no_network_available);
        } else if (error instanceof ParseError) {
            message = context.getString(R.string.msg_error_parse_error);
        } else {
            message = error.getMessage();
        }
        return message;
    }
}
