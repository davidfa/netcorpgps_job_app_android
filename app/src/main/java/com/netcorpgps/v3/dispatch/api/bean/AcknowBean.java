package com.netcorpgps.v3.dispatch.api.bean;

import java.io.Serializable;

/**
 * Created by David Fa on 23/05/2017.
 */

public class AcknowBean implements Serializable {
    private int id;
    private String itme;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getItme() {
        return itme;
    }

    public void setItme(String itme) {
        this.itme = itme;
    }
}
