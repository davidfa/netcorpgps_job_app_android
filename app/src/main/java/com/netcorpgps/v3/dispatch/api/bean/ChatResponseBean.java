package com.netcorpgps.v3.dispatch.api.bean;

/**
 * Created by David Fa on 20/09/2017.
 */

public class ChatResponseBean {
    private int id;
    private String itme;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getItme() {
        return itme;
    }

    public void setItme(String itme) {
        this.itme = itme;
    }
}
