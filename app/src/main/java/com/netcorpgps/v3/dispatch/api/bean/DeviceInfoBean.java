package com.netcorpgps.v3.dispatch.api.bean;


import java.io.Serializable;

/**
 * Created by David Fa on 10/05/2017.
 */

public class DeviceInfoBean implements Serializable {

    private String deviceName;
    private String manufacturer;
    private String model;
    private String version;
    private String os;

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String toString() {
        return "Device Name: " + this.getDeviceName()
                + "\nMANUFACTURER: " + this.getManufacturer()
                + "\nMODEL: " + this.getModel()
                + "\nVersion: " + this.getOs() + " " + this.getVersion();
    }
}
