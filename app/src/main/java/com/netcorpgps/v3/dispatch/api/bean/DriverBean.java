package com.netcorpgps.v3.dispatch.api.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by David Fa on 23/05/2017.
 */

public class DriverBean implements Serializable {

    private String id;
    private String driverRecordID;
    private String token;
    private String fname;
    private String lname;
    private String driverStatus;
    private Integer status;
    private String msg;
    private String chatMsgRetention;
    private List<AcknowBean> acknowList;
    private List<ChatResponseBean> chatResponse;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDriverRecordID() {
        return driverRecordID;
    }

    public void setDriverRecordID(String driverRecordID) {
        this.driverRecordID = driverRecordID;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<AcknowBean> getAcknowList() {
        return acknowList;
    }

    public void setAcknowList(List<AcknowBean> acknowList) {
        this.acknowList = acknowList;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getDriverStatus() {
        return driverStatus;
    }

    public void setDriverStatus(String driverStatus) {
        this.driverStatus = driverStatus;
    }

    public String getChatMsgRetention() {
        return chatMsgRetention;
    }

    public void setChatMsgRetention(String chatMsgRetention) {
        this.chatMsgRetention = chatMsgRetention;
    }

    public List<ChatResponseBean> getChatResponse() {
        return chatResponse;
    }

    public void setChatResponse(List<ChatResponseBean> chatResponse) {
        this.chatResponse = chatResponse;
    }

    public Set<String> getChatResponseSet() {
        if (getChatResponse() == null) {
            return new HashSet<String>();
        } else {
            List<String> list = new ArrayList<String>();
            for (ChatResponseBean object : getChatResponse()) {
                list.add(object.getItme());
            }
            Set<String> set = new HashSet<String>(list);
            return set;
        }

    }
}
