package com.netcorpgps.v3.dispatch.api.bean;

import com.netcorpgps.v3.dispatch.db.bean.Expense;
import com.netcorpgps.v3.dispatch.db.bean.Jobs;
import com.netcorpgps.v3.dispatch.db.bean.Waypoint;

import java.util.List;

/**
 * Created by David Fa on 25/05/2017.
 */

public class JobBean {

    private Jobs job;
    private List<Expense> expenses;
    private List<Waypoint> waypoints;
    private Integer status;
    private String msg;

    public Jobs getJob() {
        return job;
    }

    public void setJob(Jobs job) {
        this.job = job;
    }

    public List<Expense> getExpenses() {
        return expenses;
    }

    public void setExpenses(List<Expense> expenses) {
        this.expenses = expenses;
    }

    public List<Waypoint> getWaypoints() {
        return waypoints;
    }

    public void setWaypoints(List<Waypoint> waypoints) {
        this.waypoints = waypoints;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
//    private int jobID;
//    private String jobCode;
//    private String customer;
//    private String contactName;
//    private String contactNumber;
//    private String scheduledTime;
//    private String startTime;
//    private String vehicleName;
//    private String vehicleID;
//    private String driverID;
//    private String driverName;
//    private String jobType;
//    private int fromBase;
//    private int toBase;
//    private String description;
//    private String requirement;
//    private int passengers;
//    private String jobStatus;
//    private String colorCode;
//
//    public int getJobID() {
//        return jobID;
//    }
//
//    public void setJobID(int jobID) {
//        this.jobID = jobID;
//    }
//
//    public String getJobCode() {
//        return jobCode;
//    }
//
//    public void setJobCode(String jobCode) {
//        this.jobCode = jobCode;
//    }
//
//    public String getCustomer() {
//        return customer;
//    }
//
//    public void setCustomer(String customer) {
//        this.customer = customer;
//    }
//
//    public String getContactName() {
//        return contactName;
//    }
//
//    public void setContactName(String contactName) {
//        this.contactName = contactName;
//    }
//
//    public String getContactNumber() {
//        return contactNumber;
//    }
//
//    public void setContactNumber(String contactNumber) {
//        this.contactNumber = contactNumber;
//    }
//
//    public String getScheduledTime() {
//        return scheduledTime;
//    }
//
//    public void setScheduledTime(String scheduledTime) {
//        this.scheduledTime = scheduledTime;
//    }
//
//    public String getStartTime() {
//        return startTime;
//    }
//
//    public void setStartTime(String startTime) {
//        this.startTime = startTime;
//    }
//
//    public String getVehicleName() {
//        return vehicleName;
//    }
//
//    public void setVehicleName(String vehicleName) {
//        this.vehicleName = vehicleName;
//    }
//
//    public String getVehicleID() {
//        return vehicleID;
//    }
//
//    public void setVehicleID(String vehicleID) {
//        this.vehicleID = vehicleID;
//    }
//
//    public String getDriverID() {
//        return driverID;
//    }
//
//    public void setDriverID(String driverID) {
//        this.driverID = driverID;
//    }
//
//    public String getDriverName() {
//        return driverName;
//    }
//
//    public void setDriverName(String driverName) {
//        this.driverName = driverName;
//    }
//
//    public String getJobType() {
//        return jobType;
//    }
//
//    public void setJobType(String jobType) {
//        this.jobType = jobType;
//    }
//
//    public int getFromBase() {
//        return fromBase;
//    }
//
//    public void setFromBase(int fromBase) {
//        this.fromBase = fromBase;
//    }
//
//    public int getToBase() {
//        return toBase;
//    }
//
//    public void setToBase(int toBase) {
//        this.toBase = toBase;
//    }
//
//    public String getDescription() {
//        return description;
//    }
//
//    public void setDescription(String description) {
//        this.description = description;
//    }
//
//    public int getPassengers() {
//        return passengers;
//    }
//
//    public void setPassengers(int passengers) {
//        this.passengers = passengers;
//    }
//
//    public String getJobStatus() {
//        return jobStatus;
//    }
//
//    public void setJobStatus(String jobStatus) {
//        this.jobStatus = jobStatus;
//    }
//
//    public String getColorCode() {
//        return colorCode;
//    }
//
//    public void setColorCode(String colorCode) {
//        this.colorCode = colorCode;
//    }
//
//    public String getRequirement() {
//        return requirement;
//    }
//
//    public void setRequirement(String requirement) {
//        this.requirement = requirement;
//    }
}
