package com.netcorpgps.v3.dispatch.api.bean;

import com.netcorpgps.v3.dispatch.db.bean.Jobs;

import java.util.List;

/**
 * Created by David Fa on 25/05/2017.
 */

public class JobListBean {
    private List<Jobs> jobList;
    private Integer status;
    private String msg;

    public List<Jobs> getJobList() {
        return jobList;
    }

    public void setJobList(List<Jobs> jobList) {
        this.jobList = jobList;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
