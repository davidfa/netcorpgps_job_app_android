package com.netcorpgps.v3.dispatch.api.bean;

import com.google.gson.annotations.Expose;
import com.netcorpgps.v3.dispatch.db.bean.Message;

import java.io.Serializable;
import java.util.List;

/**
 * Created by David Fa on 7/09/2017.
 */

public class MessageBean implements Serializable {

    @Expose
    private Integer status;
    private String msg;
    private List<Message> messageList;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Message> getMessageList() {
        return messageList;
    }

    public void setMessageList(List<Message> messageList) {
        this.messageList = messageList;
    }
}
