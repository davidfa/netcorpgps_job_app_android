package com.netcorpgps.v3.dispatch.api.bean;

/**
 * Created by David Fa on 17/05/2017.
 */

public class ResponseBean {
    private Integer status;
    private String msg;
    private String ids;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        if (msg == null)
            return "";
        else
            return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        if (status != null) {
            sb.append("Status: " + status);
        }
        if (msg != null) {
            sb.append(" --- Message: " + msg);
        }
        if (ids != null) {
            sb.append(" --- ids: " + ids);
        }
        return sb.toString();
    }
}
