package com.netcorpgps.v3.dispatch.api.bean;

import java.io.Serializable;

/**
 * Created by David Fa on 17/05/2017.
 */

public class SettingBean implements Serializable {

    private String company_database;
    private String vehicle_id;
    private String device_id;
    private Integer status;
    private String msg;

    public String getCompany_database() {
        return company_database;
    }

    public void setCompany_database(String company_database) {
        this.company_database = company_database;
    }

    public String getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(String vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
