package com.netcorpgps.v3.dispatch.api.bean;

import com.netcorpgps.v3.dispatch.utils.CommonUtils;

import java.io.Serializable;

/**
 * Created by David Fa on 24/07/2017.
 */

public class SystemBean implements Serializable {
    int versionCode;
    String versionName;
    String appUrl;
    String appName;
    String appFullAddress;

    public int getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(int versionCode) {
        this.versionCode = versionCode;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public String getAppUrl() {
        return appUrl;
    }

    public void setAppUrl(String appUrl) {
        this.appUrl = appUrl;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppFullAddress() {
        return appFullAddress;
    }

    public void setAppFullAddress(String appFullAddress) {
        this.appFullAddress = appFullAddress;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("VersionCode: " + getVersionCode());
        if (!CommonUtils.isEmpty(getVersionName()))
            sb.append(" VersionName: " + getVersionName());
        if (!CommonUtils.isEmpty(getAppFullAddress()))
            sb.append(" URL: " + getAppFullAddress());
        return sb.toString();
    }
}
