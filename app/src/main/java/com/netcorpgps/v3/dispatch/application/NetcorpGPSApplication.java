package com.netcorpgps.v3.dispatch.application;

import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.os.Build;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.api.bean.DeviceInfoBean;
import com.netcorpgps.v3.dispatch.utils.CommonUtils;
import com.netcorpgps.v3.dispatch.utils.SharedPrefManager;


/**
 * Created by David Fa on 28/04/2017.
 */

public class NetcorpGPSApplication extends Application {

    private final String LOG_TAG = NetcorpGPSApplication.class.getSimpleName();
    /**
     * A flag to show how easily you can switch from standard SQLite to the encrypted SQLCipher.
     */
    public static final boolean ENCRYPTED = true;
    /**
     * Shared preferences
     */
    public static SharedPrefManager sharedPrefManager;
    private static NetcorpGPSApplication sInstance;

    /**
     * Global request queue for Volley
     */
    private RequestQueue mRequestQueue;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        CrashHandler crashHandler = CrashHandler.getInstance();
        crashHandler.init(this);
        sharedPrefManager = new SharedPrefManager(getApplicationContext());
        initApp();
    }

    private void initApp() {
//        if (CommonUtils.isEmpty(NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_completed_job_number))) {
//            NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_completed_job_number, "30");
//        }
    }

    /**
     * @return ApplicationController singleton instance
     */
    public static synchronized NetcorpGPSApplication getInstance() {
        return sInstance;
    }

    /**
     * @return The Volley Request queue, the queue will be created if it is null
     */
    public RequestQueue getRequestQueue() {
        // lazy initialize the request queue, the queue instance will be
        // created when it is accessed for the first time
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public DeviceInfoBean getDeviceInformation() {
        BluetoothAdapter myDevice = BluetoothAdapter.getDefaultAdapter();
        DeviceInfoBean deviceInfo = new DeviceInfoBean();
        if (myDevice != null) {
            String deviceName = CommonUtils.capitalize(myDevice.getName());
            String manufacturer = CommonUtils.capitalize(Build.MANUFACTURER);
            String model = CommonUtils.capitalize(Build.MODEL);
            String version = CommonUtils.capitalize(Build.VERSION.RELEASE);
            deviceInfo.setDeviceName(deviceName);
            deviceInfo.setManufacturer(manufacturer);
            deviceInfo.setModel(model);
            deviceInfo.setVersion(version);
        } else {
            deviceInfo.setDeviceName("");
            deviceInfo.setManufacturer("");
            deviceInfo.setModel("");
            deviceInfo.setVersion("");
        }

        deviceInfo.setOs("Android");

        return deviceInfo;
    }
}

