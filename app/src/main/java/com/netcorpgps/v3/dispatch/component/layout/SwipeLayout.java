package com.netcorpgps.v3.dispatch.component.layout;

import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ViewDragHelper;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.netcorpgps.v3.dispatch.R;

/**
 * Created by David Fa on 5/06/2017.
 */

public class SwipeLayout extends FrameLayout {

    public enum SwipeState {
        OPEN, CLOSE, SWIPING
    }

    private SwipeState swipeState = SwipeState.CLOSE;
    private OnSwipeChangeListener onSwipeChangeListener;

    public OnSwipeChangeListener getOnSwipeChangeListener() {
        return onSwipeChangeListener;
    }

    public void setOnSwipeChangeListener(OnSwipeChangeListener onSwipeChangeListener) {
        this.onSwipeChangeListener = onSwipeChangeListener;
    }

    public interface OnSwipeChangeListener {
        void onOpen(SwipeLayout layout);

        void onClose(SwipeLayout layout);

        void onSwiping(SwipeLayout layout);

        void onStartOpen(SwipeLayout layout);

        void onStartClose(SwipeLayout layout);

    }

    private ViewDragHelper mViewDragHelper;
    private ViewGroup mBackLayout;
    private ViewGroup mFrontLayout;
    private int mWidth;
    private int mHeight;
    private int mRange;

    public SwipeLayout(@NonNull Context context) {
        super(context);
        mViewDragHelper = ViewDragHelper.create(this, 1.0f, callBack);
    }

    public SwipeLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mViewDragHelper = ViewDragHelper.create(this, 1.0f, callBack);
    }

    public SwipeLayout(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mViewDragHelper = ViewDragHelper.create(this, 1.0f, callBack);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return mViewDragHelper.shouldInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
//        switch (event.getAction()) {
//            case MotionEvent.ACTION_UP:
//                if (swipeState == SwipeState.CLOSE) {
//                    return false;
//                }
//                break;
//        }
        getParent().requestDisallowInterceptTouchEvent(true);
        try {
            mViewDragHelper.processTouchEvent(event);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mWidth = getMeasuredWidth();
        mHeight = getMeasuredHeight();
        mRange = mBackLayout.getMeasuredWidth();
    }

    /**
     *
     * @param changed
     * @param left
     * @param top
     * @param right
     * @param bottom
     */
    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        layoutInit(false);
    }

    private void layoutInit(boolean isOpen) {
        Rect frontRect = computeFrontRect(isOpen);
        mFrontLayout.layout(frontRect.left, frontRect.top, frontRect.right, frontRect.bottom);
        Rect backRect = computeBackRect(frontRect);

        mBackLayout.layout(backRect.left, backRect.top, backRect.right, backRect.bottom);

        bringChildToFront(mFrontLayout);
    }

    /**
     *
     * @param frontRect
     * @return
     */
    private Rect computeBackRect(Rect frontRect) {
        int left = frontRect.right;
        return new Rect(left, frontRect.top, left + mRange, frontRect.bottom);
    }

    /**
     * @param isOpen
     * @return
     */
    private Rect computeFrontRect(boolean isOpen) {
        int left = 0;
        if (isOpen) {
            left = -mRange;
        } else {
            left = 0;
        }

        return new Rect(left, 0, left + mWidth, 0 + mHeight);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        int childCount = getChildCount();
        if (childCount < 2) {
            throw new IllegalStateException("You must have 2 children at least!");
        }
        if (getChildAt(0) == null || !(getChildAt(0) instanceof ViewGroup) || getChildAt(1) == null && !(getChildAt(1) instanceof ViewGroup)) {
            throw new IllegalArgumentException("your child must be instance of ViewGroup!");
        }

        mBackLayout = (ViewGroup) findViewById(R.id.layout_back);
        mFrontLayout = (ViewGroup) findViewById(R.id.layout_front);
    }

    ViewDragHelper.Callback callBack = new ViewDragHelper.Callback() {

        /**
         * @param child
         * @param pointerId
         * @return
         */
        @Override
        public boolean tryCaptureView(View child, int pointerId) {
            return true;
        }

        /**
         * @param capturedChild
         * @param activePointerId
         */
        @Override
        public void onViewCaptured(View capturedChild, int activePointerId) {
            super.onViewCaptured(capturedChild, activePointerId);
        }

        /**
         * @param child
         * @return
         */
        @Override
        public int getViewHorizontalDragRange(View child) {
            return mRange;
        }

        /**
         * @param child
         * @param left
         * @param dx
         * @return
         */
        @Override
        public int clampViewPositionHorizontal(View child, int left, int dx) {

            if (child == mFrontLayout) {
                left = fixedFrontLeft(left);
            } else if (child == mBackLayout) {
                left = fixedBackLeft(left);
            }

            return left;
        }


        private int fixedFrontLeft(int left) {
            if (left < -mRange) {
                left = -mRange;
            } else if (left > 0) {
                left = 0;
            }
            return left;
        }

        private int fixedBackLeft(int left) {
            if (left < (mWidth - mRange)) {
                left = mWidth - mRange;
            } else if (left > mWidth) {
                left = mWidth;
            }
            return left;
        }

        /**
         * @param changedView
         * @param left
         * @param top
         * @param dx
         * @param dy
         */
        @Override
        public void onViewPositionChanged(View changedView, int left, int top, int dx, int dy) {

            if (changedView == mFrontLayout) {
                mBackLayout.offsetLeftAndRight(dx);
            } else if (changedView == mBackLayout) {
                mFrontLayout.offsetLeftAndRight(dx);
            }

            dispatchEvent();

            invalidate();

        }

        @Override
        public void onViewReleased(View releasedChild, float xvel, float yvel) {

            if (xvel > 0) {
                close();
//            } else if (mFrontLayout.getLeft() < 0) { // move to left
//                open();
            } else {
                open();
            }
        }


    };

    private void dispatchEvent() {
        SwipeState preState = swipeState;
        swipeState = updateState();
        if (onSwipeChangeListener != null) {
            onSwipeChangeListener.onSwiping(this);
            if (swipeState != preState) {
                if (swipeState == SwipeState.OPEN) {
                    onSwipeChangeListener.onOpen(this);
                } else if (swipeState == SwipeState.CLOSE) {
                    onSwipeChangeListener.onClose(this);
                } else if (preState == SwipeState.OPEN) {
                    onSwipeChangeListener.onStartClose(this);
                } else if (preState == SwipeState.CLOSE) {
                    onSwipeChangeListener.onStartOpen(this);
                }
            }
        }
    }

    private SwipeState updateState() {
        if (mFrontLayout.getLeft() == -mRange) {
            return SwipeState.OPEN;
        } else if (mFrontLayout.getLeft() == 0) {
            return SwipeState.CLOSE;
        }
        return SwipeState.SWIPING;
    }

    public boolean isOpen() {
        if (mFrontLayout.getLeft() == -mRange) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void computeScroll() {
        super.computeScroll();
        if (mViewDragHelper.continueSettling(true)) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    public void open(boolean isSmooth) {
        if (isSmooth) {
            int finalLeft = -mRange;
            boolean b = mViewDragHelper.smoothSlideViewTo(mFrontLayout, finalLeft, 0);
            if (b) {
                ViewCompat.postInvalidateOnAnimation(this);
            }

        } else {
            layoutInit(true);
        }
    }

    public void open() {
        open(true);
    }

    public void close(boolean isSmooth) {
        if (isSmooth) {
            int finalLeft = 0;
            boolean b = mViewDragHelper.smoothSlideViewTo(mFrontLayout, finalLeft, 0);
            if (b) {
                ViewCompat.postInvalidateOnAnimation(this);
            }
        } else {
            layoutInit(false);
        }
    }

    public void close() {
        close(true);
    }
}
