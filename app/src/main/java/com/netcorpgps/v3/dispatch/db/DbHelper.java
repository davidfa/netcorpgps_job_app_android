package com.netcorpgps.v3.dispatch.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.netcorpgps.v3.dispatch.db.contract.DriverRecordContract;
import com.netcorpgps.v3.dispatch.db.contract.DriverStatusRecordContract;
import com.netcorpgps.v3.dispatch.db.contract.ExpenseContract;
import com.netcorpgps.v3.dispatch.db.contract.FCMRecordContract;
import com.netcorpgps.v3.dispatch.db.contract.JobStatusContract;
import com.netcorpgps.v3.dispatch.db.contract.JobsContract;
import com.netcorpgps.v3.dispatch.db.contract.MessageContract;
import com.netcorpgps.v3.dispatch.db.contract.WaypointContract;
import com.netcorpgps.v3.dispatch.db.contract.WaypointRecordContract;

/**
 * Created by David Fa on 30/05/2017.
 */

public class DbHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 10;
    public static final String DATABASE_NAME = "Netcorpgps.db";
    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(JobsContract.SQL_CREATE_ENTRIES);
        db.execSQL(ExpenseContract.SQL_CREATE_ENTRIES);
        db.execSQL(WaypointContract.SQL_CREATE_ENTRIES);
        db.execSQL(JobStatusContract.SQL_CREATE_ENTRIES);
        db.execSQL(DriverRecordContract.SQL_CREATE_ENTRIES);
        db.execSQL(DriverStatusRecordContract.SQL_CREATE_ENTRIES);
        db.execSQL(WaypointRecordContract.SQL_CREATE_ENTRIES);
        db.execSQL(FCMRecordContract.SQL_CREATE_ENTRIES);
        db.execSQL(MessageContract.SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(JobsContract.SQL_DELETE_ENTRIES);
        db.execSQL(ExpenseContract.SQL_DELETE_ENTRIES);
        db.execSQL(WaypointContract.SQL_DELETE_ENTRIES);
        db.execSQL(JobStatusContract.SQL_DELETE_ENTRIES);
        db.execSQL(DriverRecordContract.SQL_DELETE_ENTRIES);
        db.execSQL(DriverStatusRecordContract.SQL_DELETE_ENTRIES);
        db.execSQL(WaypointRecordContract.SQL_DELETE_ENTRIES);
        db.execSQL(FCMRecordContract.SQL_DELETE_ENTRIES);
        db.execSQL(MessageContract.SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
