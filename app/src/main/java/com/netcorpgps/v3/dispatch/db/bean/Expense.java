package com.netcorpgps.v3.dispatch.db.bean;

/**
 * Created by David Fa on 2/06/2017.
 */

public class Expense {
    private Integer jobID;
    private String expense;
    private String expenseName;

    public Integer getJobID() {
        return jobID;
    }

    public void setJobID(Integer jobID) {
        this.jobID = jobID;
    }

    public String getExpense() {
        return expense;
    }

    public void setExpense(String expense) {
        this.expense = expense;
    }

    public String getExpenseName() {
        return expenseName;
    }

    public void setExpenseName(String expenseName) {
        this.expenseName = expenseName;
    }
}
