package com.netcorpgps.v3.dispatch.db.bean;

/**
 * Created by David Fa on 28/07/2017.
 */

public class FCMRecord {
    String id;
    String fcmToken;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }
}
