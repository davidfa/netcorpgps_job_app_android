package com.netcorpgps.v3.dispatch.db.bean;

/**
 * Created by David Fa on 26/06/2017.
 */

public class JobStatus {
    private String id;
    private String jobID;
    private String jobStatus;
    private String driverID;
    private String vehicleID;
    private String deviceID;
    private String deviceDatetime;
    private String deviceTimezoneName;
    private String deviceTimezoneID;
    private String deviceCoordinate;
    private String deviceAddress;
    private String sysDatetime;
    private String companyBD;
    private String companyID;

    public String getJobID() {
        return jobID;
    }

    public void setJobID(String jobID) {
        this.jobID = jobID;
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }

    public String getDriverID() {
        return driverID;
    }

    public void setDriverID(String driverID) {
        this.driverID = driverID;
    }

    public String getVehicleID() {
        return vehicleID;
    }

    public void setVehicleID(String vehicleID) {
        this.vehicleID = vehicleID;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public String getDeviceDatetime() {
        return deviceDatetime;
    }

    public void setDeviceDatetime(String deviceDatetime) {
        this.deviceDatetime = deviceDatetime;
    }

    public String getDeviceTimezoneName() {
        return deviceTimezoneName;
    }

    public void setDeviceTimezoneName(String deviceTimezoneName) {
        this.deviceTimezoneName = deviceTimezoneName;
    }

    public String getDeviceTimezoneID() {
        return deviceTimezoneID;
    }

    public void setDeviceTimezoneID(String deviceTimezoneID) {
        this.deviceTimezoneID = deviceTimezoneID;
    }

    public String getDeviceCoordinate() {
        return deviceCoordinate;
    }

    public void setDeviceCoordinate(String deviceCoordinate) {
        this.deviceCoordinate = deviceCoordinate;
    }

    public String getSysDatetime() {
        return sysDatetime;
    }

    public void setSysDatetime(String sysDatetime) {
        this.sysDatetime = sysDatetime;
    }

    public String getCompanyBD() {
        return companyBD;
    }

    public void setCompanyBD(String companyBD) {
        this.companyBD = companyBD;
    }

    public String getDeviceAddress() {
        return deviceAddress;
    }

    public void setDeviceAddress(String deviceAddress) {
        this.deviceAddress = deviceAddress;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyID() {
        return companyID;
    }

    public void setCompanyID(String companyID) {
        this.companyID = companyID;
    }
}
