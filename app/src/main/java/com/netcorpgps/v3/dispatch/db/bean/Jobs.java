package com.netcorpgps.v3.dispatch.db.bean;

public class Jobs {
    private Integer jobID;
    private String jobCode;
    private String customer;
    private String contactName;
    private String contactNumber;
    private String scheduledTime;
    private String startTime;
    private String vehicleName;
    private String vehicleID;
    private String driverID;
    private String driverName;
    private String jobType;
    private Integer fromBase;
    private Integer toBase;
    private String description;
    private String requirement;
    private Integer passengers;
    private String jobStatus;
    private String colorCode;
    private String pagedNumber;
    private String lastStatusTime;

    public Integer getJobID() {
        return jobID;
    }

    public void setJobID(Integer jobID) {
        this.jobID = jobID;
    }

    public String getJobCode() {
        return jobCode;
    }

    public void setJobCode(String jobCode) {
        this.jobCode = jobCode;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getScheduledTime() {
        return scheduledTime;
    }

    public void setScheduledTime(String scheduledTime) {
        this.scheduledTime = scheduledTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public String getVehicleID() {
        return vehicleID;
    }

    public void setVehicleID(String vehicleID) {
        this.vehicleID = vehicleID;
    }

    public String getDriverID() {
        return driverID;
    }

    public void setDriverID(String driverID) {
        this.driverID = driverID;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public Integer getFromBase() {
        return fromBase;
    }

    public void setFromBase(Integer fromBase) {
        this.fromBase = fromBase;
    }

    public Integer getToBase() {
        return toBase;
    }

    public void setToBase(Integer toBase) {
        this.toBase = toBase;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPassengers() {
        return passengers;
    }

    public void setPassengers(Integer passengers) {
        this.passengers = passengers;
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public String getRequirement() {
        return requirement;
    }

    public void setRequirement(String requirement) {
        this.requirement = requirement;
    }

    public String getPagedNumber() {
        return pagedNumber;
    }

    public void setPagedNumber(String pagedNumber) {
        this.pagedNumber = pagedNumber;
    }

    public String getLastStatusTime() {
        return lastStatusTime;
    }

    public void setLastStatusTime(String lastStatusTime) {
        this.lastStatusTime = lastStatusTime;
    }
}
