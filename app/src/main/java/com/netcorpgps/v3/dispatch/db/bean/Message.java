package com.netcorpgps.v3.dispatch.db.bean;

import java.io.Serializable;

/**
 * Created by David Fa on 17/08/2017.
 */

public class Message implements Serializable {

    private String sender;
    private String role;
    private String time;
    private String msg;
    private String companyID;
    private String vehicleID;
    private String uniqueID;
    private String type;
    private String fileUrl;
    private String serverUrl;
    private String voiceTime;
    private String isSynced;
    private String isRead;

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCompanyID() {
        return companyID;
    }

    public void setCompanyID(String companyID) {
        this.companyID = companyID;
    }

    public String getVehicleID() {
        return vehicleID;
    }

    public void setVehicleID(String vehicleID) {
        this.vehicleID = vehicleID;
    }

    public String getUniqueID() {
        return uniqueID;
    }

    public void setUniqueID(String uniqueID) {
        this.uniqueID = uniqueID;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getVoiceTime() {
        return voiceTime;
    }

    public void setVoiceTime(String voiceTime) {
        this.voiceTime = voiceTime;
    }

    public String getSynced() {
        return isSynced;
    }

    public void setSynced(String synced) {
        isSynced = synced;
    }

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getIsRead() {
        return isRead;
    }

    public void setIsRead(String isRead) {
        this.isRead = isRead;
    }

    public String toString() {
        return "Sender: " + sender + " Role: " + role + " Time: " + time + " Msg: " + msg + " CompanyID: " + companyID + " VehicleID: " + vehicleID + " UniqueID: " + uniqueID + " type: " + type + " fileUrl: " + fileUrl + " voiceTime: " + voiceTime + " isSynced: " + isSynced + " isRead: " + isRead + " serverUrl: " + serverUrl;
    }
}
