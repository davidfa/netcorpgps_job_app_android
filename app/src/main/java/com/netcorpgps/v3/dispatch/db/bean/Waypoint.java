package com.netcorpgps.v3.dispatch.db.bean;

import java.io.Serializable;

/**
 * Created by David Fa on 1/06/2017.
 */

public class Waypoint implements Serializable {

    private String id;
    private Integer jobID;
    private String estTime;
    private Integer estTimeVal;
    private Integer waitTime;
    private String estDistance;
    private Integer estDistanceVal;
    private String deviceDatetimeArrivedAt;
    private String sysDatetimeArrivedAt;
    private String deviceTimezoneName;
    private String deviceTimezoneID;

    /**
     * 0: From base
     * 1: Waypoint
     * 2: To base
     */
    private Integer base;
    private String address;

    public Integer getJobID() {
        return jobID;
    }

    public void setJobID(Integer jobID) {
        this.jobID = jobID;
    }

    public String getEstTime() {
        return estTime;
    }

    public void setEstTime(String estTime) {
        this.estTime = estTime;
    }

    public Integer getEstTimeVal() {
        return estTimeVal;
    }

    public void setEstTimeVal(Integer estTimeVal) {
        this.estTimeVal = estTimeVal;
    }

    public Integer getWaitTime() {
        return waitTime;
    }

    public void setWaitTime(Integer waitTime) {
        this.waitTime = waitTime;
    }

    public String getEstDistance() {
        return estDistance;
    }

    public void setEstDistance(String estDistance) {
        this.estDistance = estDistance;
    }

    public Integer getEstDistanceVal() {
        return estDistanceVal;
    }

    public void setEstDistanceVal(Integer estDistanceVal) {
        this.estDistanceVal = estDistanceVal;
    }

    public Integer getBase() {
        return base;
    }

    public void setBase(Integer base) {
        this.base = base;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDeviceDatetimeArrivedAt() {
        return deviceDatetimeArrivedAt;
    }

    public void setDeviceDatetimeArrivedAt(String deviceDatetimeArrivedAt) {
        this.deviceDatetimeArrivedAt = deviceDatetimeArrivedAt;
    }

    public String getSysDatetimeArrivedAt() {
        return sysDatetimeArrivedAt;
    }

    public void setSysDatetimeArrivedAt(String sysDatetimeArrivedAt) {
        this.sysDatetimeArrivedAt = sysDatetimeArrivedAt;
    }

    public String getDeviceTimezoneName() {
        return deviceTimezoneName;
    }

    public void setDeviceTimezoneName(String deviceTimezoneName) {
        this.deviceTimezoneName = deviceTimezoneName;
    }

    public String getDeviceTimezoneID() {
        return deviceTimezoneID;
    }

    public void setDeviceTimezoneID(String deviceTimezoneID) {
        this.deviceTimezoneID = deviceTimezoneID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
