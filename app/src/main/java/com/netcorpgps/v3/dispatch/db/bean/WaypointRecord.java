package com.netcorpgps.v3.dispatch.db.bean;

/**
 * Created by David Fa on 26/07/2017.
 */

public class WaypointRecord {
    private String id;
    private String jobID;
    private String companyBD;
    private String address;
    private String deviceDatetimeArrivedAt;
    private String sysDatetimeArrivedAt;
    private String deviceTimezoneName;
    private String deviceTimezoneID;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJobID() {
        return jobID;
    }

    public void setJobID(String jobID) {
        this.jobID = jobID;
    }

    public String getCompanyBD() {
        return companyBD;
    }

    public void setCompanyBD(String companyBD) {
        this.companyBD = companyBD;
    }

    public String getDeviceDatetimeArrivedAt() {
        return deviceDatetimeArrivedAt;
    }

    public void setDeviceDatetimeArrivedAt(String deviceDatetimeArrivedAt) {
        this.deviceDatetimeArrivedAt = deviceDatetimeArrivedAt;
    }

    public String getSysDatetimeArrivedAt() {
        return sysDatetimeArrivedAt;
    }

    public void setSysDatetimeArrivedAt(String sysDatetimeArrivedAt) {
        this.sysDatetimeArrivedAt = sysDatetimeArrivedAt;
    }

    public String getDeviceTimezoneName() {
        return deviceTimezoneName;
    }

    public void setDeviceTimezoneName(String deviceTimezoneName) {
        this.deviceTimezoneName = deviceTimezoneName;
    }

    public String getDeviceTimezoneID() {
        return deviceTimezoneID;
    }

    public void setDeviceTimezoneID(String deviceTimezoneID) {
        this.deviceTimezoneID = deviceTimezoneID;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
