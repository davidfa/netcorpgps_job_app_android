package com.netcorpgps.v3.dispatch.db.contract;

import android.content.ContentValues;
import android.database.Cursor;
import android.provider.BaseColumns;

import com.netcorpgps.v3.dispatch.db.bean.DriverRecord;
import com.netcorpgps.v3.dispatch.db.bean.JobStatus;

import java.util.Map;

/**
 * Created by David Fa on 28/06/2017.
 */

public class DriverRecordContract {

    private DriverRecordContract() {
    }

    public static class DriverRecordEntry implements BaseColumns {
        public static final String TABLE_NAME = "DRIVERRECORD";
        public static final String driverRecordID = "driverRecordID";
        public static final String driverID = "driverID";
        public static final String vehicleID = "vehicleID";
        public static final String deviceID = "deviceID";
        public static final String deviceDatetime = "deviceDatetime";
        public static final String deviceTimezoneName = "deviceTimezoneName";
        public static final String deviceTimezoneID = "deviceTimezoneID";
        public static final String deviceCoordinate = "deviceCoordinate";
        public static final String deviceAddress = "deviceAddress";
        public static final String sysDatetime = "sysDatetime";
        public static final String type = "type";
        public static final String companyBD = "companyBD";
    }

    public static final String[] COLUMNS = {
            DriverRecordEntry._ID,
            DriverRecordEntry.driverRecordID,
            DriverRecordEntry.driverID,
            DriverRecordEntry.vehicleID,
            DriverRecordEntry.deviceID,
            DriverRecordEntry.deviceDatetime,
            DriverRecordEntry.deviceTimezoneName,
            DriverRecordEntry.deviceTimezoneID,
            DriverRecordEntry.deviceCoordinate,
            DriverRecordEntry.deviceAddress,
            DriverRecordEntry.sysDatetime,
            DriverRecordEntry.type,
            DriverRecordEntry.companyBD
    };

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + DriverRecordContract.DriverRecordEntry.TABLE_NAME + " (" +
                    DriverRecordEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    DriverRecordEntry.driverRecordID + " TEXT," +
                    DriverRecordEntry.driverID + " TEXT," +
                    DriverRecordEntry.vehicleID + " TEXT," +
                    DriverRecordEntry.deviceID + " TEXT," +
                    DriverRecordEntry.deviceDatetime + " TEXT," +
                    DriverRecordEntry.deviceTimezoneName + " TEXT," +
                    DriverRecordEntry.deviceTimezoneID + " TEXT," +
                    DriverRecordEntry.deviceCoordinate + " TEXT," +
                    DriverRecordEntry.deviceAddress + " TEXT," +
                    DriverRecordEntry.sysDatetime + " TEXT," +
                    DriverRecordEntry.type + " TEXT," +
                    DriverRecordEntry.companyBD + " TEXT" +
                    ")";

    public static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + DriverRecordEntry.TABLE_NAME;

    public static DriverRecord parseCursorToJobs(Cursor cursor) {
        DriverRecord record = new DriverRecord();
        record.setId(cursor.getString(cursor.getColumnIndexOrThrow(DriverRecordEntry._ID)));
        record.setDriverRecordID(cursor.getString(cursor.getColumnIndexOrThrow(DriverRecordEntry.driverRecordID)));
        record.setDriverID(cursor.getString(cursor.getColumnIndexOrThrow(DriverRecordEntry.driverID)));
        record.setVehicleID(cursor.getString(cursor.getColumnIndexOrThrow(DriverRecordEntry.vehicleID)));
        record.setDeviceID(cursor.getString(cursor.getColumnIndexOrThrow(DriverRecordEntry.deviceID)));
        record.setDeviceDatetime(cursor.getString(cursor.getColumnIndexOrThrow(DriverRecordEntry.deviceDatetime)));
        record.setDeviceTimezoneName(cursor.getString(cursor.getColumnIndexOrThrow(DriverRecordEntry.deviceTimezoneName)));
        record.setDeviceTimezoneID(cursor.getString(cursor.getColumnIndexOrThrow(DriverRecordEntry.deviceTimezoneID)));
        record.setDeviceCoordinate(cursor.getString(cursor.getColumnIndexOrThrow(DriverRecordEntry.deviceCoordinate)));
        record.setDeviceAddress(cursor.getString(cursor.getColumnIndexOrThrow(DriverRecordEntry.deviceAddress)));
        record.setSysDatetime(cursor.getString(cursor.getColumnIndexOrThrow(DriverRecordEntry.sysDatetime)));
        record.setType(cursor.getString(cursor.getColumnIndexOrThrow(DriverRecordEntry.type)));
        record.setCompanyBD(cursor.getString(cursor.getColumnIndexOrThrow(DriverRecordEntry.companyBD)));
        return record;
    }

    public static ContentValues setContentValues(Map<String, String> record) {
        ContentValues values = new ContentValues();
        values.put(DriverRecordEntry.driverRecordID, record.get("driverRecordID"));
        values.put(DriverRecordEntry.driverID, record.get("driverID"));
        values.put(DriverRecordEntry.vehicleID, record.get("vehicleID"));
        values.put(DriverRecordEntry.deviceID, record.get("deviceID"));
        values.put(DriverRecordEntry.deviceDatetime, record.get("device_datetime"));
        values.put(DriverRecordEntry.deviceTimezoneName, record.get("device_timezone_name"));
        values.put(DriverRecordEntry.deviceTimezoneID, record.get("device_timezone_id"));
        values.put(DriverRecordEntry.deviceCoordinate, record.get("device_coordinate"));
        values.put(DriverRecordEntry.deviceAddress, record.get("device_address"));
        values.put(DriverRecordEntry.sysDatetime, record.get("sys_datetime"));
        values.put(DriverRecordEntry.type, record.get("type"));
        values.put(DriverRecordEntry.companyBD, record.get("company"));
        return values;
    }
}
