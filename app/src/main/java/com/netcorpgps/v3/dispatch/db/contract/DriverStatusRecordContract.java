package com.netcorpgps.v3.dispatch.db.contract;

import android.content.ContentValues;
import android.database.Cursor;
import android.provider.BaseColumns;

import com.netcorpgps.v3.dispatch.db.bean.DriverRecord;
import com.netcorpgps.v3.dispatch.db.bean.DriverStatusRecord;

import java.util.Map;

/**
 * Created by David Fa on 28/06/2017.
 */

public class DriverStatusRecordContract {

    private DriverStatusRecordContract() {
    }

    public static class DriverStatusRecordEntry implements BaseColumns {
        public static final String TABLE_NAME = "DRIVERSTATUSRECORD";
        public static final String driverRecordID = "driverRecordID";
        public static final String driverID = "driverID";
        public static final String vehicleID = "vehicleID";
        public static final String driverStatus = "driverStatus";
        public static final String deviceDatetime = "deviceDatetime";
        public static final String deviceTimezoneName = "deviceTimezoneName";
        public static final String deviceTimezoneID = "deviceTimezoneID";
        public static final String deviceCoordinate = "deviceCoordinate";
        public static final String deviceAddress = "deviceAddress";
        public static final String sysDatetime = "sysDatetime";
        public static final String companyBD = "companyBD";
    }

    public static final String[] COLUMNS = {
            DriverStatusRecordEntry._ID,
            DriverStatusRecordEntry.driverRecordID,
            DriverStatusRecordEntry.driverID,
            DriverStatusRecordEntry.vehicleID,
            DriverStatusRecordEntry.driverStatus,
            DriverStatusRecordEntry.deviceDatetime,
            DriverStatusRecordEntry.deviceTimezoneName,
            DriverStatusRecordEntry.deviceTimezoneID,
            DriverStatusRecordEntry.deviceCoordinate,
            DriverStatusRecordEntry.deviceAddress,
            DriverStatusRecordEntry.sysDatetime,
            DriverStatusRecordEntry.companyBD
    };

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + DriverStatusRecordContract.DriverStatusRecordEntry.TABLE_NAME + " (" +
                    DriverStatusRecordEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    DriverStatusRecordEntry.driverRecordID + " TEXT," +
                    DriverStatusRecordEntry.driverID + " TEXT," +
                    DriverStatusRecordEntry.vehicleID + " TEXT," +
                    DriverStatusRecordEntry.driverStatus + " TEXT," +
                    DriverStatusRecordEntry.deviceDatetime + " TEXT," +
                    DriverStatusRecordEntry.deviceTimezoneName + " TEXT," +
                    DriverStatusRecordEntry.deviceTimezoneID + " TEXT," +
                    DriverStatusRecordEntry.deviceCoordinate + " TEXT," +
                    DriverStatusRecordEntry.deviceAddress + " TEXT," +
                    DriverStatusRecordEntry.sysDatetime + " TEXT," +
                    DriverStatusRecordEntry.companyBD + " TEXT" +
                    ")";

    public static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + DriverStatusRecordEntry.TABLE_NAME;

    public static DriverStatusRecord parseCursorToDriverStatusRecord(Cursor cursor) {
        DriverStatusRecord record = new DriverStatusRecord();
        record.setId(cursor.getString(cursor.getColumnIndexOrThrow(DriverStatusRecordEntry._ID)));
        record.setDriverRecordID(cursor.getString(cursor.getColumnIndexOrThrow(DriverStatusRecordEntry.driverRecordID)));
        record.setDriverID(cursor.getString(cursor.getColumnIndexOrThrow(DriverStatusRecordEntry.driverID)));
        record.setVehicleID(cursor.getString(cursor.getColumnIndexOrThrow(DriverStatusRecordEntry.vehicleID)));
        record.setDriverStatus(cursor.getString(cursor.getColumnIndexOrThrow(DriverStatusRecordEntry.driverStatus)));
        record.setDeviceDatetime(cursor.getString(cursor.getColumnIndexOrThrow(DriverStatusRecordEntry.deviceDatetime)));
        record.setDeviceTimezoneName(cursor.getString(cursor.getColumnIndexOrThrow(DriverStatusRecordEntry.deviceTimezoneName)));
        record.setDeviceTimezoneID(cursor.getString(cursor.getColumnIndexOrThrow(DriverStatusRecordEntry.deviceTimezoneID)));
        record.setDeviceCoordinate(cursor.getString(cursor.getColumnIndexOrThrow(DriverStatusRecordEntry.deviceCoordinate)));
        record.setDeviceAddress(cursor.getString(cursor.getColumnIndexOrThrow(DriverStatusRecordEntry.deviceAddress)));
        record.setSysDatetime(cursor.getString(cursor.getColumnIndexOrThrow(DriverStatusRecordEntry.sysDatetime)));
        record.setCompanyBD(cursor.getString(cursor.getColumnIndexOrThrow(DriverStatusRecordEntry.companyBD)));
        return record;
    }

    public static ContentValues setContentValues(Map<String, String> record) {
        ContentValues values = new ContentValues();
        values.put(DriverStatusRecordEntry.driverRecordID, record.get("driverRecordID"));
        values.put(DriverStatusRecordEntry.driverID, record.get("driverID"));
        values.put(DriverStatusRecordEntry.vehicleID, record.get("vehicleID"));
        values.put(DriverStatusRecordEntry.driverStatus, record.get("driverStatus"));
        values.put(DriverStatusRecordEntry.deviceDatetime, record.get("device_datetime"));
        values.put(DriverStatusRecordEntry.deviceTimezoneName, record.get("device_timezone_name"));
        values.put(DriverStatusRecordEntry.deviceTimezoneID, record.get("device_timezone_id"));
        values.put(DriverStatusRecordEntry.deviceCoordinate, record.get("device_coordinate"));
        values.put(DriverStatusRecordEntry.deviceAddress, record.get("device_address"));
        values.put(DriverStatusRecordEntry.sysDatetime, record.get("sys_datetime"));
        values.put(DriverStatusRecordEntry.companyBD, record.get("company"));
        return values;
    }
}
