package com.netcorpgps.v3.dispatch.db.contract;

import android.content.ContentValues;
import android.database.Cursor;
import android.provider.BaseColumns;

import com.netcorpgps.v3.dispatch.db.bean.Expense;
import com.netcorpgps.v3.dispatch.db.bean.Waypoint;

/**
 * Created by David Fa on 2/06/2017.
 */

public class ExpenseContract {

    private ExpenseContract() {
    }

    public static class ExpenseEntry implements BaseColumns {
        public static final String TABLE_NAME = "JobExpense";
        public static final String jobID = "jobID";
        public static final String expense = "expense";
        public static final String expenseName = "expenseName";
    }

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + ExpenseEntry.TABLE_NAME + " (" +
                    ExpenseEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    ExpenseEntry.jobID + " INTEGER," +
                    ExpenseEntry.expense + " TEXT," +
                    ExpenseEntry.expenseName + " TEXT" +
                    ")";

    public static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + ExpenseEntry.TABLE_NAME;

    public static final String[] COLUMNS = {
            ExpenseEntry.jobID,
            ExpenseEntry.expense,
            ExpenseEntry.expenseName
    };

    public static Expense parseCursorToWaypoint(Cursor cursor) {
        Expense expense = new Expense();
        expense.setJobID(cursor.getInt(cursor.getColumnIndexOrThrow(ExpenseEntry.jobID)));
        expense.setExpense(cursor.getString(cursor.getColumnIndexOrThrow(ExpenseEntry.expense)));
        expense.setExpenseName(cursor.getString(cursor.getColumnIndexOrThrow(ExpenseEntry.expenseName)));

        return expense;
    }

    public static ContentValues setContentValues(Expense expense) {
        ContentValues values = new ContentValues();
        values.put(ExpenseEntry.jobID, expense.getJobID());
        values.put(ExpenseEntry.expense, expense.getExpense());
        values.put(ExpenseEntry.expenseName, expense.getExpenseName());

        return values;
    }
}
