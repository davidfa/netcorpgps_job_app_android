package com.netcorpgps.v3.dispatch.db.contract;

import android.content.ContentValues;
import android.database.Cursor;
import android.provider.BaseColumns;

import com.netcorpgps.v3.dispatch.db.bean.DriverRecord;
import com.netcorpgps.v3.dispatch.db.bean.FCMRecord;

import java.util.Map;

/**
 * Created by David Fa on 28/06/2017.
 */

public class FCMRecordContract {

    private FCMRecordContract() {
    }

    public static class FCMRecordEntry implements BaseColumns {
        public static final String TABLE_NAME = "FCMRECORD";
        public static final String fcmToken = "fcmToken";
    }

    public static final String[] COLUMNS = {
            FCMRecordEntry._ID,
            FCMRecordEntry.fcmToken
    };

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + FCMRecordEntry.TABLE_NAME + " (" +
                    FCMRecordEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    FCMRecordEntry.fcmToken + " TEXT" +
                    ")";

    public static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + FCMRecordEntry.TABLE_NAME;

    public static FCMRecord parseCursorToFcm(Cursor cursor) {
        FCMRecord record = new FCMRecord();
        record.setId(cursor.getString(cursor.getColumnIndexOrThrow(FCMRecordEntry._ID)));
        record.setFcmToken(cursor.getString(cursor.getColumnIndexOrThrow(FCMRecordEntry.fcmToken)));
        return record;
    }

    public static ContentValues setContentValues(Map<String, String> record) {
        ContentValues values = new ContentValues();
        values.put(FCMRecordEntry.fcmToken, record.get("fcmToken"));
        return values;
    }
}
