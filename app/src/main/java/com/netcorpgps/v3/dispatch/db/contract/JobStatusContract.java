package com.netcorpgps.v3.dispatch.db.contract;

import android.content.ContentValues;
import android.database.Cursor;
import android.provider.BaseColumns;

import com.netcorpgps.v3.dispatch.db.bean.JobStatus;

import java.util.Map;

/**
 * Created by David Fa on 30/05/2017.
 */

public class JobStatusContract {

    private JobStatusContract() {
    }

    public static class JobStatusEntry implements BaseColumns {
        public static final String TABLE_NAME = "JOBSTATUS";
        public static final String jobID = "jobID";
        public static final String jobStatus = "jobStatus";
        public static final String driverID = "driverID";
        public static final String vehicleID = "vehicleID";
        public static final String deviceID = "deviceID";
        public static final String deviceDatetime = "deviceDatetime";
        public static final String deviceTimezoneName = "deviceTimezoneName";
        public static final String deviceTimezoneID = "deviceTimezoneID";
        public static final String deviceCoordinate = "deviceCoordinate";
        public static final String deviceAddress = "deviceAddress";
        public static final String sysDatetime = "sysDatetime";
        public static final String companyBD = "companyBD";
        public static final String companyID = "companyID";
    }

    public static final String[] COLUMNS = {
            JobStatusEntry._ID,
            JobStatusEntry.jobID,
            JobStatusEntry.jobStatus,
            JobStatusEntry.driverID,
            JobStatusEntry.vehicleID,
            JobStatusEntry.deviceID,
            JobStatusEntry.deviceDatetime,
            JobStatusEntry.deviceTimezoneName,
            JobStatusEntry.deviceTimezoneID,
            JobStatusEntry.deviceCoordinate,
            JobStatusEntry.deviceAddress,
            JobStatusEntry.sysDatetime,
            JobStatusEntry.companyBD,
            JobStatusEntry.companyID
    };

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + JobStatusEntry.TABLE_NAME + " (" +
                    JobStatusEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    JobStatusEntry.jobID + " TEXT," +
                    JobStatusEntry.jobStatus + " TEXT," +
                    JobStatusEntry.driverID + " TEXT," +
                    JobStatusEntry.vehicleID + " TEXT," +
                    JobStatusEntry.deviceID + " TEXT," +
                    JobStatusEntry.deviceDatetime + " TEXT," +
                    JobStatusEntry.deviceTimezoneName + " TEXT," +
                    JobStatusEntry.deviceTimezoneID + " TEXT," +
                    JobStatusEntry.deviceCoordinate + " TEXT," +
                    JobStatusEntry.deviceAddress + " TEXT," +
                    JobStatusEntry.sysDatetime + " TEXT," +
                    JobStatusEntry.companyBD + " TEXT," +
                    JobStatusEntry.companyID + " TEXT" +
                    ")";

    public static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + JobStatusEntry.TABLE_NAME;

    public static JobStatus parseCursorToJobs(Cursor cursor) {
        JobStatus jobStatus = new JobStatus();
        jobStatus.setId(cursor.getString(cursor.getColumnIndexOrThrow(JobStatusEntry._ID)));
        jobStatus.setJobID(cursor.getString(cursor.getColumnIndexOrThrow(JobStatusEntry.jobID)));
        jobStatus.setJobStatus(cursor.getString(cursor.getColumnIndexOrThrow(JobStatusEntry.jobStatus)));
        jobStatus.setDriverID(cursor.getString(cursor.getColumnIndexOrThrow(JobStatusEntry.driverID)));
        jobStatus.setVehicleID(cursor.getString(cursor.getColumnIndexOrThrow(JobStatusEntry.vehicleID)));
        jobStatus.setDeviceID(cursor.getString(cursor.getColumnIndexOrThrow(JobStatusEntry.deviceID)));
        jobStatus.setDeviceDatetime(cursor.getString(cursor.getColumnIndexOrThrow(JobStatusEntry.deviceDatetime)));
        jobStatus.setDeviceTimezoneName(cursor.getString(cursor.getColumnIndexOrThrow(JobStatusEntry.deviceTimezoneName)));
        jobStatus.setDeviceTimezoneID(cursor.getString(cursor.getColumnIndexOrThrow(JobStatusEntry.deviceTimezoneID)));
        jobStatus.setDeviceCoordinate(cursor.getString(cursor.getColumnIndexOrThrow(JobStatusEntry.deviceCoordinate)));
        jobStatus.setDeviceAddress(cursor.getString(cursor.getColumnIndexOrThrow(JobStatusEntry.deviceAddress)));
        jobStatus.setSysDatetime(cursor.getString(cursor.getColumnIndexOrThrow(JobStatusEntry.sysDatetime)));
        jobStatus.setCompanyBD(cursor.getString(cursor.getColumnIndexOrThrow(JobStatusEntry.companyBD)));
        jobStatus.setCompanyID(cursor.getString(cursor.getColumnIndexOrThrow(JobStatusEntry.companyID)));
        return jobStatus;
    }

    public static ContentValues setContentValues(JobStatus jobStatus) {
        ContentValues values = new ContentValues();
        values.put(JobStatusEntry.jobID, jobStatus.getJobID());
        values.put(JobStatusEntry.jobStatus, jobStatus.getJobStatus());
        values.put(JobStatusEntry.driverID, jobStatus.getDriverID());
        values.put(JobStatusEntry.vehicleID, jobStatus.getVehicleID());
        values.put(JobStatusEntry.deviceID, jobStatus.getDeviceID());
        values.put(JobStatusEntry.deviceDatetime, jobStatus.getDeviceDatetime());
        values.put(JobStatusEntry.deviceTimezoneName, jobStatus.getDeviceTimezoneName());
        values.put(JobStatusEntry.deviceTimezoneID, jobStatus.getDeviceTimezoneID());
        values.put(JobStatusEntry.deviceCoordinate, jobStatus.getDeviceCoordinate());
        values.put(JobStatusEntry.deviceAddress, jobStatus.getDeviceAddress());
        values.put(JobStatusEntry.sysDatetime, jobStatus.getSysDatetime());
        values.put(JobStatusEntry.companyBD, jobStatus.getCompanyBD());
        values.put(JobStatusEntry.companyID, jobStatus.getCompanyID());

        return values;
    }

    public static ContentValues setContentValues(Map<String, String> jobStatus) {
        ContentValues values = new ContentValues();
        values.put(JobStatusEntry.jobID, jobStatus.get("jobID"));
        values.put(JobStatusEntry.jobStatus, jobStatus.get("jobStatus"));
        values.put(JobStatusEntry.driverID, jobStatus.get("driverID"));
        values.put(JobStatusEntry.vehicleID, jobStatus.get("vehicleID"));
        values.put(JobStatusEntry.deviceID, jobStatus.get("deviceID"));
        values.put(JobStatusEntry.deviceDatetime, jobStatus.get("device_datetime"));
        values.put(JobStatusEntry.deviceTimezoneName, jobStatus.get("device_timezone_name"));
        values.put(JobStatusEntry.deviceTimezoneID, jobStatus.get("device_timezone_id"));
        values.put(JobStatusEntry.deviceCoordinate, jobStatus.get("device_coordinate"));
        values.put(JobStatusEntry.deviceAddress, jobStatus.get("device_address"));
        values.put(JobStatusEntry.sysDatetime, jobStatus.get("sys_datetime"));
        values.put(JobStatusEntry.companyBD, jobStatus.get("company"));
        values.put(JobStatusEntry.companyID, jobStatus.get("companyID"));
        return values;
    }
}
