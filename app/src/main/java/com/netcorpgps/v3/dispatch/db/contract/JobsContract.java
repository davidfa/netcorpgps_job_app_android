package com.netcorpgps.v3.dispatch.db.contract;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.provider.BaseColumns;

import com.netcorpgps.v3.dispatch.db.bean.Jobs;
import com.netcorpgps.v3.dispatch.utils.CommonUtils;

import java.util.Map;

/**
 * Created by David Fa on 30/05/2017.
 */

public class JobsContract {

    private JobsContract() {
    }

    public static class JobsEntry implements BaseColumns {
        public static final String TABLE_NAME = "JOBS";
        public static final String jobID = "jobID";
        public static final String jobCode = "jobCode";
        public static final String customer = "customer";
        public static final String contactName = "contactName";
        public static final String contactNumber = "contactNumber";
        public static final String scheduledTime = "scheduledTime";
        public static final String startTime = "startTime";
        public static final String vehicleName = "vehicleName";
        public static final String vehicleID = "vehicleID";
        public static final String driverID = "driverID";
        public static final String driverName = "driverName";
        public static final String jobType = "jobType";
        public static final String fromBase = "fromBase";
        public static final String toBase = "toBase";
        public static final String description = "description";
        public static final String requirement = "requirement";
        public static final String passengers = "passengers";
        public static final String jobStatus = "jobStatus";
        public static final String colorCode = "colorCode";
        public static final String pagedNumber = "pagedNumber";
        public static final String priority = "priority";
    }

    public static final String[] COLUMNS = {
            JobsEntry.jobID,
            JobsEntry.jobCode,
            JobsEntry.customer,
            JobsEntry.contactName,
            JobsEntry.contactNumber,
            JobsEntry.scheduledTime,
            JobsEntry.startTime,
            JobsEntry.vehicleName,
            JobsEntry.vehicleID,
            JobsEntry.driverID,
            JobsEntry.driverName,
            JobsEntry.jobType,
            JobsEntry.fromBase,
            JobsEntry.toBase,
            JobsEntry.description,
            JobsEntry.requirement,
            JobsEntry.passengers,
            JobsEntry.jobStatus,
            JobsEntry.colorCode,
            JobsEntry.pagedNumber,
            JobsEntry.priority
    };

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + JobsEntry.TABLE_NAME + " (" +
                    JobsEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    JobsEntry.jobID + " INTEGER," +
                    JobsEntry.jobCode + " TEXT," +
                    JobsEntry.customer + " TEXT," +
                    JobsEntry.contactName + " TEXT," +
                    JobsEntry.contactNumber + " TEXT," +
                    JobsEntry.scheduledTime + " TEXT," +
                    JobsEntry.startTime + " TEXT," +
                    JobsEntry.vehicleName + " TEXT," +
                    JobsEntry.vehicleID + " TEXT," +
                    JobsEntry.driverID + " TEXT," +
                    JobsEntry.driverName + " TEXT," +
                    JobsEntry.jobType + " TEXT," +
                    JobsEntry.fromBase + " INTEGER," +
                    JobsEntry.toBase + " INTEGER," +
                    JobsEntry.description + " TEXT," +
                    JobsEntry.requirement + " TEXT," +
                    JobsEntry.passengers + " INTEGER," +
                    JobsEntry.jobStatus + " TEXT," +
                    JobsEntry.colorCode + " TEXT," +
                    JobsEntry.pagedNumber + " TEXT," +
                    JobsEntry.priority + " TEXT" +
                    ")";

    public static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + JobsEntry.TABLE_NAME;

    public static Jobs parseCursorToJobs(Cursor cursor) {
        Jobs job = new Jobs();
        job.setJobID(cursor.getInt(cursor.getColumnIndexOrThrow(JobsEntry.jobID)));
        job.setJobCode(cursor.getString(cursor.getColumnIndexOrThrow(JobsEntry.jobCode)));
        job.setCustomer(cursor.getString(cursor.getColumnIndexOrThrow(JobsEntry.customer)));
        job.setContactName(cursor.getString(cursor.getColumnIndexOrThrow(JobsEntry.contactName)));
        job.setContactNumber(cursor.getString(cursor.getColumnIndexOrThrow(JobsEntry.contactNumber)));
        job.setScheduledTime(cursor.getString(cursor.getColumnIndexOrThrow(JobsEntry.scheduledTime)));
        job.setStartTime(cursor.getString(cursor.getColumnIndexOrThrow(JobsEntry.startTime)));
        job.setVehicleName(cursor.getString(cursor.getColumnIndexOrThrow(JobsEntry.vehicleName)));
        job.setVehicleID(cursor.getString(cursor.getColumnIndexOrThrow(JobsEntry.vehicleID)));
        job.setDriverID(cursor.getString(cursor.getColumnIndexOrThrow(JobsEntry.driverID)));
        job.setDriverName(cursor.getString(cursor.getColumnIndexOrThrow(JobsEntry.driverName)));
        job.setJobType(cursor.getString(cursor.getColumnIndexOrThrow(JobsEntry.jobType)));
        job.setFromBase(cursor.getInt(cursor.getColumnIndexOrThrow(JobsEntry.fromBase)));
        job.setToBase(cursor.getInt(cursor.getColumnIndexOrThrow(JobsEntry.toBase)));
        job.setDescription(cursor.getString(cursor.getColumnIndexOrThrow(JobsEntry.description)));
        job.setRequirement(cursor.getString(cursor.getColumnIndexOrThrow(JobsEntry.requirement)));
        job.setPassengers(cursor.getInt(cursor.getColumnIndexOrThrow(JobsEntry.passengers)));
        job.setJobStatus(cursor.getString(cursor.getColumnIndexOrThrow(JobsEntry.jobStatus)));
        job.setColorCode(cursor.getString(cursor.getColumnIndexOrThrow(JobsEntry.colorCode)));
        job.setPagedNumber(cursor.getString(cursor.getColumnIndexOrThrow(JobsEntry.pagedNumber)));
        return job;
    }

    public static Jobs parseFCMDataToJobs(Map messageMap) {
        Jobs job = new Jobs();
        job.setJobID(Integer.valueOf((String) messageMap.get("jobID")));
        job.setJobCode((String) messageMap.get("jobCode"));
        job.setCustomer((String) messageMap.get("customer"));
        job.setContactName((String) messageMap.get("contactName"));
        job.setContactNumber((String) messageMap.get("contactNumber"));
        job.setScheduledTime((String) messageMap.get("scheduledTime"));
        job.setStartTime((String) messageMap.get("startTime"));
        job.setVehicleName((String) messageMap.get("vehicleName"));
        job.setVehicleID((String) messageMap.get("vehicleID"));
        job.setDriverID((String) messageMap.get("driverID"));
        job.setDriverName((String) messageMap.get("driverName"));
        job.setJobType((String) messageMap.get("jobType"));
        job.setFromBase(Integer.valueOf((String) messageMap.get("fromBase")));
        job.setToBase(Integer.valueOf((String) messageMap.get("toBase")));
        job.setDescription((String) messageMap.get("description"));
        job.setRequirement((String) messageMap.get("requirement"));
        job.setPassengers(Integer.valueOf((String) messageMap.get("passengers")));
        job.setJobStatus((String) messageMap.get("jobStatus"));
        job.setColorCode((String) messageMap.get("colorCode"));
        job.setPagedNumber((String) messageMap.get("pagedNumber"));
        return job;
    }

    public static ContentValues setContentValues(Context context, Jobs job) {
        ContentValues values = new ContentValues();
        values.put(JobsEntry.jobID, job.getJobID());
        values.put(JobsEntry.jobCode, job.getJobCode());
        values.put(JobsEntry.customer, job.getCustomer());
        values.put(JobsEntry.contactName, job.getContactName());
        values.put(JobsEntry.contactNumber, job.getContactNumber());
        values.put(JobsEntry.scheduledTime, job.getScheduledTime());
        values.put(JobsEntry.startTime, job.getStartTime());
        values.put(JobsEntry.vehicleName, job.getVehicleName());
        values.put(JobsEntry.vehicleID, job.getVehicleID());
        values.put(JobsEntry.driverID, job.getDriverID());
        values.put(JobsEntry.driverName, job.getDriverName());
        values.put(JobsEntry.jobType, job.getJobType());
        values.put(JobsEntry.fromBase, job.getFromBase());
        values.put(JobsEntry.toBase, job.getToBase());
        values.put(JobsEntry.description, job.getDescription());
        values.put(JobsEntry.requirement, job.getRequirement());
        values.put(JobsEntry.passengers, job.getPassengers());
        values.put(JobsEntry.jobStatus, job.getJobStatus());
        values.put(JobsEntry.colorCode, job.getColorCode());
        values.put(JobsEntry.pagedNumber, job.getPagedNumber());
        values.put(JobsEntry.priority, CommonUtils.getPriority(context, job.getJobStatus()));
        return values;
    }

}
