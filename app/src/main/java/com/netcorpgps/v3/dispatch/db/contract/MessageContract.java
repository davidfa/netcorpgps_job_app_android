package com.netcorpgps.v3.dispatch.db.contract;

import android.content.ContentValues;
import android.database.Cursor;
import android.provider.BaseColumns;

import com.netcorpgps.v3.dispatch.db.bean.Message;

/**
 * Created by David Fa on 18/08/2017.
 */

public class MessageContract {

    private MessageContract() {
    }

    public static class MessageEntry implements BaseColumns {
        public static final String TABLE_NAME = "ChatMessage";
        public static final String sender = "sender";
        public static final String role = "role";
        public static final String time = "time";
        public static final String msg = "msg";
        public static final String companyID = "companyID";
        public static final String vehicleID = "vehicleID";
        public static final String uniqueID = "uniqueID";
        public static final String type = "type";
        public static final String fileUrl = "fileUrl";
        public static final String serverUrl = "serverUrl";
        public static final String isSynced = "isSynced";
        public static final String voiceTime = "voiceTime";
    }

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + MessageEntry.TABLE_NAME + " (" +
                    MessageEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    MessageEntry.sender + " TEXT," +
                    MessageEntry.role + " TEXT," +
                    MessageEntry.time + " TEXT," +
                    MessageEntry.msg + " TEXT," +
                    MessageEntry.companyID + " TEXT," +
                    MessageEntry.vehicleID + " TEXT," +
                    MessageEntry.uniqueID + " TEXT," +
                    MessageEntry.type + " TEXT," +
                    MessageEntry.voiceTime + " TEXT," +
                    MessageEntry.isSynced + " TEXT," +
                    MessageEntry.serverUrl + " TEXT," +
                    MessageEntry.fileUrl + " TEXT" +
                    ")";

    public static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + MessageEntry.TABLE_NAME;

    public static final String[] COLUMNS = {
            MessageEntry.sender,
            MessageEntry.role,
            MessageEntry.time,
            MessageEntry.msg,
            MessageEntry.companyID,
            MessageEntry.vehicleID,
            MessageEntry.uniqueID,
            MessageEntry.type,
            MessageEntry.voiceTime,
            MessageEntry.isSynced,
            MessageEntry.serverUrl,
            MessageEntry.fileUrl
    };

    public static Message parseCursor(Cursor cursor) {
        Message message = new Message();
        message.setSender(cursor.getString(cursor.getColumnIndexOrThrow(MessageEntry.sender)));
        message.setRole(cursor.getString(cursor.getColumnIndexOrThrow(MessageEntry.role)));
        message.setTime(cursor.getString(cursor.getColumnIndexOrThrow(MessageEntry.time)));
        message.setMsg(cursor.getString(cursor.getColumnIndexOrThrow(MessageEntry.msg)));
        message.setCompanyID(cursor.getString(cursor.getColumnIndexOrThrow(MessageEntry.companyID)));
        message.setVehicleID(cursor.getString(cursor.getColumnIndexOrThrow(MessageEntry.vehicleID)));
        message.setUniqueID(cursor.getString(cursor.getColumnIndexOrThrow(MessageEntry.uniqueID)));
        message.setType(cursor.getString(cursor.getColumnIndexOrThrow(MessageEntry.type)));
        message.setVoiceTime(cursor.getString(cursor.getColumnIndexOrThrow(MessageEntry.voiceTime)));
        message.setSynced(cursor.getString(cursor.getColumnIndexOrThrow(MessageEntry.isSynced)));
        message.setFileUrl(cursor.getString(cursor.getColumnIndexOrThrow(MessageEntry.fileUrl)));
        message.setServerUrl(cursor.getString(cursor.getColumnIndexOrThrow(MessageEntry.serverUrl)));

        return message;
    }

    public static ContentValues setContentValues(Message message) {
        ContentValues values = new ContentValues();
        values.put(MessageEntry.sender, message.getSender());
        values.put(MessageEntry.role, message.getRole());
        values.put(MessageEntry.time, message.getTime());
        values.put(MessageEntry.msg, message.getMsg());
        values.put(MessageEntry.companyID, message.getCompanyID());
        values.put(MessageEntry.vehicleID, message.getVehicleID());
        values.put(MessageEntry.uniqueID, message.getUniqueID());
        values.put(MessageEntry.type, message.getType());
        values.put(MessageEntry.fileUrl, message.getFileUrl());
        values.put(MessageEntry.serverUrl, message.getServerUrl());
        values.put(MessageEntry.voiceTime, message.getVoiceTime());
        values.put(MessageEntry.isSynced, message.getSynced());
        return values;
    }
}
