package com.netcorpgps.v3.dispatch.db.contract;

import android.content.ContentValues;
import android.database.Cursor;
import android.provider.BaseColumns;

import com.netcorpgps.v3.dispatch.db.bean.Jobs;
import com.netcorpgps.v3.dispatch.db.bean.Waypoint;

/**
 * Created by David Fa on 1/06/2017.
 */

public class WaypointContract {

    private WaypointContract() {
    }

    public static class WaypointEntry implements BaseColumns {
        public static final String TABLE_NAME = "JobWaypoint";
        public static final String jobID = "jobID";
        public static final String estTime = "estTime";
        public static final String estTimeVal = "estTimeVal";
        public static final String waitTime = "waitTime";
        public static final String estDistance = "estDistance";
        public static final String estDistanceVal = "estDistanceVal";
        public static final String address = "address";
        public static final String deviceDatetimeArrivedAt = "deviceDatetimeArrivedAt";
        public static final String sysDatetimeArrivedAt = "sysDatetimeArrivedAt";
        public static final String deviceTimezoneName = "deviceTimezoneName";
        public static final String deviceTimezoneID = "deviceTimezoneID";
        public static final String base = "base";
    }

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + WaypointEntry.TABLE_NAME + " (" +
                    WaypointEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    WaypointEntry.jobID + " INTEGER," +
                    WaypointEntry.estTime + " TEXT," +
                    WaypointEntry.estTimeVal + " INTEGER," +
                    WaypointEntry.waitTime + " INTEGER," +
                    WaypointEntry.estDistance + " TEXT," +
                    WaypointEntry.estDistanceVal + " INTEGER," +
                    WaypointEntry.address + " TEXT," +
                    WaypointEntry.deviceDatetimeArrivedAt + " TEXT," +
                    WaypointEntry.sysDatetimeArrivedAt + " TEXT," +
                    WaypointEntry.deviceTimezoneName + " TEXT," +
                    WaypointEntry.deviceTimezoneID + " TEXT," +
                    WaypointEntry.base + " INTEGER" +
                    ")";

    public static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + WaypointEntry.TABLE_NAME;

    public static final String[] COLUMNS = {
            WaypointEntry._ID,
            WaypointEntry.jobID,
            WaypointEntry.estTime,
            WaypointEntry.estTimeVal,
            WaypointEntry.waitTime,
            WaypointEntry.estDistance,
            WaypointEntry.estDistanceVal,
            WaypointEntry.address,
            WaypointEntry.deviceDatetimeArrivedAt,
            WaypointEntry.sysDatetimeArrivedAt,
            WaypointEntry.deviceTimezoneName,
            WaypointEntry.deviceTimezoneID,
            WaypointEntry.base
    };

    public static Waypoint parseCursorToWaypoint(Cursor cursor) {
        Waypoint waypoint = new Waypoint();
        waypoint.setId(cursor.getString(cursor.getColumnIndexOrThrow(WaypointEntry._ID)));
        waypoint.setJobID(cursor.getInt(cursor.getColumnIndexOrThrow(WaypointEntry.jobID)));
        waypoint.setEstTime(cursor.getString(cursor.getColumnIndexOrThrow(WaypointEntry.estTime)));
        waypoint.setEstTimeVal(cursor.getInt(cursor.getColumnIndexOrThrow(WaypointEntry.estTimeVal)));
        waypoint.setWaitTime(cursor.getInt(cursor.getColumnIndexOrThrow(WaypointEntry.waitTime)));
        waypoint.setEstDistance(cursor.getString(cursor.getColumnIndexOrThrow(WaypointEntry.estDistance)));
        waypoint.setEstDistanceVal(cursor.getInt(cursor.getColumnIndexOrThrow(WaypointEntry.estDistanceVal)));
        waypoint.setAddress(cursor.getString(cursor.getColumnIndexOrThrow(WaypointEntry.address)));
        waypoint.setDeviceDatetimeArrivedAt(cursor.getString(cursor.getColumnIndexOrThrow(WaypointEntry.deviceDatetimeArrivedAt)));
        waypoint.setSysDatetimeArrivedAt(cursor.getString(cursor.getColumnIndexOrThrow(WaypointEntry.sysDatetimeArrivedAt)));
        waypoint.setDeviceTimezoneName(cursor.getString(cursor.getColumnIndexOrThrow(WaypointEntry.deviceTimezoneName)));
        waypoint.setDeviceTimezoneID(cursor.getString(cursor.getColumnIndexOrThrow(WaypointEntry.deviceTimezoneID)));
        waypoint.setBase(cursor.getInt(cursor.getColumnIndexOrThrow(WaypointEntry.base)));

        return waypoint;
    }

    public static ContentValues setContentValues(Waypoint waypoint) {
        ContentValues values = new ContentValues();
        values.put(WaypointEntry.jobID, waypoint.getJobID());
        values.put(WaypointEntry.estTime, waypoint.getEstTime());
        values.put(WaypointEntry.estTimeVal, waypoint.getEstTimeVal());
        values.put(WaypointEntry.waitTime, waypoint.getWaitTime());
        values.put(WaypointEntry.estDistance, waypoint.getEstDistance());
        values.put(WaypointEntry.estDistanceVal, waypoint.getEstDistanceVal());
        values.put(WaypointEntry.address, waypoint.getAddress());
        values.put(WaypointEntry.deviceDatetimeArrivedAt, waypoint.getDeviceDatetimeArrivedAt());
        values.put(WaypointEntry.sysDatetimeArrivedAt, waypoint.getSysDatetimeArrivedAt());
        values.put(WaypointEntry.deviceTimezoneName, waypoint.getDeviceTimezoneName());
        values.put(WaypointEntry.deviceTimezoneID, waypoint.getDeviceTimezoneID());
        values.put(WaypointEntry.base, waypoint.getBase());

        return values;
    }
}
