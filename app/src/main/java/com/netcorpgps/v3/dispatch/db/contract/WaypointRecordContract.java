package com.netcorpgps.v3.dispatch.db.contract;

import android.content.ContentValues;
import android.database.Cursor;
import android.provider.BaseColumns;

import com.netcorpgps.v3.dispatch.db.bean.Waypoint;
import com.netcorpgps.v3.dispatch.db.bean.WaypointRecord;

import java.util.Map;

/**
 * Created by David Fa on 1/06/2017.
 */

public class WaypointRecordContract {

    private WaypointRecordContract() {
    }

    public static class WaypointRecordEntry implements BaseColumns {
        public static final String TABLE_NAME = "WaypointRecord";
        public static final String jobID = "jobID";
        public static final String address = "address";
        public static final String deviceDatetimeArrivedAt = "deviceDatetimeArrivedAt";
        public static final String sysDatetimeArrivedAt = "sysDatetimeArrivedAt";
        public static final String deviceTimezoneName = "deviceTimezoneName";
        public static final String deviceTimezoneID = "deviceTimezoneID";
        public static final String companyBD = "companyBD";
    }

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + WaypointRecordEntry.TABLE_NAME + " (" +
                    WaypointRecordEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    WaypointRecordEntry.jobID + " INTEGER," +
                    WaypointRecordEntry.address + " TEXT," +
                    WaypointRecordEntry.deviceDatetimeArrivedAt + " TEXT," +
                    WaypointRecordEntry.sysDatetimeArrivedAt + " TEXT," +
                    WaypointRecordEntry.deviceTimezoneName + " TEXT," +
                    WaypointRecordEntry.deviceTimezoneID + " TEXT," +
                    WaypointRecordEntry.companyBD + " INTEGER" +
                    ")";

    public static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + WaypointRecordEntry.TABLE_NAME;

    public static final String[] COLUMNS = {
            WaypointRecordEntry._ID,
            WaypointRecordEntry.jobID,
            WaypointRecordEntry.address,
            WaypointRecordEntry.deviceDatetimeArrivedAt,
            WaypointRecordEntry.sysDatetimeArrivedAt,
            WaypointRecordEntry.deviceTimezoneName,
            WaypointRecordEntry.deviceTimezoneID,
            WaypointRecordEntry.companyBD
    };

    public static WaypointRecord parseCursorToWaypoint(Cursor cursor) {
        WaypointRecord waypoint = new WaypointRecord();
        waypoint.setId(cursor.getString(cursor.getColumnIndexOrThrow(WaypointRecordEntry._ID)));
        waypoint.setJobID(cursor.getString(cursor.getColumnIndexOrThrow(WaypointRecordEntry.jobID)));
        waypoint.setAddress(cursor.getString(cursor.getColumnIndexOrThrow(WaypointRecordEntry.address)));
        waypoint.setDeviceDatetimeArrivedAt(cursor.getString(cursor.getColumnIndexOrThrow(WaypointRecordEntry.deviceDatetimeArrivedAt)));
        waypoint.setSysDatetimeArrivedAt(cursor.getString(cursor.getColumnIndexOrThrow(WaypointRecordEntry.sysDatetimeArrivedAt)));
        waypoint.setDeviceTimezoneName(cursor.getString(cursor.getColumnIndexOrThrow(WaypointRecordEntry.deviceTimezoneName)));
        waypoint.setDeviceTimezoneID(cursor.getString(cursor.getColumnIndexOrThrow(WaypointRecordEntry.deviceTimezoneID)));
        waypoint.setCompanyBD(cursor.getString(cursor.getColumnIndexOrThrow(WaypointRecordEntry.companyBD)));

        return waypoint;
    }

    public static ContentValues setContentValues(Map<String, String> record) {
        ContentValues values = new ContentValues();
        values.put(WaypointRecordEntry.jobID, record.get("jobID"));
        values.put(WaypointRecordEntry.address, record.get("address"));
        values.put(WaypointRecordEntry.deviceDatetimeArrivedAt, record.get("device_datetime"));
        values.put(WaypointRecordEntry.deviceTimezoneName, record.get("device_timezone_name"));
        values.put(WaypointRecordEntry.deviceTimezoneID, record.get("device_timezone_id"));
        values.put(WaypointRecordEntry.sysDatetimeArrivedAt, record.get("sys_datetime"));
        values.put(WaypointRecordEntry.companyBD, record.get("company"));
        return values;
    }
}
