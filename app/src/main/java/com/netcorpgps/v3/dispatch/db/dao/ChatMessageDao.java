package com.netcorpgps.v3.dispatch.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.netcorpgps.v3.dispatch.db.bean.Message;
import com.netcorpgps.v3.dispatch.db.contract.MessageContract;
import com.netcorpgps.v3.dispatch.utils.LogUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by David Fa on 28/06/2017.
 */

public class ChatMessageDao extends SQLiteDao {
    private static final String LOG_TAG = ChatMessageDao.class.getSimpleName();

    public ChatMessageDao(Context context) {
        super(context);
    }

    public List<Message> queryChatMessages(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        List<Message> items = new ArrayList<Message>();
        try {
            db = openDB();
            Cursor cursor = db.query(table, columns, selection, selectionArgs, groupBy, having, orderBy);
            while (cursor.moveToNext()) {
                Message message = MessageContract.parseCursor(cursor);
                items.add(message);
            }
            cursor.close();
        } catch (Exception e) {
            LogUtils.errorLog(LOG_TAG, e.getMessage());
        } finally {
            closeDB();
        }
        return items;
    }

    public long insert(String table, String nullColumnHack, ContentValues values) {
        long id = -1;
        try {
            db = openDB();
            id = db.insert(table, nullColumnHack, values);
        } catch (Exception e) {
            LogUtils.errorLog(LOG_TAG, e.getMessage());
        } finally {
            closeDB();
        }
        return id;
    }

    public int update(String table, ContentValues values, String whereClause, String[] whereArgs) {
        int id = -1;
        try {
            db = openDB();
            id = db.update(table, values, whereClause, whereArgs);
        } catch (Exception e) {
            LogUtils.errorLog(LOG_TAG, e.getMessage());
        } finally {
            closeDB();
        }
        return id;
    }

    public int delete(String table, String whereClause, String[] whereArgs) {
        int rows = -1;
        try {
            db = openDB();
            rows = db.delete(table, whereClause, whereArgs);
        } catch (Exception e) {
            LogUtils.errorLog(LOG_TAG, e.getMessage());
        } finally {
            closeDB();
        }
        return rows;
    }
}
