package com.netcorpgps.v3.dispatch.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.netcorpgps.v3.dispatch.db.bean.Expense;
import com.netcorpgps.v3.dispatch.db.bean.JobStatus;
import com.netcorpgps.v3.dispatch.db.bean.Jobs;
import com.netcorpgps.v3.dispatch.db.bean.Waypoint;
import com.netcorpgps.v3.dispatch.db.bean.WaypointRecord;
import com.netcorpgps.v3.dispatch.db.contract.ExpenseContract;
import com.netcorpgps.v3.dispatch.db.contract.JobStatusContract;
import com.netcorpgps.v3.dispatch.db.contract.JobsContract;
import com.netcorpgps.v3.dispatch.db.contract.WaypointContract;
import com.netcorpgps.v3.dispatch.db.contract.WaypointRecordContract;
import com.netcorpgps.v3.dispatch.utils.LogUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by David Fa on 29/06/2017.
 */

public class JobsDao extends SQLiteDao {
    private static final String LOG_TAG = JobsDao.class.getSimpleName();

    public JobsDao(Context context) {
        super(context);
    }

    /**
     * getting cursor count
     * @param table
     * @param columns
     * @param selection
     * @param selectionArgs
     * @param groupBy
     * @param having
     * @param orderBy
     * @return
     */
    public int getCurCount(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        db = openDB();
        Cursor cursor = db.query(table, columns, selection, selectionArgs, groupBy, having, orderBy);
        int result = cursor.getCount();
        cursor.close();
        closeDB();
        return result;
    }

    public Jobs getJobByID(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        Jobs job = null;
        try {
            db = openDB();
            Cursor cursor = db.query(table, columns, selection, selectionArgs, groupBy, having, orderBy);

            while (cursor.moveToNext()) {
                job = JobsContract.parseCursorToJobs(cursor);
                break;
            }

        } catch (Exception e) {
            LogUtils.errorLog(LOG_TAG, e.getMessage());
        } finally {
            closeDB();
        }
        return job;
    }

    public List<Jobs> queryJobs(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        List<Jobs> items = new ArrayList<Jobs>();
        try {
            db = openDB();
            Cursor cursor = db.query(table, columns, selection, selectionArgs, groupBy, having, orderBy);

            while (cursor.moveToNext()) {
                Jobs job = JobsContract.parseCursorToJobs(cursor);
                items.add(job);
            }
            cursor.close();
            closeDB();
        } catch (Exception e) {
            LogUtils.errorLog(LOG_TAG, e.getMessage());
        } finally {
            closeDB();
        }
        return items;
    }

    public List<Integer> queryJobIds(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        List<Integer> items = new ArrayList<Integer>();
        try {
            db = openDB();
            Cursor cursor = db.query(table, columns, selection, selectionArgs, groupBy, having, orderBy);

            while (cursor.moveToNext()) {
                Jobs job = JobsContract.parseCursorToJobs(cursor);
                items.add(job.getJobID());
            }
            cursor.close();
            closeDB();
        } catch (Exception e) {
            LogUtils.errorLog(LOG_TAG, e.getMessage());
        } finally {
            closeDB();
        }
        return items;
    }

    public Waypoint queryWaypointByID(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        Waypoint waypoint = null;
        try {
            db = openDB();
            Cursor cursor = db.query(table, columns, selection, selectionArgs, groupBy, having, orderBy);
            while (cursor.moveToNext()) {
                waypoint = WaypointContract.parseCursorToWaypoint(cursor);
            }
            closeDB();
        } catch (Exception e) {
            LogUtils.errorLog(LOG_TAG, e.getMessage());
        } finally {
            closeDB();
        }
        return waypoint;
    }

    public List<Waypoint> queryWaypointByJobID(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        List<Waypoint> waypointList = new ArrayList<>();
        try {
            db = openDB();
            Cursor cursor = db.query(table, columns, selection, selectionArgs, groupBy, having, orderBy);
            while (cursor.moveToNext()) {
                Waypoint waypoint = WaypointContract.parseCursorToWaypoint(cursor);
                waypointList.add(waypoint);
            }
            closeDB();
        } catch (Exception e) {
            LogUtils.errorLog(LOG_TAG, e.getMessage());
        } finally {
            closeDB();
        }
        return waypointList;
    }

    public List<Expense> queryExpenseByJobID(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        List<Expense> expenseList = new ArrayList<>();
        try {
            db = openDB();
            Cursor cursor = db.query(table, columns, selection, selectionArgs, groupBy, having, orderBy);
            while (cursor.moveToNext()) {
                Expense expense = ExpenseContract.parseCursorToWaypoint(cursor);
                expenseList.add(expense);
            }
            cursor.close();
        } catch (Exception e) {
            LogUtils.errorLog(LOG_TAG, e.getMessage());
        } finally {
            closeDB();
        }
        return expenseList;
    }

    public String queryMaxSysDatetime(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        String sysDatetime = "";
        try {
            db = openDB();
            Cursor cursor = db.query(table, columns, selection, selectionArgs, groupBy, having, orderBy);
            while (cursor.moveToNext()) {
                sysDatetime = cursor.getString(cursor.getColumnIndexOrThrow("sysDatetime"));
            }
            cursor.close();
        } catch (Exception e) {
            LogUtils.errorLog(LOG_TAG, e.getMessage());
        } finally {
            closeDB();
        }
        return sysDatetime;
    }

    public String queryJobStatusBySysDatetime(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        String jobStatus = "";
        try {
            db = openDB();
            Cursor cursor = db.query(table, columns, selection, selectionArgs, groupBy, having, orderBy);
            while (cursor.moveToNext()) {
                JobStatus js = JobStatusContract.parseCursorToJobs(cursor);
                jobStatus = js.getJobStatus();
            }
            cursor.close();
        } catch (Exception e) {
            LogUtils.errorLog(LOG_TAG, e.getMessage());
        } finally {
            closeDB();
        }
        return jobStatus;
    }

    public List<JobStatus> queryJobStatuses(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        List<JobStatus> jobStatusList = new ArrayList<>();
        try {
            db = openDB();
            Cursor cursor = db.query(table, columns, selection, selectionArgs, groupBy, having, orderBy);
            while (cursor.moveToNext()) {
                JobStatus jobStatus = JobStatusContract.parseCursorToJobs(cursor);
                jobStatusList.add(jobStatus);
            }
            cursor.close();
        } catch (Exception e) {
            LogUtils.errorLog(LOG_TAG, e.getMessage());
        } finally {
            closeDB();
        }
        return jobStatusList;
    }

    public List<WaypointRecord> queryWaypointRecords(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        List<WaypointRecord> waypointRecordList = new ArrayList<>();
        try {
            db = openDB();
            Cursor cursor = db.query(table, columns, selection, selectionArgs, groupBy, having, orderBy);
            while (cursor.moveToNext()) {
                WaypointRecord waypointRecord = WaypointRecordContract.parseCursorToWaypoint(cursor);
                waypointRecordList.add(waypointRecord);
            }
            cursor.close();
        } catch (Exception e) {
            LogUtils.errorLog(LOG_TAG, e.getMessage());
        } finally {
            closeDB();
        }
        return waypointRecordList;
    }

    public WaypointRecord queryWaypointRecord(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        WaypointRecord waypointRecord = null;
        try {
            db = openDB();
            Cursor cursor = db.query(table, columns, selection, selectionArgs, groupBy, having, orderBy);
            while (cursor.moveToNext()) {
                waypointRecord = WaypointRecordContract.parseCursorToWaypoint(cursor);
            }
            cursor.close();
        } catch (Exception e) {
            LogUtils.errorLog(LOG_TAG, e.getMessage());
        } finally {
            closeDB();
        }
        return waypointRecord;
    }

    public long insertJob(String table, String nullColumnHack, ContentValues values) {
        return insert(table, nullColumnHack, values);
    }

    public long insertJobStatus(String table, String nullColumnHack, ContentValues values) {
        return insert(table, nullColumnHack, values);
    }

    public long insertWaypoint(String table, String nullColumnHack, ContentValues values) {
        return insert(table, nullColumnHack, values);
    }

    public long insertExpense(String table, String nullColumnHack, ContentValues values) {
        return insert(table, nullColumnHack, values);
    }

    public long insert(String table, String nullColumnHack, ContentValues values) {
        long id = -1;
        try {
            db = openDB();
            id = db.insert(table, nullColumnHack, values);
        } catch (Exception e) {
            LogUtils.errorLog(LOG_TAG, e.getMessage());
        } finally {
            closeDB();
        }
        return id;
    }

    public int updateJob(String table, ContentValues values, String whereClause, String[] whereArgs) {
        return update(table, values, whereClause, whereArgs);
    }

    public int update(String table, ContentValues values, String whereClause, String[] whereArgs) {
        int id = -1;
        try {
            db = openDB();
            id = db.update(table, values, whereClause, whereArgs);
        } catch (Exception e) {
            LogUtils.errorLog(LOG_TAG, e.getMessage());
        } finally {
            closeDB();
        }
        return id;
    }

    public int deleteJobs(String table, String whereClause, String[] whereArgs) {
        return delete(table, whereClause, whereArgs);
    }

    public int deleteWaypoint(String table, String whereClause, String[] whereArgs) {
        return delete(table, whereClause, whereArgs);
    }

    public int deleteExpense(String table, String whereClause, String[] whereArgs) {
        return delete(table, whereClause, whereArgs);
    }

    public int delete(String table, String whereClause, String[] whereArgs) {
        int rows = -1;
        try {
            db = openDB();
            rows = db.delete(table, whereClause, whereArgs);
        } catch (Exception e) {
            LogUtils.errorLog(LOG_TAG, e.getMessage());
        } finally {
            closeDB();
        }
        return rows;
    }

}
