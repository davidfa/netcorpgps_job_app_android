package com.netcorpgps.v3.dispatch.db.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.netcorpgps.v3.dispatch.db.DbHelper;
import com.netcorpgps.v3.dispatch.utils.LogUtils;

/**
 * Created by David Fa on 29/06/2017.
 */

public class SQLiteDao {

    private static final String LOG_TAG = SQLiteDao.class.getSimpleName();
    protected Context mContext;
    private DbHelper helper = null;
    protected SQLiteDatabase db = null;

    protected SQLiteDao(Context context) {
        this.mContext = context;
    }

    protected SQLiteDatabase openDB() {
        try {
            if (helper == null) {
                helper = new DbHelper(mContext);
            }

            if (db == null || !db.isOpen())
                db = helper.getWritableDatabase();
        } catch (Exception e) {
            LogUtils.errorLog(LOG_TAG, e.getMessage());
        }
        return db;
    }

    protected void closeDB() {
        try {
            if (db != null && db.isOpen()) {
                db.close();
                db = null;
            }

            if (helper != null) {
                helper.close();
                helper = null;
            }
        } catch (Exception e) {
            LogUtils.errorLog(LOG_TAG, e.getMessage());
        }
    }
}
