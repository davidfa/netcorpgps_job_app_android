package com.netcorpgps.v3.dispatch.db.service;

import android.content.ContentValues;
import android.content.Context;

import com.netcorpgps.v3.dispatch.db.bean.Message;
import com.netcorpgps.v3.dispatch.db.contract.MessageContract;
import com.netcorpgps.v3.dispatch.db.dao.ChatMessageDao;

import java.util.Collections;
import java.util.List;

/**
 * Created by David Fa on 29/06/2017.
 */

public class ChatMessageService extends SQLiteService {
    private ChatMessageDao dao;


    public ChatMessageService(Context context) {
        super(context);
        dao = new ChatMessageDao(mContext);
    }

    public List<Message> getChatMessages(String datetime, int num, String companyID, String vehicleID) {
        String selection = MessageContract.MessageEntry.time + " < \"" + datetime + "\" and " + MessageContract.MessageEntry.companyID + " = ? and " + MessageContract.MessageEntry.vehicleID + " = ? ";
        String[] selectionArgs = {companyID, vehicleID};
        String orderBy = MessageContract.MessageEntry.time + " Desc Limit " + num;
        List<Message> list = dao.queryChatMessages(MessageContract.MessageEntry.TABLE_NAME, MessageContract.COLUMNS, selection, selectionArgs, null, null, orderBy);
        Collections.reverse(list);
        return list;
    }

    public long insert(Message message) {
        String selection = MessageContract.MessageEntry.uniqueID + " = ? ";
        String[] selectionArgs = {message.getUniqueID()};
        List<Message> list = dao.queryChatMessages(MessageContract.MessageEntry.TABLE_NAME, MessageContract.COLUMNS, selection, selectionArgs, null, null, null);
        long id = 0;
        if (list.size() == 0) {
            ContentValues values = MessageContract.setContentValues(message);
            id = dao.insert(MessageContract.MessageEntry.TABLE_NAME, null, values);
        }

        return id;
    }

    public int updateSynced(Message message) {
        String whereClause = MessageContract.MessageEntry.uniqueID + " = ? ";
        String[] whereArgs = {message.getUniqueID()};
        ContentValues values = new ContentValues();
        values.put(MessageContract.MessageEntry.isSynced, message.getSynced());
        return dao.update(MessageContract.MessageEntry.TABLE_NAME, values, whereClause, whereArgs);
    }

    public int setSyncedStatusToFalse() {
        String whereClause = MessageContract.MessageEntry.isSynced + " = ? ";
        String[] whereArgs = { null };
        ContentValues values = new ContentValues();
        values.put(MessageContract.MessageEntry.isSynced, "false");
        return dao.update(MessageContract.MessageEntry.TABLE_NAME, values, whereClause, whereArgs);
    }

    public int deleteByDate(String date) {
        String whereClause = MessageContract.MessageEntry.time + " < \"" + date + "\"";
        return dao.delete(MessageContract.MessageEntry.TABLE_NAME, whereClause, null);
    }
}
