package com.netcorpgps.v3.dispatch.db.service;

import android.content.ContentValues;
import android.content.Context;

import com.netcorpgps.v3.dispatch.db.bean.DriverRecord;
import com.netcorpgps.v3.dispatch.db.contract.DriverRecordContract;
import com.netcorpgps.v3.dispatch.db.dao.DriverRecordDao;

import java.util.List;
import java.util.Map;

/**
 * Created by David Fa on 29/06/2017.
 */

public class DriverRecordService extends SQLiteService {
    private DriverRecordDao dao;


    public DriverRecordService (Context context) {
        super(context);
        dao = new DriverRecordDao(mContext);
    }

    public List<DriverRecord> getDriverRecords() {
        return dao.queryDriverRecords(DriverRecordContract.DriverRecordEntry.TABLE_NAME, DriverRecordContract.COLUMNS, null, null, null, null, null);
    }

    public long insert(Map<String, String> record) {
        ContentValues values = DriverRecordContract.setContentValues(record);
        long id = dao.insert(DriverRecordContract.DriverRecordEntry.TABLE_NAME, null, values);
        return id;
    }

    public int delete() {
        return dao.delete(DriverRecordContract.DriverRecordEntry.TABLE_NAME, null, null);
    }

    public int deleteByIDs(String ids) {
        String whereClause = DriverRecordContract.DriverRecordEntry._ID + " in ( " + ids + " )";
        return dao.delete(DriverRecordContract.DriverRecordEntry.TABLE_NAME, whereClause, null);
    }
}
