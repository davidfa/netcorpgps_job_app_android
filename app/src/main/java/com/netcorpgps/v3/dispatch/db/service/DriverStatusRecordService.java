package com.netcorpgps.v3.dispatch.db.service;

import android.content.ContentValues;
import android.content.Context;

import com.netcorpgps.v3.dispatch.db.bean.DriverStatusRecord;
import com.netcorpgps.v3.dispatch.db.contract.DriverStatusRecordContract;
import com.netcorpgps.v3.dispatch.db.dao.DriverStatusRecordDao;

import java.util.List;
import java.util.Map;

/**
 * Created by David Fa on 29/06/2017.
 */

public class DriverStatusRecordService extends SQLiteService {
    private DriverStatusRecordDao dao;


    public DriverStatusRecordService(Context context) {
        super(context);
        dao = new DriverStatusRecordDao(mContext);
    }

    public List<DriverStatusRecord> getDriverRecords() {
        return dao.queryDriverRecords(DriverStatusRecordContract.DriverStatusRecordEntry.TABLE_NAME, DriverStatusRecordContract.COLUMNS, null, null, null, null, null);
    }

    public long insert(Map<String, String> record) {
        ContentValues values = DriverStatusRecordContract.setContentValues(record);
        long id = dao.insert(DriverStatusRecordContract.DriverStatusRecordEntry.TABLE_NAME, null, values);
        return id;
    }

    public int delete() {
        return dao.delete(DriverStatusRecordContract.DriverStatusRecordEntry.TABLE_NAME, null, null);
    }

    public int deleteByIDs(String ids) {
        String whereClause = DriverStatusRecordContract.DriverStatusRecordEntry._ID + " in ( " + ids + " )";
        return dao.delete(DriverStatusRecordContract.DriverStatusRecordEntry.TABLE_NAME, whereClause, null);
    }
}
