package com.netcorpgps.v3.dispatch.db.service;

import android.content.ContentValues;
import android.content.Context;

import com.netcorpgps.v3.dispatch.db.bean.FCMRecord;
import com.netcorpgps.v3.dispatch.db.contract.FCMRecordContract;
import com.netcorpgps.v3.dispatch.db.dao.FCMRecordDao;

import java.util.List;
import java.util.Map;

/**
 * Created by David Fa on 29/06/2017.
 */

public class FCMRecordService extends SQLiteService {
    private FCMRecordDao dao;


    public FCMRecordService(Context context) {
        super(context);
        dao = new FCMRecordDao(mContext);
    }

    public List<FCMRecord> getFCMRecord() {
        return dao.queryFCMRecord(FCMRecordContract.FCMRecordEntry.TABLE_NAME, FCMRecordContract.COLUMNS, null, null, null, null, null);
    }

    public long insert(Map<String, String> record) {
        List<FCMRecord> list = getFCMRecord();
        if (list != null && list.size() > 0) {
            delete();
        }
        ContentValues values = FCMRecordContract.setContentValues(record);
        long id = dao.insert(FCMRecordContract.FCMRecordEntry.TABLE_NAME, null, values);
        return id;
    }

    public int delete() {
        return dao.delete(FCMRecordContract.FCMRecordEntry.TABLE_NAME, null, null);
    }
}
