package com.netcorpgps.v3.dispatch.db.service;

import android.content.ContentValues;
import android.content.Context;
import android.text.TextUtils;

import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.application.NetcorpGPSApplication;
import com.netcorpgps.v3.dispatch.db.bean.Expense;
import com.netcorpgps.v3.dispatch.db.bean.JobStatus;
import com.netcorpgps.v3.dispatch.db.bean.Jobs;
import com.netcorpgps.v3.dispatch.db.bean.Waypoint;
import com.netcorpgps.v3.dispatch.db.bean.WaypointRecord;
import com.netcorpgps.v3.dispatch.db.contract.ExpenseContract;
import com.netcorpgps.v3.dispatch.db.contract.JobStatusContract;
import com.netcorpgps.v3.dispatch.db.contract.JobsContract;
import com.netcorpgps.v3.dispatch.db.contract.WaypointContract;
import com.netcorpgps.v3.dispatch.db.contract.WaypointRecordContract;
import com.netcorpgps.v3.dispatch.db.dao.JobsDao;
import com.netcorpgps.v3.dispatch.utils.CommonUtils;
import com.netcorpgps.v3.dispatch.utils.LogUtils;

import java.util.List;
import java.util.Map;

/**
 * Created by David Fa on 29/06/2017.
 */

public class JobsService extends SQLiteService {

    private static final String LOG_TAG = JobsService.class.getSimpleName();

    private JobsDao dao;

    public JobsService (Context context) {
        super(context);
        dao = new JobsDao(mContext);
    }

    public Jobs getJobByID(String jobID) {
        String selection = JobsContract.JobsEntry.jobID + " = ? ";
        String[] selectionArgs = { String.valueOf(jobID) };
        return dao.getJobByID(JobsContract.JobsEntry.TABLE_NAME, JobsContract.COLUMNS, selection, selectionArgs, null, null, null);
    }

    public Jobs getJobByID(int jobID) {
        return getJobByID(String.valueOf(jobID));
    }

    public List<Jobs> getAvailableJobs() {
        String selection = JobsContract.JobsEntry.jobStatus + " = ? ";
        String[] selectionArgs = {mContext.getString(R.string.job_status_paged)};
        String orderBy = JobsContract.JobsEntry.scheduledTime + " Desc";
        return queryJobs(JobsContract.JobsEntry.TABLE_NAME, JobsContract.COLUMNS, selection, selectionArgs, null, null, orderBy);
    }

    public List<Jobs> getAccepedJobs() {
        String vehicleID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id);
        String selection = JobsContract.JobsEntry.jobStatus + " IN (?,?,?,?)  and " + JobsContract.JobsEntry.vehicleID + " = ? ";
        String[] selectionArgs = {mContext.getString(R.string.job_status_assigned), mContext.getString(R.string.job_status_in_progress), mContext.getString(R.string.job_status_waiting), mContext.getString(R.string.job_status_loading), vehicleID};
        String orderBy = JobsContract.JobsEntry.priority + " asc, " + JobsContract.JobsEntry.scheduledTime + " asc";
        return queryJobs(JobsContract.JobsEntry.TABLE_NAME, JobsContract.COLUMNS, selection, selectionArgs, null, null, orderBy);
    }

    // obtain upcoming job
    public Jobs getCurrentJob() {
        String vehicleID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id);
        String selection = JobsContract.JobsEntry.jobStatus + " IN (?,?,?)  and " + JobsContract.JobsEntry.vehicleID + " = ? ";
        String[] selectionArgs = {mContext.getString(R.string.job_status_in_progress), mContext.getString(R.string.job_status_waiting), mContext.getString(R.string.job_status_loading), vehicleID};
        String orderBy = JobsContract.JobsEntry.priority + " asc, " + JobsContract.JobsEntry.scheduledTime + " asc";
        List<Jobs> jobs = queryJobs(JobsContract.JobsEntry.TABLE_NAME, JobsContract.COLUMNS, selection, selectionArgs, null, null, orderBy);
        if (jobs != null && jobs.size() > 0) {
            return jobs.get(0);
        } else {
            String selection1 = JobsContract.JobsEntry.jobStatus + " = ?  and " + JobsContract.JobsEntry.vehicleID + " = ? ";
            String[] selectionArgs1 = {mContext.getString(R.string.job_status_assigned), vehicleID};
            String orderBy1 = JobsContract.JobsEntry.scheduledTime + " asc";
            List<Jobs> jobs1 = queryJobs(JobsContract.JobsEntry.TABLE_NAME, JobsContract.COLUMNS, selection1, selectionArgs1, null, null, orderBy1);
            if (jobs1 != null && jobs1.size() > 0) {
                return jobs1.get(0);
            }
        }
        return null;
    }

    public List<Jobs> queryJobs(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        return dao.queryJobs(table, columns, selection, selectionArgs, groupBy, having, orderBy);
    }

    public List<Jobs> getComplatedJobs() {
        String vehicleID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id);
        String selection = JobsContract.JobsEntry.jobStatus + " = ?  and " + JobsContract.JobsEntry.vehicleID + " = ? ";
        String[] selectionArgs = {mContext.getString(R.string.job_status_completed), vehicleID};
        String orderBy = JobsContract.JobsEntry.scheduledTime + " Desc";
        List<Jobs> items = dao.queryJobs(JobsContract.JobsEntry.TABLE_NAME, JobsContract.COLUMNS, selection, selectionArgs, null, null, orderBy);
        return items;
    }

    public int getAvaJobsNum() {
        String[] columns = new String[]{JobsContract.JobsEntry.jobID};
        String selection = JobsContract.JobsEntry.jobStatus + " = ? ";
        String[] selectionArgs = {mContext.getString(R.string.job_status_paged)};
        return getCurCount(JobsContract.JobsEntry.TABLE_NAME, columns, selection, selectionArgs, null, null, null);
    }

    public int getAccJobsNum() {
        String vehicleID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id);
        String[] columns = new String[]{JobsContract.JobsEntry.jobID};
        String selection = JobsContract.JobsEntry.jobStatus + " IN (?,?,?,?)  and " + JobsContract.JobsEntry.vehicleID + " = ? ";
        String[] selectionArgs = {mContext.getString(R.string.job_status_assigned), mContext.getString(R.string.job_status_in_progress), mContext.getString(R.string.job_status_waiting), mContext.getString(R.string.job_status_loading), vehicleID};
        return getCurCount(JobsContract.JobsEntry.TABLE_NAME, columns, selection, selectionArgs, null, null, null);
    }

    public int getStartedJobsNum() {
        String vehicleID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id);
        String driverID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_user_id);
        String[] columns = new String[]{JobsContract.JobsEntry.jobID};
        String selection = JobsContract.JobsEntry.jobStatus + " IN (?,?,?)  and " + JobsContract.JobsEntry.vehicleID + " = ? ";
        String[] selectionArgs = {mContext.getString(R.string.job_status_in_progress), mContext.getString(R.string.job_status_waiting), mContext.getString(R.string.job_status_loading), vehicleID};
        return getCurCount(JobsContract.JobsEntry.TABLE_NAME, columns, selection, selectionArgs, null, null, null);
    }

    public int getComJobsNum() {
        String vehicleID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id);
        String[] columns = new String[]{JobsContract.JobsEntry.jobID};
        String selection = JobsContract.JobsEntry.jobStatus + " = ?  and " + JobsContract.JobsEntry.vehicleID + " = ? ";
        String[] selectionArgs = {mContext.getString(R.string.job_status_completed), vehicleID};
        return getCurCount(JobsContract.JobsEntry.TABLE_NAME, columns, selection, selectionArgs, null, null, null);
    }

    public int getCurCount(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        return dao.getCurCount(table, columns, selection, selectionArgs, null, null, null);
    }

    public long insertJob(Jobs job) {
        ContentValues values = JobsContract.setContentValues(mContext, job);
        return dao.insertJob(JobsContract.JobsEntry.TABLE_NAME, null, values);
    }

    public int updateJobStatus(String jobID, String status) {
        String vehicleID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id);
        String vehicleName = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_name);
        String driverID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_user_id);
        String driverName = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_drivername);
        String whereClause = JobsContract.JobsEntry.jobID + " = ? ";
        String[] whereArgs = {jobID};
        ContentValues values = new ContentValues();
        values.put(JobsContract.JobsEntry.vehicleID, vehicleID);
        values.put(JobsContract.JobsEntry.vehicleName, vehicleName);
        values.put(JobsContract.JobsEntry.driverID, driverID);
        values.put(JobsContract.JobsEntry.driverName, driverName);
        values.put(JobsContract.JobsEntry.jobStatus, status);
        return updateJob(JobsContract.JobsEntry.TABLE_NAME, values, whereClause, whereArgs);
    }

    public int updateJob(Jobs job, String jobID) {
        String whereClause = JobsContract.JobsEntry.jobID + " = ? ";
        String[] whereArgs = {String.valueOf(jobID)};
        ContentValues values = JobsContract.setContentValues(mContext, job);
        return dao.updateJob(JobsContract.JobsEntry.TABLE_NAME, values, whereClause, whereArgs);
    }

    public int updateJob(Jobs job, int jobID) {
        return updateJob(job, String.valueOf(jobID));
    }

    public int updateJob(String table, ContentValues values, String whereClause, String[] whereArgs) {
        return dao.updateJob(table, values, whereClause, whereArgs);
    }

    public int deleteAllJobs() {
        return deleteJobs(JobsContract.JobsEntry.TABLE_NAME, null, null);
    }

    public int deleteAvailableJobs() {
        String whereClause = JobsContract.JobsEntry.jobStatus + " = ? ";
        String[] whereArgs = {mContext.getString(R.string.job_status_paged)};
        return deleteJobs(JobsContract.JobsEntry.TABLE_NAME, whereClause, whereArgs);
    }

    public int deleteAcceptedJobs() {
        String vehicleID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id);
        String whereClause = JobsContract.JobsEntry.jobStatus + " IN (?, ?, ?, ?)  and " + JobsContract.JobsEntry.vehicleID + " = ? ";
        String[] whereArgs = {mContext.getString(R.string.job_status_assigned), mContext.getString(R.string.job_status_in_progress), mContext.getString(R.string.job_status_waiting), mContext.getString(R.string.job_status_loading), vehicleID};
        return deleteJobs(JobsContract.JobsEntry.TABLE_NAME, whereClause, whereArgs);
    }

    public int deleteCompletedJobs() {
        String driverID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_user_id);
        String vehicleID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id);
        String whereClause = JobsContract.JobsEntry.jobStatus + " = ?  and " + JobsContract.JobsEntry.vehicleID + " = ? ";
        String[] whereArgs = {mContext.getString(R.string.job_status_completed), vehicleID};
        return deleteJobs(JobsContract.JobsEntry.TABLE_NAME, whereClause, whereArgs);
    }

    public int deleteJobByID(String jobID) {
        String whereClause = JobsContract.JobsEntry.jobID + " = ? ";
        String[] whereArgs = {jobID};
        return deleteJobs(JobsContract.JobsEntry.TABLE_NAME, whereClause, whereArgs);
    }

    public int deleteJobByID(int jobID) {
        return deleteJobByID(String.valueOf(jobID));
    }

    public int deleteJobs(String table, String whereClause, String[] whereArgs) {
        return dao.deleteJobs(table, whereClause, whereArgs);
    }

    public String querySysDatetime(String jobID) {
        String[] columns = {"max(" + JobStatusContract.JobStatusEntry.sysDatetime + ") as sysDatetime"};
        String selection = JobStatusContract.JobStatusEntry.jobID + " = ?";
        String[] selectionArgs = {jobID};
        return dao.queryMaxSysDatetime(JobStatusContract.JobStatusEntry.TABLE_NAME, columns, selection, selectionArgs, null, null, null);
    }

    public String querySysDatetime(int jobID) {
        return querySysDatetime(String.valueOf(jobID));
    }

    public Jobs queryJobStatusBySysDatetime(String jobID, String sysDatetime, Jobs job) {
        String selStatus = JobStatusContract.JobStatusEntry.jobID + " = ?  and " + JobStatusContract.JobStatusEntry.sysDatetime + " = ? ";
        String[] selStatusArgs = {jobID, sysDatetime};
        String jobStatus = dao.queryJobStatusBySysDatetime(JobStatusContract.JobStatusEntry.TABLE_NAME, JobStatusContract.COLUMNS, selStatus, selStatusArgs, null, null, null);
        if (!CommonUtils.isEmpty(jobStatus)) {
            job.setJobStatus(jobStatus);
        }
        return job;
    }

    public List<JobStatus> getJobStatuses() {
        return dao.queryJobStatuses(JobStatusContract.JobStatusEntry.TABLE_NAME, JobStatusContract.COLUMNS, null, null, null, null, null);
    }

    public long insertJobStatus(Map jobStatus) {
        ContentValues values = JobStatusContract.setContentValues(jobStatus);
        return dao.insertJobStatus(JobStatusContract.JobStatusEntry.TABLE_NAME, null, values);
    }

    public int deleteJobStatus() {
        return dao.delete(JobStatusContract.JobStatusEntry.TABLE_NAME, null, null);
    }

    public int deleteJobStatusByIDs(String ids) {
        String whereClause = JobStatusContract.JobStatusEntry._ID + " in ( " + ids + " )";
        return dao.delete(JobStatusContract.JobStatusEntry.TABLE_NAME, whereClause, null);
    }

    /** Operation Waypoint **/
    public int updateWaypoint(Waypoint waypoint) {
        String whereClause = WaypointContract.WaypointEntry._ID + " = ? ";
        String[] whereArgs = {waypoint.getId()};
        ContentValues values = WaypointContract.setContentValues(waypoint);
        return dao.updateJob(WaypointContract.WaypointEntry.TABLE_NAME, values, whereClause, whereArgs);
    }

    public Waypoint getWaypointsByID(String id) {
        String selection = WaypointContract.WaypointEntry._ID + " = ? ";
        String[] selectionArgs = { id };
        Waypoint waypoint = dao.queryWaypointByID(WaypointContract.WaypointEntry.TABLE_NAME, WaypointContract.COLUMNS, selection, selectionArgs, null, null, null);
        return waypoint;
    }

    public List<Waypoint> getWaypointsByJobID(int jobID) {
        return getWaypointsByJobID(String.valueOf(jobID));
    }

    public List<Waypoint> getWaypointsByJobID(String jobID) {
        String selection = WaypointContract.WaypointEntry.jobID + " = ? ";
        String[] selectionArgs = { jobID };
        List<Waypoint> waypointList = dao.queryWaypointByJobID(WaypointContract.WaypointEntry.TABLE_NAME, WaypointContract.COLUMNS, selection, selectionArgs, null, null, null);
        return waypointList;
    }

    public List<Waypoint> getWaypointsWithoutBaseByJobID(int jobID) {
        return getWaypointsWithoutBaseByJobID(String.valueOf(jobID));
    }

    public List<Waypoint> getWaypointsWithoutBaseByJobID(String jobID) {
        String selection = WaypointContract.WaypointEntry.jobID + " = ? and " + WaypointContract.WaypointEntry.base + " = ? ";
        String[] selectionArgs = { jobID, "1" };
        List<Waypoint> waypointList = dao.queryWaypointByJobID(WaypointContract.WaypointEntry.TABLE_NAME, WaypointContract.COLUMNS, selection, selectionArgs, null, null, null);
        return waypointList;
    }

    public List<WaypointRecord> getWaypointRecords() {
        return dao.queryWaypointRecords(WaypointRecordContract.WaypointRecordEntry.TABLE_NAME, WaypointRecordContract.COLUMNS, null, null, null, null, null);
    }

    public long insertWaypointRecord(Map waypointRecord) {
        ContentValues values = WaypointRecordContract.setContentValues(waypointRecord);
        return dao.insert(WaypointRecordContract.WaypointRecordEntry.TABLE_NAME, null, values);
    }

    public int deleteWaypointRecordByID(String ids) {
        String whereClause = WaypointRecordContract.WaypointRecordEntry._ID + " in ( " + ids + " ) ";
        return dao.delete(WaypointRecordContract.WaypointRecordEntry.TABLE_NAME, whereClause, null);
    }

    public long insertWaypoint(Waypoint waypoint) {
        String selection = WaypointRecordContract.WaypointRecordEntry.jobID + " = ? and " + WaypointRecordContract.WaypointRecordEntry.address + " = ? ";
        String[] selectionArgs = {String.valueOf(waypoint.getJobID()), waypoint.getAddress()};
        WaypointRecord waypointRecord = dao.queryWaypointRecord(WaypointRecordContract.WaypointRecordEntry.TABLE_NAME, WaypointRecordContract.COLUMNS, selection, selectionArgs, null, null, null);
        if (waypointRecord != null) {
            waypoint.setDeviceDatetimeArrivedAt(waypointRecord.getDeviceDatetimeArrivedAt());
            waypoint.setSysDatetimeArrivedAt(waypointRecord.getSysDatetimeArrivedAt());
            waypoint.setDeviceTimezoneName(waypointRecord.getDeviceTimezoneName());
            waypoint.setDeviceTimezoneID(waypointRecord.getDeviceTimezoneID());
        }
        ContentValues values = WaypointContract.setContentValues(waypoint);
        return dao.insertWaypoint(WaypointContract.WaypointEntry.TABLE_NAME, null, values);
    }

    public int deleteWaypointByJobID(int jobID) {
        return deleteWaypointByJobID(String.valueOf(jobID));
    }

    public int deleteWaypointByJobID(String jobID) {
        String whereClause = WaypointContract.WaypointEntry.jobID + " = ? ";
        String[] whereArgs = {String.valueOf(jobID)};
        return deleteWaypoint(WaypointContract.WaypointEntry.TABLE_NAME, whereClause, whereArgs);
    }

    public int deleteWaypointOutOfJobID(String jobIDs) {
        String whereClause = WaypointContract.WaypointEntry.jobID + " NOT IN ( " + jobIDs + " )";
        return deleteExpense(WaypointContract.WaypointEntry.TABLE_NAME, whereClause, null);
    }

    public int deleteWaypoint(String table, String whereClause, String[] whereArgs) {
        return dao.deleteWaypoint(table, whereClause, whereArgs);
    }

    /** Operation Expense **/
    public List<Expense> getExpensesByJobID(int jobID) {
        return getExpensesByJobID(String.valueOf(jobID));
    }

    public List<Expense> getExpensesByJobID(String jobID) {
        String selection = ExpenseContract.ExpenseEntry.jobID + " = ? ";
        String[] selectionArgs = { String.valueOf(jobID) };
        List<Expense> expenseList = dao.queryExpenseByJobID(ExpenseContract.ExpenseEntry.TABLE_NAME, ExpenseContract.COLUMNS, selection, selectionArgs, null, null, null);
        return expenseList;
    }

    public long insertExpense(Expense expense) {
        ContentValues values = ExpenseContract.setContentValues(expense);
        return dao.insertExpense(ExpenseContract.ExpenseEntry.TABLE_NAME, null, values);
    }

    public int deleteExpenseByJobID(int jobID) {
        return deleteExpenseByJobID(String.valueOf(jobID));
    }

    public int deleteExpenseByJobID(String jobID) {
        String whereClause = ExpenseContract.ExpenseEntry.jobID + " = ? ";
        String[] whereArgs = {String.valueOf(jobID)};
        return deleteExpense(ExpenseContract.ExpenseEntry.TABLE_NAME, whereClause, whereArgs);
    }

    public int deleteExpenseOutOfJobID(String jobIDs) {
        String whereClause = ExpenseContract.ExpenseEntry.jobID + " NOT IN ( " + jobIDs + " )";
        return deleteExpense(ExpenseContract.ExpenseEntry.TABLE_NAME, whereClause, null);
    }

    public int deleteExpense(String table, String whereClause, String[] whereArgs) {
        return dao.deleteExpense(table, whereClause, whereArgs);
    }

    public void syncWaypointAndExpenseWithJob() {
        List<Integer> idList = dao.queryJobIds(JobsContract.JobsEntry.TABLE_NAME, JobsContract.COLUMNS, null, null, null, null, null);
        String ids = TextUtils.join(",", idList);

        int expenseNums = deleteExpenseOutOfJobID(ids);
        int waypointNums = deleteWaypointOutOfJobID(ids);
        if (expenseNums > 0 || waypointNums > 0) {
            LogUtils.debugLog(LOG_TAG, " job ids " + ids);
            LogUtils.debugLog(LOG_TAG, " expense numbers cleared: " + expenseNums + " waypoint numbers cleared: " + waypointNums);
        }
    }
}
