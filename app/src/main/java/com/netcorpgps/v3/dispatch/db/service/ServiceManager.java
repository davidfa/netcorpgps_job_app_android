package com.netcorpgps.v3.dispatch.db.service;

import android.content.Context;

import com.netcorpgps.v3.dispatch.sync.SyncDriverRecord;
import com.netcorpgps.v3.dispatch.sync.SyncDriverStatusRecord;
import com.netcorpgps.v3.dispatch.sync.SyncFCMRecord;
import com.netcorpgps.v3.dispatch.sync.SyncJobStatus;
import com.netcorpgps.v3.dispatch.sync.SyncWaypointRecord;

/**
 * Created by David Fa on 30/06/2017.
 */

public class ServiceManager {

    private Context mContext;
    private SyncDriverRecord syncDriverRecord;
    private SyncJobStatus syncJobStatus;
    private SyncDriverStatusRecord syncDriverStatusRecord;
    private SyncWaypointRecord syncWaypointRecord;
    private SyncFCMRecord syncFCMRecord;

    public ServiceManager (Context context) {
        mContext = context;
        syncDriverRecord = new SyncDriverRecord(mContext);
        syncJobStatus = new SyncJobStatus(mContext);
        syncDriverStatusRecord = new SyncDriverStatusRecord(mContext);
        syncWaypointRecord = new SyncWaypointRecord(mContext);
        syncFCMRecord = new SyncFCMRecord(mContext);
    }

    public void syncToServer() {
        syncDriverRecord.syncToServer();
        syncJobStatus.syncToServer();
        syncDriverStatusRecord.syncToServer();
        syncWaypointRecord.syncToServer();
        syncFCMRecord.syncToServer();
    }
}
