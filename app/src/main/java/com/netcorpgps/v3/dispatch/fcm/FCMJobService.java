//package com.netcorpgps.v3.dispatch.fcm;
//
//import android.os.AsyncTask;
//
//import com.firebase.jobdispatcher.JobParameters;
//import com.firebase.jobdispatcher.JobService;
//import com.netcorpgps.v3.dispatch.R;
//import com.netcorpgps.v3.dispatch.application.NetcorpGPSApplication;
//import com.netcorpgps.v3.dispatch.utils.CommonUtils;
//import com.netcorpgps.v3.dispatch.utils.LogFileUtils;
//import com.netcorpgps.v3.dispatch.utils.LogUtils;
//
//import java.io.DataOutputStream;
//import java.io.File;
//import java.io.FileInputStream;
//import java.net.HttpURLConnection;
//import java.net.URL;
//
///**
// * Created by David Fa on 13/06/2017.
// */
//
//public class FCMJobService extends JobService {
//    private static final String LOG_TAG = FCMJobService.class.getSimpleName();
//
//    @Override
//    public boolean onStartJob(JobParameters jobParameters) {
//        LogUtils.debugLog(LOG_TAG, "Start uploading log file.");
//        String tag = jobParameters.getTag();
//        if(tag.equalsIgnoreCase("my-job-tag")){
//            UploadingLogFileTask uploadingLogFile = new UploadingLogFileTask();
//            uploadingLogFile.execute("2017-08-03");
//        }
//        return false;
//    }
//
//    @Override
//    public boolean onStopJob(JobParameters jobParameters) {
//        return false;
//    }
//
//    private class UploadingLogFileTask extends AsyncTask<String,Void,Void> {
//
//        public void onPreExecute() {
//        }
//
//        @Override
//        protected Void doInBackground(String[] params) {
//            uploadLogToServer(params[0]);
//            return null;
//        }
//
//        public void onPostExecute(Void unused) {
//        }
//    }
//
//    private void uploadLogToServer(String dirName) {
//
//        int serverResponseCode = 0;
//        String dir = LogFileUtils.getLogStorageRootDir() + File.separator + dirName;
//        File directory = new File(dir);
//        if (directory.exists()) {
//            File log = new File(dir, LogFileUtils.fileName);
//            if (log.exists()) {
//                String companyName = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_name);
//                String vehicleName = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_name);
//                String boundary = "*****";
//                String lineEnd = "\r\n";
//                String twoHyphens = "--";
//                String crlf = "\r\n";
//                int bytesRead, bytesAvailable, bufferSize;
//                byte[] buffer;
//                int maxBufferSize = 1 * 1024 * 1024;
//                String ip = getString(R.string.sys_ip);
//                String api = getString(R.string.api_store_log_file);
//                String urlStr = ip + api;
////                String urlStr = "http://192.168.5.136:8089/uploadfile/UploadToServer.php";
//                HttpURLConnection conn = null;
//                DataOutputStream dos = null;
//                FileInputStream fis = null;
//                try {
//                    fis = new FileInputStream(log);
//                    URL url = new URL(urlStr);
//                    conn = (HttpURLConnection)url.openConnection();
//                    conn.setDoInput(true);
//                    conn.setDoOutput(true);
//                    conn.setUseCaches(false);
//                    conn.setRequestMethod("POST");
//                    conn.setRequestProperty("Connection", "Keep-Alive");
//                    conn.setRequestProperty("ENCTYPE", "multipart/form-data");
//                    conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
//
//                    dos = new DataOutputStream(conn.getOutputStream());
//                    dos.writeBytes(twoHyphens + boundary + lineEnd);
//                    dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_log\"; filename=\"" + LogFileUtils.fileName + "\"" + lineEnd);
//                    dos.writeBytes(lineEnd);
//                    // create a buffer of  maximum size
//                    bytesAvailable = fis.available();
//
//                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
//                    buffer = new byte[bufferSize];
//
//                    // read file and write it into form...
//                    bytesRead = fis.read(buffer, 0, bufferSize);
//                    while (bytesRead > 0) {
//
//                        dos.write(buffer, 0, bufferSize);
//                        bytesAvailable = fis.available();
//                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
//                        bytesRead = fis.read(buffer, 0, bufferSize);
//
//                    }
//                    // send multipart form data necesssary after file data...
//                    dos.writeBytes(lineEnd);
//                    dos.writeBytes(twoHyphens + boundary + lineEnd);
//
//                    // add parameters
//                    dos.writeBytes("Content-Disposition: form-data; name=\"company\"" + crlf);
//                    dos.writeBytes(lineEnd);
//                    dos.writeBytes(companyName);
//                    dos.writeBytes(lineEnd);
//                    dos.writeBytes(twoHyphens + boundary + lineEnd);
//                    dos.writeBytes("Content-Disposition: form-data; name=\"vehicle\"" + crlf);
//                    dos.writeBytes(lineEnd);
//                    dos.writeBytes(vehicleName);
//                    dos.writeBytes(lineEnd);
//                    dos.writeBytes(twoHyphens + boundary + lineEnd);
//                    dos.writeBytes("Content-Disposition: form-data; name=\"dir\"" + crlf);
//                    dos.writeBytes(lineEnd);
//                    dos.writeBytes(dirName);
//                    dos.writeBytes(lineEnd);
//                    dos.writeBytes(twoHyphens + boundary + twoHyphens);
//
//                    // Responses from the server (code and message)
//                    serverResponseCode = conn.getResponseCode();
//                    String serverResponseMessage = conn.getResponseMessage();
//                    LogUtils.infoLog(LOG_TAG, "UploadFile-- HTTP Response is : " + serverResponseMessage + ": " + serverResponseCode);
//                    //close the streams //
//                    fis.close();
//                    dos.flush();
//                    dos.close();
//                    conn.disconnect();
//                } catch (Exception e) {
//                    LogUtils.errorLog(LOG_TAG, "Failure in uploading log file-- " + e.getMessage(), e);
//                } finally {
//                    if (conn != null) {
//                        try {
//                            conn.disconnect();
//                        } catch (Exception e) {
//
//                        }
//                    }
//                    if (fis != null) {
//                        try {
//                            fis.close();
//                        } catch (Exception e) {
//
//                        }
//                    }
//                    if (dos != null) {
//                        try {
//                            dos.close();
//                        } catch (Exception e) {
//
//                        }
//                    }
//                }
//            }
//
//
//        }
//    }
//}
