package com.netcorpgps.v3.dispatch.fcm;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.activity.MainActivity;
import com.netcorpgps.v3.dispatch.api.bean.JobListBean;
import com.netcorpgps.v3.dispatch.api.volley.GsonRequest;
import com.netcorpgps.v3.dispatch.application.NetcorpGPSApplication;
import com.netcorpgps.v3.dispatch.db.service.FCMRecordService;
import com.netcorpgps.v3.dispatch.fragment.AvailableJobListFragment;
import com.netcorpgps.v3.dispatch.utils.CommonUtils;
import com.netcorpgps.v3.dispatch.utils.LogUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by David Fa on 13/06/2017.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String LOG_TAG = MyFirebaseInstanceIDService.class.getSimpleName();

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        LogUtils.debugLog(LOG_TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
//        if(CommonUtils.isConfigured()) {
//            String ip = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_ip);
//            String companyDB = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_db);
//            String uniqueID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_imei, null);
//            String apiSetDeviceToken = getString(R.string.api_set_device_token);
//            String url = ip + apiSetDeviceToken;
//            Map map = new HashMap();
//            map.put("company", companyDB);
//            map.put("imei", uniqueID);
//            map.put("fcmToken", token);
//            GsonRequest jsObjRequest = new GsonRequest<>(Request.Method.POST, url, JobListBean.class, map, null, null, null);
//            jsObjRequest.setShouldCache(false);
//            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(Integer.parseInt(getString(R.string.sys_timeout)), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//            NetcorpGPSApplication.getInstance().getRequestQueue().add(jsObjRequest);
//        }
        FCMRecordService service = new FCMRecordService(getApplicationContext());
        Map record = new HashMap();
        record.put("fcmToken", token);
        service.insert(record);
    }

}
