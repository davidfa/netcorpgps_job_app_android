package com.netcorpgps.v3.dispatch.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.activity.LoginActivity;
import com.netcorpgps.v3.dispatch.activity.MainActivity;
import com.netcorpgps.v3.dispatch.application.NetcorpGPSApplication;
import com.netcorpgps.v3.dispatch.db.bean.Jobs;
import com.netcorpgps.v3.dispatch.db.contract.JobsContract;
import com.netcorpgps.v3.dispatch.db.service.JobsService;
import com.netcorpgps.v3.dispatch.http.HttpHelper;
import com.netcorpgps.v3.dispatch.utils.CommonUtils;
import com.netcorpgps.v3.dispatch.utils.LogFileUtils;
import com.netcorpgps.v3.dispatch.utils.LogUtils;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

/**
 * Created by David Fa on 13/06/2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String LOG_TAG = MyFirebaseMessagingService.class.getSimpleName();

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        LogUtils.debugLog(LOG_TAG, "From: " + remoteMessage.getFrom());
        Map messageMap = remoteMessage.getData();
        // Check if message contains a data payload.
        boolean isNotificated = false;
        if (messageMap.size() > 0) {
            LogUtils.debugLog(LOG_TAG, "Message data payload: " + messageMap);
//            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
//                scheduleJob();
//            } else {
                // Handle message within 10 seconds
//                handleNow();
//            }
            isNotificated = handleNow(messageMap);
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null && isNotificated) {
            LogUtils.debugLog(LOG_TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            // Also if you intend on generating your own notifications as a result of a received FCM
            // message, here is where that should be initiated. See sendNotification method below.
            sendNotification(remoteMessage.getNotification(), messageMap);
        }

    }
    // [END receive_message]

    /**
     * Schedule a job using FirebaseJobDispatcher.
     */
//    private void scheduleJob() {
//        // [START dispatch_job]
//        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
//        Job myJob = dispatcher.newJobBuilder()
//                .setService(FCMJobService.class)
//                .setTag("my-job-tag")
//                .build();
//        dispatcher.schedule(myJob);
//        // [END dispatch_job]
//    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private boolean handleNow(Map messageMap) {


        JobsService jobsService = new JobsService(this);
        String type = (String) messageMap.get("type");

        if (!CommonUtils.isEmpty(type)) {

            if (type.equalsIgnoreCase(getString(R.string.notification_type_paged)) || type.equalsIgnoreCase(getString(R.string.notification_type_assigned))) {

                Jobs job = JobsContract.parseFCMDataToJobs(messageMap);
                long rowId = jobsService.updateJob(job, job.getJobID());
                if (rowId == 0) {
                    rowId = jobsService.insertJob(job);
                }
                LogUtils.debugLog(LOG_TAG, rowId + "");
                return true;
            } else if (type.equalsIgnoreCase(getString(R.string.notification_type_removedJob))) {
                String jobID = (String) messageMap.get("jobID");
                jobsService.deleteJobByID(jobID);
                jobsService.deleteWaypointByJobID(jobID);
                jobsService.deleteExpenseByJobID(jobID);
            } else if (type.equalsIgnoreCase(getString(R.string.notification_type_command))) {
                Intent intent = new Intent(type);
                Bundle bundle = new Bundle();
                String command = (String) messageMap.get(getString(R.string.notification_type_command));
                bundle.putString(type, command);
                intent.putExtras(bundle);
                getApplicationContext().sendBroadcast(intent);
            } else if (type.equalsIgnoreCase(getString(R.string.notification_type_operation))) {
                String operation = (String) messageMap.get(getString(R.string.notification_type_operation));
                if (operation.equalsIgnoreCase("uploadLogFile")) {
                    String dirName = (String) messageMap.get("date");
                    LogUtils.debugLog(LOG_TAG, "operation: " + operation + ", dirName: " + dirName);
                    UploadingLogFileTask uploadingLogFile = new UploadingLogFileTask();
                    uploadingLogFile.execute(dirName);
                } else if (operation.equalsIgnoreCase("freeDevice")) {
                    NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_timezone_name, "");
                    NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_company_db, "");
                    NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_company_id, "");
                    NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_company_name, "");
                    NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_vehicle_id, "");
                    NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_vehicle_name, "");
                    NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_vehicle_name, "");
                    NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_driver_access_token, "");
                    NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_driver_record_id, "");
                    NetcorpGPSApplication.sharedPrefManager.setBoolean(R.string.shared_pref_login_status, false);

                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    LogUtils.debugLog(LOG_TAG, operation);
                    startActivity(intent);
                }

            }
        }

        return false;
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param notification FCM notification body received.
     */
    private void sendNotification(RemoteMessage.Notification notification, Map messageMap) {
        boolean login_status = NetcorpGPSApplication.sharedPrefManager.getBoolean(R.string.shared_pref_login_status);
        String type = (String) messageMap.get("type");
        int jobID = Integer.valueOf((String) messageMap.get("jobID"));
        String vehicleName = (String) messageMap.get("vehicleName");
        Intent intent = null;
        intent = new Intent(this, MainActivity.class);
//        if (login_status) {
//            intent = new Intent(this, MainActivity.class);
//        } else {
//            intent = new Intent(this, LoginActivity.class);
//        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("type", type);
        intent.putExtra("jobID", jobID);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, getApplicationContext().getResources().getInteger(R.integer.pending_intent_request_code_job), intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);

        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.appicon);
//        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Uri job_url = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.notification_new_job);
        long[] pattern = {0, 100, 1000};

        int smallIcon = 0;
        int iconColor = 0;
        if (type != null && type.equalsIgnoreCase(getString(R.string.notification_type_paged))) {
            iconColor = ContextCompat.getColor(getApplicationContext(), R.color.job_status_paged);
            smallIcon = R.drawable.ic_menu_job_available;
        } else if (type != null && type.equalsIgnoreCase(getString(R.string.notification_type_assigned))) {
            iconColor = ContextCompat.getColor(getApplicationContext(), R.color.job_status_assigned);
            smallIcon = R.drawable.ic_menu_job_accepted;
        }
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setVibrate(pattern)
                .setColor(iconColor)
                .setSmallIcon(smallIcon)
                .setContentTitle(notification.getTitle())
                .setContentText(notification.getBody())
                .setSubText(vehicleName)
                .setLargeIcon(largeIcon)
                .setAutoCancel(true)
                .setSound(job_url)
                .setContentIntent(pendingIntent);
//        int numMessages = 0;
//        notificationBuilder.setContentText(notification.getBody()).setNumber(++numMessages);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(getApplicationContext().getResources().getInteger(R.integer.notification_id_job), notificationBuilder.build());
    }

    private class UploadingLogFileTask extends AsyncTask<String,Void,Void> {

        public void onPreExecute() {
        }

        @Override
        protected Void doInBackground(String[] params) {
            uploadLogToServer(params[0]);
            return null;
        }

        public void onPostExecute(Void unused) {
        }
    }

    private void uploadLogToServer(String dirName) {

        int serverResponseCode = 0;
        String dir = LogFileUtils.getLogStorageRootDir() + File.separator + dirName;
        File directory = new File(dir);
        if (directory.exists()) {
            File log = new File(dir, LogFileUtils.fileName);
            if (log.exists()) {
                String companyName = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_name);
                String vehicleName = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_name);
                String boundary = "*****";
                String lineEnd = "\r\n";
                String twoHyphens = "--";
                String crlf = "\r\n";
                int bytesRead, bytesAvailable, bufferSize;
                byte[] buffer;
                int maxBufferSize = 1 * 1024;
                String ip = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_ip);
                String api = getString(R.string.api_store_log_file);
                String urlStr = ip + api;

                LogUtils.debugLog(LOG_TAG, "UploadFile URL: " + urlStr);

                HttpURLConnection conn = null;
                DataOutputStream dos = null;
                FileInputStream fis = null;
                try {
                    fis = new FileInputStream(log);
                    URL url = new URL(urlStr);
                    conn = (HttpURLConnection)url.openConnection();
                    conn.setDoInput(true);
                    conn.setDoOutput(true);
                    conn.setUseCaches(false);
//                    conn.setChunkedStreamingMode(maxBufferSize);
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Connection", "Keep-Alive");
                    conn.setRequestProperty("Accept", "text/*");
//                    conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                    conn.setConnectTimeout(HttpHelper.CONN_TIMEOUT_VALUE);
                    conn.setReadTimeout(HttpHelper.READ_TIMEOUT_VALUE);
                    conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

                    dos = new DataOutputStream(conn.getOutputStream());
                    dos.writeBytes(twoHyphens + boundary + lineEnd);
                    dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_log\"; filename=\"" + LogFileUtils.fileName + "\"" + lineEnd);
                    dos.writeBytes(lineEnd);
                    // create a buffer of  maximum size
                    bytesAvailable = fis.available();

                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    buffer = new byte[bufferSize];

                    // read file and write it into form...
                    bytesRead = fis.read(buffer, 0, bufferSize);
                    while (bytesRead > 0) {
                        try {
                            dos.write(buffer, 0, bufferSize);
                        } catch (OutOfMemoryError e) {
                            LogUtils.debugLog(LOG_TAG, "UploadFile Error: OutOfMemoryError", e);
                        }
//                        dos.write(buffer, 0, bufferSize);
                        bytesAvailable = fis.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        bytesRead = fis.read(buffer, 0, bufferSize);

                    }
                    // send multipart form data necesssary after file data...
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(twoHyphens + boundary + lineEnd);

                    // add parameters
                    dos.writeBytes("Content-Disposition: form-data; name=\"company\"" + crlf);
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(companyName);
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(twoHyphens + boundary + lineEnd);
                    dos.writeBytes("Content-Disposition: form-data; name=\"vehicle\"" + crlf);
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(vehicleName);
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(twoHyphens + boundary + lineEnd);
                    dos.writeBytes("Content-Disposition: form-data; name=\"dir\"" + crlf);
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(dirName);
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(twoHyphens + boundary + twoHyphens);
                    LogUtils.infoLog(LOG_TAG, "UploadFile-- company: " + companyName + ", vehicle: " + vehicleName + ", dir: " + dirName);
                    // Responses from the server (code and message)
                    serverResponseCode = conn.getResponseCode();
                    String serverResponseMessage = conn.getResponseMessage();
                    LogUtils.infoLog(LOG_TAG, "UploadFile-- HTTP Response is : " + serverResponseMessage + ": " + serverResponseCode);
                    //close the streams //
                    fis.close();
                    dos.flush();
                    dos.close();
                    dos = null;
                    fis = null;
                    conn.disconnect();
                } catch (Exception e) {
                    LogUtils.errorLog(LOG_TAG, "Failure in uploading log file-- " + e.getMessage(), e);
                } finally {
                    if (conn != null) {
                        try {
                            conn.disconnect();
                        } catch (Exception e) {

                        }
                    }
                    if (fis != null) {
                        try {
                            fis.close();
                            fis = null;
                        } catch (Exception e) {

                        }
                    }
                    if (dos != null) {
                        try {
                            dos.close();
                            dos = null;
                        } catch (Exception e) {

                        }
                    }

                }
            }


        }
    }
}
