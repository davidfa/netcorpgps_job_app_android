package com.netcorpgps.v3.dispatch.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.activity.MainActivity;
import com.netcorpgps.v3.dispatch.api.bean.DeviceInfoBean;
import com.netcorpgps.v3.dispatch.application.NetcorpGPSApplication;
import com.netcorpgps.v3.dispatch.db.service.JobsService;

public class AboutFragment extends Fragment {

    private final String LOG_TAG = AboutFragment.class.getSimpleName();
    private AppCompatActivity activity;
    private View fragmentView;
    private TextView txtCompany, txtVehicle, txtDeviceName, txtImei, txtOS, txtVersion;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        activity = (AppCompatActivity) getActivity();

        if (fragmentView == null) {
            fragmentView = inflater.inflate(R.layout.fragment_about, container, false);
        }

        txtCompany = (TextView) fragmentView.findViewById(R.id.txtCompany);
        txtVehicle = (TextView) fragmentView.findViewById(R.id.txtVehicle);
        txtDeviceName = (TextView) fragmentView.findViewById(R.id.txtDeviceName);
        txtImei = (TextView) fragmentView.findViewById(R.id.txtImei);
        txtOS = (TextView) fragmentView.findViewById(R.id.txtOS);
        txtVersion = (TextView) fragmentView.findViewById(R.id.txtVersion);

        return fragmentView;
    }

    public void onStart() {
        super.onStart();
        closeSysMsgLayout();
        checkNetwork();
        setActivityTitle();
        setVariables();
    }

    public void onResume() {
        if (activity instanceof MainActivity) {
            ((MainActivity)activity).showFab();
        }
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void setActivityTitle() {
        if (activity instanceof MainActivity) {
            ((MainActivity)activity).setActionBarTitle(getString(R.string.menu_about));
        }
    }

    private void setVariables() {
        String company = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_name);
        String vehicle = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_name);
        String imei = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_imei);
        txtCompany.setText(company);
        txtVehicle.setText(vehicle);
        DeviceInfoBean deviceInfo = NetcorpGPSApplication.getInstance().getDeviceInformation();
        txtDeviceName.setText(deviceInfo.getDeviceName());
        txtImei.setText(imei);
        txtOS.setText(deviceInfo.getOs());
        txtVersion.setText(deviceInfo.getVersion());
    }

    private void checkNetwork() {
        if(!isConnected()) {
            showSysMessage(getString(R.string.msg_error_no_network_available));
        }
    }

    private boolean isConnected() {
        if(activity instanceof MainActivity){
            return ((MainActivity) activity).isConnected();
        }
        return false;
    }

    private void showSysMessage(String message) {
        if(activity instanceof MainActivity){
            ((MainActivity) activity).setSystemMessage(message);
        }
    }

    private void closeSysMsgLayout() {
        if(activity instanceof MainActivity){
            ((MainActivity) activity).closeSysMsgLayout();
        }
    }
}
