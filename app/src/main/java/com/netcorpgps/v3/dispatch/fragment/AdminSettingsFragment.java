package com.netcorpgps.v3.dispatch.fragment;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.iid.FirebaseInstanceId;
import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.activity.MainActivity;
import com.netcorpgps.v3.dispatch.activity.SlideInActivity;
import com.netcorpgps.v3.dispatch.api.ErrorHandler.VolleyErrorHandler;
import com.netcorpgps.v3.dispatch.api.bean.AdminBean;
import com.netcorpgps.v3.dispatch.api.bean.DeviceInfoBean;
import com.netcorpgps.v3.dispatch.api.bean.SettingBean;
import com.netcorpgps.v3.dispatch.api.bean.VehiclesBean;
import com.netcorpgps.v3.dispatch.api.volley.GsonRequest;
import com.netcorpgps.v3.dispatch.application.NetcorpGPSApplication;
import com.netcorpgps.v3.dispatch.db.service.JobsService;
import com.netcorpgps.v3.dispatch.receiver.NetworkStateReceiver;
import com.netcorpgps.v3.dispatch.utils.CommonUtils;
import com.netcorpgps.v3.dispatch.utils.LogUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AdminSettingsFragment extends Fragment {
    private final String LOG_TAG = AdminSettingsFragment.class.getSimpleName();
    private AdminBean admin;
    private TextView txt_company;
    private TextView txt_company_name;
    private RelativeLayout row_company;
    private TextView txt_vehicle;
    private TextView txt_vehicle_name;
    private RelativeLayout row_vehicle;
    private AdminBean.Company company = null;
    private VehiclesBean.Vehicle vehicle = null;
    private AdminSettingBroadcastReceiver receiver;
    private boolean isAdminReceiverRegistered = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings_admin, container, false);
        setHasOptionsMenu(true);
        admin = (AdminBean) getActivity().getIntent().getSerializableExtra("admin");
        Activity activity = getActivity();
        if (activity instanceof MainActivity) {
            ((MainActivity) activity).setActionBarTitle("Setting");
        }
        row_company = (RelativeLayout) view.findViewById(R.id.row_company);
        txt_company = (TextView) view.findViewById(R.id.txt_company);
        txt_company_name = (TextView) view.findViewById(R.id.txt_company_name);

        String companyDB = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_db);
        String companyName = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_name);
        String vehicleID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id);
        String vehicleName = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_name);
        if (!CommonUtils.isEmpty(companyDB) && !CommonUtils.isEmpty(companyName)) {
            txt_company_name.setText(companyName);
            List<AdminBean.Company> companies = admin.getCompanyList();
            for (AdminBean.Company cpy : companies) {
                if (cpy.getDatabase().equalsIgnoreCase(companyDB)) {
                    company = cpy;
                    break;
                }
            }
        }

        row_vehicle = (RelativeLayout) view.findViewById(R.id.row_vehicle);
        txt_vehicle = (TextView) view.findViewById(R.id.txt_vehicle);
        txt_vehicle_name = (TextView) view.findViewById(R.id.txt_vehicle_name);

        if (!CommonUtils.isEmpty(vehicleID) && !CommonUtils.isEmpty(vehicleName)) {
            txt_vehicle_name.setText(vehicleName);
        }

        txt_company_name.addTextChangedListener(new CompanyChanged());

        row_company.setOnClickListener(new OpenCompanySelectionActivity());
        row_vehicle.setOnClickListener(new OpenVehicleSelectionActivity());

//        AdminSettingBroadcastReceiver receiver = new AdminSettingBroadcastReceiver();
//        getActivity().registerReceiver(receiver, new IntentFilter(AdminSettingBroadcastReceiver.ACTION));
        return view;
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerAdminReceiver();
    }


    public void onDestroy() {
        unregisterAdminReceiver();
        super.onDestroy();
    }

    private void registerAdminReceiver() {
        if (!isAdminReceiverRegistered) {
            receiver = new AdminSettingBroadcastReceiver();
            getActivity().registerReceiver(receiver, new IntentFilter(AdminSettingBroadcastReceiver.ACTION));
            isAdminReceiverRegistered = true;
        }
    }

    private void unregisterAdminReceiver() {
        if (receiver != null){
            getActivity().unregisterReceiver(receiver);
            isAdminReceiverRegistered = false;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.menu_action_save, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_save:
                String fcmToken = FirebaseInstanceId.getInstance().getToken();
                String companyName = txt_company_name.getText().toString();
                if (CommonUtils.isEmpty(companyName)) {
                    CommonUtils.toast(getActivity(), getString(R.string.msg_select_company));
                    return false;
                }
                String vehicleName = txt_vehicle_name.getText().toString();
                if (CommonUtils.isEmpty(vehicleName)) {
                    CommonUtils.toast(getActivity(), getString(R.string.msg_select_vehicle));
                    return false;
                }
                String companyDB = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_db);
                if (company != null) {
                    companyDB = company.getDatabase();
                }
                String vehicleID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id);
                if (vehicle != null) {
                    vehicleID = String.valueOf(vehicle.getId());
                }

                String pre_company = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_db);
                String pref_vehicle = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id);
                String pref_device = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_device_id);
//                if (pre_company.equals(companyDB) && pref_vehicle.equals(vehicleID)) {
//                    CommonUtils.toast(getActivity(), getString(R.string.msg_admin_setting_success));
//                    return false;
//                }
                Map<String, String> map = new HashMap();
                DeviceInfoBean deviceInfo = NetcorpGPSApplication.getInstance().getDeviceInformation();
                String deviceName = deviceInfo.getDeviceName();
                String model = deviceInfo.getModel();
                String manufacturer = deviceInfo.getManufacturer();
                String version = deviceInfo.getVersion();
                String os = deviceInfo.getOs();
                String ip = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_ip);
                String imei = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_imei);
                String apiSaveDeviceInfo = getString(R.string.api_save_device_info);
                String url = ip + apiSaveDeviceInfo;
                map.put("deviceName", deviceName);
                map.put("imei", imei);
                map.put("model", model);
                map.put("manufacturer", manufacturer);
                map.put("version", version);
                map.put("os", os);
                map.put("created_at", CommonUtils.getCurrentTime());
                map.put("pre_company", pre_company);
                map.put("company", companyDB);
                map.put("pre_vehicle", pref_vehicle);
                map.put("vehicle", vehicleID);
                map.put("operator", String.valueOf(admin.getId()));
                map.put("pre_device", pref_device);
                map.put("fcmToken", fcmToken);

                GsonRequest<SettingBean> jsObjRequest = new GsonRequest<>(Request.Method.POST, url, SettingBean.class, map, null, new SaveDeviceInfoListener(), new SaveDeviceInfoErrorListener());
                jsObjRequest.setShouldCache(false);
                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(Integer.parseInt(getString(R.string.sys_timeout)), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                jsObjRequest.setTag(getString(R.string.request_tag_save_device_info));
                NetcorpGPSApplication.getInstance().getRequestQueue().add(jsObjRequest);
                return true;

            default:
                break;
        }

        return false;
    }

    class OpenCompanySelectionActivity implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            AdminBean admin = (AdminBean) getActivity().getIntent().getSerializableExtra("admin");
            String companyName = txt_company_name.getText().toString();
            Intent intent = new Intent(getActivity(), SlideInActivity.class);
            intent.putExtra("fragment", "company");
            intent.putExtra("admin", admin);
            intent.putExtra("companyName", companyName);
            startActivity(intent);
        }
    }

    class OpenVehicleSelectionActivity implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            String companyName = txt_company_name.getText().toString();
            if (CommonUtils.isEmpty(companyName)) {
                CommonUtils.toast(getActivity(), getString(R.string.msg_select_company));
            } else {
                String companyDB = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_db);
                if (company != null) {
                    companyDB = company.getDatabase();
                }
                Intent intent = new Intent(getActivity(), SlideInActivity.class);
                intent.putExtra("fragment", "vehicle");
                String vehicleName = txt_vehicle_name.getText().toString();
                intent.putExtra("vehicleName", vehicleName);
                intent.putExtra("companyDB", companyDB);
                startActivity(intent);
            }
        }
    }

    class SaveDeviceInfoListener implements Response.Listener<SettingBean> {
        @Override
        public void onResponse(SettingBean response) {
            try {
                if (response != null) {

                    if (response.getStatus() == 1) {
                        NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_timezone_name, company.getTimezoneName());
                        NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_company_db, response.getCompany_database());
                        NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_company_id, company.getId());
                        NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_company_name, txt_company_name.getText().toString());
                        NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_vehicle_id, response.getVehicle_id());
                        NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_vehicle_name, txt_vehicle_name.getText().toString());
                        NetcorpGPSApplication.sharedPrefManager.setString(R.string.shared_pref_device_id, response.getDevice_id());
                        CommonUtils.toast(getActivity(), getString(R.string.msg_admin_setting_success));
                    } else {
                        CommonUtils.toast(getActivity(), response.getMsg());
                        LogUtils.debugLog(LOG_TAG, response.getMsg());
                    }
                } else {
                    CommonUtils.toast(getActivity(), response.getMsg());
                    LogUtils.debugLog(LOG_TAG, response.getMsg());
                }
            } catch (Exception e) {
                CommonUtils.toast(getActivity(), e.getMessage());
                LogUtils.debugLog(LOG_TAG, e.getMessage());
            } finally {
            }
        }
    }

    class SaveDeviceInfoErrorListener implements Response.ErrorListener {
        @Override
        public void onErrorResponse(VolleyError error) {
            String message = VolleyErrorHandler.handleError(getActivity(), error);

            CommonUtils.toast(getActivity(), message);
            LogUtils.errorLog(LOG_TAG, error.getMessage());
        }
    }

    class CompanyChanged implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String companyNameChanged = txt_company_name.getText().toString();
            String companyName = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_name);
            if (!CommonUtils.isEmpty(companyNameChanged) && !CommonUtils.isEmpty(companyName) && companyNameChanged.equals(companyName)) {
                String vehicleName = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_name);
                txt_vehicle_name.setText(vehicleName);
            } else {
                txt_vehicle_name.setText("");
            }
            vehicle = null;
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

    public class AdminSettingBroadcastReceiver extends BroadcastReceiver {
        public static final String ACTION = "com.broadcast.action.select_company";

        @Override
        public void onReceive(Context context, Intent intent) {
            String type = intent.getStringExtra("type");
            if ("company".equals(type)) {
                company = (AdminBean.Company) intent.getSerializableExtra("company");
                if (!txt_company_name.getText().toString().equals(company.getName())) {
                    txt_company_name.setText(company.getName());
                }
            } else if ("vehicle".equals(type)) {
                vehicle = (VehiclesBean.Vehicle) intent.getSerializableExtra("vehicle");
                txt_vehicle_name.setText(vehicle.getName());
            }
        }
    }
}
