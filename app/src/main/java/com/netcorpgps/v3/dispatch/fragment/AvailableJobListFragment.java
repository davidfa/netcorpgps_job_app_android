package com.netcorpgps.v3.dispatch.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.activity.JobDetailActivity;
import com.netcorpgps.v3.dispatch.activity.MainActivity;
import com.netcorpgps.v3.dispatch.adapter.JobMenuItemAdapter;
import com.netcorpgps.v3.dispatch.api.ErrorHandler.VolleyErrorHandler;
import com.netcorpgps.v3.dispatch.api.bean.JobListBean;
import com.netcorpgps.v3.dispatch.api.bean.ResponseBean;
import com.netcorpgps.v3.dispatch.api.volley.GsonRequest;
import com.netcorpgps.v3.dispatch.application.NetcorpGPSApplication;
import com.netcorpgps.v3.dispatch.component.layout.SwipeLayout;
import com.netcorpgps.v3.dispatch.db.bean.Jobs;
import com.netcorpgps.v3.dispatch.db.service.JobsService;
import com.netcorpgps.v3.dispatch.utils.CommonUtils;
import com.netcorpgps.v3.dispatch.utils.LocationUtils;
import com.netcorpgps.v3.dispatch.utils.LogUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AvailableJobListFragment extends Fragment {

    private final String LOG_TAG = AvailableJobListFragment.class.getSimpleName();
    private SwipeRefreshLayout swipeRefreshLayout = null;
    RecyclerView recyclerView = null;
    private JobMenuItemAdapter jobMenuItemAdapter = null;
    private AppCompatActivity activity;
    private JobsService jobsService = null;
    private View fragmentView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (activity == null)
            activity = (AppCompatActivity) getActivity();

        if (fragmentView == null) {
            fragmentView = inflater.inflate(R.layout.fragment_job_list, container, false);
        }

        if (swipeRefreshLayout == null) {
            swipeRefreshLayout = (SwipeRefreshLayout) fragmentView.findViewById(R.id.swipeRefreshLayout);
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshListener());
            swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        }
        if (recyclerView == null) {
            recyclerView = (RecyclerView) fragmentView.findViewById(R.id.rvJobs);
            recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        }

        return fragmentView;
    }

    public void onStart() {
        super.onStart();
        if (jobsService == null)
            jobsService = new JobsService(getActivity());
        closeSysMsgLayout();
        checkNetwork();
        setActivityTitle();
        bindingListItems();
        refreshItems();
    }

    public void onResume() {
        if (activity instanceof MainActivity) {
            ((MainActivity)activity).showFab();
        }
        super.onResume();
    }

    public void bindingListItems() {
        List<Jobs> items = jobsService.getAvailableJobs();
        onItemsLoadComplete(items);
    }

    public void refreshItems() {
        if (!isConnected()) {
            swipeRefreshLayout.setRefreshing(false);
            return;
        }
        String ip = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_ip);
        String vehicleID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id);
        String companyDB = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_db);
        String accessToken = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_driver_access_token);
        String driverRecordID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_driver_record_id);
        String driverID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_user_id);
        String deviceID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_device_id);
        String apiGetAvailableJobList = getString(R.string.api_get_available_jobList);
        String url = ip + apiGetAvailableJobList;
        Map map = new HashMap();
        map.put("company", companyDB);
        map.put("vehicleID", vehicleID);
        map.put("driverRecordID", driverRecordID);
        map.put("accessToken", accessToken);
        map.put("driverID", driverID);
        map.put("deviceID", deviceID);
        LogUtils.infoLog(LOG_TAG, url + ": " + map.toString());
        GsonRequest<JobListBean> jsObjRequest = new GsonRequest<>(Request.Method.POST, url, JobListBean.class, map, null, new GetJobListListener(), new ErrorListener());
        jsObjRequest.setShouldCache(false);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(Integer.parseInt(getString(R.string.sys_timeout)), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsObjRequest.setTag(getString(R.string.request_tag_get_jobs));
        NetcorpGPSApplication.getInstance().getRequestQueue().add(jsObjRequest);
    }

    private void setActivityTitle() {
        if (activity instanceof MainActivity) {
            ((MainActivity)activity).setActionBarTitle(getString(R.string.menu_available_jobs));
        }
    }

    private void checkNetwork() {
        if(!isConnected()) {
            showSysMessage(getString(R.string.msg_error_no_network_available));
        }
    }

    private boolean isConnected() {
        if(activity instanceof MainActivity){
            return ((MainActivity) activity).isConnected();
        }
        return false;
    }

    private void showSysMessage(String message) {
        if(activity instanceof MainActivity){
            ((MainActivity) activity).setSystemMessage(message);
        }
    }

    private void closeSysMsgLayout() {
        if(activity instanceof MainActivity){
            ((MainActivity) activity).closeSysMsgLayout();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    class GetJobListListener implements Response.Listener<JobListBean> {

        @Override
        public void onResponse(JobListBean response) {
            try {
                if (response != null) {
                    jobsService.deleteAvailableJobs();
                    if (response.getStatus() == 1) {

                        for (Jobs job : response.getJobList()) {
                            long newRowId = jobsService.insertJob(job);
                        }
                        jobsService.syncWaypointAndExpenseWithJob();
                    }
                    closeSysMsgLayout();
                    LogUtils.debugLog(LOG_TAG, response.getMsg());
                }
            } catch (Exception e) {
                showSysMessage(e.getMessage());
                LogUtils.errorLog(LOG_TAG, e.getMessage(), e);

            } finally {
                bindingListItems();
            }
        }
    }

    class ChangeJobStatusListener implements Response.Listener<ResponseBean> {

        private String mJobID;
        private String mTag;

        public ChangeJobStatusListener(String jobID, String tag) {
            mJobID = jobID;
            mTag = tag;
        }

        @Override
        public void onResponse(ResponseBean response) {
            try {
                if (response != null) {
                    if (response.getStatus() == 1) {
                        if (mTag.equals(getString(R.string.request_tag_to_accept_job))) {
                            String jobStatus = getString(R.string.job_status_assigned);
                            jobsService.updateJobStatus(mJobID, jobStatus);
                            closeSysMsgLayout();
                            CommonUtils.toast(getActivity(), response.getMsg());
                        } else if (mTag.equals(getString(R.string.request_tag_to_reject_job))) {
                            jobsService.deleteJobByID(mJobID);
                            closeSysMsgLayout();
                            CommonUtils.toast(getActivity(), getString(R.string.msg_success));
                        }

                    } else {
                        CommonUtils.toast(getActivity(), response.getMsg());
                        LogUtils.errorLog(LOG_TAG, response.getMsg());
                    }
                }
            } catch (Exception e) {
                showSysMessage(e.getMessage());
                LogUtils.errorLog(LOG_TAG, e.getMessage(), e);

            } finally {
                bindingListItems();
                dismissPDialog();
            }
        }
    }

    class ErrorListener implements Response.ErrorListener {
        @Override
        public void onErrorResponse(VolleyError error) {
            LogUtils.errorLog(LOG_TAG, error.getMessage(), error);
            String message = VolleyErrorHandler.handleError(activity, error);

            showSysMessage(message);
            bindingListItems();
            dismissPDialog();
        }
    }

    public class SwipeRefreshListener implements SwipeRefreshLayout.OnRefreshListener {

        @Override
        public void onRefresh() {
            refreshItems();
        }
    }

    void onItemsLoadComplete(List<Jobs> items) {
        jobMenuItemAdapter = new JobMenuItemAdapter(getActivity(), items, new OnItemClickListener());
        recyclerView.setAdapter(jobMenuItemAdapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    SwipeLayout preLayout = jobMenuItemAdapter.getPreLayout();
                    if (preLayout != null) {
                        preLayout.close();
                    }
                }
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    if(activity instanceof MainActivity){
                        ((MainActivity) activity).showFab();
                    }
                } else {
                    if(activity instanceof MainActivity){
                        ((MainActivity) activity).hideFab();
                    }
                }

                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        swipeRefreshLayout.setRefreshing(false);
    }

    class OnItemClickListener implements JobMenuItemAdapter.OnItemClickListener {
        @Override
        public void onCompleted(Jobs item, int position) {

        }

        @Override
        public void onAccepted(Jobs item, int position) {
            openPDialog();

            String ip = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_ip);
            String vehicleID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id);
            String companyDB = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_db);
            String accessToken = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_driver_access_token);
            String driverRecordID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_driver_record_id);
            String driverID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_user_id);
            String deviceID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_device_id);
            String apiToAcceptJob = getString(R.string.api_to_accept_job);
            String url = ip + apiToAcceptJob;
            String jobID = String.valueOf(item.getJobID());
            Map map = new HashMap();
            map.put("company", companyDB);
            map.put("vehicleID", vehicleID);
            map.put("driverRecordID", driverRecordID);
            map.put("accessToken", accessToken);
            map.put("driverID", driverID);
            map.put("deviceID", deviceID);
            map.put("jobID", jobID);
            map.put("device_datetime", CommonUtils.getCurrentTime());
            map.put("device_timezone_name", CommonUtils.getTimeZoneName());
            map.put("device_timezone_id", CommonUtils.getTimeZoneID());
            map.put("sys_datetime", CommonUtils.getSysTime());
            String coordinate = "";
            String address = "";
            // check whether latest location is available
            if (LocationUtils.isNewLocationAvailable()) {
                coordinate = LocationUtils.getCoordinate();
                address = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_gms_location_address);
            }
            map.put("device_coordinate", coordinate);
            map.put("device_address", address);

            GsonRequest<ResponseBean> jsObjRequest = new GsonRequest<>(Request.Method.POST, url, ResponseBean.class, map, null, new ChangeJobStatusListener(jobID, getString(R.string.request_tag_to_accept_job)), new ErrorListener());
            jsObjRequest.setShouldCache(false);
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(Integer.parseInt(getString(R.string.sys_timeout)), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            jsObjRequest.setTag(getString(R.string.request_tag_to_accept_job));
            NetcorpGPSApplication.getInstance().getRequestQueue().add(jsObjRequest);
        }

        @Override
        public void onStarted(Jobs item, int position) {

        }

        @Override
        public void onRejected(Jobs item, int position) {
            openPDialog();

            String ip = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_ip);
            String vehicleID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id);
            String companyDB = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_db);
            String accessToken = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_driver_access_token);
            String driverRecordID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_driver_record_id);
            String driverID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_user_id);
            String deviceID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_device_id);
            String apiToRejectJob = getString(R.string.api_to_reject_job);
            String url = ip + apiToRejectJob;
            String jobID = String.valueOf(item.getJobID());
            Map map = new HashMap();
            map.put("company", companyDB);
            map.put("vehicleID", vehicleID);
            map.put("driverRecordID", driverRecordID);
            map.put("accessToken", accessToken);
            map.put("driverID", driverID);
            map.put("deviceID", deviceID);
            map.put("jobID", jobID);
            map.put("device_datetime", CommonUtils.getCurrentTime());
            map.put("device_timezone_name", CommonUtils.getTimeZoneName());
            map.put("device_timezone_id", CommonUtils.getTimeZoneID());
            map.put("sys_datetime", CommonUtils.getSysTime());
            String coordinate = "";
            String address = "";
            // check whether latest location is available
            if (LocationUtils.isNewLocationAvailable()) {
                coordinate = LocationUtils.getCoordinate();
                address = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_gms_location_address);
            }
            map.put("device_coordinate", coordinate);
            map.put("device_address", address);
            GsonRequest<ResponseBean> jsObjRequest = new GsonRequest<>(Request.Method.POST, url, ResponseBean.class, map, null, new ChangeJobStatusListener(jobID, getString(R.string.request_tag_to_reject_job)), new ErrorListener());
            jsObjRequest.setShouldCache(false);
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(Integer.parseInt(getString(R.string.sys_timeout)), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            jsObjRequest.setTag(getString(R.string.request_tag_to_reject_job));
            NetcorpGPSApplication.getInstance().getRequestQueue().add(jsObjRequest);
        }

        @Override
        public void onItemClick(Jobs item) {
            SwipeLayout preLayout = jobMenuItemAdapter.getPreLayout();
            if (preLayout != null && preLayout.isOpen()) {
                preLayout.close();
            } else {
                startJobDetailActivity(item.getJobID());
            }
        }
    }

    public void startJobDetailActivity(int jobID) {
        if (activity instanceof MainActivity) {
            ((MainActivity)activity).hideFab();
        }
        Intent intent = new Intent(getActivity(), JobDetailActivity.class);
        intent.putExtra("jobID", jobID);
        startActivity(intent);
    }

//    public void deleteAdapterItem(final int position) {
//        try {
//            new Thread() {
//                public void run() {
//                    getActivity().runOnUiThread(new Runnable() {
//
//                        @Override
//                        public void run() {
//                            jobMenuItemAdapter.deleteItem(position);
//                        }
//                    });
//                }
//            }.start();
//        } catch (Exception e) {
//            LogUtils.errorLog(LOG_TAG, e.getMessage(), e);
//        }
//    }

    private void openPDialog() {
        if(activity instanceof MainActivity){
            ((MainActivity) activity).openPDialog();
        }
    }

    private void dismissPDialog() {
        if(activity instanceof MainActivity){
            ((MainActivity) activity).dismissPDialog();
        }
    }
}
