package com.netcorpgps.v3.dispatch.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.activity.SlideInActivity;
import com.netcorpgps.v3.dispatch.adapter.CompanyItemAdapter;
import com.netcorpgps.v3.dispatch.api.bean.AdminBean;


public class CompanyListFragment extends Fragment {

//    private static final String ARG_COLUMN_COUNT = "column-count";
//    private int mColumnCount = 1;
    private OnCompanyListListener mListener;
//
//    /**
//     * Mandatory empty constructor for the fragment manager to instantiate the
//     * fragment (e.g. upon screen orientation changes).
//     */
//    public CompanyListFragment() {
//    }
//
//    @SuppressWarnings("unused")
//    public static CompanyListFragment newInstance(int columnCount) {
//        CompanyListFragment fragment = new CompanyListFragment();
//        Bundle args = new Bundle();
//        args.putInt(ARG_COLUMN_COUNT, columnCount);
//        fragment.setArguments(args);
//        return fragment;
//    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        if (getArguments() != null) {
//            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
//        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_company_list, container, false);

        Activity activity = getActivity();
        if (activity instanceof SlideInActivity) {
            ((SlideInActivity)activity).setActionBarTitle("Companies");
        }

        AdminBean admin = (AdminBean) getActivity().getIntent().getSerializableExtra("admin");
        String companyName = getActivity().getIntent().getStringExtra("companyName");
        // Set the adapter
        if (view instanceof RecyclerView) {
//            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
//            if (mColumnCount <= 1) {
//                recyclerView.setLayoutManager(new LinearLayoutManager(context));
//            } else {
//                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
//            }
            recyclerView.setAdapter(new CompanyItemAdapter(admin.getCompanyList(), mListener, companyName));
            recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnCompanyListListener) {
            mListener = (OnCompanyListListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnCompanyListListener {
        void onCompanyListInteraction(AdminBean.Company item);
    }
}
