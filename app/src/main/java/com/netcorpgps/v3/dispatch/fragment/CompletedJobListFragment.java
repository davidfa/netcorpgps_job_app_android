package com.netcorpgps.v3.dispatch.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.activity.JobDetailActivity;
import com.netcorpgps.v3.dispatch.activity.MainActivity;
import com.netcorpgps.v3.dispatch.adapter.JobItemAdapter;
import com.netcorpgps.v3.dispatch.api.ErrorHandler.VolleyErrorHandler;
import com.netcorpgps.v3.dispatch.api.bean.JobListBean;
import com.netcorpgps.v3.dispatch.api.volley.GsonRequest;
import com.netcorpgps.v3.dispatch.application.NetcorpGPSApplication;
import com.netcorpgps.v3.dispatch.db.bean.Jobs;
import com.netcorpgps.v3.dispatch.db.service.JobsService;
import com.netcorpgps.v3.dispatch.utils.LogUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CompletedJobListFragment extends Fragment implements JobItemAdapter.OnJobListListener {

    private final String LOG_TAG = CompletedJobListFragment.class.getSimpleName();

    private SwipeRefreshLayout swipeRefreshLayout = null;
    RecyclerView recyclerView = null;
    private AppCompatActivity activity;
    private JobsService jobsService = null;
    private View fragmentView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (activity == null)
            activity = (AppCompatActivity) getActivity();

        if (fragmentView == null) {
            fragmentView = inflater.inflate(R.layout.fragment_job_list, container, false);
        }

        if (swipeRefreshLayout == null) {
            swipeRefreshLayout = (SwipeRefreshLayout) fragmentView.findViewById(R.id.swipeRefreshLayout);
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshListener());
            swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        }
        if (recyclerView == null) {
            recyclerView = (RecyclerView) fragmentView.findViewById(R.id.rvJobs);
            recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                }

                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                        if (activity instanceof MainActivity) {
                            ((MainActivity) activity).showFab();
                        }
                    } else {
                        if (activity instanceof MainActivity) {
                            ((MainActivity) activity).hideFab();
                        }
                    }

                    super.onScrollStateChanged(recyclerView, newState);
                }
            });
        }

        return fragmentView;
    }

    public void onStart() {
        super.onStart();
        if (jobsService == null)
            jobsService = new JobsService(getActivity());
        closeSysMsgLayout();
        checkNetwork();
        setActivityTitle();
        bindingListItems();
        refreshItems();
    }

    public void onResume() {
        if (activity instanceof MainActivity) {
            ((MainActivity) activity).showFab();
        }
        super.onResume();
    }

    public void bindingListItems() {
        List<Jobs> items = jobsService.getComplatedJobs();
        onItemsLoadComplete(items);
    }

    public void refreshItems() {
        if (!isConnected()) {
            swipeRefreshLayout.setRefreshing(false);
            return;
        }
        String ip = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_ip);
        String vehicleID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id);
        String companyDB = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_db);
        String accessToken = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_driver_access_token);
        String driverRecordID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_driver_record_id);
        String driverID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_user_id);
        String deviceID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_device_id);
        String number = NetcorpGPSApplication.sharedPrefManager.getInt(R.string.shared_pref_completed_job_number, 30) + "";
        String apiGetCompletedJobList = getString(R.string.api_get_completed_jobList);
        String url = ip + apiGetCompletedJobList;
        Map map = new HashMap();
        map.put("company", companyDB);
        map.put("vehicleID", vehicleID);
        map.put("driverRecordID", driverRecordID);
        map.put("accessToken", accessToken);
        map.put("driverID", driverID);
        map.put("deviceID", deviceID);
        map.put("number", number);
        LogUtils.infoLog(LOG_TAG, url + ": " + map.toString());
        GsonRequest<JobListBean> jsObjRequest = new GsonRequest<>(Request.Method.POST, url, JobListBean.class, map, null, new GetJobListListener(), new GetJobErrorListener());
        jsObjRequest.setShouldCache(false);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(Integer.parseInt(getString(R.string.sys_timeout)), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsObjRequest.setTag(getString(R.string.request_tag_get_completed_jobs));
        NetcorpGPSApplication.getInstance().getRequestQueue().add(jsObjRequest);
    }

    private void setActivityTitle() {
        if (activity instanceof MainActivity) {
            ((MainActivity) activity).setActionBarTitle(getString(R.string.menu_completed_jobs));
        }
    }

    private void checkNetwork() {
        if (!isConnected()) {
            showSysMessage(getString(R.string.msg_error_no_network_available));
        }
    }

    private boolean isConnected() {
        if (activity instanceof MainActivity) {
            return ((MainActivity) activity).isConnected();
        }
        return false;
    }

    private void showSysMessage(String message) {
        if (activity instanceof MainActivity) {
            ((MainActivity) activity).setSystemMessage(message);
        }
    }

    private void closeSysMsgLayout() {
        if (activity instanceof MainActivity) {
            ((MainActivity) activity).closeSysMsgLayout();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onJobListInteraction(Jobs item) {
        if (activity instanceof MainActivity) {
            ((MainActivity) activity).hideFab();
        }
        Intent intent = new Intent(getActivity(), JobDetailActivity.class);

        intent.putExtra("jobID", item.getJobID());
        startActivity(intent);
    }

    class GetJobListListener implements Response.Listener<JobListBean> {

        @Override
        public void onResponse(JobListBean response) {
            try {
                if (response != null) {

                    jobsService.deleteCompletedJobs();
                    if (response.getStatus() == 1) {

                        for (Jobs job : response.getJobList()) {

                            long newRowId = jobsService.insertJob(job);
                        }
                        jobsService.syncWaypointAndExpenseWithJob();
                    }
                    closeSysMsgLayout();
                    LogUtils.debugLog(LOG_TAG, response.getMsg());
                }
            } catch (Exception e) {
                showSysMessage(e.getMessage());
                LogUtils.errorLog(LOG_TAG, e.getMessage(), e);

            } finally {
                bindingListItems();
            }
        }
    }

    class GetJobErrorListener implements Response.ErrorListener {
        @Override
        public void onErrorResponse(VolleyError error) {
            String message = VolleyErrorHandler.handleError(activity, error);

            showSysMessage(message);
            LogUtils.errorLog(LOG_TAG, error.getMessage(), error);
            bindingListItems();
        }
    }

    public class SwipeRefreshListener implements SwipeRefreshLayout.OnRefreshListener {

        @Override
        public void onRefresh() {
            refreshItems();
        }
    }

    void onItemsLoadComplete(List<Jobs> items) {
        recyclerView.setAdapter(new JobItemAdapter(items, CompletedJobListFragment.this, getActivity()));
        swipeRefreshLayout.setRefreshing(false);
    }
}
