package com.netcorpgps.v3.dispatch.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.activity.JobDetailActivity;
import com.netcorpgps.v3.dispatch.activity.MainActivity;
import com.netcorpgps.v3.dispatch.api.bean.JobBean;
import com.netcorpgps.v3.dispatch.api.bean.ResponseBean;
import com.netcorpgps.v3.dispatch.api.volley.GsonRequest;
import com.netcorpgps.v3.dispatch.application.NetcorpGPSApplication;
import com.netcorpgps.v3.dispatch.db.bean.Expense;
import com.netcorpgps.v3.dispatch.db.bean.Jobs;
import com.netcorpgps.v3.dispatch.db.bean.Waypoint;
import com.netcorpgps.v3.dispatch.db.service.JobsService;
import com.netcorpgps.v3.dispatch.utils.CommonUtils;
import com.netcorpgps.v3.dispatch.utils.LocationUtils;
import com.netcorpgps.v3.dispatch.utils.LogUtils;
import com.netcorpgps.v3.dispatch.utils.dialog.BasicDialogResponse;
import com.netcorpgps.v3.dispatch.utils.dialog.DialogUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class HomeFragment extends Fragment {
    private final String LOG_TAG = HomeFragment.class.getSimpleName();
    private AppCompatActivity activity;
    private MenuBuilder menuBuilder;
    private ViewPager viewPager;
    private View fragmentView;
    private JobsService jobsService = null;
    private Jobs currentJob = null;
    private RelativeLayout blankLayout;
    private LinearLayout jobLayout;
    private SwipeRefreshLayout swipeRefreshLayout = null;
    private int jobID = 0;
    private TextView pagination;
    private ImageButton arrowLeft;
    private ImageButton arrowRight;
    private ImageButton ibPhoneCall;
    private ImageButton ibJobDetails;
    private NestedScrollView nestScrollview;
    private ProgressDialog progressDialog = null;
    private TextView txtCurrentTime, txtJobCode, txtJobStatusLabel, txtJobStatus, txtScheduled, txtTimeLeft, txtCustomer, txtContact, txtContactNumber, txtRequirement;
    private String scheduledTime = null;
    private boolean isShowTimeLeft = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void onResume() {
        if (activity instanceof MainActivity) {
            ((MainActivity) activity).showFab();
        }
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (activity == null)
            activity = (AppCompatActivity) getActivity();
        // show option menu
        setHasOptionsMenu(true);
        // Inflate the layout for this fragment
        if (fragmentView == null) {
            fragmentView = inflater.inflate(R.layout.fragment_home, container, false);
        }
        if (blankLayout == null) {
            blankLayout = (RelativeLayout) fragmentView.findViewById(R.id.blank_layout);
        }
        if (jobLayout == null) {
            jobLayout = (LinearLayout) fragmentView.findViewById(R.id.job_layout);
        }

        if (swipeRefreshLayout == null) {
            swipeRefreshLayout = (SwipeRefreshLayout) fragmentView.findViewById(R.id.swipeRefreshLayout);
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshListener());
            swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        }
        nestScrollview = (NestedScrollView) fragmentView.findViewById(R.id.nest_scrollview);
        nestScrollview.setFillViewport(true);

        txtCurrentTime = (TextView) fragmentView.findViewById(R.id.txtCurrentTime);
        txtJobCode = (TextView) fragmentView.findViewById(R.id.txtJobCode);
        txtJobStatusLabel = (TextView) fragmentView.findViewById(R.id.txtJobStatusLabel);
        txtJobStatus = (TextView) fragmentView.findViewById(R.id.txtJobStatus);
        txtScheduled = (TextView) fragmentView.findViewById(R.id.txtScheduled);
        txtTimeLeft = (TextView) fragmentView.findViewById(R.id.txtTimeLeft);
        txtCustomer = (TextView) fragmentView.findViewById(R.id.txtCustomer);
        txtContact = (TextView) fragmentView.findViewById(R.id.txtContact);
        txtContactNumber = (TextView) fragmentView.findViewById(R.id.txtContactNumber);
        txtRequirement = (TextView) fragmentView.findViewById(R.id.txtRequirement);

        pagination = (TextView) fragmentView.findViewById(R.id.pagination);
        arrowLeft = (ImageButton) fragmentView.findViewById(R.id.arrow_left);
        arrowRight = (ImageButton) fragmentView.findViewById(R.id.arrow_right);
        ibPhoneCall = (ImageButton) fragmentView.findViewById(R.id.ibPhoneCall);
        ibJobDetails = (ImageButton) fragmentView.findViewById(R.id.ibJobDetails);
        new TimeThread().start();

//        LogUtils.debugLog(LOG_TAG, "onCreateView");
        return fragmentView;
    }

    public void onStart() {
        super.onStart();
        closeSysMsgLayout();
        createDBService();
        setActivityTitle();
        initCurrentJob();
        setMenuBuilder();
        setMenu();
//        LogUtils.debugLog(LOG_TAG, "onStart");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void setActivityTitle() {
        String driverName = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_drivername);
        if (activity instanceof MainActivity) {
            ((MainActivity) activity).setActionBarTitle(String.format(getString(R.string.msg_welcome), driverName));
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_dropdown, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @SuppressLint("RestrictedApi")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.dropdown_menu:
                View menuDropdownView = getActivity().findViewById(R.id.dropdown_menu);

                MenuPopupHelper optionsMenu = new MenuPopupHelper(getContext(), menuBuilder, menuDropdownView);
                optionsMenu.setForceShowIcon(true);
                optionsMenu.show();
                break;
        }
        return false;
    }

    @SuppressLint("RestrictedApi")
    private void setMenuBuilder() {
        menuBuilder = new MenuBuilder(getContext());
        menuBuilder.setCallback(new MenuBuilder.Callback() {

            @Override
            public boolean onMenuItemSelected(MenuBuilder menu, MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.job_in_progress:
                        changeJobStatus(getString(R.string.job_status_in_progress));
                        break;
                    case R.id.job_started:
                        changeJobStatus(getString(R.string.job_status_in_progress));
                        break;
                    case R.id.job_waiting:
                        changeJobStatus(getString(R.string.job_status_waiting));
                        break;
                    case R.id.job_loading:
                        changeJobStatus(getString(R.string.job_status_loading));
                        break;
                    case R.id.job_completed:
                        openConfirmationDialog();
                        break;
                }

                return false;
            }

            @Override
            public void onMenuModeChange(MenuBuilder menu) {

            }
        });

        MenuInflater inflater = new MenuInflater(getContext());
        inflater.inflate(R.menu.menu_job_status_dropdown, menuBuilder);

    }

    @SuppressLint("RestrictedApi")
    private void setMenu() {
        currentJob = jobsService.getCurrentJob();
        // get job status
        String jobStatus = "";
        if (currentJob != null) {
            jobStatus = currentJob.getJobStatus();
        }

        // dynamically set menu items
        if (CommonUtils.isEmpty(jobStatus)) {
            menuBuilder.findItem(R.id.job_started).setVisible(false);
            menuBuilder.findItem(R.id.job_in_progress).setVisible(false);
            menuBuilder.findItem(R.id.job_waiting).setVisible(false);
            menuBuilder.findItem(R.id.job_loading).setVisible(false);
            menuBuilder.findItem(R.id.job_completed).setVisible(false);
        } else if (jobStatus.equalsIgnoreCase(getString(R.string.job_status_assigned))) {
            menuBuilder.findItem(R.id.job_started).setVisible(true);
            menuBuilder.findItem(R.id.job_in_progress).setVisible(false);
            menuBuilder.findItem(R.id.job_waiting).setVisible(false);
            menuBuilder.findItem(R.id.job_loading).setVisible(false);
            menuBuilder.findItem(R.id.job_completed).setVisible(false);
        } else if (jobStatus.equalsIgnoreCase(getString(R.string.job_status_in_progress)) || jobStatus.equalsIgnoreCase(getString(R.string.job_status_waiting)) || jobStatus.equalsIgnoreCase(getString(R.string.job_status_loading))) {
            menuBuilder.findItem(R.id.job_started).setVisible(false);
            menuBuilder.findItem(R.id.job_in_progress).setVisible(true);
            menuBuilder.findItem(R.id.job_waiting).setVisible(true);
            menuBuilder.findItem(R.id.job_loading).setVisible(true);
            menuBuilder.findItem(R.id.job_completed).setVisible(true);
        }
    }

    private void createDBService() {
        if (jobsService == null)
            jobsService = new JobsService(getActivity());
    }

    private void initCurrentJob() {
        currentJob = jobsService.getCurrentJob();
        if (currentJob != null) {
            jobID = currentJob.getJobID();
            showJobLayout();
            bindingItems(jobID);
        } else {
            jobID = 0;
            showBlankLayout();
            swipeRefreshLayout.setRefreshing(false);
        }
        refreshItems(jobID);
    }

    public void refreshItems(int jobID) {
        if (!isConnected()) {
            swipeRefreshLayout.setRefreshing(false);
            return;
        }
        String ip = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_ip);
        String vehicleID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id);
        String companyDB = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_db);
        String accessToken = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_driver_access_token);
        String driverRecordID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_driver_record_id);
        String driverID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_user_id);
        String deviceID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_device_id);
        String apiGetJobByID = getString(R.string.api_get_job_by_id);
        String url = ip + apiGetJobByID;
        Map map = new HashMap();
        map.put("company", companyDB);
        map.put("vehicleID", vehicleID);
        map.put("driverRecordID", driverRecordID);
        map.put("accessToken", accessToken);
        map.put("driverID", driverID);
        map.put("deviceID", deviceID);
        map.put("jobID", String.valueOf(jobID));
        LogUtils.infoLog(LOG_TAG, url + ": " + map.toString());
        GsonRequest<JobBean> jsObjRequest = new GsonRequest<>(Request.Method.POST, url, JobBean.class, map, null, new GetJobListener(jobID), new GetErrorListener());
        jsObjRequest.setShouldCache(false);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(Integer.parseInt(getString(R.string.sys_timeout)), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsObjRequest.setTag(getString(R.string.request_tag_get_job_waypoints));
        NetcorpGPSApplication.getInstance().getRequestQueue().add(jsObjRequest);
    }

    public void bindingItems(final int jobID) {
        Jobs job = jobsService.getJobByID(jobID);
        if (job == null) {
            isShowTimeLeft = false;
            scheduledTime = null;
            return;
        }

        String status = job.getJobStatus();
        int statusColor = (CommonUtils.getJobStatusColor(getActivity(), status));
        final String phoneNumber = job.getContactNumber();

        txtJobCode.setText(job.getJobCode());
        txtJobStatus.setText(status);
        txtJobStatus.setTextColor(statusColor);
        txtJobStatusLabel.setTextColor(statusColor);
        txtScheduled.setText(CommonUtils.getFormatDatetime(job.getScheduledTime()));
        txtScheduled.setTextColor(CommonUtils.getScheduledColor(getActivity(), job.getScheduledTime()));
        txtCustomer.setText(job.getCustomer());
        txtContact.setText(job.getContactName());
        txtContactNumber.setText(String.format(getString(R.string.contact_phone_number), phoneNumber));
        txtRequirement.setText(job.getRequirement());
        scheduledTime = job.getScheduledTime();
        ibJobDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (activity instanceof MainActivity) {
                    ((MainActivity) activity).hideFab();
                }
                Intent intent = new Intent(getActivity(), JobDetailActivity.class);

                intent.putExtra("jobID", jobID);
                startActivity(intent);
            }
        });

        ibPhoneCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + phoneNumber));
                startActivity(callIntent);
            }
        });

        if (status.equalsIgnoreCase(getString(R.string.job_status_in_progress)) || status.equalsIgnoreCase(getString(R.string.job_status_waiting)) || status.equalsIgnoreCase(getString(R.string.job_status_loading))) {
            isShowTimeLeft = false;
        } else {
            isShowTimeLeft = true;
        }

        List<Waypoint> waypoints = jobsService.getWaypointsWithoutBaseByJobID(jobID);
        final Map<Integer, Fragment> data = new TreeMap<>();
        if (waypoints != null && waypoints.size() > 0) {
            int viewPosition = -1;
            for (int i = 0; i < waypoints.size(); i++) {
                JobWaypointFragment jwFragment = new JobWaypointFragment();
                Bundle args = new Bundle();
                Waypoint waypoint = waypoints.get(i);
                args.putSerializable("jobWaypoint", waypoint);
                jwFragment.setArguments(args);
                data.put(i, jwFragment);
                // determine which view should be shown
                if (CommonUtils.isEmpty(waypoint.getDeviceDatetimeArrivedAt())) {
                    if (viewPosition == -1) {
                        viewPosition = i;
                    }
                }
            }

            if (viewPosition == -1) {
                viewPosition = 0;
            }

            viewPager = (ViewPager) fragmentView.findViewById(R.id.view_pager);
            viewPager.setAdapter(new FragmentStatePagerAdapter(getChildFragmentManager()) {
                @Override
                public Fragment getItem(int position) {
                    return data.get(position);
                }

                @Override
                public int getCount() {
                    return data.size();
                }
            });

            pagination.setText((viewPager.getCurrentItem() + 1) + "/" + data.size());

            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    Fragment fragment = data.get(position);
                    if (fragment != null) {
                        fragment.setUserVisibleHint(true);
                        fragment.onResume();
                    }

                    pagination.setText((position + 1) + "/" + data.size());
                    setArrow(position, data.size());
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
            viewPager.setCurrentItem(viewPosition);

            setArrow(viewPosition, data.size());

            arrowRight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int currentItem = viewPager.getCurrentItem();
                    if ((currentItem + 1) < data.size()) {
                        viewPager.setCurrentItem((currentItem + 1));
                    }
                }
            });

            arrowLeft.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int currentItem = viewPager.getCurrentItem();
                    if (currentItem > 0) {
                        viewPager.setCurrentItem((currentItem - 1));
                    }
                }
            });
        }

        swipeRefreshLayout.setRefreshing(false);
    }

    private void setArrow(int position, int size) {
        if (position == 0 && (position + 1) == size) {
            arrowLeft.setVisibility(View.GONE);
            arrowRight.setVisibility(View.GONE);
        } else if (position == 0) {
            arrowLeft.setVisibility(View.GONE);
            arrowRight.setVisibility(View.VISIBLE);
        } else if ((position + 1) == size) {
            arrowLeft.setVisibility(View.VISIBLE);
            arrowRight.setVisibility(View.GONE);
        } else {
            arrowLeft.setVisibility(View.VISIBLE);
            arrowRight.setVisibility(View.VISIBLE);
        }
    }

    class GetJobListener implements Response.Listener<JobBean> {

        private int jobID;

        public GetJobListener(int jobID) {
            this.jobID = jobID;
        }

        @Override
        public void onResponse(JobBean response) {
            try {
                if (response != null) {

                    jobsService.deleteExpenseByJobID(jobID);

                    jobsService.deleteWaypointByJobID(jobID);

                    if (response.getStatus() == 1) {
                        jobsService.deleteJobByID(jobID);
                        Jobs job = response.getJob();
                        if (job == null) {
                            return;
                        }

                        String sysDatetime = jobsService.querySysDatetime(job.getJobID());

                        if(CommonUtils.isEmpty(job.getLastStatusTime())) {
                            job = jobsService.queryJobStatusBySysDatetime(String.valueOf(job.getJobID()), sysDatetime, job);
                        } else if (!CommonUtils.isEmpty(sysDatetime) && !CommonUtils.isEmpty(job.getLastStatusTime())) {
                            if (CommonUtils.compareDatetime(sysDatetime, job.getLastStatusTime())) {
                                job = jobsService.queryJobStatusBySysDatetime(String.valueOf(job.getJobID()), sysDatetime, job);
                            }
                        }

                        jobsService.insertJob(job);

                        for (Waypoint waypoint : response.getWaypoints()) {
                            jobsService.insertWaypoint(waypoint);
                        }

                        double total = 0;
                        for (Expense expense : response.getExpenses()) {
                            String money = expense.getExpense();
                            if (!CommonUtils.isEmpty(money)) {
                                String pure = money.replace("$", "");
                                if (!CommonUtils.isEmpty(money)) {
                                    total += Double.valueOf(pure);
                                }
                            }

                            jobsService.insertExpense(expense);
                        }
                        if (response.getExpenses() != null && response.getExpenses().size() > 0) {
                            Expense expense = new Expense();
                            expense.setJobID(jobID);
                            expense.setExpenseName("Total");
                            expense.setExpense("$" + total);

                            jobsService.insertExpense(expense);
                        }
                    } else {
                        jobsService.deleteJobByID(jobID);
                        LogUtils.errorLog(LOG_TAG, response.getMsg());
                    }
                    closeSysMsgLayout();
                }
            } catch (Exception e) {
                LogUtils.errorLog(LOG_TAG, e.getMessage(), e);
            } finally {
                if (getActivity() != null && !getActivity().isFinishing()) {
                    setMenu();
                    bindingItems(jobID);
                }
            }
        }
    }

    class GetErrorListener implements Response.ErrorListener {
        @Override
        public void onErrorResponse(VolleyError error) {
            if (getActivity() != null && !getActivity().isFinishing()) {
                setMenu();
                swipeRefreshLayout.setRefreshing(false);
            }
            LogUtils.errorLog(LOG_TAG, error.getMessage(), error);

        }
    }

    private void changeJobStatus(String jobStatus) {

        openPDialog();

        String ip = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_ip);
        String vehicleID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id);
        String companyDB = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_db);
        String companyID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_id);
        String accessToken = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_driver_access_token);
        String driverRecordID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_driver_record_id);
        String driverID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_user_id);
        String deviceID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_device_id);
        String toChangeJobStatus = getString(R.string.api_to_change_job_status);
        String url = ip + toChangeJobStatus;

        Map map = new HashMap();
        map.put("company", companyDB);
        map.put("companyID", companyID);
        map.put("vehicleID", vehicleID);
        map.put("driverRecordID", driverRecordID);
        map.put("accessToken", accessToken);
        map.put("driverID", driverID);
        map.put("deviceID", deviceID);
        map.put("jobID", String.valueOf(jobID));
        map.put("jobStatus", jobStatus);
        map.put("device_datetime", CommonUtils.getCurrentTime());
        map.put("device_timezone_name", CommonUtils.getTimeZoneName());
        map.put("device_timezone_id", CommonUtils.getTimeZoneID());
        String coordinate = "";
        String address = "";
        // check whether latest location is available
        if (LocationUtils.isNewLocationAvailable()) {
            coordinate = LocationUtils.getCoordinate();
            address = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_gms_location_address);
        }
        map.put("device_coordinate", coordinate);
        map.put("device_address", address);
        map.put("sys_datetime", CommonUtils.getSysTime());
        LogUtils.infoLog(LOG_TAG, url + ": " + map.toString());
        GsonRequest<ResponseBean> jsObjRequest = new GsonRequest<>(Request.Method.POST, url, ResponseBean.class, map, null, new ChangeJobStatusListener(String.valueOf(jobID), jobStatus, map), new ChangeJobStatusErrorListener(String.valueOf(jobID), jobStatus, map));
        jsObjRequest.setShouldCache(false);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(Integer.parseInt(getString(R.string.sys_timeout)), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsObjRequest.setTag(getString(R.string.request_tag_to_change_job_status));
        NetcorpGPSApplication.getInstance().getRequestQueue().add(jsObjRequest);
    }

    class ChangeJobStatusListener implements Response.Listener<ResponseBean> {

        private String mJobID;
        private String mStatus;
        private Map mMap;

        public ChangeJobStatusListener(String jobID, String status, Map map) {
            mJobID = jobID;
            mStatus = status;
            mMap = map;
        }

        @Override
        public void onResponse(ResponseBean response) {
            try {
                if (response != null) {
                    if (response.getStatus() == 1) {
//                        if (mStatus.equals(getString(R.string.job_status_in_progress))) {
//                            CommonUtils.toast(getActivity(), response.getMsg());
//                            LogUtils.debugLog(LOG_TAG, response.getMsg());
//                        }
                        LogUtils.debugLog(LOG_TAG, response.getMsg() + " job status: " + mMap.get("jobStatus"));
                    } else {
                        saveSyncJobStatus(mMap);
                        LogUtils.errorLog(LOG_TAG, response.getMsg());
                    }
                }
            } catch (Exception e) {
                saveSyncJobStatus(mMap);
                LogUtils.errorLog(LOG_TAG, e.getMessage(), e);

            } finally {
                updateLocalJobStatus(mJobID, mStatus);
                initCurrentJob();
                setMenu();
                dismissPDialog();
            }
        }
    }

    class ChangeJobStatusErrorListener implements Response.ErrorListener {

        private String mJobID;
        private String mStatus;
        private Map mMap;

        public ChangeJobStatusErrorListener(String jobID, String status, Map map) {
            mJobID = jobID;
            mStatus = status;
            mMap = map;
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            LogUtils.errorLog(LOG_TAG, error.getMessage(), error);

            updateLocalJobStatus(mJobID, mStatus);
            saveSyncJobStatus(mMap);
            initCurrentJob();
            setMenu();
            dismissPDialog();
        }
    }

    private void updateLocalJobStatus(String jobID, String status) {
        jobsService.updateJobStatus(jobID, status);
    }

    private void saveSyncJobStatus(Map jobStatus) {
        LogUtils.infoLog(LOG_TAG, "Save Sync Job Status: " + jobStatus);
        jobsService.insertJobStatus(jobStatus);
    }

    private void showBlankLayout() {
        blankLayout.setVisibility(View.VISIBLE);
        jobLayout.setVisibility(View.GONE);
    }

    private void showJobLayout() {
        jobLayout.setVisibility(View.VISIBLE);
        blankLayout.setVisibility(View.GONE);
    }

    public class SwipeRefreshListener implements SwipeRefreshLayout.OnRefreshListener {

        @Override
        public void onRefresh() {
            initCurrentJob();
        }
    }

    private boolean isConnected() {
        if (activity instanceof MainActivity) {
            return ((MainActivity) activity).isConnected();
        }
        return false;
    }

    private static final int msgKey1 = 1;

    public class TimeThread extends Thread {
        @Override
        public void run() {
            do {
                try {
                    Thread.sleep(1000);
                    Message msg = new Message();
                    msg.what = msgKey1;
                    mHandler.sendMessage(msg);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } while (true);
        }
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case msgKey1:
                    String currentTime = CommonUtils.getFormatDatetime(CommonUtils.getCurrentTime());
                    txtCurrentTime.setText(currentTime);
                    if (isShowTimeLeft) {
                        String dateDiffInMins = CommonUtils.getDateDiffInMins(CommonUtils.getCurrentTime(), scheduledTime);
                        txtTimeLeft.setText(dateDiffInMins);
                    } else {
                        txtTimeLeft.setText(null);
                    }

                    break;

                default:
                    break;
            }
        }
    };

    private void openPDialog() {
        if (activity instanceof MainActivity) {
            ((MainActivity) activity).openPDialog();
        }
    }

    private void dismissPDialog() {
        if (activity instanceof MainActivity) {
            ((MainActivity) activity).dismissPDialog();
        }
    }

    private void openConfirmationDialog() {
        String title = getString(R.string.dialog_title_job_completed);
        String posButton = getString(R.string.dialog_btn_yes);
        String nagButton = getString(R.string.dialog_btn_no);
        DialogUtils.showConfirmationDialog(activity, title, null, null, null, posButton, nagButton, new DialogResponse());
    }

//    private class OpenDialogClickListener implements View.OnClickListener {
//        @Override
//        public void onClick(View v) {
//
//            String title = getString(R.string.dialog_title_login_as_admin);
//            String msg = String.format(getString(R.string.dialog_msg_login_as_admin), getString(R.string.dialog_btn_no));
//            String posButton = getString(R.string.dialog_btn_yes);
//            String nagButton = getString(R.string.dialog_btn_no);
//            DialogUtils.showConfirmationDialog(activity, title, msg, null, null, posButton, nagButton, new DialogResponse());
//        }
//    }

    private class DialogResponse implements BasicDialogResponse {

        @Override
        public MaterialDialog.SingleButtonCallback onPositive() {
            MaterialDialog.SingleButtonCallback callback = new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    changeJobStatus(getString(R.string.job_status_completed));
                }
            };
            return callback;
        }

        @Override
        public MaterialDialog.SingleButtonCallback onNegative() {
            return null;
        }

        @Override
        public MaterialDialog.SingleButtonCallback onNeutral() {
            return null;
        }
    }

    private void closeSysMsgLayout() {
        if (activity instanceof MainActivity) {
            ((MainActivity) activity).closeSysMsgLayout();
        }
    }
}
