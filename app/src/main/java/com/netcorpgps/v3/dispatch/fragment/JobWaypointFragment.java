package com.netcorpgps.v3.dispatch.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.api.bean.ResponseBean;
import com.netcorpgps.v3.dispatch.api.volley.GsonRequest;
import com.netcorpgps.v3.dispatch.application.NetcorpGPSApplication;
import com.netcorpgps.v3.dispatch.db.bean.Jobs;
import com.netcorpgps.v3.dispatch.db.bean.Waypoint;
import com.netcorpgps.v3.dispatch.db.service.JobsService;
import com.netcorpgps.v3.dispatch.utils.CommonUtils;
import com.netcorpgps.v3.dispatch.utils.LogUtils;
import com.netcorpgps.v3.dispatch.utils.dialog.BasicDialogResponse;
import com.netcorpgps.v3.dispatch.utils.dialog.DialogUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by David Fa on 19/07/2017.
 */

public class JobWaypointFragment extends Fragment {
    private final String LOG_TAG = JobWaypointFragment.class.getSimpleName();
    private View fragmentView;
    private Waypoint waypoint;
    private TextView address;
    private TextView txtArrived;
    private ImageButton navigation;
    private ImageButton ibArrived;
    private JobsService jobsService = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public void onStart() {
        createDBService();
        super.onStart();
    }

    public void onResume() {
        if (getUserVisibleHint()) {
            initVariables();
        }
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (fragmentView == null) {
            fragmentView = inflater.inflate(R.layout.fragment_waypoint, container, false);
        }

        address = (TextView) fragmentView.findViewById(R.id.address);
        navigation = (ImageButton) fragmentView.findViewById(R.id.navigation);

        ibArrived = (ImageButton) fragmentView.findViewById(R.id.ibArrived);
        txtArrived = (TextView) fragmentView.findViewById(R.id.txtArrived);

        Bundle args = getArguments();
        waypoint = (Waypoint) args.getSerializable("jobWaypoint");

        address.setOnClickListener(new StartGoogleMapListener());
        navigation.setOnClickListener(new StartGoogleMapListener());
        ibArrived.setOnClickListener(new ArrivedListener());

        return fragmentView;
    }

    private void initVariables() {
        if (waypoint != null && jobsService != null) {
            waypoint = jobsService.getWaypointsByID(waypoint.getId());
            if (waypoint != null) {
                address.setText(waypoint.getAddress());
                checkIfArrived(waypoint);
//                LogUtils.debugLog(LOG_TAG, waypoint.getAddress());
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void startGoogleMap(String address) {
        String geoUri = "http://maps.google.com/maps?q=" + address;
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
        startActivity(intent);
    }

    private class StartGoogleMapListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            if (waypoint != null && !CommonUtils.isEmpty(waypoint.getAddress()))
                startGoogleMap(waypoint.getAddress());
        }
    }

    private class ArrivedListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            if (waypoint != null && jobsService != null) {
                Jobs job = jobsService.getJobByID(waypoint.getJobID());
                if (job != null && (job.getJobStatus().equalsIgnoreCase(getString(R.string.job_status_in_progress))
                        || job.getJobStatus().equalsIgnoreCase(getString(R.string.job_status_loading))
                        || job.getJobStatus().equalsIgnoreCase(getString(R.string.job_status_waiting)))) {
                    openConfirmationDialog();
                } else {
                    CommonUtils.toast(getActivity(), getString(R.string.msg_have_not_started_job));
                }
            }
        }
    }

    private void checkIfArrived(Waypoint waypoint) {

        if (waypoint == null) {
            ibArrived.setVisibility(View.GONE);
            txtArrived.setVisibility(View.GONE);
        }
        if (CommonUtils.isEmpty(waypoint.getDeviceDatetimeArrivedAt())) {
            navigation.setVisibility(View.VISIBLE);
            ibArrived.setVisibility(View.VISIBLE);
            txtArrived.setVisibility(View.GONE);
        } else {
            navigation.setVisibility(View.GONE);
            ibArrived.setVisibility(View.GONE);
            txtArrived.setVisibility(View.VISIBLE);
            txtArrived.setText("Arrived at: " + CommonUtils.getFormatArrivedTime(waypoint.getDeviceDatetimeArrivedAt()));
        }
    }

    private void createDBService() {
        if (jobsService == null)
            jobsService = new JobsService(getActivity());
    }

    private void openConfirmationDialog() {
        String title = getString(R.string.dialog_title_waypoint_completed);
        String posButton = getString(R.string.dialog_btn_yes);
        String nagButton = getString(R.string.dialog_btn_no);
        DialogUtils.showConfirmationDialog(getActivity(), title, null, null, null, posButton, nagButton, new DialogResponse());
    }

    private class DialogResponse implements BasicDialogResponse {

        @Override
        public MaterialDialog.SingleButtonCallback onPositive() {
            MaterialDialog.SingleButtonCallback callback = new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    if (waypoint != null && !CommonUtils.isEmpty(waypoint.getAddress())) {
                        waypoint.setSysDatetimeArrivedAt(CommonUtils.getSysTime());
                        waypoint.setDeviceDatetimeArrivedAt(CommonUtils.getCurrentTime());
                        waypoint.setDeviceTimezoneName(CommonUtils.getTimeZoneName());
                        waypoint.setDeviceTimezoneID(CommonUtils.getTimeZoneID());
                        jobsService.updateWaypoint(waypoint);
                        initVariables();
                        updateWaypointToServer();
                    }
                }
            };
            return callback;
        }

        @Override
        public MaterialDialog.SingleButtonCallback onNegative() {
            return null;
        }

        @Override
        public MaterialDialog.SingleButtonCallback onNeutral() {
            return null;
        }
    }

    private void updateWaypointToServer() {

        String ip = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_ip);
        String companyDB = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_db);
        String api = getString(R.string.api_to_change_waypoint);
        String url = ip + api;
        Map map = new HashMap();
        map.put("company", companyDB);
        map.put("jobID", String.valueOf(waypoint.getJobID()));
        map.put("address", waypoint.getAddress());
        map.put("device_datetime", CommonUtils.getCurrentTime());
        map.put("device_timezone_name", CommonUtils.getTimeZoneName());
        map.put("device_timezone_id", CommonUtils.getTimeZoneID());
        map.put("sys_datetime", CommonUtils.getSysTime());
        LogUtils.infoLog(LOG_TAG, url + ": " + map.toString());
        GsonRequest<ResponseBean> jsObjRequest = new GsonRequest<>(Request.Method.POST, url, ResponseBean.class, map, null, new RequestListener(map), new RequestErrorListener(map));
        jsObjRequest.setShouldCache(false);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(Integer.parseInt(getString(R.string.sys_timeout)), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsObjRequest.setTag(getString(R.string.request_tag_update_job_waypoint));
        NetcorpGPSApplication.getInstance().getRequestQueue().add(jsObjRequest);
    }

    class RequestListener implements Response.Listener<ResponseBean> {

        private Map mMap;

        public RequestListener(Map map) {
            mMap = map;
        }

        @Override
        public void onResponse(ResponseBean response) {
            try {
                if (response != null) {
                    if (response.getStatus() == 1) {
                        LogUtils.debugLog(LOG_TAG, response.getMsg());
                    } else {
                        saveSyncJobWaypoint(mMap);
                        LogUtils.errorLog(LOG_TAG, response.getMsg());
                    }
                }
            } catch (Exception e) {
                saveSyncJobWaypoint(mMap);
                LogUtils.errorLog(LOG_TAG, e.getMessage(), e);

            } finally {
            }
        }
    }

    class RequestErrorListener implements Response.ErrorListener {

        private Map mMap;

        public RequestErrorListener(Map map) {
            mMap = map;
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            LogUtils.errorLog(LOG_TAG, error.getMessage(), error);
            saveSyncJobWaypoint(mMap);
        }
    }

    private void saveSyncJobWaypoint(Map waypointRecord) {
        jobsService.insertWaypointRecord(waypointRecord);
    }
}
