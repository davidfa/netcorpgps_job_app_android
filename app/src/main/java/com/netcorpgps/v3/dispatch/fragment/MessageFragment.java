package com.netcorpgps.v3.dispatch.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.provider.MediaStore;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.activity.MainActivity;
import com.netcorpgps.v3.dispatch.activity.PictureViewPagerActivity;
import com.netcorpgps.v3.dispatch.adapter.MessageItemAdapter;
import com.netcorpgps.v3.dispatch.adapter.MoreOptionsGridViewAdapter;
import com.netcorpgps.v3.dispatch.adapter.QuickReponseGridViewAdapter;
import com.netcorpgps.v3.dispatch.adapter.QuickReponsePagerAdapter;
import com.netcorpgps.v3.dispatch.application.NetcorpGPSApplication;
import com.netcorpgps.v3.dispatch.db.bean.Message;
import com.netcorpgps.v3.dispatch.db.service.ChatMessageService;
import com.netcorpgps.v3.dispatch.http.HttpHelper;
import com.netcorpgps.v3.dispatch.receiver.MessageReceiver;
import com.netcorpgps.v3.dispatch.utils.BitmapUtils;
import com.netcorpgps.v3.dispatch.utils.CommonUtils;
import com.netcorpgps.v3.dispatch.utils.LogFileUtils;
import com.netcorpgps.v3.dispatch.utils.LogUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class MessageFragment extends Fragment {

    private final String LOG_TAG = MessageFragment.class.getSimpleName();
    private MessageReceiver messageReceiver;

    private View fragmentView;
    private SwipeRefreshLayout swipeRefreshLayout = null;
    private RecyclerView recyclerView = null;
    private AppCompatActivity activity;
    private InputMethodManager manager;
    private EditText et_msg;
    private List<Message> msgItems;
    private MessageItemAdapter messageItemAdapter;
    private ChatMessageService chatMessageService = null;
    private LinearLayout rlBottom;
    private Button buttonSetModeVoice;
    private Button buttonSetModeKeyboard;
    private LinearLayout buttonPressToSpeak;
    private Button buttonSend;
    private Button btnMore;
    private TextView recordingHint;
    private RelativeLayout edittext_layout;
    private RelativeLayout recordingContainer;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private LinearLayout lyMore;
    private ImageView micImage;
    private Drawable[] micImages;

    private PowerManager.WakeLock wakeLock;

    private MediaRecorder myRecorder;
    private MediaPlayer myPlayer;
    private String audioFile;
    private long startVoiceT, endVoiceT;

    // Hold a reference to the current animator,
    // so that it can be canceled mid-way.
    private Animator mCurrentAnimator;

    // The system "short" animation time duration, in milliseconds. This
    // duration is ideal for subtle animations or animations that occur
    // very frequently.
    private int mShortAnimationDuration;

    private final int HANDLER_MESSAGE_ADAPTER_NOFI = 1;
    private final int Media_Recorder_Max_Duration = 30 * 1000;
    private final int MESSAGE_NUMBER_PER_TIME = 10;
    public static final int Activity_Result_Request_Code_pickPhoto = 0;
    public static final int Activity_Result_Request_Code_takePicture = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        activity = (AppCompatActivity) getActivity();

        try {
            messageReceiver = new MessageReceiver(activity, new MessageReceiver.ReceivedMessageResponse() {

                @Override
                public void receiveMsg(Bundle bundle) {
                    try {
                        Message message = (Message) bundle.getSerializable(getString(R.string.bundle_key_msg));
                        String role = message.getRole();
                        if (!role.equalsIgnoreCase(activity.getString(R.string.message_role_mobile))) {
                            msgItems.add(message);
                            messageItemAdapter.notifyDataSetChanged();
                            recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView, null, messageItemAdapter.getItemCount() - 1);
                        }
                    } catch (Exception e) {
                        LogUtils.errorLog(LOG_TAG, "MessageReceiver:" + e.getMessage(), e);
                    }
                }

                @Override
                public void sendMsg(Bundle bundle) {

                }
            });
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(getString(R.string.intent_filter_receive_message));
            activity.registerReceiver(messageReceiver, intentFilter);
            LogUtils.debugLog(LOG_TAG, "Register messageReceiver");
        } catch (Exception e) {
            LogUtils.errorLog(LOG_TAG, e.getMessage(), e);
        }
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (chatMessageService == null)
            chatMessageService = new ChatMessageService(activity);
        // Inflate the layout for this fragment
        if (fragmentView == null) {
            fragmentView = inflater.inflate(R.layout.fragment_message, container, false);
        }

        if (swipeRefreshLayout == null) {
            swipeRefreshLayout = (SwipeRefreshLayout) fragmentView.findViewById(R.id.swipeRefreshLayout);
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshListener());
            swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        }

        if (recyclerView == null) {
            recyclerView = (RecyclerView) fragmentView.findViewById(R.id.rvMsgs);
        }
        activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        wakeLock = ((PowerManager) activity.getSystemService(Context.POWER_SERVICE)).newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, LOG_TAG);
        micImages = new Drawable[]{
                ContextCompat.getDrawable(activity, R.drawable.record_animate_01),
                ContextCompat.getDrawable(activity, R.drawable.record_animate_02),
                ContextCompat.getDrawable(activity, R.drawable.record_animate_03),
                ContextCompat.getDrawable(activity, R.drawable.record_animate_04),
                ContextCompat.getDrawable(activity, R.drawable.record_animate_05),
                ContextCompat.getDrawable(activity, R.drawable.record_animate_06),
                ContextCompat.getDrawable(activity, R.drawable.record_animate_07),
                ContextCompat.getDrawable(activity, R.drawable.record_animate_08),
                ContextCompat.getDrawable(activity, R.drawable.record_animate_09),
                ContextCompat.getDrawable(activity, R.drawable.record_animate_10),
                ContextCompat.getDrawable(activity, R.drawable.record_animate_11),
                ContextCompat.getDrawable(activity, R.drawable.record_animate_12),
                ContextCompat.getDrawable(activity, R.drawable.record_animate_13),
                ContextCompat.getDrawable(activity, R.drawable.record_animate_14)};

        manager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        buttonSetModeVoice = (Button) fragmentView.findViewById(R.id.btn_set_mode_voice);
        buttonSetModeKeyboard = (Button) fragmentView.findViewById(R.id.btn_set_mode_keyboard);
        buttonSend = (Button) fragmentView.findViewById(R.id.btn_send);
        buttonPressToSpeak = (LinearLayout) fragmentView.findViewById(R.id.btn_press_to_speak);
        btnMore = (Button) fragmentView.findViewById(R.id.btn_more);
        edittext_layout = (RelativeLayout) fragmentView.findViewById(R.id.edittext_layout);
        recordingHint = (TextView) fragmentView.findViewById(R.id.recording_hint);
        recordingContainer = (RelativeLayout) fragmentView.findViewById(R.id.recording_container);
        rlBottom = (LinearLayout) fragmentView.findViewById(R.id.rl_bottom);
        lyMore = (LinearLayout) fragmentView.findViewById(R.id.more);
        et_msg = (EditText) fragmentView.findViewById(R.id.et_msg);
        micImage = (ImageView) fragmentView.findViewById(R.id.mic_image);
        viewPager = (ViewPager) fragmentView.findViewById(R.id.vPager);

        List<View> views = new ArrayList<View>();
        // quick response view
        View gv1 = getGridChildView();
        views.add(gv1);
        // more option view
        View gv2 = getGridChildOptionsView();
        views.add(gv2);
        viewPager.setAdapter(new QuickReponsePagerAdapter(views));

        tabLayout = (TabLayout) fragmentView.findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager, true);

        btnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                if (lyMore != null && lyMore.isShown()) {
                    lyMore.setVisibility(View.GONE);
                } else if (lyMore != null && !lyMore.isShown()) {
                    lyMore.setVisibility(View.VISIBLE);
                }
            }
        });

        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = et_msg.getText().toString();
                if (CommonUtils.isEmpty(msg)) {
                    CommonUtils.toast(activity, getString(R.string.msg_enter_message));
                    return;
                }
                sendBroadcastMsg(msg);

            }
        });


        et_msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rlBottom.requestFocus();
                manager.showSoftInput(rlBottom, InputMethodManager.SHOW_IMPLICIT);
                LogUtils.debugLog("setOnClickListener", "setOnClickListener");
            }
        });

        et_msg.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if (!TextUtils.isEmpty(s)) {
                    btnMore.setVisibility(View.GONE);
                    buttonSend.setVisibility(View.VISIBLE);
                } else {
                    btnMore.setVisibility(View.VISIBLE);
                    buttonSend.setVisibility(View.GONE);
                }
//                manager.showSoftInput(rlBottom, InputMethodManager.SHOW_IMPLICIT);

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        buttonSetModeVoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setModeVoice(v);
            }
        });

        buttonSetModeKeyboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setModeKeyboard(v);
            }
        });

        buttonPressToSpeak.setOnTouchListener(new PressToSpeakListen());

        msgItems = new ArrayList<>();
        setSyncedStatus();
        fetchMsgs();
        messageItemAdapter = new MessageItemAdapter(msgItems, new MessageListener(), activity);
        recyclerView.setAdapter(messageItemAdapter);
        scrollToPosition();

        // Retrieve and cache the system's default "short" animation time.
        mShortAnimationDuration = getResources().getInteger(android.R.integer.config_shortAnimTime);

        return fragmentView;
    }

    public void onStart() {
        closeSysMsgLayout();
        checkNetwork();
        setActivityTitle();
        hideFab();
        super.onStart();
    }

    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        try {
            if (messageReceiver != null) {
                LogUtils.debugLog(LOG_TAG, "Unregister messageReceiver");
                activity.unregisterReceiver(messageReceiver);
            }

        } catch (Exception e) {
            LogUtils.errorLog(LOG_TAG, e.getMessage(), e);
        }
        super.onDestroy();
    }

    public void onBackPressed() {
        if (et_msg != null)
            et_msg.clearFocus();
        hideKeyboard();
        lyMore.setVisibility(View.GONE);
    }

    public class SwipeRefreshListener implements SwipeRefreshLayout.OnRefreshListener {

        @Override
        public void onRefresh() {
            fetchMsgs();
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    /**
     * Set message isRead status from null to false
     */
    private void setSyncedStatus() {
        chatMessageService.setSyncedStatusToFalse();
    }

    private void fetchMsgs() {
        String datetime = "";
        int size = msgItems.size();
        if (msgItems == null || size == 0) {
            datetime = CommonUtils.getCurrentTime();
        } else {
            datetime = msgItems.get(0).getTime();
        }
        String vehicleID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id);
        String companyID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_id);
        List newItmes = chatMessageService.getChatMessages(datetime, MESSAGE_NUMBER_PER_TIME, companyID, vehicleID);
        msgItems.addAll(0, newItmes);
        if (messageItemAdapter != null)
            messageItemAdapter.notifyDataSetChanged();
    }

    private void closeSysMsgLayout() {
        if (activity instanceof MainActivity) {
            ((MainActivity) activity).closeSysMsgLayout();
        }
    }

    private void checkNetwork() {
        if (!isConnected()) {
            showSysMessage(getString(R.string.msg_error_no_network_available));
        }
    }

    private boolean isConnected() {
        if (activity instanceof MainActivity) {
            return ((MainActivity) activity).isConnected();
        }
        return false;
    }

    private void showSysMessage(String message) {
        if (activity instanceof MainActivity) {
            ((MainActivity) activity).setSystemMessage(message);
        }
    }

    private void setActivityTitle() {
        if (activity instanceof MainActivity) {
            ((MainActivity) activity).setActionBarTitle(getString(R.string.menu_message));
        }
    }

    private void hideFab() {
        if (activity instanceof MainActivity) {
            ((MainActivity) activity).hideFab();
        }
    }

    private void scrollToPosition() {
//        LogUtils.debugLog(LOG_TAG, "ItemCount: " + messageItemAdapter.getItemCount());
        if (messageItemAdapter.getItemCount() > 0) {
            recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView, null, messageItemAdapter.getItemCount() - 1);
        }
    }

    public void setModeVoice(View view) {
        hideKeyboard();
        edittext_layout.setVisibility(View.GONE);
//        more.setVisibility(View.GONE);
        view.setVisibility(View.GONE);
        buttonSetModeKeyboard.setVisibility(View.VISIBLE);
        buttonSend.setVisibility(View.GONE);
        btnMore.setVisibility(View.VISIBLE);
        buttonPressToSpeak.setVisibility(View.VISIBLE);
        lyMore.setVisibility(View.GONE);
    }

    public void setModeKeyboard(View view) {
        edittext_layout.setVisibility(View.VISIBLE);
        view.setVisibility(View.GONE);
        buttonSetModeVoice.setVisibility(View.VISIBLE);
        et_msg.requestFocus();
        buttonPressToSpeak.setVisibility(View.GONE);
        if (TextUtils.isEmpty(et_msg.getText())) {
            btnMore.setVisibility(View.VISIBLE);
            buttonSend.setVisibility(View.GONE);
        } else {
            btnMore.setVisibility(View.GONE);
            buttonSend.setVisibility(View.VISIBLE);
        }
    }

    private void hideKeyboard() {
        if (activity.getWindow().getAttributes().softInputMode != WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN) {
            if (activity.getCurrentFocus() != null)
                manager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    class PressToSpeakListen implements View.OnTouchListener {
        @Override
        public boolean onTouch(View v, MotionEvent event) {

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
//                    LogUtils.debugLog(LOG_TAG, event.getAction() + ": ACTION_DOWN");
                    try {
                        v.setPressed(true);
                        wakeLock.acquire();
                        recordingContainer.setVisibility(View.VISIBLE);
                        recordingContainer.bringToFront();
                        recordingHint.setText(activity.getString(R.string.message_slide_up_to_cancel));
                        recordingHint.setBackgroundColor(Color.TRANSPARENT);
                        startRecording();
                        startVoiceT = System.currentTimeMillis();
                    } catch (Exception e) {
                        e.printStackTrace();
                        v.setPressed(false);
                        if (wakeLock.isHeld())
                            wakeLock.release();
//                        if (voiceRecorder != null)
//                            voiceRecorder.discardRecording();
                        stopRecording();
                        recordingContainer.setVisibility(View.INVISIBLE);
                        CommonUtils.toast(activity, getString(R.string.msg_failed_to_send_voice));
                        return false;
                    }

                    return true;
                case MotionEvent.ACTION_MOVE: {
//                    LogUtils.debugLog(LOG_TAG, event.getAction() + ": ACTION_MOVE");
                    if (myRecorder == null) {
                        return false;
                    }

                    try {
                        int amplitude = myRecorder.getMaxAmplitude();
                        int what = Math.min((amplitude / 2000), 13);
                        micImageHandler.sendEmptyMessage(what);
                        if (event.getY() < 0) {
                            recordingHint.setText(activity.getString(R.string.message_release_to_cancel));
                            recordingHint.setBackgroundResource(R.drawable.recording_text_hint_bg);
                        } else {
                            recordingHint.setText(activity.getString(R.string.message_slide_up_to_cancel));
                            recordingHint.setBackgroundColor(Color.TRANSPARENT);
                        }
                    } catch (Exception e) {

                        LogUtils.errorLog(LOG_TAG, e.getMessage(), e);
                    }

                    endVoiceT = System.currentTimeMillis();
                    int time = (int) ((endVoiceT - startVoiceT) / 1000);

                    // the maximum voice duration is 50 seconds
                    if (time > 50) {
                        v.setPressed(false);
                        recordingContainer.setVisibility(View.INVISIBLE);
                        if (wakeLock.isHeld())
                            wakeLock.release();
                        stopRecording();
                        int index = addAudioMsg(audioFile, String.valueOf(time));
                        UploadingFileTask uploadingFileTask = new UploadingFileTask();
                        uploadingFileTask.execute(String.valueOf(index));
                    }

                    return true;
                }

                case MotionEvent.ACTION_CANCEL: {
//                    LogUtils.debugLog(LOG_TAG, event.getAction() + ": ACTION_CANCEL");
                    v.setPressed(false);
                    recordingContainer.setVisibility(View.INVISIBLE);
                    if (wakeLock.isHeld())
                        wakeLock.release();
                    stopRecording();
                    return true;
                }
                case MotionEvent.ACTION_UP:
//                    LogUtils.debugLog(LOG_TAG, event.getAction() + ": ACTION_UP");
                    v.setPressed(false);
                    recordingContainer.setVisibility(View.INVISIBLE);
                    if (wakeLock.isHeld())
                        wakeLock.release();
                    if (event.getY() < 0) {
                        stopRecording();

                    } else {
                        endVoiceT = System.currentTimeMillis();
                        int time = (int) ((endVoiceT - startVoiceT) / 1000);

                        if (time < 1) {
                            stopRecording();
                            return false;
                        }
                        stopRecording();
                        int index = addAudioMsg(audioFile, String.valueOf(time));
                        UploadingFileTask uploadingFileTask = new UploadingFileTask();
                        uploadingFileTask.execute(String.valueOf(index));
                    }

                    return true;
                default:
//                    LogUtils.debugLog(LOG_TAG, event.getAction() + ": default");
//                    recordingContainer.setVisibility(View.INVISIBLE);
//                    stopRecording();

                    return false;
            }
        }
    }


    private void startRecording() {
        try {
            myRecorder = new MediaRecorder();
            myRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            myRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            myRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
            myRecorder.setMaxDuration(Media_Recorder_Max_Duration);
            File audioDir = new File(LogFileUtils.getFilesDir());
            if (!audioDir.exists()) {
                audioDir.mkdir();
            }
            audioFile = LogFileUtils.getFilesDir() + File.separator + System.currentTimeMillis() + ".3gp";
//            audioFile = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + System.currentTimeMillis() + ".3gp";
            myRecorder.setOutputFile(audioFile);
            myRecorder.prepare();
            myRecorder.start();

        } catch (IllegalStateException e) {
            LogUtils.errorLog(LOG_TAG, "IllegalStateException: " + e.getMessage(), e);
        } catch (IOException e) {
            LogUtils.errorLog(LOG_TAG, "IOException: " + e.getMessage(), e);
        } catch (Exception e) {
            LogUtils.errorLog(LOG_TAG, e.getMessage(), e);
        }
    }

    private void stopRecording() {
        try {
            if (myRecorder != null) {
                myRecorder.stop();
                myRecorder.release();
                myRecorder = null;
            }

        } catch (IllegalStateException e) {
            LogUtils.errorLog(LOG_TAG, "IllegalStateException: " + e.getMessage(), e);
            CommonUtils.toast(activity, getString(R.string.msg_failed_send_voice));
        } catch (RuntimeException e) {
            LogUtils.errorLog(LOG_TAG, "RuntimeException: " + e.getMessage(), e);
            // delete file if the file was invalid
            File file = new File(audioFile);
            if (file.exists()) {
                file.delete();
            }
            CommonUtils.toast(activity, getString(R.string.msg_failed_send_voice));
        } catch (Exception e) {
            LogUtils.errorLog(LOG_TAG, e.getMessage(), e);
            CommonUtils.toast(activity, getString(R.string.msg_failed_send_voice));
        }
    }

    private Handler micImageHandler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            micImage.setImageDrawable(micImages[msg.what]);
        }
    };

    private Handler uiHandler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            if (msg.what == HANDLER_MESSAGE_ADAPTER_NOFI) {
                messageItemAdapter.notifyDataSetChanged();
            }

        }
    };

    private class UploadingFileTask extends AsyncTask<String, Void, Void> {

        public void onPreExecute() {

        }

        @Override
        protected Void doInBackground(String[] params) {
            int index = Integer.valueOf(params[0]);
            Message message = msgItems.get(index);
            String isSynced = HttpHelper.uploadVoiceToServer(activity, message.getFileUrl(), message.getUniqueID());
            message.setSynced(isSynced);
            message.setIsRead("false");
            message.setServerUrl("device_files/" + message.getUniqueID() + ".3gp");
            chatMessageService.updateSynced(message);
            uiHandler.sendEmptyMessage(HANDLER_MESSAGE_ADAPTER_NOFI);

            if (isSynced.equalsIgnoreCase("true")) {
                Intent intent = new Intent(getString(R.string.intent_filter_send_message));
                Bundle bundle = new Bundle();
                bundle.putSerializable(getString(R.string.bundle_key_msg), message);
                intent.putExtras(bundle);

                activity.sendBroadcast(intent);
            }

            return null;
        }

        public void onPostExecute(Void unused) {
        }
    }

    class ImageUploadTask extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Void doInBackground(String[] params) {
            try {
                int index = Integer.valueOf(params[0]);
                Message message = msgItems.get(index);
                String isSynced = HttpHelper.uploadPictureToServer(activity, message.getFileUrl(), message.getUniqueID());
                message.setSynced(isSynced);
                message.setIsRead("false");
                message.setServerUrl("device_files/" + message.getUniqueID() + ".png");
                chatMessageService.updateSynced(message);
                uiHandler.sendEmptyMessage(HANDLER_MESSAGE_ADAPTER_NOFI);

                if (isSynced.equalsIgnoreCase("true")) {
                    Intent intent = new Intent(getString(R.string.intent_filter_send_message));
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(getString(R.string.bundle_key_msg), message);
                    intent.putExtras(bundle);

                    activity.sendBroadcast(intent);
                }
                return null;
            } catch (Exception e) {
                // something went wrong. connection with the server error
            }
            return null;
        }

        @Override
        public void onPostExecute(Void unused) {
        }

    }

    private void sendBroadcastMsg(String msg) {
        Intent intent = new Intent(getString(R.string.intent_filter_send_message));
        Bundle bundle = new Bundle();
        Message message = new Message();

        message.setSender(NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_drivername));
        message.setRole(getString(R.string.message_role_mobile));
        message.setTime(CommonUtils.getCurrentTime());
        message.setMsg(msg);
        message.setType(getString(R.string.message_content_type_text));
        message.setCompanyID(NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_id));
        message.setVehicleID(NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id));
        message.setUniqueID(String.valueOf(System.currentTimeMillis()));
        message.setSynced(null);
        message.setIsRead("false");
        bundle.putSerializable(getString(R.string.bundle_key_msg), message);
        intent.putExtras(bundle);

        activity.sendBroadcast(intent);
        et_msg.setText("");

        msgItems.add(message);
        messageItemAdapter.notifyDataSetChanged();
        scrollToPosition();

    }

    private int addAudioMsg(String audioFile, String voiceDuration) {

        // obtain audio duration
        MediaPlayer mPlayer = new MediaPlayer();
        int audioTime = 0;
        try {
            mPlayer.setDataSource(audioFile);
            mPlayer.prepare();
            audioTime = mPlayer.getDuration() / 1000;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (mPlayer != null) {
                mPlayer.stop();
                mPlayer.release();
            }
        }

        Message message = new Message();
        message.setSender(NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_drivername));
        message.setRole(getString(R.string.message_role_mobile));
        message.setTime(CommonUtils.getCurrentTime());
        message.setMsg("");
        message.setType(getString(R.string.message_content_type_audio));
        message.setCompanyID(NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_id));
        message.setVehicleID(NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id));
        message.setUniqueID(String.valueOf(System.currentTimeMillis()));
        message.setVoiceTime(String.valueOf(audioTime));
        message.setFileUrl(audioFile);
        message.setSynced(null);
        message.setIsRead("false");
        chatMessageService.insert(message);
        msgItems.add(message);

        messageItemAdapter.notifyDataSetChanged();
        scrollToPosition();
        return msgItems.indexOf(message);
    }

    private int addPicMsg(String picPath, String uniqueID) {
        Message message = new Message();
        message.setSender(NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_drivername));
        message.setRole(getString(R.string.message_role_mobile));
        message.setTime(CommonUtils.getCurrentTime());
        message.setMsg("");
        message.setType(getString(R.string.message_content_type_picture));
        message.setCompanyID(NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_id));
        message.setVehicleID(NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id));
        message.setUniqueID(uniqueID);
        message.setVoiceTime("");
        message.setFileUrl(picPath);
        message.setSynced(null);
        message.setIsRead("false");
        chatMessageService.insert(message);
        msgItems.add(message);

        messageItemAdapter.notifyDataSetChanged();
        scrollToPosition();
        return msgItems.indexOf(message);
    }

    private class MessageListener implements MessageItemAdapter.OnMessageListener {

        @Override
        public void OnMessageListInteraction(Message item) {
            try {
                if (myPlayer != null && myPlayer.isPlaying()) {
                    myPlayer.stop();
                    myPlayer = null;
                }
                LogUtils.debugLog(LOG_TAG, "OnMessageListInteraction: " + item.getFileUrl());

                if (item == null || CommonUtils.isEmpty(item.getFileUrl()) || !new File(item.getFileUrl()).exists()) {
                    CommonUtils.toast(activity, "File expired");
                } else {
                    myPlayer = new MediaPlayer();
                    myPlayer.setDataSource(item.getFileUrl());
                    myPlayer.prepare();
                    myPlayer.start();
                }

            } catch (Exception e) {

                LogUtils.errorLog(LOG_TAG, e.getMessage(), e);

            }
        }

        @Override
        public void OnClickPictureInteraction(Message item) {
//            zoomImageFromThumb(view, bitmap);
            Intent intent = new Intent(activity, PictureViewPagerActivity.class);
            intent.putExtra("imgUrl", item.getFileUrl());
            activity.startActivity(intent);
            activity.overridePendingTransition(R.anim.activity_zoom_open, 0);
        }

        @Override
        public void ResendMsgFileInteraction(Message item, int index) {
            Message message = msgItems.get(index);
            message.setSynced(null);
            chatMessageService.updateSynced(message);
            uiHandler.sendEmptyMessage(HANDLER_MESSAGE_ADAPTER_NOFI);
            if(item.getType().equalsIgnoreCase(activity.getString(R.string.message_content_type_audio))) {
                UploadingFileTask uploadingFileTask = new UploadingFileTask();
                uploadingFileTask.execute(String.valueOf(index));
            } else if(item.getType().equalsIgnoreCase(activity.getString(R.string.message_content_type_picture))) {
                ImageUploadTask imageUploadTask = new ImageUploadTask();
                imageUploadTask.execute(String.valueOf(index));
            }

        }
    }

    private View getGridChildView() {

        View view = View.inflate(activity, R.layout.quick_response_gridview, null);
        try {
            GridView gridView = (GridView) view.findViewById(R.id.gridview);
            Set<String> set = NetcorpGPSApplication.sharedPrefManager.getStringSet(R.string.shared_pref_quick_msg_response);

            gridView.setAdapter(new QuickReponseGridViewAdapter(activity, set.toArray(new String[set.size()]), new QuickReponseGridViewAdapter.OnResponseListener() {
                @Override
                public void onResponse(String message) {
                    et_msg.setText(message);
                    setModeKeyboard(buttonSetModeKeyboard);
                }
            }));
        } catch (Exception e) {
            LogUtils.errorLog(LOG_TAG, e.getMessage(), e);
        }
        return view;
    }

    private View getGridChildOptionsView() {

        View view = View.inflate(activity, R.layout.more_options_gridview, null);
        try {
            GridView gridView = (GridView) view.findViewById(R.id.gridview);
            List<View> items = new ArrayList<>();

            View albumView = View.inflate(activity, R.layout.chat_option, null);
            ImageView imageAlbum = (ImageView) albumView.findViewById(R.id.ivIcon);
            imageAlbum.setImageResource(R.drawable.ic_option_pic);
            TextView textAlbum = (TextView) albumView.findViewById(R.id.tVContent);
            textAlbum.setText("Album");
            albumView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, Activity_Result_Request_Code_pickPhoto);
                }
            });
            items.add(albumView);

            View cameraView = View.inflate(activity, R.layout.chat_option, null);
            ImageView imageCamera = (ImageView) cameraView.findViewById(R.id.ivIcon);
            imageCamera.setImageResource(R.drawable.ic_option_camera);
            TextView textCamera = (TextView) cameraView.findViewById(R.id.tVContent);
            textCamera.setText("Camera");
            cameraView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                    takePicture.putExtra(MediaStore.EXTRA_OUTPUT, "fd");
//                    startActivityForResult(takePicture, Activity_Result_Request_Code_takePicture);
                    File fos = null;
                    try {
                        File fileDir = new File(LogFileUtils.getFilesDir());
                        if (!fileDir.exists()) {
                            fileDir.mkdir();
                        }
                        fos = new File(LogFileUtils.getFilesDir() + File.separator + "tem.png");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//                    Uri u = Uri.fromFile(fos);
                    Uri u = FileProvider.getUriForFile(activity, "com.netcorpgps.v3.dispatch.application.provider", fos);
                    if (Build.VERSION.SDK_INT >= 24) {
                        u = FileProvider.getUriForFile(activity, "com.netcorpgps.v3.dispatch.application.provider", fos);
                    }
                    Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    i.putExtra(MediaStore.Images.Media.ORIENTATION, 0);
                    i.putExtra(MediaStore.EXTRA_OUTPUT, u);

                    startActivityForResult(i, Activity_Result_Request_Code_takePicture);
                }
            });
            items.add(cameraView);

            gridView.setAdapter(new MoreOptionsGridViewAdapter(activity, items, new MoreOptionsGridViewAdapter.OnResponseListener() {
                @Override
                public void onResponse(String message) {
                }
            }));
        } catch (Exception e) {
            LogUtils.errorLog(LOG_TAG, e.getMessage(), e);
        }
        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            // choose picture from gallery
            case Activity_Result_Request_Code_pickPhoto:
                if (resultCode == Activity.RESULT_OK) {
                    Uri uri = data.getData();
//                    String path = getPath(uri);

                    String uniqueID = String.valueOf(System.currentTimeMillis());
//                    String filePath = BitmapUtils.regenerateBitMap(path, uniqueID);
                    String filePath = BitmapUtils.regenerateBitMap(activity, uri, uniqueID);

                    if (CommonUtils.isEmpty(filePath)) {
                        CommonUtils.toast(activity, getString(R.string.msg_failed_send_picture));
                    } else {
                        LogUtils.debugLog(LOG_TAG, filePath);
                        int index = addPicMsg(filePath, uniqueID);
                        ImageUploadTask imageUploadTask = new ImageUploadTask();
                        imageUploadTask.execute(String.valueOf(index));
                    }
                }

                break;
            // choose picture by camera
            case Activity_Result_Request_Code_takePicture:
                if (resultCode == Activity.RESULT_OK) {
//                    Bitmap original = (Bitmap) data.getExtras().get("data");
                    String uniqueID = String.valueOf(System.currentTimeMillis());
//                    String filePath = BitmapUtils.regenerateBitMap(activity, original, uniqueID);
                    String filePath = BitmapUtils.regenerateBitMap(activity, LogFileUtils.getFilesDir() + File.separator + "tem.png", uniqueID);

                    if (CommonUtils.isEmpty(filePath)) {
                        CommonUtils.toast(activity, getString(R.string.msg_failed_send_picture));
                    } else {
                        LogUtils.debugLog(LOG_TAG, filePath);
                        int index = addPicMsg(filePath, uniqueID);
                        ImageUploadTask imageUploadTask = new ImageUploadTask();
                        imageUploadTask.execute(String.valueOf(index));
                    }

                }
                break;
        }
    }

    private void zoomImageFromThumb(final View thumbView, Bitmap bitmap) {
        // If there's an animation in progress, cancel it
        // immediately and proceed with this one.
        if (mCurrentAnimator != null) {
            mCurrentAnimator.cancel();
        }

        // Load the high-resolution "zoomed-in" image.
//        final ImageView expandedImageView = (ImageView) activity.findViewById(R.id.expanded_image);
        final ImageView expandedImageView = null;
        expandedImageView.setImageBitmap(bitmap);

        // Calculate the starting and ending bounds for the zoomed-in image.
        // This step involves lots of math. Yay, math.
        final Rect startBounds = new Rect();
        final Rect finalBounds = new Rect();
        final Point globalOffset = new Point();

        // The start bounds are the global visible rectangle of the thumbnail,
        // and the final bounds are the global visible rectangle of the container
        // view. Also set the container view's offset as the origin for the
        // bounds, since that's the origin for the positioning animation
        // properties (X, Y).
        thumbView.getGlobalVisibleRect(startBounds);
        activity.findViewById(R.id.swipeRefreshLayout).getGlobalVisibleRect(finalBounds, globalOffset);
        startBounds.offset(-globalOffset.x, -globalOffset.y);
        finalBounds.offset(-globalOffset.x, -globalOffset.y);

        // Adjust the start bounds to be the same aspect ratio as the final
        // bounds using the "center crop" technique. This prevents undesirable
        // stretching during the animation. Also calculate the start scaling
        // factor (the end scaling factor is always 1.0).
        float startScale;
        if ((float) finalBounds.width() / finalBounds.height()
                > (float) startBounds.width() / startBounds.height()) {
            // Extend start bounds horizontally
            startScale = (float) startBounds.height() / finalBounds.height();
            float startWidth = startScale * finalBounds.width();
            float deltaWidth = (startWidth - startBounds.width()) / 2;
            startBounds.left -= deltaWidth;
            startBounds.right += deltaWidth;
        } else {
            // Extend start bounds vertically
            startScale = (float) startBounds.width() / finalBounds.width();
            float startHeight = startScale * finalBounds.height();
            float deltaHeight = (startHeight - startBounds.height()) / 2;
            startBounds.top -= deltaHeight;
            startBounds.bottom += deltaHeight;
        }

        // Hide the thumbnail and show the zoomed-in view. When the animation
        // begins, it will position the zoomed-in view in the place of the
        // thumbnail.
        thumbView.setAlpha(0f);
        expandedImageView.setVisibility(View.VISIBLE);

        // Set the pivot point for SCALE_X and SCALE_Y transformations
        // to the top-left corner of the zoomed-in view (the default
        // is the center of the view).
        expandedImageView.setPivotX(0f);
        expandedImageView.setPivotY(0f);

        // Construct and run the parallel animation of the four translation and
        // scale properties (X, Y, SCALE_X, and SCALE_Y).
        AnimatorSet set = new AnimatorSet();
        set.play(ObjectAnimator.ofFloat(expandedImageView, View.X, startBounds.left, finalBounds.left)).with(ObjectAnimator.ofFloat(expandedImageView, View.Y, startBounds.top, finalBounds.top))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
                        startScale, 1f)).with(ObjectAnimator.ofFloat(expandedImageView,
                View.SCALE_Y, startScale, 1f));
        set.setDuration(mShortAnimationDuration);
        set.setInterpolator(new DecelerateInterpolator());
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mCurrentAnimator = null;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                mCurrentAnimator = null;
            }
        });
        set.start();
        mCurrentAnimator = set;

        // Upon clicking the zoomed-in image, it should zoom back down
        // to the original bounds and show the thumbnail instead of
        // the expanded image.
        final float startScaleFinal = startScale;
        expandedImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCurrentAnimator != null) {
                    mCurrentAnimator.cancel();
                }

                // Animate the four positioning/sizing properties in parallel,
                // back to their original values.
                AnimatorSet set = new AnimatorSet();
                set.play(ObjectAnimator
                        .ofFloat(expandedImageView, View.X, startBounds.left))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.Y, startBounds.top))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.SCALE_X, startScaleFinal))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.SCALE_Y, startScaleFinal));
                set.setDuration(mShortAnimationDuration);
                set.setInterpolator(new DecelerateInterpolator());
                set.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        thumbView.setAlpha(1f);
                        expandedImageView.setVisibility(View.GONE);
                        mCurrentAnimator = null;
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        thumbView.setAlpha(1f);
                        expandedImageView.setVisibility(View.GONE);
                        mCurrentAnimator = null;
                    }
                });
                set.start();
                mCurrentAnimator = set;
                expandedImageView.setVisibility(View.INVISIBLE);
            }
        });
    }
}
