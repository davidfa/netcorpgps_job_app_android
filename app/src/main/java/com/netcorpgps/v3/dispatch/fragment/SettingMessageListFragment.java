package com.netcorpgps.v3.dispatch.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.activity.SlideInActivity;
import com.netcorpgps.v3.dispatch.adapter.SettingChatMessageItemAdapter;
import com.netcorpgps.v3.dispatch.application.NetcorpGPSApplication;
import com.netcorpgps.v3.dispatch.component.layout.SwipeLayout;
import com.netcorpgps.v3.dispatch.utils.CommonUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SettingMessageListFragment extends Fragment {

    private final String LOG_TAG = SettingMessageListFragment.class.getSimpleName();
    RecyclerView recyclerView = null;
    private SettingChatMessageItemAdapter msgItemAdapter = null;
    private AppCompatActivity activity;
    private View fragmentView;
    private List<String> itemList;
    private MenuBuilder menuBuilder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (activity == null)
            activity = (AppCompatActivity) getActivity();
        // show option menu
        setHasOptionsMenu(true);
        if (fragmentView == null) {
            fragmentView = inflater.inflate(R.layout.fragment_setting_message_list, container, false);
        }

        if (recyclerView == null) {
            recyclerView = (RecyclerView) fragmentView.findViewById(R.id.rvJobs);
            recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        }

        Set set = NetcorpGPSApplication.sharedPrefManager.getStringSet(R.string.shared_pref_quick_msg_response);
//        String[] items = activity.getResources().getStringArray(R.array.message_response_array);
        itemList = new ArrayList<>(set);
        return fragmentView;
    }

    public void onStart() {
        super.onStart();
        setActivityTitle();
        bindingListItems();
        setMenuBuilder();
    }

    public void onResume() {
        super.onResume();
    }

    public void bindingListItems() {

        msgItemAdapter = new SettingChatMessageItemAdapter(getActivity(), itemList, new OnItemClickListener());
        recyclerView.setAdapter(msgItemAdapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    SwipeLayout preLayout = msgItemAdapter.getPreLayout();
                    if (preLayout != null) {
                        preLayout.close();
                    }
                }

                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }


    private void setActivityTitle() {
        if (activity instanceof SlideInActivity) {
            ((SlideInActivity) activity).setActionBarTitle("Chat Message");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_dropdown, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.dropdown_menu:
                View menuDropdownView = getActivity().findViewById(R.id.dropdown_menu);

                MenuPopupHelper optionsMenu = new MenuPopupHelper(getContext(), menuBuilder, menuDropdownView);
                optionsMenu.setForceShowIcon(true);
                optionsMenu.show();
                break;
        }
        return false;
    }

    private void setMenuBuilder() {
        menuBuilder = new MenuBuilder(getContext());
        menuBuilder.setCallback(new MenuBuilder.Callback() {

            @Override
            public boolean onMenuItemSelected(MenuBuilder menu, MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.addItem:
                        addItem();
                        break;
                    case R.id.delAllItems:
                        deleteAllItems();
                        break;
                }

                return false;
            }

            @Override
            public void onMenuModeChange(MenuBuilder menu) {

            }
        });

        MenuInflater inflater = new MenuInflater(getContext());
        inflater.inflate(R.menu.menu_setting_chat_dropdown, menuBuilder);

    }

    private void addItem() {
        if (itemList != null && itemList.size() >= 6) {
            CommonUtils.toast(activity, getString(R.string.setting_max_msg_number));
            return;
        }
        new MaterialDialog.Builder(activity)
                .title(R.string.setting_dialog_title_add_msg)
                .inputRangeRes(2, 30, R.color.red)
                .input(null, null, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, CharSequence input) {
                        itemList.add(input.toString());
                        msgItemAdapter.notifyDataSetChanged();
                        updateSharedPre();
                    }
                }).show();
    }

    private void deleteAllItems() {
        itemList.clear();
        msgItemAdapter.notifyDataSetChanged();
        updateSharedPre();
    }

    class OnItemClickListener implements SettingChatMessageItemAdapter.OnItemClickListener {
        @Override
        public void onEdit(int position) {

        }

        @Override
        public void onDelete(int position) {
            itemList.remove(position);
            msgItemAdapter.notifyDataSetChanged();
            updateSharedPre();
        }
    }

    private void updateSharedPre() {
        Set<String> set = new HashSet<String>(itemList);
        NetcorpGPSApplication.sharedPrefManager.setStringSet(R.string.shared_pref_quick_msg_response, set);
    }
}
