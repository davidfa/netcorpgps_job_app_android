package com.netcorpgps.v3.dispatch.fragment;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.activity.MainActivity;
import com.netcorpgps.v3.dispatch.adapter.JobMenuItemAdapter;
import com.netcorpgps.v3.dispatch.adapter.SettingItemAdapter;

/**
 * Created by David Fa on 2/05/2017.
 */

public class SettingsFragment extends Fragment {

    private final String LOG_TAG = SettingsFragment.class.getSimpleName();
    private AppCompatActivity activity;
    private RecyclerView recyclerView = null;
    private View fragmentView;
    private SettingItemAdapter settingItemAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (AppCompatActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (fragmentView == null) {
            fragmentView = inflater.inflate(R.layout.fragment_settings, container, false);
        }

        if (recyclerView == null) {
            recyclerView = (RecyclerView) fragmentView.findViewById(R.id.rvSetting);
            recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        }
        settingItemAdapter = new SettingItemAdapter(getActivity());
        recyclerView.setAdapter(settingItemAdapter);
        return fragmentView;
    }

    public void onStart() {
        closeSysMsgLayout();
        setActivityTitle();
        super.onStart();
    }

    private void closeSysMsgLayout() {
        if (activity instanceof MainActivity) {
            ((MainActivity) activity).closeSysMsgLayout();
        }
    }

    private void setActivityTitle() {
        if (activity instanceof MainActivity) {
            ((MainActivity) activity).setActionBarTitle(getString(R.string.menu_setting));
        }
    }
}
