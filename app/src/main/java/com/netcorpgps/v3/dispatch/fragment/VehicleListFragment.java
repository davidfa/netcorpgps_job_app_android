package com.netcorpgps.v3.dispatch.fragment;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.activity.SlideInActivity;
import com.netcorpgps.v3.dispatch.adapter.VehicleItemAdapter;
import com.netcorpgps.v3.dispatch.api.ErrorHandler.VolleyErrorHandler;
import com.netcorpgps.v3.dispatch.api.bean.VehiclesBean;
import com.netcorpgps.v3.dispatch.api.volley.GsonRequest;
import com.netcorpgps.v3.dispatch.application.NetcorpGPSApplication;
import com.netcorpgps.v3.dispatch.utils.CommonUtils;
import com.netcorpgps.v3.dispatch.utils.LogUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by David Fa on 15/05/2017.
 */

public class VehicleListFragment extends Fragment {
    private final String LOG_TAG = VehicleListFragment.class.getSimpleName();
    private OnVehicleListListener mListener;
    ProgressDialog dialog = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vehicle_list, container, false);
        dialog = ProgressDialog.show(getActivity(), "", getString(R.string.msg_loading), true);
        Activity activity = getActivity();
        if (activity instanceof SlideInActivity) {
            ((SlideInActivity) activity).setActionBarTitle("Vehicles");
        }
        String companyDB = getActivity().getIntent().getStringExtra("companyDB");
        Map map = new HashMap();
        map.put("company", companyDB);
        String ip = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_ip);
        String apiGetVehicleList = getString(R.string.api_get_vehicleList);
        String url = ip + apiGetVehicleList;
        GsonRequest<VehiclesBean> jsObjRequest = new GsonRequest<>(Request.Method.POST, url, VehiclesBean.class, map, null, new GetVehicleListListener(view), new GetVehicleErrorListener());
        jsObjRequest.setShouldCache(false);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(Integer.parseInt(getString(R.string.sys_timeout)), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsObjRequest.setTag(getString(R.string.request_tag_get_vehicles));
        NetcorpGPSApplication.getInstance().getRequestQueue().add(jsObjRequest);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnVehicleListListener) {
            mListener = (OnVehicleListListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnVehicleListListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnVehicleListListener {
        // TODO: Update argument type and name
        void onVehicleListInteraction(VehiclesBean.Vehicle item);
    }

    class GetVehicleListListener implements Response.Listener<VehiclesBean> {
        View view;

        GetVehicleListListener(View view) {
            this.view = view;
        }

        @Override
        public void onResponse(VehiclesBean response) {
            try {
                if (response != null) {
                    if (response.getStatus() == 1) {

                        // Set the adapter
                        if (view instanceof RecyclerView) {
                            String vehicleName = getActivity().getIntent().getStringExtra("vehicleName");
                            RecyclerView recyclerView = (RecyclerView) view;

                            recyclerView.setAdapter(new VehicleItemAdapter(response.getVehicleList(), mListener, vehicleName, recyclerView));
                            recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
                            // set focus on position vehicle selected
                            for (int i = 0; i < response.getVehicleList().size(); i++) {
                                VehiclesBean.Vehicle vehicle = response.getVehicleList().get(i);
                                if (!CommonUtils.isEmpty(vehicleName) && vehicleName.equals(vehicle.getName())) {
                                    int position = (i >= 1) ? i - 1 : i;
                                    recyclerView.scrollToPosition(position);
                                    break;
                                }
                            }

                        }
                    } else {
                        CommonUtils.toast(getActivity(), response.getMsg());
                        LogUtils.errorLog(LOG_TAG, response.getMsg());
                    }
                }
            } catch (Exception e) {
                CommonUtils.toast(getActivity(), e.getMessage());
                LogUtils.errorLog(LOG_TAG, e.getMessage());
            } finally {
                dialog.dismiss();
            }
        }
    }

    class GetVehicleErrorListener implements Response.ErrorListener {
        @Override
        public void onErrorResponse(VolleyError error) {
            dialog.dismiss();
            String message = VolleyErrorHandler.handleError(getActivity(), error);

            CommonUtils.toast(getActivity(), message);
            LogUtils.errorLog(LOG_TAG, error.getMessage());
        }
    }
}
