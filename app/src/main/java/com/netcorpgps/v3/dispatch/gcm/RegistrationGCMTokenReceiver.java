package com.netcorpgps.v3.dispatch.gcm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.application.NetcorpGPSApplication;

/**
 * Created by David Fa on 9/06/2017.
 */

public class RegistrationGCMTokenReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        boolean sentToken = NetcorpGPSApplication.sharedPrefManager.getBoolean(R.string.shared_pref_gcm_sentTokenToServer);
        if (sentToken) {
            String token = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_gcm_token);
//                    CommonUtils.toast(MainActivity.this, token);
        } else {
            // Todo implement send alert to server of which device cannot getting gcm token
//                    CommonUtils.toast(MainActivity.this, "Failed");
        }
    }
}
