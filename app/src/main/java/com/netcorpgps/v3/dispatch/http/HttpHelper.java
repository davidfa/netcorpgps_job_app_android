package com.netcorpgps.v3.dispatch.http;

import android.content.Context;

import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.application.NetcorpGPSApplication;
import com.netcorpgps.v3.dispatch.utils.CommonUtils;
import com.netcorpgps.v3.dispatch.utils.LogFileUtils;
import com.netcorpgps.v3.dispatch.utils.LogUtils;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by David Fa on 23/08/2017.
 */

public class HttpHelper {

    private static final String LOG_TAG = HttpHelper.class.getSimpleName();
    public static final int CONN_TIMEOUT_VALUE = 20 * 1000;
    public static final int READ_TIMEOUT_VALUE = 10 * 1000;

    public static String uploadVoiceToServer(Context context, String fileUrl, String uniqueID) {

        int serverResponseCode = 0;

        LogUtils.debugLog(LOG_TAG, "FileUrl: " + fileUrl);
        if(CommonUtils.isEmpty(fileUrl)) {
            return "false";
        }

        File directory = new File(LogFileUtils.getFilesDir());
        if (directory.exists()) {

            File voice = new File(fileUrl);
            if (voice.exists()) {
                String companyID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_id);
                String vehicleID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id);
                String boundary = "*****";
                String lineEnd = "\r\n";
                String twoHyphens = "--";
                String crlf = "\r\n";
                int bytesRead, bytesAvailable, bufferSize;
                byte[] buffer;
                int maxBufferSize = 1 * 1024;
                String ip = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_ip);
                String api = context.getString(R.string.api_store_voice_file);
                String urlStr = ip + api;

                LogUtils.debugLog(LOG_TAG, "UploadFile URL: " + urlStr);

                HttpURLConnection conn = null;
                DataOutputStream dos = null;
                FileInputStream fis = null;
                try {
                    fis = new FileInputStream(voice);
                    URL url = new URL(urlStr);
                    conn = (HttpURLConnection) url.openConnection();
                    conn.setDoInput(true);
                    conn.setDoOutput(true);
                    conn.setUseCaches(false);
//                    conn.setChunkedStreamingMode(maxBufferSize);
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Connection", "Keep-Alive");
                    conn.setRequestProperty("Accept", "text/*");
                    conn.setConnectTimeout(CONN_TIMEOUT_VALUE);
                    conn.setReadTimeout(READ_TIMEOUT_VALUE);
//                    conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                    conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

                    dos = new DataOutputStream(conn.getOutputStream());
                    dos.writeBytes(twoHyphens + boundary + lineEnd);
                    dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_files\"; filename=\"" + uniqueID + "\"" + lineEnd);
                    dos.writeBytes(lineEnd);
                    // create a buffer of  maximum size
                    bytesAvailable = fis.available();

                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    buffer = new byte[bufferSize];

                    // read file and write it into form...
                    bytesRead = fis.read(buffer, 0, bufferSize);
                    while (bytesRead > 0) {
                        try {
                            dos.write(buffer, 0, bufferSize);
                        } catch (OutOfMemoryError e) {
                            LogUtils.debugLog(LOG_TAG, "UploadFile Error: OutOfMemoryError", e);
                        }
//                        dos.write(buffer, 0, bufferSize);
                        bytesAvailable = fis.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        bytesRead = fis.read(buffer, 0, bufferSize);

                    }
                    // send multipart form data necesssary after file data...
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(twoHyphens + boundary + lineEnd);

                    // add parameters
//                    dos.writeBytes("Content-Disposition: form-data; name=\"company\"" + crlf);
//                    dos.writeBytes(lineEnd);
//                    dos.writeBytes(companyID);
//                    dos.writeBytes(lineEnd);
//                    dos.writeBytes(twoHyphens + boundary + lineEnd);
//                    dos.writeBytes("Content-Disposition: form-data; name=\"vehicle\"" + crlf);
//                    dos.writeBytes(lineEnd);
//                    dos.writeBytes(vehicleID);
//                    dos.writeBytes(lineEnd);
//                    dos.writeBytes(twoHyphens + boundary + lineEnd);
//                    dos.writeBytes("Content-Disposition: form-data; name=\"sender\"" + crlf);
//                    dos.writeBytes(lineEnd);
//                    dos.writeBytes(NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_drivername));
//                    dos.writeBytes(lineEnd);
//                    dos.writeBytes(twoHyphens + boundary + lineEnd);
//                    dos.writeBytes("Content-Disposition: form-data; name=\"time\"" + crlf);
//                    dos.writeBytes(lineEnd);
//                    dos.writeBytes(CommonUtils.getCurrentTime());
//                    dos.writeBytes(lineEnd);
//                    dos.writeBytes(twoHyphens + boundary + lineEnd);
//                    dos.writeBytes("Content-Disposition: form-data; name=\"role\"" + crlf);
//                    dos.writeBytes(lineEnd);
//                    dos.writeBytes(context.getString(R.string.message_role_mobile));
//                    dos.writeBytes(lineEnd);
//                    dos.writeBytes(twoHyphens + boundary + lineEnd);
//                    dos.writeBytes("Content-Disposition: form-data; name=\"type\"" + crlf);
//                    dos.writeBytes(lineEnd);
//                    dos.writeBytes(context.getString(R.string.message_content_type_audio));
//                    dos.writeBytes(lineEnd);
//                    dos.writeBytes(twoHyphens + boundary + lineEnd);
                    dos.writeBytes("Content-Disposition: form-data; name=\"uniqueID\"" + crlf);
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(uniqueID);
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(twoHyphens + boundary + twoHyphens);
                    LogUtils.infoLog(LOG_TAG, "UploadFile-- company: " + companyID + ", vehicle: " + vehicleID + ", uniqueID: " + uniqueID);
                    // Responses from the server (code and message)
                    serverResponseCode = conn.getResponseCode();
                    String serverResponseMessage = conn.getResponseMessage();
                    LogUtils.infoLog(LOG_TAG, "UploadFile-- HTTP Response is : " + serverResponseMessage + ": " + serverResponseCode);
                    //close the streams //
                    fis.close();
                    dos.flush();
                    dos.close();
                    dos = null;
                    fis = null;
                    conn.disconnect();
                } catch (Exception e) {
                    LogUtils.errorLog(LOG_TAG, "Failure in uploading log file-- " + e.getMessage(), e);
                } finally {
                    if (conn != null) {
                        try {
                            conn.disconnect();
                        } catch (Exception e) {

                        }
                    }
                    if (fis != null) {
                        try {
                            fis.close();
                            fis = null;
                        } catch (Exception e) {

                        }
                    }
                    if (dos != null) {
                        try {
                            dos.close();
                            dos = null;
                        } catch (Exception e) {

                        }
                    }

                }
            }


        }

        if (serverResponseCode == 200) {
            return "true";
        } else {
            return "false";
        }
    }

    public static String uploadPictureToServer(Context context, String fileUrl, String uniqueID) {

        int serverResponseCode = 0;

        LogUtils.debugLog(LOG_TAG, "FileUrl: " + fileUrl);
        if(CommonUtils.isEmpty(fileUrl)) {
            return "false";
        }

        File directory = new File(LogFileUtils.getFilesDir());
        if (directory.exists()) {

            File voice = new File(fileUrl);
            if (voice.exists()) {
                String companyID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_id);
                String vehicleID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id);
                String boundary = "*****";
                String lineEnd = "\r\n";
                String twoHyphens = "--";
                String crlf = "\r\n";
                int bytesRead, bytesAvailable, bufferSize;
                byte[] buffer;
                int maxBufferSize = 1 * 1024;
                String ip = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_ip);
                String api = context.getString(R.string.api_store_picture_file);
                String urlStr = ip + api;

                LogUtils.debugLog(LOG_TAG, "UploadFile URL: " + urlStr);

                HttpURLConnection conn = null;
                DataOutputStream dos = null;
                FileInputStream fis = null;
                try {
                    fis = new FileInputStream(voice);
                    URL url = new URL(urlStr);
                    conn = (HttpURLConnection) url.openConnection();
                    conn.setDoInput(true);
                    conn.setDoOutput(true);
                    conn.setUseCaches(false);
//                    conn.setChunkedStreamingMode(maxBufferSize);
                    conn.setConnectTimeout(CONN_TIMEOUT_VALUE);
                    conn.setReadTimeout(READ_TIMEOUT_VALUE);
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Connection", "Keep-Alive");
                    conn.setRequestProperty("Accept", "text/*");
//                    conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                    conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

                    dos = new DataOutputStream(conn.getOutputStream());
                    dos.writeBytes(twoHyphens + boundary + lineEnd);
                    dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_files\"; filename=\"" + uniqueID + "\"" + lineEnd);
                    dos.writeBytes(lineEnd);
                    // create a buffer of  maximum size
                    bytesAvailable = fis.available();

                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    buffer = new byte[bufferSize];

                    // read file and write it into form...
                    bytesRead = fis.read(buffer, 0, bufferSize);
                    while (bytesRead > 0) {
                        try {
                            dos.write(buffer, 0, bufferSize);
                        } catch (OutOfMemoryError e) {
                            LogUtils.debugLog(LOG_TAG, "UploadFile Error: OutOfMemoryError", e);
                        }
//                        dos.write(buffer, 0, bufferSize);
                        bytesAvailable = fis.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        bytesRead = fis.read(buffer, 0, bufferSize);

                    }
                    // send multipart form data necesssary after file data...
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(twoHyphens + boundary + lineEnd);

                    // add parameters
//                    dos.writeBytes("Content-Disposition: form-data; name=\"company\"" + crlf);
//                    dos.writeBytes(lineEnd);
//                    dos.writeBytes(companyID);
//                    dos.writeBytes(lineEnd);
//                    dos.writeBytes(twoHyphens + boundary + lineEnd);
//                    dos.writeBytes("Content-Disposition: form-data; name=\"vehicle\"" + crlf);
//                    dos.writeBytes(lineEnd);
//                    dos.writeBytes(vehicleID);
//                    dos.writeBytes(lineEnd);
//                    dos.writeBytes(twoHyphens + boundary + lineEnd);
//                    dos.writeBytes("Content-Disposition: form-data; name=\"sender\"" + crlf);
//                    dos.writeBytes(lineEnd);
//                    dos.writeBytes(NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_drivername));
//                    dos.writeBytes(lineEnd);
//                    dos.writeBytes(twoHyphens + boundary + lineEnd);
//                    dos.writeBytes("Content-Disposition: form-data; name=\"time\"" + crlf);
//                    dos.writeBytes(lineEnd);
//                    dos.writeBytes(CommonUtils.getCurrentTime());
//                    dos.writeBytes(lineEnd);
//                    dos.writeBytes(twoHyphens + boundary + lineEnd);
//                    dos.writeBytes("Content-Disposition: form-data; name=\"role\"" + crlf);
//                    dos.writeBytes(lineEnd);
//                    dos.writeBytes(context.getString(R.string.message_role_mobile));
//                    dos.writeBytes(lineEnd);
//                    dos.writeBytes(twoHyphens + boundary + lineEnd);
//                    dos.writeBytes("Content-Disposition: form-data; name=\"type\"" + crlf);
//                    dos.writeBytes(lineEnd);
//                    dos.writeBytes(context.getString(R.string.message_content_type_audio));
//                    dos.writeBytes(lineEnd);
//                    dos.writeBytes(twoHyphens + boundary + lineEnd);
                    dos.writeBytes("Content-Disposition: form-data; name=\"uniqueID\"" + crlf);
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(uniqueID);
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(twoHyphens + boundary + twoHyphens);
                    LogUtils.infoLog(LOG_TAG, "UploadFile-- company: " + companyID + ", vehicle: " + vehicleID + ", uniqueID: " + uniqueID);
                    // Responses from the server (code and message)
                    serverResponseCode = conn.getResponseCode();
                    String serverResponseMessage = conn.getResponseMessage();
                    LogUtils.infoLog(LOG_TAG, "UploadFile-- HTTP Response is : " + serverResponseMessage + ": " + serverResponseCode);
                    //close the streams //
                    fis.close();
                    dos.flush();
                    dos.close();
                    dos = null;
                    fis = null;
                    conn.disconnect();
                } catch (Exception e) {
                    LogUtils.errorLog(LOG_TAG, "Failure in uploading log file-- " + e.getMessage(), e);
                } finally {
                    if (conn != null) {
                        try {
                            conn.disconnect();
                        } catch (Exception e) {

                        }
                    }
                    if (fis != null) {
                        try {
                            fis.close();
                            fis = null;
                        } catch (Exception e) {

                        }
                    }
                    if (dos != null) {
                        try {
                            dos.close();
                            dos = null;
                        } catch (Exception e) {

                        }
                    }

                }
            }


        }

        if (serverResponseCode == 200) {
            return "true";
        } else {
            return "false";
        }
    }
}
