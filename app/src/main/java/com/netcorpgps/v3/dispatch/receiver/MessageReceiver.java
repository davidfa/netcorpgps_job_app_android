package com.netcorpgps.v3.dispatch.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.netcorpgps.v3.dispatch.R;

/**
 * Created by David Fa on 15/08/2017.
 */

public class MessageReceiver extends BroadcastReceiver {

    private final String LOG_TAG = MessageReceiver.class.getSimpleName();
    private ReceivedMessageResponse mResponse;
    private Context mContext;

    public MessageReceiver(Context context, ReceivedMessageResponse response) {
        mResponse = response;
        mContext = context;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            if (action.equalsIgnoreCase(mContext.getString(R.string.intent_filter_receive_message))) {
                mResponse.receiveMsg(bundle);
            } else if (action.equalsIgnoreCase(mContext.getString(R.string.intent_filter_send_message))) {
                mResponse.sendMsg(bundle);
            }
        }
//        if (bundle != null) {
//            String sender = (String) bundle.get("sender");
//            String role = (String) bundle.get("role");
//            String time = (String) bundle.get("time");
//            String msg = (String) bundle.get("msg");
//            String companyID = (String) bundle.get("companyID");
//            String vehicleID = (String) bundle.get("vehicleID");
//            mResponse.response(bundle);
//        }
    }

    public interface ReceivedMessageResponse {
        void receiveMsg(Bundle bundle);
        void sendMsg(Bundle bundle);
    }
}
