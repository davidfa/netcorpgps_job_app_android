package com.netcorpgps.v3.dispatch.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by David Fa on 8/06/2017.
 */

public class NetworkStateReceiver extends BroadcastReceiver {
    private final String LOG_TAG = NetworkStateReceiver.class.getSimpleName();

    public interface NetworkStateResponse {
        void responseToConnectionOn();
        void responseToConnectionOff();
    }

    private NetworkStateResponse mResponse;
    public NetworkStateReceiver(NetworkStateResponse response) {
        mResponse = response;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getExtras() != null) {
            final ConnectivityManager connectivityManager;
            connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
            final NetworkInfo ni = connectivityManager.getActiveNetworkInfo();

            if (ni != null && ni.isConnectedOrConnecting()) {
                mResponse.responseToConnectionOn();
            } else if (intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, Boolean.FALSE)) {
                mResponse.responseToConnectionOff();
            }
        }
    }
}
