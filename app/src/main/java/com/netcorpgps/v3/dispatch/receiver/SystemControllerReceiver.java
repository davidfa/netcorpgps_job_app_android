package com.netcorpgps.v3.dispatch.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.utils.CommonUtils;

/**
 * Created by David Fa on 31/07/2017.
 */

public class SystemControllerReceiver extends BroadcastReceiver {

    private SystemControllerResponse mResponse;
    private Context mContext;

    public SystemControllerReceiver(Context context, SystemControllerResponse response) {
        mResponse = response;
        mContext = context;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            String command = (String) bundle.get("command");
            if (!CommonUtils.isEmpty(command) && command.equalsIgnoreCase(mContext.getString(R.string.intent_filter_exit_app))) {
                mResponse.exitApp();
            }
        }
    }

    public interface SystemControllerResponse {
        void exitApp();
    }
}
