package com.netcorpgps.v3.dispatch.service;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.api.bean.MessageBean;
import com.netcorpgps.v3.dispatch.api.volley.GsonRequest;
import com.netcorpgps.v3.dispatch.application.NetcorpGPSApplication;
import com.netcorpgps.v3.dispatch.db.bean.Message;
import com.netcorpgps.v3.dispatch.db.service.ChatMessageService;
import com.netcorpgps.v3.dispatch.receiver.MessageReceiver;
import com.netcorpgps.v3.dispatch.utils.CommonUtils;
import com.netcorpgps.v3.dispatch.utils.LogFileUtils;
import com.netcorpgps.v3.dispatch.utils.LogUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by David Fa on 14/08/2017.
 */

public class ChatService extends Service {

    private final String LOG_TAG = ChatService.class.getSimpleName();

    private Socket mSocket;
    private MessageReceiver messageReceiver;
    private ChatMessageService chatMessageService = null;

    @Override
    public void onCreate() {
        try {
            messageReceiver = new MessageReceiver(this, new MessageReceiver.ReceivedMessageResponse() {

                @Override
                public void receiveMsg(Bundle bundle) {

                }

                @Override
                public void sendMsg(Bundle bundle) {

                    Message message = (Message) bundle.getSerializable(getString(R.string.bundle_key_msg));
//                    JSONObject data = new JSONObject();
//                    try {
//                        data.put("sender", message.getSender());
//                        data.put("role", message.getRole());
//                        data.put("time", message.getTime());
//                        data.put("msg", message.getMsg());
//                        data.put("type", message.getType());
//                        data.put("companyID", message.getCompanyID());
//                        data.put("vehicleID", message.getVehicleID());
//                        data.put("uniqueID", message.getUniqueID());
//                        data.put("isSynced", message.getSynced());
//                        data.put("serverUrl", message.getServerUrl());
//                        data.put("isRead", message.getIsRead());
//                        chatMessageService.insert(message);
//                        LogUtils.debugLog(LOG_TAG, "Send message:" + message.toString());
//                    } catch (JSONException e) {
//                        LogUtils.debugLog(LOG_TAG, "JSONException:" + e.getMessage(), e);
//                    }
//
//                    mSocket.emit(getString(R.string.chat_socket_channel), data);
                    socketEmit(message);
                }
            });

            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(getString(R.string.intent_filter_send_message));
            registerReceiver(messageReceiver, intentFilter);

            // if remove port and reformat host in form of protocol:domain
            String host = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_ip);
            String[] ipAddress = host.split(":");
            if (ipAddress.length > 2) {
                host = ipAddress[0] + ":" + ipAddress[1];
            }
            LogUtils.debugLog(LOG_TAG, host);
            String socketIP = host + ":" + getString(R.string.socket_port);
            mSocket = IO.socket(socketIP);

            if (chatMessageService == null)
                chatMessageService = new ChatMessageService(getApplicationContext());

            NetcorpGPSApplication.sharedPrefManager.setInt(R.string.shared_pref_chat_msg_num, 0);

            queryUnreadMessage();
        } catch (URISyntaxException e) {
            LogUtils.errorLog(LOG_TAG, "Socket URISyntaxException Error: " + e.getMessage(), e);
        }
    }

    private void socketEmit(Message message) {
        JSONObject data = new JSONObject();
        try {
            data.put("sender", message.getSender());
            data.put("role", message.getRole());
            data.put("time", message.getTime());
            data.put("msg", message.getMsg());
            data.put("type", message.getType());
            data.put("companyID", message.getCompanyID());
            data.put("vehicleID", message.getVehicleID());
            data.put("uniqueID", message.getUniqueID());
            data.put("isSynced", message.getSynced());
            data.put("serverUrl", message.getServerUrl());
            data.put("isRead", message.getIsRead());
            chatMessageService.insert(message);
            LogUtils.debugLog(LOG_TAG, "Send message:" + message.toString());
        } catch (JSONException e) {
            LogUtils.debugLog(LOG_TAG, "JSONException:" + e.getMessage(), e);
        }

        mSocket.emit(getString(R.string.chat_socket_channel), data);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            chatMessageService.deleteByDate(CommonUtils.getDate(-7));
            mSocket.connect();
            mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                    LogUtils.debugLog(LOG_TAG, "Socket Connected");
                }

            }).on(getString(R.string.chat_socket_channel), new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                    JSONObject data = (JSONObject) args[0];

                    LogUtils.debugLog(LOG_TAG, "Recieve Message: " + data.toString());

                    try {
                        Intent intent = new Intent(getString(R.string.intent_filter_receive_message));
                        Bundle bundle = new Bundle();

                        Message message = new Message();
                        message.setSender((String) data.get("sender"));
                        message.setRole((String) data.get("role"));
                        message.setTime((String) data.get("time"));
                        message.setMsg((String) data.get("msg"));
                        message.setCompanyID((String) data.get("companyID"));
                        message.setVehicleID((String) data.get("vehicleID"));
                        message.setUniqueID(String.valueOf(data.get("uniqueID")));
                        message.setType((String) data.get("type"));
                        message.setSynced("true");
                        message.setIsRead("true");
                        message.setFileUrl("");
                        message.setServerUrl("");

                        if (validation(message)) {
                            if (!message.getRole().equalsIgnoreCase(getString(R.string.message_role_mobile))) {
                                int shared_pref_chat_msg_num = NetcorpGPSApplication.sharedPrefManager.getInt(R.string.shared_pref_chat_msg_num);
                                NetcorpGPSApplication.sharedPrefManager.setInt(R.string.shared_pref_chat_msg_num, ++shared_pref_chat_msg_num);

                                chatMessageService.insert(message);
                                Intent ackIntent = new Intent(getString(R.string.intent_filter_send_message));
                                Bundle ackBundle = new Bundle();
                                ackBundle.putSerializable(getString(R.string.bundle_key_msg), message);
                                ackIntent.putExtras(ackBundle);
                                sendBroadcast(ackIntent);
                            }
                            bundle.putSerializable(getString(R.string.bundle_key_msg), message);
                            intent.putExtras(bundle);
                            getApplicationContext().sendBroadcast(intent);
                        }

                    } catch (JSONException e) {
                        LogUtils.errorLog(LOG_TAG, "Socket Data JSONException: " + e.getMessage(), e);
                    }
                }

            }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                    LogUtils.debugLog(LOG_TAG, "Socket Disconnected");
                }

            });
        } catch (Exception e) {
            LogUtils.errorLog(LOG_TAG, "Socket Error: " + e.getMessage(), e);
        }

        // delete old message and files
        deleteExpiredMessageAndFiles();

        return START_REDELIVER_INTENT;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        try {
            if (mSocket != null) {
                mSocket.disconnect();
                mSocket = null;
            }
            if (messageReceiver != null) {
                unregisterReceiver(messageReceiver);
            }
        } catch (Exception e) {
            LogUtils.errorLog(LOG_TAG, "Socket Disconnection Error: " + e.getMessage(), e);
        }

        super.onDestroy();
    }

    private boolean validation(Message message) {
        String companyID = message.getCompanyID();
        String vehicleID = message.getVehicleID();
        if (companyID.equalsIgnoreCase(NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_id)) && vehicleID.equalsIgnoreCase(NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id))) {
            return true;
        }
        return false;
    }

    /**
     * Delete expired media files
     */
    private void deleteExpiredMessageAndFiles() {
        String chatMsgRetention = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_chat_msg_retention);
        String date = CommonUtils.getDate(-Integer.valueOf(chatMsgRetention)) + " 00:00:00";
        chatMessageService.deleteByDate(date);

        try {
            File audioDic = new File(LogFileUtils.getFilesDir());
            if (audioDic.exists()) {
                File[] files = audioDic.listFiles();
                for (File file : files) {
                    if (file.exists()) {
                        // get file name
                        String fileName = file.getName();
//                        LogUtils.debugLog(LOG_TAG, fileName);
                        // remove suffix to get milliseconds
                        String milliseconds = fileName.split(Pattern.quote("."))[0];
                        if (milliseconds.equalsIgnoreCase("tem")) {
                            file.delete();
                        } else {
                            // LogUtils.debugLog(LOG_TAG, milliseconds);
                            String datetime2 = CommonUtils.convertMilliToDate(Long.valueOf(milliseconds));
//                        LogUtils.debugLog(LOG_TAG, fileName + ": " + datetime2);
                            if (CommonUtils.compareDate(date, datetime2)) {
                                file.delete();
                            }
                        }

                    }
                }
            }
        } catch (Exception e) {
            LogUtils.errorLog(LOG_TAG, e.getMessage(), e);
        }
    }

    private void queryUnreadMessage() {
        // obtain unread message from server
        Map<String, String> map = new HashMap<>();
        String companyID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_id);
        String vehicleID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id);
        String ip = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_ip);
        String api = getString(R.string.api_query_unread_message);
        String url = ip + api;
        map.put("vehicleID", vehicleID);
        map.put("companyID", companyID);
        LogUtils.infoLog(LOG_TAG, url + ": " + map.toString());
        GsonRequest<MessageBean> jsObjRequest = new GsonRequest<>(Request.Method.POST, url, MessageBean.class, map, null, new ResponseListener(), new ErrorListener());
        jsObjRequest.setShouldCache(false);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(Integer.parseInt(getString(R.string.sys_timeout)), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsObjRequest.setTag(getString(R.string.request_tag_query_unread_message));
        NetcorpGPSApplication.getInstance().getRequestQueue().add(jsObjRequest);
    }

    class ResponseListener implements Response.Listener<MessageBean> {

        @Override
        public void onResponse(MessageBean response) {
            try {
                if (response != null) {
                    if (response.getStatus() == 1) {
//                        LogUtils.debugLog(LOG_TAG, response.getMessageList().toString());
                        List<Message> messages = response.getMessageList();
                        int size = messages.size();
                        NetcorpGPSApplication.sharedPrefManager.setInt(R.string.shared_pref_chat_msg_num, size);
                        for (Message message : messages) {
                            message.setIsRead("true");
                            socketEmit(message);
                        }
                    }
                }
            } catch (Exception e) {
                LogUtils.errorLog(LOG_TAG, e.getMessage(), e);
            } finally {
            }
        }
    }

    class ErrorListener implements Response.ErrorListener {

        @Override
        public void onErrorResponse(VolleyError error) {

            LogUtils.errorLog(LOG_TAG, error.getMessage(), error);
        }
    }
}
