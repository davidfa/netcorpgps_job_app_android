package com.netcorpgps.v3.dispatch.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;

import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.activity.MainActivity;
import com.netcorpgps.v3.dispatch.application.NetcorpGPSApplication;

/**
 * Created by David Fa on 22/06/2017.
 */

public class ForegroundService extends Service {

    @Override
    public void onCreate() {

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String driverName = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_drivername);
        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, getApplicationContext().getResources().getInteger(R.integer.pending_intent_request_code_foreground), notificationIntent, 0);

        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.appicon);
        Notification notification = new Notification.Builder(this)
                .setContentTitle(getString(R.string.notification_foreground_title))
                .setContentText(String.format(getString(R.string.notification_foreground_content), driverName))
                .setSmallIcon(R.drawable.ic_menu_status)
                .setColor(ContextCompat.getColor(getApplicationContext(), R.color.notifi_foreground))
                .setLargeIcon(Bitmap.createScaledBitmap(largeIcon, 128, 128, false))
                .setContentIntent(pendingIntent)
                .build();
        notification.flags |= Notification.FLAG_NO_CLEAR;
        startForeground(getApplicationContext().getResources().getInteger(R.integer.notification_id_foreground), notification);
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        stopForeground(true);
        stopSelf();
        super.onDestroy();
    }
}
