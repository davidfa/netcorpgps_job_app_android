package com.netcorpgps.v3.dispatch.service;

import android.Manifest;
import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;

import com.netcorpgps.v3.dispatch.utils.CommonUtils;
import com.netcorpgps.v3.dispatch.utils.LogUtils;


/**
 * Created by David Fa on 19/05/2017.
 */

public class LocationService extends Service implements LocationListener {
    private final String LOG_TAG = LocationService.class.getSimpleName();

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    public void onCreate() {
        super.onCreate();
//        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//
//        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1, this);
    }

    @Override
    public void onLocationChanged(Location location) {
        int latitude = (int) (location.getLatitude());
        int longitude = (int) (location.getLongitude());
        CommonUtils.toast(getApplicationContext(), latitude+","+longitude);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        LogUtils.debugLog(LOG_TAG, "onStatusChanged");
    }

    @Override
    public void onProviderEnabled(String provider) {
        LogUtils.debugLog(LOG_TAG, "onProviderEnabled");
    }

    @Override
    public void onProviderDisabled(String provider) {
        LogUtils.debugLog(LOG_TAG, "onProviderDisabled");
    }
}
