package com.netcorpgps.v3.dispatch.service;


import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;

import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.application.NetcorpGPSApplication;
import com.netcorpgps.v3.dispatch.db.service.ServiceManager;
import com.netcorpgps.v3.dispatch.receiver.NetworkStateReceiver;
import com.netcorpgps.v3.dispatch.utils.LogUtils;

/**
 * Created by David Fa on 22/06/2017.
 */

public class SyncService extends Service {

    private final String LOG_TAG = SyncService.class.getSimpleName();
    public final int THREAD_SLEEP_TIME = 1000 * 60;// * 30; // 30 mins
    private final long THREAD_SLEEP_TIME_FOR_NETCORK = 1000 * 60;// * 30; // 30 mins
    private boolean isNetworkReceiverRegistered;
    private NetworkStateReceiver networkStateReceiver;
    private boolean isConnected = false;
    private ServiceManager serviceManager;
    private SyncHandler syncHandler;
    private boolean loop = false;

    @Override
    public void onCreate() {
        HandlerThread handlerThread = new HandlerThread(LOG_TAG);
        handlerThread.start();
        syncHandler = new SyncHandler(handlerThread.getLooper());
        serviceManager = new ServiceManager(this);
        loop = true;
        registerNetworkReceiver();
    }

    private class SyncHandler extends Handler {

        public SyncHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {

                while (loop) {
                    try {

                        while (!isConnected) {
                            // suspend thread when network is unavailable
                            Thread.sleep(THREAD_SLEEP_TIME_FOR_NETCORK);
                        }
                        LogUtils.debugLog(LOG_TAG, getString(R.string.log_start_sync_to_server));
                        serviceManager.syncToServer();
//                        Thread.sleep(THREAD_SLEEP_TIME);
                        Thread.sleep(NetcorpGPSApplication.sharedPrefManager.getInt(R.string.shared_pref_sync_interval_time, THREAD_SLEEP_TIME));

                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }

            }

            stopSelf(msg.arg1);

        }

        // suspend thread when network is unavailable
        public void suspend() {
            isConnected = false;
        }

        // wake up thread when network is available
        public synchronized void resume() {
            isConnected = true;
//            notify();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Message msg = syncHandler.obtainMessage();
        msg.arg1 = startId;
        syncHandler.sendMessage(msg);
        return START_REDELIVER_INTENT;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        loop = false;
        unregisterNetworkReceiver();
    }

    private void registerNetworkReceiver() {
        if (!isNetworkReceiverRegistered) {
            networkStateReceiver = new NetworkStateReceiver(new NetworkStateReceiver.NetworkStateResponse() {
                @Override
                public void responseToConnectionOn() {
                    syncHandler.resume();
                }

                @Override
                public void responseToConnectionOff() {
                    syncHandler.suspend();
                }
            });
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
            intentFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
            intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
            registerReceiver(networkStateReceiver, intentFilter);
            isNetworkReceiverRegistered = true;
        }
    }

    private void unregisterNetworkReceiver() {
        if (networkStateReceiver != null) {
            unregisterReceiver(networkStateReceiver);
            isNetworkReceiverRegistered = false;
        }
    }

}
