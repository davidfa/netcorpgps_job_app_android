package com.netcorpgps.v3.dispatch.service;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.activity.LoginActivity;
import com.netcorpgps.v3.dispatch.api.bean.ResponseBean;
import com.netcorpgps.v3.dispatch.api.volley.GsonRequest;
import com.netcorpgps.v3.dispatch.application.NetcorpGPSApplication;
import com.netcorpgps.v3.dispatch.db.service.DriverRecordService;
import com.netcorpgps.v3.dispatch.db.service.DriverStatusRecordService;
import com.netcorpgps.v3.dispatch.receiver.SystemControllerReceiver;
import com.netcorpgps.v3.dispatch.utils.CommonUtils;
import com.netcorpgps.v3.dispatch.utils.LocationUtils;
import com.netcorpgps.v3.dispatch.utils.LogUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by David Fa on 31/07/2017.
 */

public class SystemService extends Service {

    private final String LOG_TAG = SystemService.class.getSimpleName();
    private SystemControllerReceiver systemControllerReceiver;

    @Override
    public void onCreate() {
        registerSystemReceiver();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        unregisterSystemReceiver();
    }

    private void registerSystemReceiver() {
        try {
            systemControllerReceiver = new SystemControllerReceiver(this, new SystemControllerReceiver.SystemControllerResponse() {
                @Override
                public void exitApp() {
                    logout();
                }
            });
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(getString(R.string.notification_type_command));
            registerReceiver(systemControllerReceiver, intentFilter);
        } catch (Exception e) {
            LogUtils.errorLog(LOG_TAG, e.getMessage(), e);
        }

    }

    private void unregisterSystemReceiver() {
        try {
            unregisterReceiver(systemControllerReceiver);
        } catch (Exception e) {
            LogUtils.errorLog(LOG_TAG, e.getMessage(), e);
        }
    }

    private void logout() {
        Map<String, String> map = new HashMap<>();
        String companyDB = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_db);
        String vehicleID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id);
        String deviceID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_device_id);
        String driverID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_user_id);
        String driverRecordID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_driver_record_id);
        String ip = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_ip);
        String apiLoginAsDriver = getString(R.string.api_logout_as_driver);
        String url = ip + apiLoginAsDriver;
        map.put("driverID", driverID);
        map.put("vehicleID", vehicleID);
        map.put("company", companyDB);
        map.put("deviceID", deviceID);
        map.put("driverRecordID", driverRecordID);
        map.put("type", getString(R.string.record_type_logout));
        map.put("device_datetime", CommonUtils.getCurrentTime());
        map.put("device_timezone_name", CommonUtils.getTimeZoneName());
        map.put("device_timezone_id", CommonUtils.getTimeZoneID());
        String coordinate = "";
        String address = "";
        // check whether latest location is available
        if (LocationUtils.isNewLocationAvailable()) {
            coordinate = LocationUtils.getCoordinate();
            address = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_gms_location_address);
        }
        map.put("device_coordinate", coordinate);
        map.put("device_address", address);
        map.put("sys_datetime", CommonUtils.getSysTime());
        map.put("driverStatus", getString(R.string.driver_status_offline));
        // query accpedted jobs
        GsonRequest<ResponseBean> jsObjRequest = new GsonRequest<>(Request.Method.POST, url, ResponseBean.class, map, null, new LogoutListener(map), new ErrorListener(map));
        jsObjRequest.setShouldCache(false);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(Integer.parseInt(getString(R.string.sys_timeout)), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsObjRequest.setTag(getString(R.string.request_tag_logout_driver));
        NetcorpGPSApplication.getInstance().getRequestQueue().add(jsObjRequest);
    }

    class LogoutListener implements Response.Listener<ResponseBean> {

        private Map<String, String> mMap;

        public LogoutListener(Map<String, String> map) {
            mMap = map;
        }

        @Override
        public void onResponse(ResponseBean response) {
            try {
                if (response != null) {
                    if (response.getStatus() == 0) {
                        saveSyncDriverRecord(mMap);
                    }
                }
            } catch (Exception e) {
                saveSyncDriverRecord(mMap);
                LogUtils.errorLog(LOG_TAG, e.getMessage(), e);
            } finally {
                finishActivity();
            }
        }
    }

    class ErrorListener implements Response.ErrorListener {

        private Map<String, String> mMap;

        public ErrorListener(Map<String, String> map) {
            mMap = map;
        }

        @Override
        public void onErrorResponse(VolleyError error) {
//            String message = VolleyErrorHandler.handleError(MainActivity.this, error);
            saveSyncDriverRecord(mMap);
            finishActivity();
            LogUtils.errorLog(LOG_TAG, error.getMessage(), error);
        }
    }

    private void saveSyncDriverRecord(Map<String, String> record) {
        DriverRecordService service = new DriverRecordService(this);
        service.insert(record);
        DriverStatusRecordService dsrsService = new DriverStatusRecordService(this);
        dsrsService.insert(record);
    }

    private void finishActivity() {
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        LogUtils.debugLog(LOG_TAG, "User logout");
        NetcorpGPSApplication.sharedPrefManager.setBoolean(R.string.shared_pref_login_status, false);
        startActivity(intent);
    }
}
