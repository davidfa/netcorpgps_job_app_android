package com.netcorpgps.v3.dispatch.sync;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.api.bean.ResponseBean;
import com.netcorpgps.v3.dispatch.api.volley.GsonRequest;
import com.netcorpgps.v3.dispatch.application.NetcorpGPSApplication;
import com.netcorpgps.v3.dispatch.db.bean.DriverRecord;
import com.netcorpgps.v3.dispatch.db.bean.DriverStatusRecord;
import com.netcorpgps.v3.dispatch.db.service.DriverRecordService;
import com.netcorpgps.v3.dispatch.db.service.DriverStatusRecordService;
import com.netcorpgps.v3.dispatch.utils.LogUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by David Fa on 28/06/2017.
 */

public class SyncDriverStatusRecord extends SyncManager {

    private final String LOG_TAG = SyncDriverStatusRecord.class.getSimpleName();
    DriverStatusRecordService service = null;

    public SyncDriverStatusRecord(Context context) {
        super(context);
        service = new DriverStatusRecordService(mContext);
    }

    public void syncToServer() {
        List<DriverStatusRecord> list = service.getDriverRecords();
        int size = list.size();
        if (size > 0) {
            try {
                JSONArray array = new JSONArray();
                for (DriverStatusRecord driverStatusRecord : list) {
                    JSONObject object = new JSONObject();
                    object.put("id", driverStatusRecord.getId());
                    object.put("driverRecordID", driverStatusRecord.getDriverRecordID());
                    object.put("driverID", driverStatusRecord.getDriverID());
                    object.put("vehicleID", driverStatusRecord.getVehicleID());
                    object.put("driverStatus", driverStatusRecord.getDriverStatus());
                    object.put("deviceDatetime", driverStatusRecord.getDeviceDatetime());
                    object.put("deviceTimezoneName", driverStatusRecord.getDeviceTimezoneName());
                    object.put("deviceTimezoneID", driverStatusRecord.getDeviceTimezoneID());
                    object.put("deviceCoordinate", driverStatusRecord.getDeviceCoordinate());
                    object.put("deviceAddress", driverStatusRecord.getDeviceAddress());
                    object.put("sysDatetime", driverStatusRecord.getSysDatetime());
                    object.put("company", driverStatusRecord.getCompanyBD());
                    array.put(object);
                }

                String ip = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_ip);
                String vehicleID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id);
                String companyDB = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_db);
                String accessToken = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_driver_access_token);
                String driverRecordID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_driver_record_id);
                String driverID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_user_id);
                String deviceID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_device_id);
                String apiSyncDriverStatusRecord = mContext.getString(R.string.api_sync_driver_status_record);
                String url = ip + apiSyncDriverStatusRecord;
                Map map = new HashMap();
                map.put("company", companyDB);
                map.put("vehicleID", vehicleID);
                map.put("driverRecordID", driverRecordID);
                map.put("accessToken", accessToken);
                map.put("driverID", driverID);
                map.put("deviceID", deviceID);
                map.put("json", array.toString());
                LogUtils.infoLog(LOG_TAG, url + ": " + map.toString());
                GsonRequest<ResponseBean> jsObjRequest = new GsonRequest<>(Request.Method.POST, url, ResponseBean.class, map, null, new ResponseListener(), new ErrorListener());
                jsObjRequest.setShouldCache(false);
                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(Integer.parseInt(mContext.getString(R.string.sys_timeout)), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                jsObjRequest.setTag(mContext.getString(R.string.request_tag_sync_driver_status_record));
                NetcorpGPSApplication.getInstance().getRequestQueue().add(jsObjRequest);
            } catch (Exception e) {
                LogUtils.errorLog(LOG_TAG, e.getMessage(), e);
            }

        } else {
//            LogUtils.debugLog(LOG_TAG, mContext.getString(R.string.log_no_data_sync));
        }
    }

    class ResponseListener implements Response.Listener<ResponseBean> {

        @Override
        public void onResponse(ResponseBean response) {
            try {
                if (response != null) {

                    if (response.getStatus() == 1) {
                        service.deleteByIDs(response.getIds());
                    }
                    LogUtils.debugLog(LOG_TAG, response.toString());
                }
            } catch (Exception e) {
                LogUtils.errorLog(LOG_TAG, e.getMessage(), e);
            }
        }
    }

    class ErrorListener implements Response.ErrorListener {
        @Override
        public void onErrorResponse(VolleyError error) {
            LogUtils.errorLog(LOG_TAG, error.getMessage(), error);
        }
    }
}
