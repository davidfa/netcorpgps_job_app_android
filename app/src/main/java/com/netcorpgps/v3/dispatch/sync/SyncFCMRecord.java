package com.netcorpgps.v3.dispatch.sync;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.api.bean.ResponseBean;
import com.netcorpgps.v3.dispatch.api.volley.GsonRequest;
import com.netcorpgps.v3.dispatch.application.NetcorpGPSApplication;
import com.netcorpgps.v3.dispatch.db.bean.FCMRecord;
import com.netcorpgps.v3.dispatch.db.service.FCMRecordService;
import com.netcorpgps.v3.dispatch.utils.LogUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by David Fa on 28/06/2017.
 */

public class SyncFCMRecord extends SyncManager {

    private final String LOG_TAG = SyncFCMRecord.class.getSimpleName();
    FCMRecordService service = null;

    public SyncFCMRecord(Context context) {
        super(context);
        service = new FCMRecordService(mContext);
    }

    public void syncToServer() {
        List<FCMRecord> list = service.getFCMRecord();
        int size = list.size();
        if (size > 0) {
            try {
                String uniqueID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_imei, null);
                String ip = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_ip);
                String vehicleID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id);
                String companyDB = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_db);
                String accessToken = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_driver_access_token);
                String driverRecordID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_driver_record_id);
                String driverID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_user_id);
                String deviceID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_device_id);
                String api = mContext.getString(R.string.api_sync_fcm_record);
                String url = ip + api;
                Map map = new HashMap();
                map.put("company", companyDB);
                map.put("vehicleID", vehicleID);
                map.put("driverRecordID", driverRecordID);
                map.put("accessToken", accessToken);
                map.put("driverID", driverID);
                map.put("deviceID", deviceID);

                JSONArray array = new JSONArray();
                for (FCMRecord record : list) {
                    JSONObject object = new JSONObject();
                    object.put("imei", uniqueID);
                    object.put("fcmToken", record.getFcmToken());
                    object.put("company", companyDB);
                    array.put(object);
                }
                map.put("json", array.toString());
                LogUtils.infoLog(LOG_TAG, url + ": " + map.toString());
                GsonRequest<ResponseBean> jsObjRequest = new GsonRequest<>(Request.Method.POST, url, ResponseBean.class, map, null, new ResponseListener(), new ErrorListener());
                jsObjRequest.setShouldCache(false);
                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(Integer.parseInt(mContext.getString(R.string.sys_timeout)), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                jsObjRequest.setTag(mContext.getString(R.string.request_tag_sync_fcm_record));
                NetcorpGPSApplication.getInstance().getRequestQueue().add(jsObjRequest);
            } catch (Exception e) {
                LogUtils.errorLog(LOG_TAG, e.getMessage());
            }
        } else {
//            LogUtils.debugLog(LOG_TAG, mContext.getString(R.string.log_no_data_sync));
        }
    }

    class ResponseListener implements Response.Listener<ResponseBean> {

        @Override
        public void onResponse(ResponseBean response) {
            try {
                if (response != null) {

                    if (response.getStatus() == 1) {

                        service.delete();
                    }
                    LogUtils.debugLog(LOG_TAG, response.toString());
                }
            } catch (Exception e) {
                LogUtils.errorLog(LOG_TAG, e.getMessage(), e);
            }
        }
    }

    class ErrorListener implements Response.ErrorListener {
        @Override
        public void onErrorResponse(VolleyError error) {
            LogUtils.errorLog(LOG_TAG, error.getMessage(), error);
        }
    }
}
