package com.netcorpgps.v3.dispatch.utils;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

/**
 * Created by David Fa on 15/09/2017.
 */

public class BitmapUtils {

    private final static String LOG_TAG = BitmapUtils.class.getSimpleName();
    private final static int IMAGE_MAX_SIZE = 1000000;

//    public static Bitmap resizeBitmap1280(String filePath) {
//        // Decode image size
//        BitmapFactory.Options o = new BitmapFactory.Options();
//        o.inJustDecodeBounds = true;
//        BitmapFactory.decodeFile(filePath, o);
//
//        // The new size we want to scale to
//        final int REQUIRED_SIZE_width = 1280;
//        final int REQUIRED_SIZE_height = 800;
//
//        // Find the correct scale value. It should be the power of 2.
//        int width_tmp = o.outWidth, height_tmp = o.outHeight;
//        int scale = 1;
//        while (true) {
//            if (width_tmp < REQUIRED_SIZE_width && height_tmp < REQUIRED_SIZE_height)
//                break;
//            width_tmp /= 2;
//            height_tmp /= 2;
//            scale *= 2;
//        }
//
//        // Decode with inSampleSize
//        BitmapFactory.Options o2 = new BitmapFactory.Options();
//        o2.inSampleSize = scale;
//        return BitmapFactory.decodeFile(filePath, o2);
//    }

//    public static Bitmap resizeBitmap1M(Context context, Uri uri) {
//        InputStream in = null;
//        try {
//            in = context.getContentResolver().openInputStream(uri);
//
//            // Decode image size
//            BitmapFactory.Options options = new BitmapFactory.Options();
//            options.inJustDecodeBounds = true;
//            BitmapFactory.decodeStream(in, null, options);
//            in.close();
//
//            int scale = 1;
//            while ((options.outWidth * options.outHeight) * (1 / Math.pow(scale, 2)) > IMAGE_MAX_SIZE) {
//                scale++;
//            }
//
//            Bitmap resultBitmap = null;
//            in = context.getContentResolver().openInputStream(uri);
//            if (scale > 1) {
//                scale--;
//                // scale to max possible inSampleSize that still yields an image
//                // larger than target
//                options = new BitmapFactory.Options();
//                options.inSampleSize = scale;
//                resultBitmap = BitmapFactory.decodeStream(in, null, options);
//
//                // resize to desired dimensions
//                int height = resultBitmap.getHeight();
//                int width = resultBitmap.getWidth();
//
//                double y = Math.sqrt(IMAGE_MAX_SIZE / (((double) width) / height));
//                double x = (y / height) * width;
//
//                Bitmap scaledBitmap = Bitmap.createScaledBitmap(resultBitmap, (int) x, (int) y, true);
//                resultBitmap.recycle();
//                resultBitmap = scaledBitmap;
//
//                System.gc();
//            } else {
//                resultBitmap = BitmapFactory.decodeStream(in);
//            }
//            in.close();
//
//            return resultBitmap;
//        } catch (Exception e) {
//            LogUtils.errorLog(LOG_TAG, e.getMessage(), e);
//            return null;
//        }
//    }

    public static Bitmap resizeBitmap1024(Context context, Uri uri) {
        InputStream in = null;
        try {
            in = context.getContentResolver().openInputStream(uri);
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();

            // The new size we want to scale to
            final int REQUIRED_SIZE_WIDTH = 1024;
            final int REQUIRED_SIZE_HEIGHT = 800;

            // Find the correct scale value. It should be the power of 2.
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true) {
                if (width_tmp < REQUIRED_SIZE_WIDTH && height_tmp < REQUIRED_SIZE_HEIGHT)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            in = context.getContentResolver().openInputStream(uri);
            Bitmap resultBitmap = BitmapFactory.decodeStream(in, null, o2);
            in.close();
            System.gc();
            return resultBitmap;

        } catch (Exception e) {
            LogUtils.errorLog(LOG_TAG, e.getMessage(), e);
            return null;
        }
    }


    public static Bitmap resizeBitmap1024(String path) {

        try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, o);

            // The new size we want to scale to
            final int REQUIRED_SIZE_WIDTH = 1024;
            final int REQUIRED_SIZE_HEIGHT = 800;

            // Find the correct scale value. It should be the power of 2.
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true) {
                if (width_tmp < REQUIRED_SIZE_WIDTH && height_tmp < REQUIRED_SIZE_HEIGHT)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            Bitmap resultBitmap = BitmapFactory.decodeFile(path, o2);
            System.gc();
            return resultBitmap;

        } catch (Exception e) {
            LogUtils.errorLog(LOG_TAG, e.getMessage(), e);
            return null;
        }
    }

//    public static Bitmap resizeBitmap1M(String path) {
//
//        try {
//            // Decode image size
//            BitmapFactory.Options options = new BitmapFactory.Options();
//            options.inJustDecodeBounds = true;
//            BitmapFactory.decodeFile(path, options);
////            in.close();
//
//            int scale = 1;
//            while ((options.outWidth * options.outHeight) * (1 / Math.pow(scale, 2)) > IMAGE_MAX_SIZE) {
//                scale++;
//            }
//
//            Bitmap resultBitmap = null;
//
//            if (scale > 1) {
//                scale--;
//                // scale to max possible inSampleSize that still yields an image
//                // larger than target
//                options = new BitmapFactory.Options();
//                options.inSampleSize = scale;
//                resultBitmap = BitmapFactory.decodeFile(path, options);
//
//                // resize to desired dimensions
//                int height = resultBitmap.getHeight();
//                int width = resultBitmap.getWidth();
//
//                double y = Math.sqrt(IMAGE_MAX_SIZE / (((double) width) / height));
//                double x = (y / height) * width;
//
//                Bitmap scaledBitmap = Bitmap.createScaledBitmap(resultBitmap, (int) x, (int) y, true);
//                resultBitmap.recycle();
//                resultBitmap = scaledBitmap;
//
//                System.gc();
//            } else {
//                resultBitmap = BitmapFactory.decodeFile(path);
//            }
//
//            return resultBitmap;
//        } catch (Exception e) {
//            LogUtils.errorLog(LOG_TAG, e.getMessage(), e);
//            return null;
//        }
//    }

//    public static String regenerateBitMap(String path, String uniqueID) {
//        Bitmap out = BitmapUtils.resizeBitmap1280(path);
//        if (out == null) {
//            return null;
//        }
//
//        String filePath = LogFileUtils.getFilesDir() + File.separator + uniqueID + ".png";
//        File file = new File(filePath);
//        FileOutputStream fOut;
//        try {
//            fOut = new FileOutputStream(file);
//            out.compress(Bitmap.CompressFormat.PNG, 100, fOut);
//            fOut.flush();
//            fOut.close();
//            out.recycle();
//        } catch (Exception e) {
//            LogUtils.debugLog(LOG_TAG, e.getMessage(), e);
//            return null;
//        }
//        return filePath;
//    }

    public static String regenerateBitMap(Context context, Uri uri, String uniqueID) {
        Bitmap out = resizeBitmap1024(context, uri);
        if (out == null) {
            return null;
        }
        return outputPicture(out, uniqueID);
    }

    public static String regenerateBitMap(Context context, String path, String uniqueID) {
        Bitmap out = resizeBitmap1024(path);
        if (out == null) {
            return null;
        }

        return outputPicture(out, uniqueID);
    }

    public static String outputPicture(Bitmap out, String uniqueID) {
        String filePath = LogFileUtils.getFilesDir() + File.separator + uniqueID + ".png";
        File file = new File(filePath);
        FileOutputStream fOut;
        try {
            fOut = new FileOutputStream(file);
            out.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
            out.recycle();
        } catch (Exception e) {
            LogUtils.debugLog(LOG_TAG, e.getMessage(), e);
            return null;
        }
        return filePath;
    }


    public String getPath(Context context, Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
}
