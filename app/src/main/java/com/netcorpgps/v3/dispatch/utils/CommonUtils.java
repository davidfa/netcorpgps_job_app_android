package com.netcorpgps.v3.dispatch.utils;

import android.content.Context;
import android.graphics.Color;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.widget.Toast;

import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.application.NetcorpGPSApplication;
import com.netcorpgps.v3.dispatch.component.RoundedBackgroundSpan;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Created by David Fa on 28/04/2017.
 */

public class CommonUtils {

    public static final String LOGIN_MODE_DRIVER = "0"; // driver mode
    public static final String LOGIN_MODE_ADMIN = "1"; // admin mode
//    public static final int REQUEST_READ_PHONE_STATE = 1;

    /**
     * get Unique id of device
     *
     * @return
     */
    public static String getUniqueID(Context context) {
        String myAndroidDeviceId = null;
//        int permissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE);

//        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_READ_PHONE_STATE);
//        } else {
        TelephonyManager mTelephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (mTelephony.getDeviceId() != null) {
            myAndroidDeviceId = mTelephony.getDeviceId();
        } else {
            myAndroidDeviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        }
//        }

        return myAndroidDeviceId;
    }

    public static String generateAPKName() {
        return System.currentTimeMillis() + ".apk";
    }

    public static String getSysTimezoneName() {
        return NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_timezone_name);
    }

    public static boolean isEmpty(String str) {
        if (str == null || "".equals(str.trim()))
            return true;
        else
            return false;
    }

    public static boolean isConfigured() {
        String companyDB = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_db);
        String companyID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_company_id);
        String vehicleID = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_vehicle_id);
        if (isEmpty(companyDB) || isEmpty(companyID) || isEmpty(vehicleID)) {
            return false;
        } else {
            return true;
        }
    }

    public static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    public static void toast(Context context, String msg) {
        if (!isEmpty(msg))
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static int getResId(String variableName) {
        return getResId(variableName, null);
    }

    public static int getResId(String variableName, Class<?> c) {

        try {
            if (c == null) {
                c = R.drawable.class;
            }
            Field idField = c.getDeclaredField(variableName);
            return idField.getInt(idField);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    // gain GMT time based on where company is
    public static String getSysTime() {
        SimpleDateFormat inputFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss 'GMT' yyyy");
        inputFormat.setTimeZone(TimeZone.getTimeZone(getTimeZoneName()));

        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        outputFormat.setTimeZone(TimeZone.getTimeZone(getSysTimezoneName()));
        Date date = null;
        try {
            date = inputFormat.parse(inputFormat.format(new Date()));
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
        String outputText = outputFormat.format(date);
        return outputText;
    }

    public static String getCurrentTime() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
    }

    public static String getDate(int offet) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, offet);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(cal.getTime());
    }

    public static String getTimeZoneName() {
        return TimeZone.getDefault().getDisplayName(false, TimeZone.SHORT);
    }

    public static String getTimeZoneID() {
        return TimeZone.getDefault().getID();
    }

    public static int getJobStatusColor(Context context, String status) {
        if (status.equalsIgnoreCase(context.getString(R.string.job_status_paged))) {
            return ContextCompat.getColor(context, R.color.job_status_paged);
        } else if(status.equalsIgnoreCase(context.getString(R.string.job_status_assigned)) || status.equalsIgnoreCase(context.getString(R.string.job_status_waiting)) || status.equalsIgnoreCase(context.getString(R.string.job_status_loading))) {
            return ContextCompat.getColor(context, R.color.job_status_assigned);
        } else if(status.equalsIgnoreCase(context.getString(R.string.job_status_completed))) {
            return ContextCompat.getColor(context, R.color.job_status_completed);
        } else if(status.equalsIgnoreCase(context.getString(R.string.job_status_in_progress))) {
            return ContextCompat.getColor(context, R.color.job_status_start);
        }
        return ContextCompat.getColor(context, R.color.black);
    }

    public static String getPriority(Context context, String status) {
        if (status.equalsIgnoreCase(context.getString(R.string.job_status_paged))) {
            return context.getString(R.string.priority_paged);
        } else if(status.equalsIgnoreCase(context.getString(R.string.job_status_assigned))) {
            return context.getString(R.string.priority_assigned);
        } else if(status.equalsIgnoreCase(context.getString(R.string.job_status_completed))) {
            return context.getString(R.string.priority_completed);
        } else if(status.equalsIgnoreCase(context.getString(R.string.job_status_in_progress)) || status.equalsIgnoreCase(context.getString(R.string.job_status_waiting)) || status.equalsIgnoreCase(context.getString(R.string.job_status_loading))) {
            return context.getString(R.string.priority_started);
        }
        return context.getString(R.string.priority_assigned);
    }

    public static int getScheduledColor(Context context, String datetime) {
        if(!compareDate(datetime)) {
            return ContextCompat.getColor(context, R.color.job_scheduled);
        }
        return ContextCompat.getColor(context, R.color.txt_gray);
    }

    /**
     * @param inputDate1
     * @param inputDate2
     * @return if inputDate1 > inputDate2 then true, else then false
     */
    public static boolean compareDate(String inputDate1, String inputDate2) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        int result = 0;
        try {
            Date date1 = sdf.parse(inputDate1);
            Date date2 = sdf.parse(inputDate2);
            result = date1.compareTo(date2);
        } catch (ParseException e) {
            result = 0;
        }

        return (result < 0) ? false : true;
    }

    /**
     * if inputDate > today then true, else then false
     * @param inputDate
     * @return
     */
    public static boolean compareDate(String inputDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String date = sdf.format(new Date());
        return compareDate(inputDate, date);
    }

    /**
     * @param inputDatetime1
     * @param inputDatetime2
     * @return if inputDate1 > inputDate2 then true, else then false
     */
    public static boolean compareDatetime(String inputDatetime1, String inputDatetime2) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        int result = 0;
        try {
            Date date1 = sdf.parse(inputDatetime1);
            Date date2 = sdf.parse(inputDatetime2);
            result = date1.compareTo(date2);
        } catch (ParseException e) {
            result = 0;
        }

        return (result < 0) ? false : true;
    }

    public static String getFormatDatetime(String datetime) {
        String format = "hh:mm a EEE, d MMM yy";
        return getFormatDatetime(datetime, format);
    }

    public static String getFormatArrivedTime(String datetime) {
        String format = "hh:mm a";
        return getFormatDatetime(datetime, format);
    }

    public static String getFormatDatetime(String datetime, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        try {
            Date date = dateFormat.parse(datetime);
            SimpleDateFormat sd = new SimpleDateFormat(format);
            return sd.format(date);
        } catch (ParseException e) {
            return datetime;
        }
    }

    public static String getDateDiffInMins(String datetime1, String datetime2) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        if (isEmpty(datetime1) || isEmpty(datetime2)) {
            return "";
        }
        try {
            Date date1 = dateFormat.parse(datetime1);
            Date date2 = dateFormat.parse(datetime2);
            long diffInMillies = date2.getTime() - date1.getTime();
            String result = TimeUnit.MINUTES.convert(diffInMillies, TimeUnit.MILLISECONDS) + " mins";
            return result;
        } catch (ParseException e) {
            return "";
        }
    }

    public static long getDateDiffInMinsReturnLong(String datetime1, String datetime2) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        if (isEmpty(datetime1) || isEmpty(datetime2)) {
            return 0;
        }
        try {
            Date date1 = dateFormat.parse(datetime1);
            Date date2 = dateFormat.parse(datetime2);
            long diffInMillies = date2.getTime() - date1.getTime();
            return TimeUnit.MINUTES.convert(diffInMillies, TimeUnit.MILLISECONDS);
        } catch (ParseException e) {
            return 0;
        }
    }

    public static String convertMilliToDate(Long milliseconds) {
        Calendar calendar = Calendar.getInstance();

        calendar.setTimeInMillis(Long.valueOf(milliseconds));
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return formatter.format(calendar.getTime());
    }

    public static CharSequence getMenuTitleWithNotification(Context context, String title, int count) {
        String[] titles = title.split("    ");
        String before = titles[0];
        String counter = "";
        if (count == 0) {
            return before;
        } else if (count > 99) {
            counter = "99+";
        } else {
            counter = String.valueOf(count);
        }

        String s = before + "     " + counter + " ";
        SpannableString sColored = new SpannableString(s);
        sColored.setSpan(new RoundedBackgroundSpan(context), s.length() - (counter.length() + 2), s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        sColored.setSpan(new ForegroundColorSpan(Color.WHITE), s.length() - (counter.length()), s.length(), 0);
        return sColored;
    }
}
