package com.netcorpgps.v3.dispatch.utils;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;

import com.netcorpgps.v3.dispatch.R;
import com.netcorpgps.v3.dispatch.application.NetcorpGPSApplication;

import java.io.IOException;
import java.util.List;


/**
 * Created by David Fa on 19/05/2017.
 */

public class LocationUtils {

    public static final long UPDATE_INTERVAL = 30 * 1000;  /* 30 secs */
    public static final long FASTEST_INTERVAL = 10 * 1000; /* 10 sec */
    public static final int REQUEST_ACCESS_LOCATION = 22;
    public static final int TIMESTAMP_GAP = 1000 * 60 * 3; /* 3 minutes */

    public static boolean isGPSEnabled(Context context) {
        LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public static boolean isNewLocationAvailable(long timestamp) {
        if (Math.abs(System.currentTimeMillis() - timestamp) < TIMESTAMP_GAP) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isNewLocationAvailable() {
        long timestamp = NetcorpGPSApplication.sharedPrefManager.getLong(R.string.shared_pref_gms_location_timestamp);
        return isNewLocationAvailable(timestamp);
    }

    public static String getCoordinate() {
        String latitude = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_gms_latitude);
        String longitude = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_gms_longitude);
        return latitude + "," + longitude;
    }

    public static Address getAddressObject(Context context) {
        Geocoder geocoder = new Geocoder(context);
        String latitude = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_gms_latitude);
        String longitude = NetcorpGPSApplication.sharedPrefManager.getString(R.string.shared_pref_gms_longitude);
        Address address = null;
        try {
            List<Address> list = geocoder.getFromLocation(Double.valueOf(latitude), Double.valueOf(longitude), 1);
            if (list != null && list.size() > 0) {
                address = list.get(0);
            }
        } catch (Exception e) {
//            LogUtils.warnLog(context.getClass().getSimpleName(), "Geocoding failture: " + latitude + "," + longitude + " --- " + e.getMessage());
        }

        return address;
    }

    /**
     * @param context
     * @param coordinate e.g. -33.9255304,150.9714646
     * @return
     */
    public static Address getAddressObject(Context context, String coordinate) {
        if (CommonUtils.isEmpty(coordinate)) {
            return null;
        }
        Geocoder geocoder = new Geocoder(context);
        String[] coordinates = coordinate.split(",");
        double latitude = Double.valueOf(coordinates[0]);
        double longitude = Double.valueOf(coordinates[1]);
        List<Address> list = null;
        try {
            list = geocoder.getFromLocation(latitude, longitude, 1);
        } catch (IOException e) {
//            LogUtils.warnLog(context.getClass().getSimpleName(), "Geocoding failture: " + latitude + "," + longitude + " --- " + e.getMessage());
        }

        Address address = null;
        if (list != null && list.size() > 0) {
            address = list.get(0);
        }
        return address;
    }

    public static String getFullAddress(Context context) {

        StringBuffer addressline = new StringBuffer();
        Address address = getAddressObject(context);
        if (address != null) {
            if (!CommonUtils.isEmpty(address.getSubThoroughfare())) {
                addressline.append(address.getSubThoroughfare() + " ");
            }
            // road
            if (!CommonUtils.isEmpty(address.getThoroughfare())) {
                addressline.append(address.getThoroughfare() + ", ");
            }
            // suburb
            if (!CommonUtils.isEmpty(address.getLocality())) {
                addressline.append(address.getLocality() + ", ");
            }
            // state
            if (!CommonUtils.isEmpty(address.getAdminArea())) {
                addressline.append(address.getAdminArea() + " ");
            }
            if (!CommonUtils.isEmpty(address.getPostalCode())) {
                addressline.append(address.getPostalCode());
            }
        } else {
            addressline.append("");
        }
        return addressline.toString();
    }

    /**
     * @param context
     * @param coordinate e.g. -33.9255304,150.9714646
     * @return
     */
    public static String getFullAddress(Context context, String coordinate) {

        StringBuffer addressline = new StringBuffer();
        Address address = getAddressObject(context, coordinate);
        if (address != null) {
            if (!CommonUtils.isEmpty(address.getSubThoroughfare())) {
                addressline.append(address.getSubThoroughfare() + " ");
            }
            // road
            if (!CommonUtils.isEmpty(address.getThoroughfare())) {
                addressline.append(address.getThoroughfare() + ", ");
            }
            // suburb
            if (!CommonUtils.isEmpty(address.getLocality())) {
                addressline.append(address.getLocality() + ", ");
            }
            // state
            if (!CommonUtils.isEmpty(address.getAdminArea())) {
                addressline.append(address.getAdminArea() + " ");
            }
            if (!CommonUtils.isEmpty(address.getPostalCode())) {
                addressline.append(address.getPostalCode());
            }
        }
        return addressline.toString();
    }
}
