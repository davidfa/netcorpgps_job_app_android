package com.netcorpgps.v3.dispatch.utils;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

/**
 * Created by David Fa on 27/07/2017.
 */

public class LogFileUtils {

    private static final String LOG_TAG = LogFileUtils.class.getSimpleName();
    private static final String Dir = "N3JD"; // Netcorpgps v3 Job Dispatch
    private static final String fileDir = "files";
    public static final String fileName = "logcat.txt";
    private static final int Log_Retention = 7;
    private static boolean isDirCreated = false;
    private static boolean isLogFileCreated = false;
    //    private static Process process;
    private volatile static LogFileUtils instance;
    private static FileWriter fileOutputStream = null;


    private LogFileUtils() {
        try {
            File root = new File(getLogStorageRootDir());
            if (root.exists()) {
                File[] files = root.listFiles();
                for (File file : files) {
                    if (file.exists()) {
                        String fileName = file.getName();
                        if(fileName.equalsIgnoreCase(fileDir)) {
                            continue;
                        }

                        // delete logs
                        String date = CommonUtils.getDate(-Log_Retention);
                        if (CommonUtils.compareDate(date, fileName)) {
                            deletAllFiles(file);
                            Log.i(LOG_TAG, "delete file: " + fileName);
                        }
                    }

                }
                File audioDir = new File(getFilesDir());
                if (!audioDir.exists()) {
                    audioDir.mkdir();
                }
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, e.getMessage(), e);
        }

    }

    public static LogFileUtils getInstance() {
        if (instance == null) {
            synchronized (LogFileUtils.class) {
                instance = new LogFileUtils();
            }
        }
        if (isExternalStorageWritable() && isExternalStorageReadable()) {
            isDirCreated = createLogStorageDir();
        }

        if (isDirCreated) {
            File file = createLogFile();
            if (isLogFileCreated) {
                openFileOutputStream(file);
            }
        }
        return instance;
    }

    public static void processLogcat() {
//        LogUtils.debugLog(LOG_TAG, "processLogcat --- isDirCreated: " + isDirCreated);
//        if (isExternalStorageWritable() && isExternalStorageReadable()) {
//            isDirCreated = createLogStorageDir();
//        }
//        if (isDirCreated) {
//            File file = createLogFile();
//            if (isLogFileCreated) {
//                try {
//                    Process process = Runtime.getRuntime().exec("logcat -c");
//                    process = Runtime.getRuntime().exec("logcat -f " + file + " N3JD:D *:S");
////                    if (process == null) {
////                        LogUtils.debugLog(LOG_TAG, "process logcat");
////                    }
//                } catch (IOException e) {
//                    LogUtils.errorLog(LOG_TAG, e.getMessage(), e);
//                }
//            }
//        }
    }

    public void writeLog(String tag, String priority, String msg) {
        writeLog(tag, priority, msg, null);
    }

    public void writeLog(String tag, String priority, String msg, Throwable tr) {
        if (!CommonUtils.isEmpty(tag)) {
            if (isLogFileCreated) {
                if (fileOutputStream != null) {
                    try {
                        String systemTime = CommonUtils.getCurrentTime();
                        if (tr != null) {
                            Writer writer = new StringWriter();
                            PrintWriter pw = new PrintWriter(writer);
                            tr.printStackTrace(pw);
                            Throwable cause = tr.getCause();
                            // loop all causes and writer into log
                            while (cause != null) {
                                cause.printStackTrace(pw);
                                cause = cause.getCause();
                            }

//                            StackTraceElement[] elements = tr.getStackTrace();
//                            if (elements != null && elements.length > 0) {
//                                for (StackTraceElement element : elements) {
//
//                                }
//                            }
                            pw.close();
                            String result = writer.toString();
                            if (!CommonUtils.isEmpty(result))
                                fileOutputStream.write(systemTime + " " + priority + "/" + tag + ": " + result + " \n");
                        } else {
                            fileOutputStream.write(systemTime + " " + priority + "/" + tag + ": " + msg + " \n");
                        }
                        fileOutputStream.flush();

//                        fileOutputStream.write(formatLog(tag, priority, msg));
//                        if (!CommonUtils.isEmpty(result))
//                            fileOutputStream.write(result.getBytes());
                    } catch (IOException e) {
                        Log.e(LOG_TAG, e.getMessage(), e);
                    }
                }
            }
        }
    }

    /* Checks if external storage is available for read and write */
    private static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    private static boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) || Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    public static boolean createLogStorageDir() {
        String dirName = CommonUtils.getFormatDatetime(CommonUtils.getCurrentTime(), "yyyy-MM-dd");
        File dir = new File(getLogStorageRootDir(), dirName);
        if (!(isDirCreated = dir.exists())) {
            if (!(isDirCreated = dir.mkdirs())) {
                Log.e(LOG_TAG, "Directory not created");
            } else {
                Log.d(LOG_TAG, "Directory has been created --- " + dir.getAbsolutePath());
            }
        }
        if (isDirCreated) {
            Log.d(LOG_TAG, "Directory exists" + dir.getAbsolutePath());
        }
        return isDirCreated;
    }

    private static File createLogFile() {

        File file = new File(getLogFilePath());
        if (!(isLogFileCreated = file.exists())) {
            try {
                if (!(isLogFileCreated = file.createNewFile())) {
                    Log.e(LOG_TAG, "Log file not created");
                } else {
                    Log.d(LOG_TAG, "Log file has been created --- " + file.getAbsolutePath());
                }
            } catch (IOException e) {
                isLogFileCreated = false;
                Log.e(LOG_TAG, e.getMessage(), e);
            }
        }
        return file;
    }

    public static String getLogStorageDir() {
        return getLogStorageRootDir() + File.separator + CommonUtils.getFormatDatetime(CommonUtils.getCurrentTime(), "yyyy-MM-dd");
    }

    public static String getLogStorageRootDir() {
        return Environment.getExternalStorageDirectory() + File.separator + Dir;
    }

    // files such as voice, pic, video
    public static String getFilesDir() {
        return getLogStorageRootDir() + File.separator + fileDir;
    }

    private static String getLogFilePath() {
        return getLogStorageDir() + File.separator + fileName;
    }

    private static void openFileOutputStream(File file) {
        if (fileOutputStream == null) {
            try {
                fileOutputStream = new FileWriter(file, true);
            } catch (Exception e) {
                closeFileOutputStream();
                Log.e(LOG_TAG, e.getMessage(), e);
            }
        }
    }

    private static void closeFileOutputStream() {
        if (fileOutputStream != null) {
            try {
                fileOutputStream.close();
            } catch (Exception e) {
                Log.e(LOG_TAG, e.getMessage(), e);
            } finally {
                fileOutputStream = null;
            }
        }
    }

//    private byte[] formatLog(String tag, String priority, String msg) {
//        String systemTime = CommonUtils.getCurrentTime();
//        return (systemTime + " " + priority + "/" + tag + ": " + msg + " \n").getBytes();
//    }

    private static void deletAllFiles(File file) {
        try {
            if (file == null) {
                return;
            }

            if (file.exists()) {

                if (file.isFile()) {
                    file.delete();
                } else if (file.isDirectory()) {

                    File[] listFiles = file.listFiles();

                    if (listFiles == null) {
                        return;
                    }
                    for (File file2 : listFiles) {
                        deletAllFiles(file2);
                    }

                    file.delete();
                }
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, e.getMessage(), e);
        }

    }

}
