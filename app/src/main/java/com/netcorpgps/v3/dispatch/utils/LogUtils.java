package com.netcorpgps.v3.dispatch.utils;

import android.util.Log;

/**
 * Created by David Fa on 1/05/2017.
 */

public class LogUtils {
    private static boolean isToShowDebugLog = true;
    private static boolean isToShowErrorLog = true;
    private static String APP_TAG = "N3JD";
    private static String PRIORITY_D = "D";
    private static String PRIORITY_W = "W";
    private static String PRIORITY_I = "I";
    private static String PRIORITY_E = "E";

    public static void debugLog(String tag, String message) {
        if (isToShowDebugLog && !CommonUtils.isEmpty(message)) {
            LogFileUtils.getInstance().writeLog(tag, PRIORITY_D, message);
            Log.d(APP_TAG, tag + ": " + message);
        }
    }

    public static void debugLog(String tag, String message, Throwable tr) {
        if (isToShowDebugLog && tr != null) {
            LogFileUtils.getInstance().writeLog(tag, PRIORITY_D, message, tr);
            Log.d(APP_TAG, tag + ": " + message, tr);
        }
    }

    public static void infoLog(String tag, String message) {
        if (isToShowDebugLog && !CommonUtils.isEmpty(message)) {
            try {
                LogFileUtils.getInstance().writeLog(tag, PRIORITY_I, message);
                Log.i(APP_TAG, tag + ": " + message);
            } catch (Exception e) {
                errorLog(tag, e.getMessage(), e);
            }
        }
    }

    public static void infoLog(String tag, String message, Throwable tr) {
        if (isToShowDebugLog && tr != null) {
            try {
                LogFileUtils.getInstance().writeLog(tag, PRIORITY_I, message, tr);
                Log.i(APP_TAG, tag + ": " + message, tr);
            } catch (Exception e) {
                errorLog(tag, e.getMessage(), e);
            }
        }
    }

    public static void warnLog(String tag, String message) {
        if (isToShowDebugLog && !CommonUtils.isEmpty(message)) {
            try {
                LogFileUtils.getInstance().writeLog(tag, PRIORITY_W, message);
                Log.w(APP_TAG, tag + ": " + message);
            } catch (Exception e) {
                errorLog(tag, e.getMessage(), e);
            }
        }
    }

    public static void warnLog(String tag, String message, Throwable tr) {
        if (isToShowDebugLog && tr != null) {
            try {
                LogFileUtils.getInstance().writeLog(tag, PRIORITY_W, message, tr);
                Log.w(APP_TAG, tag + ": " + message, tr);
            } catch (Exception e) {
                errorLog(tag, e.getMessage(), e);
            }
        }
    }

    public static void errorLog(String tag, String message) {
        if (isToShowErrorLog && !CommonUtils.isEmpty(message)) {
            try {
                LogFileUtils.getInstance().writeLog(tag, PRIORITY_E, message);
                Log.e(APP_TAG, tag + ": " + message);
            } catch (Exception e) {
                Log.e(APP_TAG, tag + ": " + e.getMessage(), e);
            }
        }
    }

    public static void errorLog(String tag, String message, Throwable tr) {
        if (isToShowErrorLog && tr != null) {
            try {
                LogFileUtils.getInstance().writeLog(tag, PRIORITY_E, message, tr);
                Log.e(APP_TAG, tag + ": " + message, tr);
            } catch (Exception e) {
                Log.e(APP_TAG, tag + ": " + e.getMessage(), e);
            }
        }
    }
}
