package com.netcorpgps.v3.dispatch.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

/**
 * Created by David Fa on 25/07/2017.
 */

public class PermissionUtils {

    public static final int PERMISSIONS = 1;

    public static String[] checkAllPermissions(Context context) {

        StringBuffer sb = new StringBuffer();
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            sb.append(Manifest.permission.ACCESS_FINE_LOCATION + ";");
        }

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            sb.append(Manifest.permission.ACCESS_COARSE_LOCATION + ";");
        }

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            sb.append(Manifest.permission.CALL_PHONE + ";");
        }

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            sb.append(Manifest.permission.READ_PHONE_STATE + ";");
        }

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            sb.append(Manifest.permission.WRITE_EXTERNAL_STORAGE + ";");
        }

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            sb.append(Manifest.permission.READ_EXTERNAL_STORAGE + ";");
        }
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            sb.append(Manifest.permission.RECORD_AUDIO + ";");
        }

        if (CommonUtils.isEmpty(sb.toString())) {
            return null;
        } else {
            String[] permissions = sb.toString().split(";");
            return permissions;
        }
    }
}
