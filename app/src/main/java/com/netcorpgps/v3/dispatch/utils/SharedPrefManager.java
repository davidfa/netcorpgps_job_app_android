package com.netcorpgps.v3.dispatch.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.netcorpgps.v3.dispatch.R;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by David Fa on 28/04/2017.
 */

public class SharedPrefManager {
    private SharedPreferences sp;
    private Context context;

    /**
     * @param context
     */
    public SharedPrefManager(Context context) {
        this.context = context;
        sp = context.getSharedPreferences(context.getString(R.string.shared_pref_name), Context.MODE_PRIVATE);
    }

    public SharedPreferences getPreferences() {
        return sp = context.getSharedPreferences(context.getString(R.string.shared_pref_name), Context.MODE_PRIVATE);
    }


    public void setString(int keyResId, String value) {
        sp.edit().putString(context.getString(keyResId), value).commit();
    }

    public String getString(int keyResId) {
        return sp.getString(context.getString(keyResId), "");
    }

    public String getString(int keyResId, String defaultStr) {
        return sp.getString(context.getString(keyResId), defaultStr);
    }

    public void remove(int keyResId) {
        sp.edit().remove(context.getString(keyResId)).commit();
    }

    public void clear() {
        sp.edit().clear().commit();
    }

    public void setBoolean(int keyResId, boolean value) {
        sp.edit().putBoolean(context.getString(keyResId), value).commit();
    }

    public boolean getBoolean(int keyResId) {
        return sp.getBoolean(context.getString(keyResId), true);
    }


    public void setFloat(int keyResId, float value) {
        sp.edit().putFloat(context.getString(keyResId), value).commit();
    }

    public float getFloat(int keyResId) {
        return sp.getFloat(context.getString(keyResId), 0);
    }


    public void setInt(int keyResId, int value) {
        sp.edit().putInt(context.getString(keyResId), value).commit();
    }

    public int getInt(int keyResId) {
        return sp.getInt(context.getString(keyResId), 0);
    }

    public int getInt(int keyResId, int defaultVal) {
        return sp.getInt(context.getString(keyResId), defaultVal);
    }


    public void setLong(int keyResId, long value) {
        sp.edit().putLong(context.getString(keyResId), value).commit();
    }

    public long getLong(int keyResId) {
        return sp.getLong(context.getString(keyResId), 0);
    }

    public void setStringSet(int keyResId, Set<String> value) {
        sp.edit().putStringSet(context.getString(keyResId), value).commit();
    }

    public Set<String> getStringSet(int keyResId) {
        return sp.getStringSet(context.getString(keyResId), new HashSet<String>());
    }
}
