package com.netcorpgps.v3.dispatch.utils.dialog;

import android.app.Activity;

import com.afollestad.materialdialogs.MaterialDialog;

/**
 * Created by David Fa on 23/05/2017.
 */

public interface BasicDialogResponse {
    public MaterialDialog.SingleButtonCallback onPositive();
    public MaterialDialog.SingleButtonCallback onNegative();
    public MaterialDialog.SingleButtonCallback onNeutral();
}
