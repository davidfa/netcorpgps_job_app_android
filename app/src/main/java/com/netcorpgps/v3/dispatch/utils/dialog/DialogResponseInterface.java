package com.netcorpgps.v3.dispatch.utils.dialog;

import android.app.Activity;

/**
 * Created by David Fa on 28/04/2017.
 */

public interface DialogResponseInterface {
    public void doOnPositiveBtnClick(Activity activity);

    public void doOnNegativeBtnClick(Activity activity);
}
