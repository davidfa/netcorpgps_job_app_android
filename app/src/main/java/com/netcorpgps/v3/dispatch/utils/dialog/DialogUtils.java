package com.netcorpgps.v3.dispatch.utils.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.view.ContextThemeWrapper;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.netcorpgps.v3.dispatch.R;

/**
 * Created by David Fa on 28/04/2017.
 */

public class DialogUtils {
    public static void showMultipleDialog(final Activity activity,
                                          final String title, final String message,
                                          final Boolean cancelable, final Integer iconResId,
                                          final String[] items, final int[] itemsIds,
                                          final String positiveBtnName, final String negativeBtnName, final String neutralBtnName,
                                          final MultiChoiceResponse response) {
        if (activity == null || activity.isFinishing()) {
            return;
        }
        MaterialDialog.Builder builder = new MaterialDialog.Builder(activity);
        if (title != null)
            builder.title(title);
        if (message != null)
            builder.content(message);
        if (iconResId != null)
            builder.iconRes(iconResId);
        if (positiveBtnName != null) {
            builder.positiveText(positiveBtnName);
            builder.onPositive(response.onPositive());
        }
        if (negativeBtnName != null) {
            builder.negativeText(negativeBtnName);
        }
        if (cancelable != null)
            builder.cancelable(cancelable);
        else
            builder.cancelable(false);

//        if (neutralBtnName != null) {
//            builder.neutralText(neutralBtnName);
//            builder.onNeutral(null);
//        }
        builder.items(items);
        if (itemsIds != null) {
            builder.itemsIds(itemsIds);
        }
        builder.itemsCallbackMultiChoice(null, response.itemsCallbackMultiChoice());
        builder.alwaysCallMultiChoiceCallback();
        MaterialDialog dialog = builder.build();
        dialog.getActionButton(DialogAction.POSITIVE).setEnabled(false);
//        if (neutralBtnName != null) {
//            dialog.setOnShowListener(new DialogInterface.OnShowListener() {
//                @Override
//                public void onShow(DialogInterface dialogInterface) {
//                    final MaterialDialog dialog = ((MaterialDialog) dialogInterface);
//                    dialog.getActionButton(DialogAction.NEUTRAL).setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            Integer[] indices = dialog.getSelectedIndices();
//                            dialog.setSelectedIndices(indices);
//                        }
//                    });
//                }
//            });
//        }
        dialog.show();
    }

    public static void showConfirmationDialog(final Activity activity,
                                              final String title, final String message,
                                              final Boolean cancelable, final Integer iconResId,
                                              final String positiveBtnName, final String negativeBtnName,
                                              final BasicDialogResponse response) {
        if (activity == null || activity.isFinishing()) {
            return;
        }
        MaterialDialog.Builder builder = new MaterialDialog.Builder(activity);
        if (title != null)
            builder.title(title);
        if (message != null)
            builder.content(message);
        if (positiveBtnName != null)
            builder.positiveText(positiveBtnName);
        if (iconResId != null)
            builder.iconRes(iconResId);
        if (negativeBtnName != null)
            builder.negativeText(negativeBtnName);
        if (cancelable != null)
            builder.cancelable(cancelable);
        else
            builder.cancelable(false);
        builder.onPositive(response.onPositive());
        builder.onNegative(response.onNegative());
        builder.onNeutral(response.onNeutral());

        MaterialDialog dialog = builder.build();
        dialog.show();
    }

    public static void showConfirmationDialog(final Activity activity,
                                              final String title, final String message,
                                              final boolean isCancelable, final Integer iconResId,
                                              final String positiveBtnName, final String negativeBtnName,
                                              final DialogResponseInterface dri) {

        try {
            if (activity == null || activity.isFinishing()) {
                return;
            }
            AlertDialog.Builder builder = null;
            if (activity.getParent() != null)
                builder = new AlertDialog.Builder(new ContextThemeWrapper(activity.getParent(), R.style.AlertDialogConfirm01));
            else
                builder = new AlertDialog.Builder(new ContextThemeWrapper(activity, R.style.AlertDialogConfirm01));

            if (title != null)
                builder.setTitle(title);
            builder.setCancelable(isCancelable);
            if (iconResId != null)
                builder.setIcon(iconResId);
            if (message != null)
                builder.setMessage(message);

            builder.setPositiveButton(positiveBtnName,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            dri.doOnPositiveBtnClick(activity);
                        }
                    });

            builder.setNegativeButton(negativeBtnName,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            dri.doOnNegativeBtnClick(activity);
                        }
                    });
            AlertDialog msg = builder.create();
            msg.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
