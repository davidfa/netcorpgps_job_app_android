package com.netcorpgps.v3.dispatch.utils.dialog;

import com.afollestad.materialdialogs.MaterialDialog;

/**
 * Created by David Fa on 23/05/2017.
 */

public interface MultiChoiceResponse {
    public MaterialDialog.ListCallbackMultiChoice itemsCallbackMultiChoice();
    public MaterialDialog.SingleButtonCallback onPositive();
    public MaterialDialog.SingleButtonCallback onNegative();
    public MaterialDialog.SingleButtonCallback onNeutral();
}
